from python.dataset_scripts import CIFAR_10, CIFAR_100, STL_10, nips_challenge, augmented_dataset, sd, microarray
from tensorflow.examples.tutorials.mnist import input_data


def load_data(directory, dataset, **kwargs):
    if dataset in ['arcene', 'dexter', 'dorothea', 'gisette', 'madelon']:
        return nips_challenge.load_data(directory + '/' + dataset + '/' + dataset)
    elif 'augmented' in dataset:
        return augmented_dataset.read_dataset(directory, dataset)
    elif dataset == 'cifar-10':
        return CIFAR_10.load_data(directory, zha_whitening=False)
    elif dataset == 'cifar-100':
        return CIFAR_100.load_data(directory, zha_whitening=False)
    elif dataset == 'stl-10':
        return STL_10.load_data(directory)
    elif dataset == 'mnist':
        mnist_struct = input_data.read_data_sets(directory, one_hot=False)
        return {
            'train': {'data': mnist_struct.train.images, 'label': mnist_struct.train.labels},
            'validation': {'data': mnist_struct.validation.images, 'label': mnist_struct.validation.labels},
            'test': {'data': mnist_struct.test.images, 'label': mnist_struct.test.labels},
        }
    elif dataset in ['sd1', 'sd2', 'sd3']:
        return sd.load_data(directory + '/' + dataset)
    else:
        try:
            return microarray.load_data(directory + '/' + dataset)
        except:
            pass
    raise Exception('Dataset ' + dataset + ' not recognized')