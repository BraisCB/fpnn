import numpy as np


def load_data(source):
    info = {
        'train': {}, 'test': {}
    }

    file = source + '_train.targets'
    info['train']['label'] = np.loadtxt(file, dtype=np.int16)
    info['train']['label'][info['train']['label'] < 0] = 0

    file = source + '_train.data'
    info['train']['data'] = np.loadtxt(file, dtype=np.int16).astype(np.float32)

    file = source + '_test.data'
    info['test']['data'] = np.loadtxt(file, dtype=np.int16).astype(np.float32)

    return info
