import numpy as np


def read_dataset(directory, dataset):
    subsets = ['train', 'validation', 'test']
    output = {}
    for subset in subsets:
        output[subset] = {}
        filename = directory + '/' + dataset + '_' + subset
        try:
            with open(filename + '_data.npy', 'rb') as infile:
                output[subset]['data'] = np.load(infile)
        except:
            pass
        try:
            with open(filename + '_label.npy', 'rb') as infile:
                output[subset]['label'] = np.load(infile)
        except:
            pass
    return output


def create_dataset(dataset, extra_dims=3, dtype=np.uint8):
    new_dataset = {}
    valid_features = {}
    indexes = get_augmented_indexes(np.array(dataset['train']['data']).shape, extra_dims)
    for opt in dataset:
        perm = np.random.permutation(len(dataset[opt]['data']))
        new_data, new_order = augment_data(dataset[opt]['data'], extra_dims, indexes, dtype)
        new_data = new_data[perm]
        new_dataset[opt] = {
            'data': new_data
        }
        valid_features[opt] = new_order.tolist()
        if 'label' in dataset[opt]:
            new_dataset[opt]['label'] = dataset[opt]['label'][perm]
    return new_dataset, valid_features


def get_augmented_indexes(shape, extra_dims):
    new_shape = list(shape)
    new_shape[-1] *= extra_dims
    indexes = np.zeros(new_shape[1:]).astype(int)
    for i in range(new_shape[1]):
        for j in range(new_shape[2]):
            perm = np.random.permutation(new_shape[-1]).astype(int)
            indexes[i, j, :] = np.reshape(perm, indexes[i, j, :].shape)
    return indexes


def augment_data(data, extra_dims, indexes, dtype):
    shape = list(data.shape)
    if len(shape) != 4:
        raise Exception('Dataset must have 4 dimensions')
    new_shape = list(shape)
    new_shape[-1] *= extra_dims
    new_data = np.ones(new_shape, dtype=dtype)
    reorder = np.zeros(new_shape[1:])
    for i in range(shape[1]):
        for j in range(shape[2]):
            perm = indexes[i, j, :]
            for k in range(shape[3]):
                reorder[i, j, perm[k*shape[3]]] = k+1
                X = data[:, i, j, k]
                new_data[:, i, j, perm[k*shape[3]]] = X
                for l in range(1, extra_dims):
                    X_perm = np.random.permutation(range(len(X)))
                    new_data[:, i, j, perm[k * shape[3] + l]] = X[X_perm]
    return new_data, reorder


def augment_data_permutation(data, extra_dims):
    shape = list(data.shape)
    if len(shape) != 4:
        raise Exception('Dataset must have 4 dimensions')
    new_shape = list(shape)
    new_shape[-1] *= extra_dims
    new_data = np.ones(new_shape, dtype=data.dtype)
    reorder = np.zeros(new_shape[1:])
    for i in range(shape[1]):
        for j in range(shape[2]):
            perm = np.random.permutation(range(new_shape[-1])).astype(int)
            for k in range(shape[3]):
                reorder[i, j, perm[k*shape[3]]] = k+1
                new_data[:, i, j, perm[k*shape[3]]] = data[:, i, j, k]
                for l in range(1, extra_dims):
                    r_i = np.random.randint(shape[1])
                    r_j = np.random.randint(shape[2])
                    r_k = np.random.randint(shape[3])
                    X_perm = np.random.permutation(range(shape[0]))
                    new_data[:, i, j, perm[k * shape[3] + l]] = data[X_perm, r_i, r_j, r_k]
    return new_data, reorder.tolist()


def augment_data_hard(data, factor):
    shape = list(data.shape)
    if len(shape) > 2:
        raise Exception('Dataset must have 2 dimensions')
    new_shape = list(shape)
    new_shape[-1] *= factor
    new_data = np.ones(new_shape, dtype=data.dtype)
    reorder = np.zeros(new_shape[1:]).astype(np.bool)
    perm = np.random.permutation(range(new_shape[-1]))[:shape[-1]]
    new_data[:, perm] = data
    reorder[perm] = True
    non_valid = np.where(reorder == 0)[0]
    for i in non_valid:
        r_i = np.random.randint(shape[1])
        perm = np.random.permutation(range(new_shape[0])).astype(int)
        new_data[:, i] = data[perm, r_i]
    return new_data, reorder.tolist()


