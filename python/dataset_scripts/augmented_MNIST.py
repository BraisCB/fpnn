from tensorflow.examples.tutorials.mnist import input_data
from python.dataset_scripts.augmented_dataset import create_dataset, augment_data_permutation, augment_data_hard
import json
import numpy as np


def augment_mnist():
    mnist_struct = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
    data = np.concatenate(
        (
            np.reshape(255*mnist_struct.train.images, (-1, 28, 28, 1)).astype(np.uint8),
            np.reshape(255*mnist_struct.validation.images, (-1, 28, 28, 1)).astype(np.uint8),
            np.reshape(255*mnist_struct.test.images, (-1, 28, 28, 1)).astype(np.uint8)
        ),
        axis=0
    )
    labels = np.concatenate(
        (
            mnist_struct.train.labels,
            mnist_struct.validation.labels,
            mnist_struct.test.labels
        )
    )
    del mnist_struct

    data, valid_features = augment_data_permutation(data, 9)
    ndata = len(data)
    ntrain = 55000
    nval = 60000
    perm = np.random.permutation(range(len(data)))
    data = data[perm]
    labels = labels[perm]

    augmented_mnist = {
        'train': {
            'data': data[:ntrain],
            'label': labels[:ntrain]
        },
        'validation': {
            'data': data[ntrain:nval],
            'label': labels[ntrain:nval]
        },
        'test': {
            'data': data[nval:],
            'label': labels[nval:]
        },
    }

    filename = './datasets/mnist/fs_mnist_'
    for opt in augmented_mnist:
        with open(filename + opt + '_data.npy', 'wb') as outfile:
            np.save(outfile, augmented_mnist[opt]['data'])
        if 'label' in augmented_mnist[opt]:
            with open(filename + opt + '_label.npy', 'wb') as outfile:
                np.save(outfile, augmented_mnist[opt]['label'])

    with open('./datasets/cifar-10/fs_mnist_features.json', 'w') as outfile:
        json.dump(valid_features, outfile)


def augment_mnist_hard():
    mnist_struct = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
    data = np.concatenate(
        (
            (255*mnist_struct.train.images).astype(np.uint8),
            (255*mnist_struct.validation.images).astype(np.uint8),
            (255*mnist_struct.test.images).astype(np.uint8)
        ),
        axis=0
    )
    labels = np.concatenate(
        (
            mnist_struct.train.labels,
            mnist_struct.validation.labels,
            mnist_struct.test.labels
        )
    )
    del mnist_struct

    data, valid_features = augment_data_hard(data, 9)
    ndata = len(data)
    ntrain = 55000
    nval = 60000
    perm = np.random.permutation(range(len(data)))
    data = data[perm]
    labels = labels[perm]

    augmented_mnist = {
        'train': {
            'data': data[:ntrain],
            'label': labels[:ntrain]
        },
        'validation': {
            'data': data[ntrain:nval],
            'label': labels[ntrain:nval]
        },
        'test': {
            'data': data[nval:],
            'label': labels[nval:]
        },
    }

    filename = './datasets/mnist/fs_mnist_hard_'
    for opt in augmented_mnist:
        with open(filename + opt + '_data.npy', 'wb') as outfile:
            np.save(outfile, augmented_mnist[opt]['data'])
        if 'label' in augmented_mnist[opt]:
            with open(filename + opt + '_label.npy', 'wb') as outfile:
                np.save(outfile, augmented_mnist[opt]['label'])

    with open('./datasets/cifar-10/fs_mnist_hard_features.json', 'w') as outfile:
        json.dump(valid_features, outfile)


if __name__ == '__main__':
    augment_mnist_hard()