import numpy as np
import pickle


def load_data(source, normalize=False):
    info = {'train': {'data': [], 'label': []},
            'test': {'data': [], 'label': []}}
    for i in range(1, 6):
        file = source + 'data_batch_' + str(i)
        fo = open(file, 'rb')
        data = pickle.load(fo, encoding='bytes')
        info['train']['data'] += data[b'data'].tolist()
        info['train']['label'] += data[b'labels']
        fo.close()

    file = source + 'test_batch'
    fo = open(file, 'rb')
    data = pickle.load(fo, encoding='bytes')
    info['test']['data'] += data[b'data'].tolist()
    info['test']['label'] += data[b'labels']
    fo.close()

    info['train']['label'] = np.array(info['train']['label'])
    info['test']['label'] = np.array(info['test']['label'])
    info['train']['data'] = np.array(info['train']['data'])
    info['test']['data'] = np.array(info['test']['data'])

    if normalize:
        mean = info['train']['data'].mean(axis=0)
        std = info['train']['data'].std(axis=0)
        info['train']['data'] = (info['train']['data'] - mean) / std
        info['test']['data'] = (info['test']['data'] - mean) / std

    info['train']['data'] = np.reshape(info['train']['data'], (-1, 3, 32, 32))
    info['train']['data'] = np.transpose(info['train']['data'], (0, 2, 3, 1))
    info['test']['data'] = np.reshape(info['test']['data'], (-1, 3, 32, 32))
    info['test']['data'] = np.transpose(info['test']['data'], (0, 2, 3, 1))

    return info
