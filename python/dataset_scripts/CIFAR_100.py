import numpy as np
import pickle


def load_data(source, normalize=False):
    info = {'train': {'data': [], 'label': []},
            'test': {'data': [], 'label': []}}
    file = source + 'train'
    fo = open(file, 'rb')
    data = pickle.load(fo, encoding='bytes')
    print(data.keys())
    info['train']['data'] += data[b'data'].tolist()
    info['train']['label'] += data[b'fine_labels']
    del data
    fo.close()

    file = source + 'test'
    fo = open(file, 'rb')
    data = pickle.load(fo, encoding='bytes')
    info['test']['data'] += data[b'data'].tolist()
    info['test']['label'] += data[b'fine_labels']
    del data
    fo.close()

    info['train']['data'] = np.array(info['train']['data'])
    info['test']['data'] = np.array(info['test']['data'])

    info['train']['data'] = np.reshape(info['train']['data'], (-1, 3, 32, 32))
    info['train']['data'] = np.transpose(info['train']['data'], (0, 2, 3, 1))
    info['train']['label'] = np.array(info['train']['label'])

    info['test']['data'] = np.reshape(info['test']['data'], (-1, 3, 32, 32))
    info['test']['data'] = np.transpose(info['test']['data'], (0, 2, 3, 1))
    info['test']['label'] = np.array(info['test']['label'])

    if normalize:
        mean = info['train']['data'].mean(axis=(0, 1, 2))
        std = 255.0 # info['train']['data'].std(axis=0)
        info['train']['data'] = (info['train']['data'] - mean) / std
        info['test']['data'] = (info['test']['data'] - mean) / std



    return info
