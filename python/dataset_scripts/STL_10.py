import numpy as np


def load_data(source, extra_data=False, normalize=False):
    info = {'train': {'data': [], 'label': []},
            'test': {'data': [], 'label': []}}

    file = source + '/train_y.bin'
    with open(file, 'rb') as f:
        info['train']['label'] = np.fromfile(f, dtype=np.uint8) - 1

    file = source + '/train_X.bin'
    with open(file, 'rb') as f:
        images = np.fromfile(f, dtype=np.uint8)
        images = np.reshape(images, (-1, 3, 96, 96))
        info['train']['data'] = np.transpose(images, (0, 3, 2, 1))

    file = source + '/test_y.bin'
    with open(file, 'rb') as f:
        info['test']['label'] = np.fromfile(f, dtype=np.uint8) - 1

    file = source + '/test_X.bin'
    with open(file, 'rb') as f:
        images = np.fromfile(f, dtype=np.uint8)
        images = np.reshape(images, (-1, 3, 96, 96))
        info['test']['data'] = np.transpose(images, (0, 3, 2, 1))

    if extra_data:
        file = source + '/unlabeled_X.bin'
        info['extra'] = {'data': [], 'label': []}
        with open(file, 'rb') as f:
            images = np.fromfile(f, dtype=np.uint8)
            images = np.reshape(images, (-1, 3, 96, 96))
            info['extra']['data'] = np.transpose(images, (0, 3, 2, 1))
        info['extra']['label'] = 10 * np.ones(info['extra']['data'].shape[0]).astype(int)

    if normalize:
        mean = info['train']['data'].mean(axis=0)
        std = info['train']['data'].std(axis=0)
        info['train']['data'] = (info['train']['data'] - mean) / std
        info['test']['data'] = (info['test']['data'] - mean) / std
        if extra_data:
            info['extra']['data'] = (info['extra']['data'] - mean) / std

    return info
