from python.dataset_scripts.CIFAR_100 import load_data
from python.dataset_scripts.augmented_dataset import create_dataset
import json
import numpy as np


cifar_100 = load_data("./datasets/cifar-100/", flip_left_right=False, zha_whitening=False)

validation_d = []
train_d = []
validation_l = []
train_l = []
labels = np.unique(cifar_100['train']['label'])
for l in labels:
    label = np.where(cifar_100['train']['label'] == l)[0]
    np.random.shuffle(label)
    total = len(label)
    ntrain = int(45/50*total)
    nval = total - ntrain
    train_data = cifar_100['train']['data'][label].astype(np.uint8)[:ntrain]
    train_label = cifar_100['train']['label'][label].astype(np.uint8)[:ntrain]
    val_data = cifar_100['train']['data'][label].astype(np.uint8)[ntrain:]
    val_label = cifar_100['train']['label'][label].astype(np.uint8)[ntrain:]
    train_d.append(train_data)
    train_l.append(train_label)
    validation_d.append(val_data)
    validation_l.append(val_label)
train_d = np.concatenate(train_d, axis=0)
train_l = np.concatenate(train_l, axis=0)
validation_d = np.concatenate(validation_d, axis=0)
validation_l = np.concatenate(validation_l, axis=0)
cifar_100['train'] = {'data': train_d, 'label': train_l}
cifar_100['validation'] = {'data': validation_d, 'label': validation_l}


augmented_cifar_100, valid_features = create_dataset(cifar_100)

filename = './datasets/cifar-100/fs_cifar_100_'
for opt in augmented_cifar_100:
    with open(filename + opt + '_data.npy', 'wb') as outfile:
        np.save(outfile, augmented_cifar_100[opt]['data'])
    if 'label' in augmented_cifar_100[opt]:
        with open(filename + opt + '_label.npy', 'wb') as outfile:
            np.save(outfile, augmented_cifar_100[opt]['label'])

with open('./datasets/cifar-100/fs_cifar_100_features.json', 'w') as outfile:
    json.dump(valid_features, outfile)
