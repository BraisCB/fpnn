import numpy as np


def load_data(source):
    info = {
        'train': {}, 'validation': {}, 'test': {}
    }

    trainfile = source + '_Train.csv'
    testfile = source + '_Test.csv'
    info['train']['data'] = np.genfromtxt(trainfile, delimiter=',')
    info['test']['data'] = np.genfromtxt(testfile, delimiter=',')
    info['train']['label'] = info['train']['data'][:, -1].astype(int)
    info['train']['data'] = info['train']['data'][:, :-1]
    info['test']['label'] = info['test']['data'][:, -1].astype(int)
    info['test']['data'] = info['test']['data'][:, :-1]

    return info
