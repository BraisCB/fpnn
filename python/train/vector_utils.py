import numpy as np
from python.train.cython.cutils import outer_sum
from copy import deepcopy
from scipy.stats import ortho_group
import tensorflow as tf
from scipy.spatial.distance import pdist, cdist, squareform


def get_index_2d(im_size, window, train_opts=None):
    if len(im_size) == 1:
        res = [list(range(np.prod(im_size)))]
    else:
        res = []
        index = np.reshape(range(np.prod(im_size)), im_size)
        for i in range(index.shape[0] - window[0] + 1):
            for j in range(index.shape[1] - window[1] + 1):
                res.append(index[i:i+window[0], j:j+window[1],:].flatten())
        # if train_opts is not None and 'flips' in train_opts:
        #     for flip in train_opts['flips']:
        #         index = np.reshape(range(np.prod(im_size)), im_size)
        #         if flip == 'left_right':
        #             index = np.fliplr(index)
        #         elif flip == 'up_down':
        #             index = np.flipud(index)
        #         for i in range(index.shape[0] - window[0] + 1):
        #             for j in range(index.shape[1] - window[1] + 1):
        #                 res.append(index[i:i+window[0], j:j+window[1],:].flatten())
    return np.array(res)


def mat_2_vector(mat):
    vector = np.zeros(mat.shape[0]*(mat.shape[0] + 1))
    cont = 0
    for i in range(mat.shape[0]):
        for j in range(i, mat.shape[0]):
            vector[cont] = mat[i, j]
            cont += 1
    return vector


def vector_2_mat(vector):
    shape = int(np.floor(np.sqrt(2*vector.size)))
    mat = np.zeros((shape, shape))
    cont = 0
    for i in range(mat.shape[0]):
        for j in range(i, mat.shape[0]):
            mat[i, j] = mat[j, i] = vector[cont]
            cont += 1
    return mat


def vector_cov(vector):
    out = np.zeros(int(vector.size * (vector.size + 1) / 2.0))
    outer_sum(vector, out)
    return out


def dict_merge(a, b):
    "merges b into a"
    output = deepcopy(a)
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                output[key] = dict_merge(a[key], b[key])
            else:
                output[key] = b[key] # same leaf value
        else:
            output[key] = b[key]
    return output


def get_basis(dim, n_c, initializer='randn', gain=1.0):
    if initializer in ['randn']:
        basis = np.random.randn(n_c, dim)
    elif initializer in ['eye', 'ones']:
        basis = np.eye(n_c, dim)
        if initializer == 'ones':
            basis[basis < 0.5] = -1
    elif initializer in ['augmented_ones', 'augmented_eye']:
        i = 0
        basis = np.zeros((n_c, dim))
        while i < dim:
            next_i = min(i + n_c, dim)
            basis[:, i:next_i] = np.eye(n_c, next_i-i)
            i = next_i
        if initializer == 'augmented_ones':
            basis[basis < 0.5] = -1
    elif 'orthogonal' in initializer:
        ortho_basis = ortho_group.rvs(dim)
        if initializer == 'orthogonal':
            basis = ortho_basis[:n_c]
        elif initializer == 'augmented_orthogonal':
            i = 0
            basis = np.zeros((n_c, dim))
            while i < dim:
                next_i = min(i + n_c, dim)
                basis[:next_i-i] += ortho_basis[i:next_i]
                i = next_i
    elif initializer == 'max_separation':
        basis = __get_max_sep_basis(dim, n_c)
    elif initializer == 'close':
        basis = np.random.randn(n_c, dim)
        for i in range(1, len(basis)):
            basis[i] = (np.random.randn(1, dim)*0.05/0.95 + 1.0) * basis[0]
    norm = np.linalg.norm(basis, axis=1, keepdims=True)
    basis /= norm
    basis *= gain
    d = squareform(pdist(basis, 'cosine'))
    # d = d + 1000*np.eye(d.shape[0])
    print('max dist', np.max(d))
    return basis.T


def __get_max_sep_basis(dim, n_c, nreps=100, nsamples=20000):
    basis = ortho_group.rvs(dim)
    basis = basis[:n_c]
    d = squareform(pdist(basis, 'euclidean'))
    d_score = np.sum(d, axis=-1)
    d = d + 1000 * np.eye(d.shape[0])
    d_min = np.min(d, axis=-1)
    worst_index = np.argmin(d_min + 1e-4*d_score)
    worst_score = d_min[worst_index]
    old_basis = basis.copy()
    while True:
        valid = False
        basis = np.delete(basis, worst_index, 0)
        for _ in range(nreps):
            new_basis = np.random.randn(nsamples, dim)
            new_basis /= np.linalg.norm(new_basis, axis=1, keepdims=True)
            c = cdist(basis, new_basis, 'euclidean')
            c_score = np.min(c, axis=0)
            best_index = np.argmax(c_score)
            best_score = c_score[best_index]
            if best_score > worst_score:
                valid = True
                basis = np.vstack((basis, new_basis[best_index]))
                d = squareform(pdist(basis, 'euclidean'))
                d_score = np.sum(d, axis=-1)
                d = d + 1000 * np.eye(d.shape[0])
                d_min = np.min(d, axis=-1)
                worst_index = np.argmin(d_min + 1e-6 * d_score)
                worst_score = d_min[worst_index]
                break
        print('worst score', worst_score)
        if not valid:
            return old_basis
        else:
            old_basis = basis.copy()


def label_to_basis(labels, basis):
    l_shape = len(labels)
    output = np.zeros((l_shape, np.array(basis).shape[1]))
    for i in range(len(labels)):
        output[i] = basis[labels[i]]
    return output