import numpy as np
import os
from python.train.vector_utils import get_index_2d, vector_cov, vector_2_mat
from python.train.cython.cutils import outer_sum_with_factor
from multiprocessing import Pool
from itertools import repeat


def get_data(X, rows, outputs, label, ntries=1, percentage=1.0):
    ntrain = int(len(X) * percentage)

    output = {}
    scatter = np.zeros(int(rows.shape[-1] * (rows.shape[-1] + 1) / 2.0))
    mean = np.zeros(rows.shape[-1])
    cont = 0
    for _ in range(ntries):
        pos = np.random.permutation(len(X))[:ntrain]
        for data, output in zip(X[pos], outputs[pos]):
            factor = np.exp(-(label - output)*(label - output))
            for row in rows:
                mean += (factor * data.flat[row])
                outer_sum_with_factor(factor, data.flat[row], scatter)
            cont += len(rows)*factor
    output['mean'] = mean
    output['cov'] = scatter
    output['cont'] = cont
    return output


class RegressionTraining:

    def __init__(self):
        self.ds = None
        self.vs = None
        self.W = None
        self.labels = None

    def get_weights(self, model, opts, data, output):
        self.ds = []
        self.vs = []

        batch_size = 100
        if 'batch_size' in opts:
            batch_size = opts['batch_size']

        max_dim = int(opts['shape'][-1] / 8)
        duplicate = True
        if 'duplicate' in opts:
            duplicate = opts['duplicate']
        if duplicate:
            max_dim = int(max_dim / 2)

        self.labels = self.__get_ranges(output, max_dim + 1)

        matrices = self.__get_Sw_Sb(model, opts, data, output, batch_size)

        train_options = [{'technique': 'lda', 'max_dim': max_dim},
                         {'technique': 'direct-lda', 'max_dim': max_dim},
                         {'technique': 'weighted-lda', 'max_dim': max_dim},
                         {'technique': 'weighted-direct-lda', 'max_dim': max_dim},
                         {'technique': 'pca'}]
        if 'train' in opts:
            train_options = opts['train']

        for train_opt in train_options:
            if train_opt['technique'] == 'lda':
                self.__get_LDA_weights(matrices['S_w'], matrices['S_b'], train_opt)
            elif train_opt['technique'] == 'direct-lda':
                self.__get_LDA_direct_weights(matrices['S_w'], matrices['S_b'], train_opt)
            elif train_opt['technique'] == 'weighted-lda':
                self.__get_LDA_weights(matrices['wS_w'], matrices['wS_b'], train_opt)
            elif train_opt['technique'] == 'weighted-direct-lda':
                self.__get_LDA_direct_weights(matrices['wS_w'], matrices['wS_b'], train_opt)
            elif train_opt['technique'] == 'pca':
                self.__get_PCA_weights(matrices['S_w'], train_opt)

        self.__get_weights(opts)
        return self.W

    def get_bias(self, model, opts, data, batch_size=100):
        offset = 0.02
        if 'offset' in opts:
            offset = opts['offset']
        factor = 0.5
        if 'factor' in opts:
            offset = opts['factor']
        out = model[-1]['operation'].eval(feed_dict={model[0]['operation']: data[:1]})
        layer_shape = out.shape[-1]

        mean_data = np.zeros(layer_shape)
        mean2_data = np.zeros(layer_shape)
        cont_data = 0

        index = 0
        while index < len(data):
            new_index = min(len(data), index + batch_size)
            out = model[-1]['operation'].eval(feed_dict={model[0]['operation']: data[index:new_index]}).astype(float)
            out_2 = out * out
            for i in range(0, layer_shape):
                if len(out.shape) > 2:
                    mean_data[i] += np.sum(out[:,:,:,i])
                    mean2_data[i] += np.sum(out_2[:,:,:,i])
                else:
                    mean_data[i] += np.sum(out[:, i])
                    mean2_data[i] += np.sum(out_2[:, i])
            cont_data += np.prod(out.shape[:-1])
            index = new_index
        mean2_data /= cont_data
        mean_data /= cont_data
        desv = np.sqrt(mean2_data - mean_data * mean_data)
        wfactor = factor / desv
        bias = factor * (offset - mean_data / desv)
        return bias, wfactor

    def __get_Sw_Sb(self, model, opts, data, label, batch_size):
        label_distances = self.__get_label_distances(model, data, self.labels, batch_size)

        nlabels = len(self.labels)
        ndata = len(data)
        conv_window = opts['shape']
        out = model[-1]['operation'].eval(feed_dict={model[0]['operation']: data[:1]})
        layer_shape = out.shape[1:]
        rows = get_index_2d(layer_shape, conv_window)
        cov = np.zeros((int((np.prod(conv_window[:-1]) + 1) * np.prod(conv_window[:-1]) / 2.0)))
        w_cov = np.zeros((int((np.prod(conv_window[:-1]) + 1) * np.prod(conv_window[:-1]) / 2.0)))
        media = []

        nproc = os.cpu_count() or 1
        pool = Pool(nproc)

        for i in range(nlabels):
            print('label', i)
            index = 0

            cov_label = np.zeros((int((np.prod(conv_window[:-1]) + 1) * np.prod(conv_window[:-1]) / 2.0)))
            media_label = np.zeros((int(np.prod(conv_window[:-1]))))
            cont_label = 0
            while index < ndata:
                data_model = []
                label_model = []
                for i in range(nproc):
                    new_index = min(ndata, index + batch_size)
                    data_model.append(model[-1]['operation'].eval(
                        feed_dict={model[0]['operation']: data[index:new_index]}).astype(float)
                    )
                    label_model.append(label[index:new_index])
                    index = new_index
                    if new_index == ndata:
                        break
                results = pool.starmap(get_data, zip(data_model, repeat(rows), repeat(label_model), repeat(self.labels[i])))
                for result in results:
                    cov_label += result['cov']
                    media_label += result['mean']
                    cont_label += result['cont']
            cov_label /= cont_label
            media_label /= cont_label
            cov += (cov_label - vector_cov(media_label))
            w_cov += label_distances[i] * (cov_label - vector_cov(media_label))
            media.append(media_label)

        pool.close()

        media = np.array(media)
        S_b = np.zeros((np.prod(conv_window[:-1]), np.prod(conv_window[:-1])))
        wS_b = np.zeros((np.prod(conv_window[:-1]), np.prod(conv_window[:-1])))
        c_b2 = 0
        m_media = np.mean(media, axis=0)
        for i in range(nlabels):
            m = media[i]
            for m2 in media[i + 1:]:
                diff = m - m2
                factor = 1.0 / (diff @ diff.T)
                wS_b += factor * np.outer(diff, diff)
                c_b2 += factor
            diff = m - m_media
            S_b += np.outer(diff, diff)

        return {'S_w': vector_2_mat(cov / nlabels), 'wS_w': vector_2_mat(w_cov / label_distances.sum()),
                'S_b': S_b / nlabels, 'wS_b': wS_b / c_b2}

    def __get_label_distances(self, model, data, output, batch_size):
        nlabels = len(self.labels)
        out = model[-1]['operation'].eval(feed_dict={model[0]['operation']: data[:1]})
        layer_shape = list(out.shape[1:])
        w_s = np.zeros(nlabels)
        cont = np.zeros(nlabels)

        media = np.zeros([nlabels] + layer_shape)

        ndata = len(data)
        index = 0
        while index < ndata:
            new_index = min(ndata, index + batch_size)
            eval_data = model[-1]['operation'].eval(feed_dict={model[0]['operation']: data[index:new_index]})
            eval_output = output[index:new_index]
            for d, o in zip(eval_data, eval_output):
                for i in range(nlabels):
                    factor = np.exp(-(o - self.labels[i])*(o - self.labels[i]))
                    media[i] += (factor * d)
                    cont[i] += factor
            index = new_index

        for i in range(nlabels):
            media[i] /= cont[i]

        for i in range(nlabels):
            for j in range(nlabels):
                if i == j:
                    continue
                diff = (media[i] - media[j]).ravel()
                w_s[i] += (diff @ diff.T)
            w_s[i] = 1.0 / w_s[i]
        return w_s

    def __get_LDA_weights(self, S_w, S_b, layer_info):
        matrix = np.linalg.pinv(S_w).dot(S_b)
        m_dim = np.linalg.matrix_rank(matrix)
        ds_lda, vs_lda = np.linalg.eig(matrix)
        order = np.argsort(np.real(ds_lda))[::-1][:m_dim]
        ds_lda = np.real(ds_lda)[order]
        ds_lda = np.abs(ds_lda) / np.sum(np.abs(ds_lda))
        for i in range(1, len(ds_lda)):
            ds_lda[i] += ds_lda[i - 1]
        vs_lda = np.real(vs_lda)
        mi_dim = 0
        if 'min_dim' in layer_info:
            mi_dim = min(m_dim, layer_info['min_dim']) - 1
        ma_dim = m_dim - 1
        if 'max_dim' in layer_info:
            ma_dim = min(m_dim, layer_info['max_dim']) - 1
        thresh = 1.0
        if 'thresh' in layer_info:
            thresh = layer_info['thresh']
        n_thresh = min(max(ds_lda[mi_dim], thresh), ds_lda[ma_dim])

        vs_lda = vs_lda.T[order][ds_lda <= n_thresh]
        ds_lda = ds_lda[ds_lda <= n_thresh]
        print(ds_lda)
        self.ds += ds_lda.tolist()
        self.vs += vs_lda.tolist()

    def __get_LDA_direct_weights(self, S_w, S_b, layer_info):
        ds_Sb, vs_Sb = np.linalg.eig(S_b)
        m_dim = np.linalg.matrix_rank(S_b)
        order = np.argsort(np.real(ds_Sb))[::-1][:m_dim]
        ds_Sb = np.real(ds_Sb)[order]
        ds_Sb = np.abs(ds_Sb) / np.sum(np.abs(ds_Sb))
        for i in range(1, len(ds_Sb)):
            ds_Sb[i] += ds_Sb[i - 1]
        vs_Sb = np.real(vs_Sb)
        mi_dim = 0
        if 'min_dim' in layer_info:
            mi_dim = min(m_dim, layer_info['min_dim']) - 1
        ma_dim = m_dim - 1
        if 'max_dim' in layer_info:
            ma_dim = min(m_dim, layer_info['max_dim']) - 1
        thresh = 1.0
        if 'thresh' in layer_info:
            thresh = layer_info['thresh']
        n_thresh = min(max(ds_Sb[mi_dim], thresh), ds_Sb[ma_dim])
        vs_Sb = vs_Sb.T[order][ds_Sb <= n_thresh].T
        ds_Sb = ds_Sb[ds_Sb <= n_thresh]
        print(ds_Sb)

        if len(ds_Sb):
            z = vs_Sb @ np.diag(1.0 / np.sqrt(ds_Sb))
            z_Sw = z.T @ S_w @ z
            ds_Sw, vs_Sw = np.linalg.eig(z_Sw)
            vs_dlda = np.diag(1.0 / np.sqrt(ds_Sw)) @ vs_Sw.T @ z.T

            self.ds += ds_Sb.tolist()
            self.vs += vs_dlda.tolist()

    def __get_PCA_weights(self, S_w, layer_info):
        ds_pca, vs_pca = np.linalg.eig(S_w)
        order = np.argsort(np.real(ds_pca))[::-1][:np.linalg.matrix_rank(S_w)]
        ds_pca = np.real(ds_pca)[order]
        ds_pca = np.abs(ds_pca) / np.sum(np.abs(ds_pca))
        for i in range(1, len(ds_pca)):
            ds_pca[i] += ds_pca[i - 1]
        vs_pca = np.real(vs_pca)
        vs_pca = vs_pca.T[order]
        self.ds += ds_pca.tolist()
        self.vs += vs_pca.tolist()

    def __get_weights(self, opts):
        conv_window = opts['shape']
        self.ds = np.array(self.ds)
        self.vs = np.array(self.vs)
        self.W = np.zeros(conv_window)
        cont = 0
        iter = 0

        duplicate = True
        if 'duplicate' in opts:
            duplicate = opts['duplicate']

        while cont < conv_window[-1]:
            for d, v in zip(self.ds, self.vs):
                if iter > 0:
                    z = v + 0.02*iter*np.random.randn(v.size)
                else:
                    z = v
                if len(conv_window) == 4:
                    self.W[:, :, :, cont] = np.reshape(z, conv_window[:-1])
                else:
                    self.W[:, cont] = z
                cont += 1
                if cont == conv_window[-1]:
                    break
                if duplicate:
                    if iter > 0:
                        z = -v + 0.02*iter*np.random.randn(v.size)
                    else:
                        z = -v
                    if len(conv_window) == 4:
                        self.W[:, :, :, cont] = np.reshape(z, conv_window[:-1])
                    else:
                        self.W[:, cont] = z
                    cont += 1
                    if cont == conv_window[-1]:
                        break
            iter += 1

    def __get_ranges(self, output, nlabels):
        sorted_output = np.sort(output)
        len_output = len(output)
        step = nlabels / (len(output) + 1)

        inc_step = 0
        self.labels = []
        while len(self.labels) < nlabels:
            inc_step += step
            self.labels.append(sorted_output[round(len_output * inc_step)])
