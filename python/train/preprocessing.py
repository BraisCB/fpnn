def get_cifar_preprocessing_layer(input_size):
    return {
        'type': 'block',
        'layers': [
            {
                'type': 'train_test_option',
                'train': {
                    'type': 'block',
                    'layers': [
                        {
                            'type': 'pad',
                            'args': {
                                'paddings': [[0, 0], [4, 4], [4, 4], [0, 0]],
                                'mode': 'REFLECT'
                            }
                        },
                        # {
                        #     'type': 'image.resize_image_with_crop_or_pad',
                        #     'args': {
                        #         'target_height': input_size[0] + 4,
                        #         'target_width': input_size[1] + 4
                        #     }
                        # },
                        # {
                        #     'type': 'random_rotate',
                        #     'opts': {
                        #         'interpolation': 'BILINEAR',
                        #         'rotation_range': 25.0
                        #     }
                        # },
                        {
                            'type': 'random_crop',
                            'args': {
                                'size': input_size
                            },
                            'map_fn': True
                        },
                        {
                            'type': 'image.random_flip_left_right',
                            'map_fn': True
                        },
                        # {
                        #     'type': 'image.random_brightness',
                        #     'args': {
                        #         'max_delta': 63
                        #     },
                        #     'map_fn': True
                        # },
                        # {
                        #     'type': 'image.random_contrast',
                        #     'args': {
                        #         'lower': 0.2,
                        #         'upper': 1.8
                        #     },
                        #     'map_fn': True
                        # },

                    ]
                },
                'test': {
                    'type': 'identity'
                    # 'type': 'image.resize_image_with_crop_or_pad',
                    # 'args': {
                    #     'target_height': input_size[0],
                    #     'target_width': input_size[1]
                    # }
                }
            },
            {
                'type': 'image.per_image_standardization',
                'map_fn': True
            }
        ]
    }