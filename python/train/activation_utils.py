import numpy as np


def inverse_elu(y, alpha=1.0):
    x = y.copy()
    x[y < 0.0] = np.log(y[y < 0.0] / alpha + 1.0)
    return x


def elu(x, alpha=1.0):
    y = x.copy()
    y[x < 0.0] = alpha * (np.exp(x[x < 0.0]) - 1.0)
    return y
