import numpy as np
from copy import deepcopy
from python.fpnn import FpNN


def init_model_layer_by_layer(fpnn_data, layers, data, label, test_data=None, test_label= None,
                              limit_index=-1, dropout_layer=None, fix_output=False,
                              batch_size=50, epochs=15000):
    fpnn = FpNN(fpnn_data['input_size'], fpnn_data['model_type'], layers,
                fpnn_data['global_opts'], fpnn_data['framework'])
    fpnn.build()
    deep_feature_shape = None
    if fix_output:
        deep_feature_shape = np.array(fpnn.model[-1]['operation'].get_shape().as_list()[1:])
    del fpnn
    new_layers = __init_recursive_model(
        fpnn_data, layers[:limit_index], data, label, test_data, test_label,
        dropout_layer, deep_feature_shape, layers[-1], batch_size, epochs
    )
    return new_layers + layers[limit_index:]


def __init_recursive_model(fpnn_data, layers, data, label, test_data, test_label,
                           dropout_layer, deep_feature_shape, classifier, batch_size, epochs):
    nlabels = len(np.unique(label))
    new_layers = deepcopy(layers)
    classifier_function = classifier['layers'][-1]
    for i in range(len(new_layers)):
        print('training layer', i)
        subset_layers = new_layers[:i+1]
        fpnn = FpNN(fpnn_data['input_size'], fpnn_data['model_type'], subset_layers,
                    fpnn_data['global_opts'], fpnn_data['framework'])
        fpnn.build()
        shape = np.array(fpnn.model[-1]['operation'].get_shape().as_list()[1:])
        del fpnn
        if 'data' in subset_layers[-1]:
            del subset_layers[-1]['data']
        output_layer = {'type': 'output', 'layers': []}
        if dropout_layer is not None:
            output_layer['layers'].append(dropout_layer)
        if len(shape) > 1:
            output_layer['layers'].append({'type': 'reshape', 'opts': {'shape': [np.prod(shape)]}})
        if deep_feature_shape is None:
            output_layer['layers'].append({'type': 'matmul', 'opts': {'shape': [nlabels]}})
        else:
            if not np.array_equal(shape, deep_feature_shape):
                print('adding extra layer')
                output_layer['layers'].append({'type': 'matmul', 'opts': {'shape': [np.prod(deep_feature_shape)]}})
            if len(deep_feature_shape) > 1:
                output_layer['layers'].append({'type': 'reshape', 'opts': {'shape': deep_feature_shape.tolist()}})
            output_layer['layers'] += classifier['layers'][:-1]
        output_layer['layers'].append(classifier_function)
        subset_layers.append(output_layer)
        fpnn = FpNN(fpnn_data['input_size'], fpnn_data['model_type'], subset_layers,
                    fpnn_data['global_opts'], fpnn_data['framework'])
        fpnn.fit(data, label, test_data, test_label, total_training=True, batch_size=batch_size, epochs=epochs)
        new_layers[i] = deepcopy(fpnn.layers[i])
        del fpnn
        if 'variables' not in new_layers[i]:
            new_layers[i]['variables'] = {}
        new_layers[i]['variables']['trainable'] = False
        # if 'layers' in new_layers[i]:
        #     for j in range(len(new_layers[i]['layers'])):
        #         if new_layers[i]['layers'][j]['type'] in ['matmul', 'conv2d']:
        #             new_layers[i]['layers'][j]['variables'] = {'trainable': False}
    return new_layers


