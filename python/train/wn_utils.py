import numpy as np


def conv(filter_size, out_channel, strides, pad='SAME', lamda=2e-3):
    return {
        'type': 'conv2d',
        'data': {
            'shape': [filter_size, filter_size, out_channel],
            'strides': [strides, strides],
            'padding': pad,
            'initializer': 'random_normal',
            'stddev': np.sqrt(2.0/filter_size/filter_size/out_channel)
        },
        'regularization': [{'ord': 2, 'lambda': lamda}]
    }


def bn(momentum=0.9):
    return {
        'type': 'batch_normalization',
        'opts': {'momentum': momentum}
    }


def bn_relu_conv(filter_size, out_channel, strides, pad='SAME', lamda=2e-3, momentum=0.9):
    return {
        'type': 'block',
        'layers': [
            bn(momentum),
            # {'type': 'dropout', 'data': {'keep_prob': 0.9}},
            {'type': 'relu'},
            conv(filter_size, out_channel, strides, pad, lamda)
        ]
    }

def block_a(out_channel, strides):
    return {
        'type': 'block',
        'layers': layers_block_b(out_channel, strides)
    }

def layers_block_b(out_channel, strides=1):
    return [
        bn_relu_conv(3, out_channel, strides),
        bn(),
        {'type': 'relu'},
        conv(3, out_channel, 1)
    ]

def block_b(out_channel):
    return {
        'type': 'block',
        'layers': layers_block_b(out_channel)
    }

def block(out_channel, strides, N):
    layers = [
        block_a(out_channel, strides)
    ]
    for _ in range(N-1):
        layers.append(
            block_b(out_channel)
        )
    return {
        'type': 'block',
        'layers': layers
    }
