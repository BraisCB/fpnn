cimport numpy as np
import numpy as np
cimport cython
ctypedef np.float64_t DTYPE_t


@cython.boundscheck(False)
@cython.wraparound(False)
def outer_sum(np.ndarray[DTYPE_t, ndim=1] a, np.ndarray[DTYPE_t, ndim=1] result):
    cdef int m = a.shape[0]
    cdef int cont = 0
    for i in range(m):
        for j in range(i,m):
            result[cont] += a[i]*a[j]
            cont += 1

@cython.boundscheck(False)
@cython.wraparound(False)
def outer_sum_with_factor(DTYPE_t factor, np.ndarray[DTYPE_t, ndim=1] a, np.ndarray[DTYPE_t, ndim=1] result):
    cdef int m = a.shape[0]
    cdef int cont = 0
    for i in range(m):
        for j in range(i,m):
            result[cont] += factor*a[i]*a[j]
            cont += 1