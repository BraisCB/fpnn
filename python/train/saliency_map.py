import numpy as np
import tensorflow as tf
from python.frameworks.tensorflow_parser import get_feed_dict_tf
from python.train.simonyan_saliency_map import tf_get_simonyan_saliency_map


def get_saliency_map(fpnn, data, labels, method, batch_size=50, is_training=False):
    if 'simonyan' in method:
        output = tf_get_simonyan_saliency_map(fpnn, data, labels, batch_size, is_training)
    else:
        output = tf_get_cancela_saliency_map(fpnn, data, labels, batch_size, is_training)
    return output


def tf_get_cancela_saliency_map(fpnn, data, labels, batch_size=50, is_training=False):
    output = []
    index = 0
    len_data = len(data)
    feed_dict = get_feed_dict_tf(fpnn, is_training=is_training)
    if 'cancela' not in fpnn.extra_ops:
        print('adding cancela gradient')
        fpnn.extra_ops['cancela'] = __add_cancela_saliency(fpnn)
    saliency_model = fpnn.extra_ops['cancela']
    while index < len_data:
        # print(index, 'of', len_data)
        new_index = min(index + batch_size, len_data)
        batch_data = data[index:new_index]
        batch_label = labels[index:new_index]
        feed_dict[fpnn.model[0]['operation']] = batch_data
        feed_dict[saliency_model[0]['operation']] = batch_label
        output.append(saliency_model[-1]['operation'].eval(feed_dict=feed_dict))
        index = new_index
    output = np.concatenate(output, axis=0)
    return output


def __is_reduced_op(layer):
    return layer['type'] == 'matmul' or layer['type'] == 'reshape' or \
           'pool' in layer['type'] or layer['type'] == 'conv2d'


def __add_cancela_saliency(fpnn):
    if fpnn.model is None:
        fpnn.build()
    saliency_model = [{
        'type': 'placeholder',
        'operation': tf.placeholder(dtype=tf.float32, shape=[None] + fpnn.outputs[-1]['block_operations'][-1]['info']['shape']['out'])
    }]
    saliency_model += __tf_recursive_get_saliency_model(saliency_model, fpnn.outputs[-1]['block_operations'][::-1], prev_op=fpnn.model[-1]['operation'])
    saliency_model += __tf_recursive_get_saliency_model(saliency_model, fpnn.model[::-1])
    saliency_model[-1]['placeholder'] = tf.placeholder(dtype=tf.float32, shape=[None] + [1] * len(fpnn.input_size))
    saliency_model[-1]['reduce_sum'] = tf.reduce_sum(saliency_model[-1]['placeholder'] * saliency_model[-1]['operation'], axis=0)
    return saliency_model


def __tf_recursive_get_saliency_model(saliency_model, layers, prev_op=None):
    output = []
    saliency_op = saliency_model[-1]
    for i, layer in enumerate(layers):
        if 'block_operations' in layer:
            pr_op = __tf_get_next_op(layers, i, prev_op)
            output += __tf_recursive_get_saliency_model(saliency_model + output, layer['block_operations'][::-1], prev_op=pr_op)
            saliency_op = output[-1]
            continue
        elif not __is_reduced_op(layer):
            continue
        next_op = __tf_get_next_op(layers, i, prev_op)
        if layer['type'] == 'matmul':
            model_layer = {
                'type': 'inv_matmul',
                'operation': tf.identity(
                    tf.matmul(saliency_op['operation'], tf.transpose(layer['variables']['weights'])) * next_op
                )
            }
        elif layer['type'] == 'reshape':
            model_layer = {
                'type': 'reshape',
                'operation': tf.reshape(saliency_op['operation'], shape=[-1] + layer['info']['shape']['in'])
            }
        elif 'pool' in layer['type'] or layer['type'] == 'conv2d':
            if layer['type'] == 'conv2d':
                weights = layer['variables']['weights']
            else:
                weights = np.zeros(
                    layer['variables']['ksize'] + layer['info']['shape']['in'][-1:] + layer['info']['shape']['in'][-1:]
                ).astype(np.float32)
                for j in range(layer['info']['shape']['in'][-1]):
                    weights[: , :, j, j] = 1.0
                weights = tf.constant(weights, dtype=tf.float32)
            model_layer = {
                'type': 'inv_' + layer['type'],
                'operation': tf.identity(tf.nn.conv2d_backprop_input(
                    [tf.shape(saliency_op['operation'])[0]] + layer['info']['shape']['in'], weights,
                    saliency_op['operation'], [1] + layer['variables']['strides'] + [1], layer['variables']['padding']
                ) * next_op)
            }
            if 'pool' in layer['type']:
                model_layer['grad'] = tf.gradients(layer['operation'], next_op)[0]
                model_layer['operation'] *= tf.abs(tf.sign(model_layer['grad']))
        else:
            continue
        output.append(model_layer)
        saliency_op = output[-1]
    # output.append({'operation': tf.abs(output[-1]['operation'])})

    return output


def __tf_get_next_op(layers, i, prev_op):
    if i != len(layers) - 1:
        return layers[i + 1]['operation']
    elif prev_op is not None:
        return prev_op


def __recursive_get_reduced_model(layers, prev_op=None):
    output = []
    ops = []
    for i, layer in enumerate(layers):
        if 'block_operations' in layer:
            pr_op = prev_op
            if i != len(layers) - 1:
                pr_op = layers[i + 1]['operation']
            output_layer, ops_layer = __recursive_get_reduced_model(layer['block_operations'][::-1], prev_op=pr_op)
            output += output_layer
            ops += ops_layer
        elif __is_reduced_op(layer):
            output.append(layer)
            if 'pool' in layer['type']:
                output[-1]['variables']['weights'] = np.zeros(
                    layer['variables']['ksize'] + layer['info']['shape']['in'][-1:] + layer['info']['shape']['in'][-1:]
                ).astype(np.float32)
                for j in range(layer['info']['shape']['in'][-1]):
                    output[-1]['variables']['weights'][: , :, j, j] = 1.0
            if i != len(layers) - 1:
                ops.append(layers[i + 1]['operation'])
            elif prev_op is not None:
                ops.append(prev_op)
    return output, ops

