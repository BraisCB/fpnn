import numpy as np
from copy import deepcopy
from python.fpnn import FpNN
# from python.train.saliency_map import get_saliency_map, tf_get_saliency_map, tf_get_saliency_model
from python.train.saliency_map import get_saliency_map
from scipy.stats import rankdata


def get_features(layers, global_opts, method, train_data, train_labels, valid_data, valid_labels,
                 test_data=None, test_labels=None, thresh=0.9, epochs=10000, batch_size=50,
                 reps=5, feat_type='wrapper', ranking=False, reduce_data=False):
    output_shape = train_data.shape[1:]
    rank = np.array(range(np.prod(output_shape)))
    alive_feats = rank.copy()
    best_score = 0
    best_test_score = 0
    stats = []
    mask = np.ones(train_data.shape[1:])

    while np.count_nonzero(mask) > 1:
        n_components = np.count_nonzero(mask)
        print('n_components =', np.count_nonzero(mask))
        print('best_score =', best_score)
        print('best_nfeats =', len(alive_feats))
        if test_data is not None:
            print('best_test_score =', best_test_score)
        info = get_feature_step(
            layers, global_opts, method, train_data, train_labels, valid_data, valid_labels,
            test_data, test_labels, epochs, batch_size,
            reps=reps, feat_type=feat_type, ranking=ranking
        )
        map = np.array(info['map']).flatten()
        score = np.mean(info['stats']['validation']['accuracy'])
        non_zero_features = np.count_nonzero(mask.flatten()*map)

        real_pos = np.where(mask.flatten() > 0.0)[0]
        score_index = np.argsort(map[real_pos])[::-1]
        map[map < 0.0] = 1e-8
        map /= map.sum() + 1e-6

        # cumscore = np.cumsum(map[real_pos[score_index]])
        # new_indexes = np.where(cumscore > thresh)[0]

        new_indexes = list(range(int(np.count_nonzero(mask)*thresh)))
        if len(new_indexes) == n_components:
            break
        if reduce_data:
            rank[:np.count_nonzero(mask)] = rank[:np.count_nonzero(mask)][real_pos[score_index]]
        else:
            rank[:np.count_nonzero(mask)] = real_pos[score_index]
        stats.append({
            'info': info,
            'features': rank[:non_zero_features].tolist(),
            'nfeatures': non_zero_features
        })
        if score > best_score or np.abs(best_score - score) < 1e-5:
            best_score = score
            alive_feats = rank[:non_zero_features]
            if test_data is not None:
                best_test_score = np.mean(info['stats']['test']['accuracy'])
        if reduce_data:
            train_data = train_data[:, score_index[new_indexes]]
            if valid_data is not None:
                valid_data = valid_data[:, score_index[new_indexes]]
            if test_data is not None:
                test_data = test_data[:, score_index[new_indexes]]
            mask = np.ones(train_data.shape[1:])
        else:
            mask = np.zeros(train_data.shape[1:])
            mask.flat[real_pos[score_index[new_indexes]]] = 1.0
            train_data *= mask
            if valid_data is not None:
                valid_data *= mask
            if test_data is not None:
                test_data *= mask
        print('score =', score)
        if test_data is not None:
            print('test_score = ', np.mean(info['stats']['test']['accuracy']))

    return {
        'features': alive_feats.tolist(),
        'rank': rank.tolist(),
        'score': best_score,
        'stats': stats
    }


def get_feature_step(layers, global_opts, method, train_data, train_labels, valid_data, valid_labels, test_data, test_labels,
                     epochs, batch_size,
                     reps=5, feat_type='wrapper', ranking=False):
    score = []
    train_score = []
    test_score = []
    loss = []
    train_loss = []
    test_loss = []
    nlabels = len(np.unique(train_labels))
    input_size = list(train_data.shape[1:])
    if feat_type == 'lasso':
        pos_saliency = []
        neg_saliency = []
    else:
        pos_saliency = np.zeros([nlabels, int(np.prod(input_size))])
        neg_saliency = np.zeros([nlabels, int(np.prod(input_size))])
    reshape_dim = np.ones(len(train_data.shape)).astype(int)
    reshape_dim[0] = -1
    for rep in range(reps):
        print('rep', rep)
        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
        info = model.fit(train_data, train_labels, valid_data, valid_labels, epochs=epochs, batch_size=batch_size)
        # saliency_model = tf_get_saliency_model(model)
        score.append(info['test']['accuracy'][-1][1])
        train_score.append(info['train']['accuracy'][-1][1])
        loss.append(info['test']['loss'][-1][1])
        train_loss.append(info['train']['loss'][-1][1])
        print('train = ', train_score[-1], ', validation', score[-1])
        if test_data is not None:
            test_score.append(model.eval(test_data, test_labels)[0])
            test_loss.append(model.eval(test_data, test_labels, output_type='loss')[0])
            print('test = ', test_score[-1])
        if feat_type == 'lasso':
            is_mask = False
            for layer in model.model:
                if layer['type'] == 'mask':
                    lasso_mask = np.reshape(
                        np.abs(layer['variables']['weights'].eval()), [-1, int(np.prod(input_size))]
                    )
                    if ranking:
                        lasso_mask = rankdata(lasso_mask)
                        lasso_mask -= np.min(lasso_mask)
                        lasso_mask /= np.max(lasso_mask)
                    lasso_mask /= np.abs(lasso_mask).sum() + 1e-6
                    pos_saliency.append(lasso_mask)
                    is_mask = True
                    break
            if not is_mask:
                raise Exception('mask layer is not declared')
        else:
            if feat_type == 'filter':
                label_probs = model.eval(train_data, output_type='probabilities')[-1, :, :]
                label_pred = model.eval(train_data, output_type='labels')[0]
            else:
                label_probs = model.eval(valid_data, output_type='probabilities')[-1, :, :]
                label_pred = model.eval(valid_data, output_type='labels')[0]
            for i in range(nlabels):
                print('label', i)
                vector = 0.0 * np.ones(nlabels)
                vector[i] = 1.0
                if feat_type in 'wrapper':
                    tp = np.where((valid_labels == i) & (label_pred == i))[0]
                    saliency_data = valid_data[tp]
                    saliency_probs = label_probs[tp, i]
                    saliency_labels = np.tile(vector, (len(tp), 1)) # valid_labels[tp]
                elif feat_type == 'filter_valid':
                    tp = np.where(valid_labels == i)[0]
                    saliency_data = valid_data[tp]
                    saliency_probs = label_probs[tp, i]
                    saliency_labels = np.tile(vector, (len(tp), 1)) # valid_labels[tp]
                elif feat_type == 'filter':
                    tp = np.where(train_labels == i)[0]
                    saliency_data = train_data[tp]
                    saliency_probs = label_probs[tp, i]
                    saliency_labels = np.tile(vector, (len(tp), 1)) # train_labels[tp]
                if len(tp) > 0:
                    # pos_factor = len(tp) / (valid_labels == i).sum()
                    pos_factor = 1.0
                    normalized = True
                    # pos_saliency_aux = tf_get_saliency_map(model, saliency_model, vector, data=saliency_data)
                    pos_saliency_aux = get_saliency_map(model, data=saliency_data, labels=saliency_labels, method=method)
                    if __check_if_fliplr(model.layers):
                        print('computing flipped version')
                        pos_saliency_aux_flipped = get_saliency_map(
                            model, data=__fliplr_data(saliency_data), labels=saliency_labels, method=method
                        )
                        pos_saliency_aux = np.concatenate((pos_saliency_aux, pos_saliency_aux_flipped), axis=0)
                        saliency_probs = np.concatenate((saliency_probs, saliency_probs), axis=0)
                    pos_saliency_aux = np.reshape(pos_saliency_aux, [-1, int(np.prod(input_size))])
                    # pos_saliency_aux[pos_saliency_aux < 0] = 0
                    if normalized:
                        pos_saliency_aux /= np.linalg.norm(pos_saliency_aux, ord=1, axis=1, keepdims=True) + 1e-6
                    if ranking:
                        pos_saliency_aux = np.array([rankdata(d) for d in pos_saliency_aux])
                        pos_saliency_aux -= np.min(pos_saliency_aux, axis=1, keepdims=True)
                        pos_saliency_aux /= np.sum(pos_saliency_aux, axis=1, keepdims=True) #ANTES ESTABA A MAX
                    pos_saliency_aux = np.sum(
                        (1.0 - saliency_probs[:, None]) * pos_saliency_aux,
                        axis=0
                    )
                    # pos_saliency_aux /= np.abs(pos_saliency_aux).sum() + 1e-6
                    pos_saliency[i] += pos_factor * pos_saliency_aux
                del saliency_data, saliency_labels
                if feat_type == 'wrapper':
                    fp = np.where((valid_labels == i) & (label_pred != i))[0]
                    fp_data = valid_data[fp]
                    if len(fp) > 0:
                        neg_factor = 1.0 #0.1 + len(fp) / (valid_labels != label_pred).sum()
                        # neg_saliency_aux = np.reshape(np.reshape(
                        #     np.sum(label_probs[fp, :], axis=-1) - label_probs[fp, i], reshape_dim
                        # ) * tf_get_saliency_map(
                        #     model, saliency_model, -1.0*vector, data=fp_data
                        # ), [-1, int(np.prod(input_size))])
                        neg_saliency_aux = get_saliency_map(model, data=valid_data[fp], labels=valid_labels[fp], method=method)
                        if __check_if_fliplr(model.layers):
                            print('computing flipped version')
                            neg_saliency_aux_flipped = get_saliency_map(
                                model, data=__fliplr_data(valid_data[fp]), labels=valid_labels[fp], method=method
                            )
                            neg_saliency_aux = np.concatenate((neg_saliency_aux, neg_saliency_aux_flipped), axis=0)
                        neg_saliency_aux = np.reshape(neg_saliency_aux, [-1, int(np.prod(input_size))])
                        if normalized:
                            neg_saliency_aux /= np.linalg.norm(neg_saliency_aux, axis=1, keepdims=True) + 1e-6
                        if ranking:
                            neg_saliency_aux = np.array([rankdata(d) for d in neg_saliency_aux])
                            neg_saliency_aux -= np.min(neg_saliency_aux, axis=1, keepdims=True)
                            neg_saliency_aux /= np.sum(neg_saliency_aux, axis=1, keepdims=True)
                        neg_saliency_aux = np.mean(neg_saliency_aux, axis=0)
                        neg_saliency_aux /= neg_saliency_aux.sum() + 1e-6
                        # pos_saliency[-1] = neg_factor * (pos_saliency[-1] - neg_saliency_aux)
                        neg_saliency[i] += neg_factor * neg_saliency_aux
        # del saliency_model, model
        del model
    pos_saliency /= np.sum(np.abs(pos_saliency), axis=1, keepdims=True) + 1e-6
    if feat_type == 'lasso':
        pos_saliency = np.sum(pos_saliency, axis=0)
    else:
        pos_saliency = np.sum(pos_saliency, axis=0)
    pos_saliency /= pos_saliency.sum() + 1e-6
    maps = [pos_saliency]
    if feat_type == 'wrapper':
        neg_saliency /= np.sum(neg_saliency, axis=1, keepdims=True) + 1e-6
        neg_saliency = np.sum(neg_saliency, axis=0)
        neg_saliency /= neg_saliency.sum() + 1e-6
        maps.append(-1.0*neg_saliency) # pos_saliency + neg_saliency
    max_maps = np.reshape(np.mean(maps, axis=0), input_size)
    # max_maps[max_maps < 0.0] = 0.0
    # max_maps /= np.sum(max_maps)
    return {
        'map': max_maps.tolist(),
        'stats': {
            'train': {'accuracy': train_score, 'loss': train_loss},
            'validation': {'accuracy': score, 'loss': loss},
            'test': {'accuracy': test_score, 'loss': test_loss}
        }
    }


def __check_if_fliplr(layers):
    for layer in layers:
        if 'random_flip_left_right' in layer['type']:
            return True
        elif 'layers' in layer:
            flag = __check_if_fliplr(layer['layers'])
            if flag:
                return True
    return False


def __fliplr_data(data):
    return np.array([np.fliplr(x) for x in data])

