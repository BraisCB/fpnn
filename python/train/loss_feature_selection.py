import numpy as np
from copy import deepcopy
from python.fpnn import FpNN
# from python.train.saliency_map import get_saliency_map, tf_get_saliency_map, tf_get_saliency_model
from python.train.saliency_map import get_saliency_map
from scipy.stats import rankdata


def get_loss_features(train_data, train_labels, method, **kwargs):
    if 'incremental_loss' in method:
        return incremental_get_features(train_data, train_labels, **kwargs)
    elif 'decremental_loss' in method:
        return decremental_get_features(train_data, train_labels, **kwargs)


def incremental_get_features(train_data, train_labels, layers, global_opts, valid_data, valid_labels,
                 epochs=10000, batch_size=50,
                 reps=5, factor=0.9):
    mask = np.zeros(train_data.shape[1:])

    input_size = list(train_data.shape[1:])
    models = []
    for rep in range(reps):
        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
        model.fit(train_data, train_labels, valid_data, valid_labels, epochs=epochs, batch_size=batch_size)
        models.append(model)
    unselected_features = list(range(int(np.prod(input_size))))
    rank = []
    scores = []
    train_accuracy = []
    accuracy = []
    last_loss = np.Inf
    blocked_features = False

    new_update = int(len(unselected_features) * factor)

    while len(unselected_features) > 0:

        n_components = len(rank)
        print('n_components =', n_components)
        print('best_nfeats =', len(rank))
        print('last loss =', last_loss)
        if len(accuracy) > 0:
            print('last train accuracy = ', train_accuracy[-1])
            print('last valid accuracy = ', accuracy[-1])
        minimum_loss = np.Inf

        for index, feature in enumerate(unselected_features):
            mask[feature] = 1.0
            loss = 0.0
            for model in models:
                loss += model.eval(mask * train_data, train_labels, output_type='loss')[0]
                if __check_if_fliplr(model.layers):
                    print('computing flipped version')
                    loss += model.eval(mask * __fliplr_data(train_data), train_labels, output_type='loss')[0]
            mask[feature] = 0.0
            if loss < minimum_loss:
                best_feature = feature
                best_index = index
                minimum_loss = loss

        if minimum_loss < last_loss and not blocked_features:
            features = rank
        else:
            blocked_features = True

        rank.append(best_feature)
        scores.append(minimum_loss)
        mask[best_feature] = 1.0
        accuracy.append(model.eval(mask * valid_data, valid_labels)[0])
        train_accuracy.append(model.eval(mask * train_data, train_labels)[0])
        del unselected_features[best_index]
        last_loss = minimum_loss

        if len(unselected_features) <= new_update:
            print('training models again')
            del models[:]
            models = []
            for rep in range(reps):
                model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                model.fit(
                    (1.0 - mask) * train_data, train_labels, (1.0 - mask) * valid_data, valid_labels,
                    epochs=epochs, batch_size=batch_size
                )
                models.append(model)
            mask = np.zeros(train_data.shape[1:])
            new_update = int(new_update * factor)

    return {
        'features': features,
        'rank': rank,
        'score': scores,
        'valid_accuracy': accuracy,
        'train_accuracy': train_accuracy
    }


def decremental_get_features(train_data, train_labels, layers, global_opts, valid_data, valid_labels,
                 epochs=10000, batch_size=50,
                 reps=5, factor=0.9):
    mask = np.ones(train_data.shape[1:])

    input_size = list(train_data.shape[1:])
    models = []
    for rep in range(reps):
        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
        model.fit(train_data, train_labels, valid_data, valid_labels, epochs=epochs, batch_size=batch_size)
        models.append(model)
    unselected_features = list(range(int(np.prod(input_size))))
    rank = []
    scores = []
    train_accuracy = []
    accuracy = []
    last_loss = np.Inf
    blocked_features = False

    new_update = int(len(unselected_features) * factor)

    while len(unselected_features) > 0:

        n_components = len(rank)
        print('n_components =', n_components)
        print('best_nfeats =', len(unselected_features))
        print('last loss =', last_loss)
        if len(accuracy) > 0:
            print('last train accuracy = ', train_accuracy[-1])
            print('last valid accuracy = ', accuracy[-1])
        minimum_loss = np.Inf

        for index, feature in enumerate(unselected_features):
            mask[feature] = 0.0
            loss = 0.0
            for model in models:
                loss += model.eval(mask * train_data, train_labels, output_type='loss')[0]
                if __check_if_fliplr(model.layers):
                    print('computing flipped version')
                    loss += model.eval(mask * __fliplr_data(train_data), train_labels, output_type='loss')[0]
            mask[feature] = 1.0
            if loss < minimum_loss:
                worst_feature = feature
                worst_index = index
                minimum_loss = loss

        rank.append(worst_feature)
        scores.append(minimum_loss)
        mask[worst_feature] = 0.0
        accuracy.append(model.eval(mask * valid_data, valid_labels)[0])
        train_accuracy.append(model.eval(mask * train_data, train_labels)[0])
        del unselected_features[worst_index]
        last_loss = minimum_loss

        if 0 < len(unselected_features) <= new_update:
            print('training models again')
            del models[:]
            models = []
            for rep in range(reps):
                model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                model.fit(
                    mask * train_data, train_labels, mask * valid_data, valid_labels,
                    epochs=epochs, batch_size=batch_size
                )
                models.append(model)
            new_update = int(new_update * factor)

    return {
        'rank': rank[::-1],
        'scores': scores[::-1]
    }


def __check_if_fliplr(layers):
    for layer in layers:
        if 'random_flip_left_right' in layer['type']:
            return True
        elif 'layers' in layer:
            flag = __check_if_fliplr(layer['layers'])
            if flag:
                return True
    return False


def __fliplr_data(data):
    return np.array([np.fliplr(x) for x in data])

