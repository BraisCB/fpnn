from skfeature.function.statistical_based import CFS
from skfeature.function.information_theoretical_based import MIM, MRMR, FCBF
from skfeature.function.similarity_based import reliefF
from skfeature.function.wrapper import svm_backward
from python.train.feature_selection import get_features
from python.train.loss_feature_selection import get_loss_features
from python.train.accuracy_feature_selection import get_accuracy_features
from copy import deepcopy
import numpy as np


def get_feature_indexes(X, y, method, **kwargs):
    output = {}
    if method == 'CFS':
        output['rank'] = CFS.cfs(X, y)
    elif method == 'MIM':
        output['rank'], output['J_CMI'], output['MIfy'] = MIM.mim(X, y)
    elif method == 'reliefF':
        output['score'] = reliefF.reliefF(X, y)
        # rank features in descending order according to score
        output['rank'] = reliefF.feature_ranking(output['score'])
    elif method == 'MRMR':
        output['rank'], output['J_CMI'], output['MIfy'] = MRMR.mrmr(X, y)
    elif method == 'FCBF':
        output['rank'], output['SU'] = FCBF.fcbf(X, y)
    elif method == 'svm-rfe':
        output['rank'] = svm_backward.svm_backward(X, y)
    elif 'ITdeepFS' in method:
        ranking = 'ranking' in method
        thresh = 0.9 if 'factor' not in kwargs else kwargs['factor']
        feat_type = 'wrapper' if 'wrapper' in method else 'filter'
        params = __get_deepfs_params(X, y, method, feat_type, thresh, ranking, **kwargs)
        output = get_features(**params)
    elif 'ITdeepLASSO' in method:
        ranking = 'ranking' in method
        thresh = 0.9 if 'factor' not in kwargs else kwargs['factor']
        params = __get_deepfs_params(X, y, method, 'lasso', thresh, ranking, **kwargs)
        output = get_features(**params)
    elif 'deepFS' in method:
        ranking = 'ranking' in method
        thresh = 0.0
        feat_type = 'wrapper' if 'wrapper' in method else 'filter'
        params = __get_deepfs_params(X, y, method, feat_type, thresh, ranking, **kwargs)
        output = get_features(**params)
    elif 'deepLASSO' in method:
        ranking = 'ranking' in method
        thresh = 0.0
        params = __get_deepfs_params(X, y, method, 'lasso', thresh, ranking, **kwargs)
        output = get_features(**params)
    elif 'loss' in method:
        new_kwargs = deepcopy(kwargs)
        if 'IT' not in method:
            new_kwargs['factor'] = 0.0
        output = get_loss_features(X, y, method, **new_kwargs)
    elif 'accuracy' in method:
        new_kwargs = deepcopy(kwargs)
        if 'IT' not in method:
            new_kwargs['factor'] = 0.0
        output = get_accuracy_features(X, y, method, **new_kwargs)
    return output


def __get_deepfs_params(X, y, method, feat_type, thresh, ranking, **kwargs):
    output = {
        'train_data': X, 'train_labels': y, 'layers': kwargs['layers'], 'global_opts': kwargs['global_opts']
    }
    output['test_data'] = None if 'test_data' not in kwargs else kwargs['test_data']
    output['test_labels'] = None if 'test_labels' not in kwargs else kwargs['test_labels']
    output['valid_data'] = None if 'valid_data' not in kwargs else kwargs['valid_data']
    output['valid_labels'] = None if 'valid_labels' not in kwargs else kwargs['valid_labels']
    output['thresh'] = thresh
    output['epochs'] = 10000 if 'epochs' not in kwargs else kwargs['epochs']
    output['batch_size'] = 50 if 'batch_size' not in kwargs else kwargs['batch_size']
    output['reps'] = 5 if 'reps' not in kwargs else kwargs['reps']
    output['feat_type'] = feat_type
    output['ranking'] = ranking
    output['method'] = method
    output['reduce_data'] = True if 'reduce_data' not in kwargs else kwargs['reduce_data']
    return output