import numpy as np


def conv(filter_size, out_channel, strides, pad='SAME', regularization=5e-4, kernel_initializer='xavier'):
    conv = {
        'type': 'conv2d',
        'data': {
            'shape': [filter_size, filter_size, out_channel],
            'strides': [strides, strides],
            'padding': pad,
            'initializer': kernel_initializer,
        }
    }
    if regularization > 0.0:
        conv['regularization'] = [{'ord': 2, 'lambda': regularization}]
    return conv


def residual_block_b(output_channels, strides=1, momentum=0.9, dropout_layer=None,
                     regularization=0.0, kernel_initializer='xavier'):
    residual_layers = [
        {'type': 'batch_normalization', 'opts': {'momentum': momentum}},
        {'type': 'relu'},
        conv(filter_size=3, out_channel=output_channels, strides=strides,
             regularization=regularization, kernel_initializer=kernel_initializer),
        {'type': 'batch_normalization', 'opts': {'momentum': momentum}},
    ]

    if dropout_layer is not None:
        residual_layers.append(dropout_layer)

    residual_layers += [
        {'type': 'relu'},
        conv(filter_size=3, out_channel=output_channels, strides=1,
             regularization=regularization, kernel_initializer=kernel_initializer),
    ]

    return {
        'type': 'residual_block',
        'layers': residual_layers
    }


def residual_block_a(output_channels, strides=1, momentum=0.9, dropout_layer=None,
                     regularization=0.0, kernel_initializer='xavier'):
    block_layers = [
        conv(filter_size=3, out_channel=output_channels, strides=strides,
             regularization=regularization, kernel_initializer=kernel_initializer),
        {'type': 'batch_normalization', 'opts': {'momentum': momentum}},
    ]

    if dropout_layer is not None:
        block_layers.append(dropout_layer)

    block_layers += [
        {'type': 'relu'},
        conv(filter_size=3, out_channel=output_channels, strides=1,
             regularization=regularization, kernel_initializer=kernel_initializer),
    ]

    layers = [
        {'type': 'batch_normalization', 'opts': {'momentum': momentum}},
    ]

    # if dropout_layer is not None:
    #     block_layers.append(dropout_layer)

    layers += [
        {'type': 'relu'},
        {
            'type': 'inception_block',
            'branches': [
                conv(1, out_channel=output_channels, strides=strides,
                     regularization=regularization, kernel_initializer=kernel_initializer),
                {
                    'type': 'block',
                    'layers': block_layers
                }
            ],
            'merge': 'add'
        }
    ]

    return {
        'type': 'block',
        'layers': layers
    }


def wrn_block(output_channels, N, strides=1, momentum=0.9, dropout_layer=None, regularization=0.0, kernel_initializer='xavier'):
    layers = [residual_block_a(
        output_channels=output_channels, strides=strides, momentum=momentum, dropout_layer=dropout_layer,
        regularization=regularization, kernel_initializer=kernel_initializer
    )]
    for _ in range(N-1):
        layers.append(
            residual_block_b(
                output_channels=output_channels, strides=1, momentum=momentum, dropout_layer=dropout_layer,
                regularization=regularization, kernel_initializer=kernel_initializer
            )
        )

    return {
        'type': 'block',
        'layers': layers
    }


def wrn_layers(l, k, num_classes, initial_stride=1, momentum=0.9, dropout_layer=None, regularization=0.0,
               kernel_initializer='xavier'):
    N = (l - 4) // 6
    layers = [conv(filter_size=3, out_channel=16, strides=initial_stride, regularization=regularization)]

    output_channel_basis = [16, 32, 64]
    strides = [1, 2, 2]

    for ocb, stride in zip(output_channel_basis, strides):
        layers.append(
            wrn_block(output_channels=ocb * k, N=N, strides=stride, momentum=momentum, dropout_layer=dropout_layer,
                  regularization=regularization, kernel_initializer=kernel_initializer
            )
        )

    layers += [
        {'type': 'batch_normalization', 'opts': {'momentum': momentum}},
        {'type': 'relu'},
        {'type': 'reduce_mean', 'args': {'axis': [1, 2]}},
        {'type': 'flatten'},
        {
            'type': 'output',
            'layers': [
                {
                    'type': 'matmul',
                    'data': {
                        'shape': [num_classes],
                        'initializer': kernel_initializer
                    },
                    'opts': {
                        'bias': True
                    }
                },
                {'type': 'softmax'}
            ]
        }
    ]

    return layers
