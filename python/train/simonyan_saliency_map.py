import numpy as np
import tensorflow as tf
from python.frameworks.tensorflow_parser import get_feed_dict_tf


def tf_get_simonyan_saliency_map(fpnn, data, labels, batch_size=50, is_training=False):
    output = []
    if 'simonyan' not in fpnn.extra_ops:
        print('adding simonyan gradient')
        fpnn.extra_ops['simonyan'] = __add_simonyan_saliency(fpnn)
    index = 0
    len_data = len(data)
    feed_dict = get_feed_dict_tf(fpnn, is_training=is_training)
    while index < len_data:
        # print(index, 'of', len_data)
        new_index = min(index + batch_size, len_data)
        batch_data = data[index:new_index]
        batch_labels = labels[index:new_index]
        feed_dict[fpnn.model[0]['operation']] = batch_data
        feed_dict[fpnn.extra_ops['simonyan']['placeholder']] = batch_labels
        output.append(fpnn.sess.run(fpnn.extra_ops['simonyan']['operation'], feed_dict=feed_dict))
        index = new_index
    output = np.concatenate(output, axis=0)
    return output


def __add_simonyan_saliency(fpnn):
    output = fpnn.outputs[-1]['block_operations'][-1]
    vector = tf.placeholder(
        dtype=tf.float32,
        shape=[None] + fpnn.outputs[-1]['block_operations'][-1]['info']['shape']['out']
    )
    gradient = __get_saliency_gradient(fpnn.model, output, vector)
    return {'operation': gradient, 'placeholder': vector}


def __get_saliency_gradient(layers, output, vector):
    _, output = __get_recursive_saliency_gradient(layers, output, vector)
    return output


def __get_recursive_saliency_gradient(layers, output, vector, prev=None):
    for i, layer in enumerate(layers):
        if 'variables' in layer and 'weights' in layer['variables']:
            prev_layer = __get_prev_layer(layers, i, prev)
            saliency_cost = tf.reduce_sum(vector * output['operation'], axis=-1)
            return True, tf.abs(tf.gradients(saliency_cost, prev_layer['operation'])[0])
        elif 'block_operations' in layer:
            prev_layer = __get_prev_layer(layers, i, prev)
            flag, gradient = __get_recursive_saliency_gradient(layer['block_operations'], output, vector, prev_layer)
            if flag:
                return flag, gradient
    return False, None


def __get_prev_layer(layers, i, prev):
    if i==0:
        return prev
    else:
        return layers[i-1]