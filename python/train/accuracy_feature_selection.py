import numpy as np
from copy import deepcopy
from python.fpnn import FpNN
# from python.train.saliency_map import get_saliency_map, tf_get_saliency_map, tf_get_saliency_model
from python.train.saliency_map import get_saliency_map
from scipy.stats import rankdata


def get_accuracy_features(train_data, train_labels, method, **kwargs):
    if 'incremental_accuracy' in method:
        return incremental_get_features(train_data, train_labels, **kwargs)
    elif 'decremental_accuracy' in method:
        return decremental_get_features(train_data, train_labels, **kwargs)


def incremental_get_features(train_data, train_labels, layers, global_opts, valid_data, valid_labels,
                 epochs=10000, batch_size=50,
                 reps=5, factor=0.9):
    mask = np.zeros(train_data.shape[1:])

    input_size = list(train_data.shape[1:])
    total_elements = int(np.prod(input_size))
    models = []
    for rep in range(reps):
        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
        model.fit(train_data, train_labels, valid_data, valid_labels, epochs=epochs, batch_size=batch_size)
        models.append(model)
    unselected_features = list(range(int(np.prod(input_size))))
    rank = []
    scores = []
    train_accuracy = []
    accuracy = []
    last_loss = np.Inf
    blocked_features = False

    new_update = int(len(unselected_features) * factor)

    while len(unselected_features) > 0:

        n_components = len(rank)
        print('n_components =', n_components)
        print('best_nfeats =', len(rank))
        print('last loss =', last_loss)
        if len(accuracy) > 0:
            print('last train accuracy = ', train_accuracy[-1])
            print('last valid accuracy = ', accuracy[-1])
        losses = np.zeros(total_elements)
        accuracies = np.zeros(total_elements)

        for index, feature in enumerate(unselected_features):
            mask[feature] = 1.0
            for model in models:
                losses[feature] += model.eval(mask * train_data, train_labels, output_type='loss')[0]
                accuracies[feature] += model.eval(mask * train_data, train_labels)[0]
                # if __check_if_fliplr(model.layers):
                #     print('computing flipped version')
                #     custom[feature] += model.eval(mask * __fliplr_data(train_data), train_labels, output_type='loss')[0]
                #     accuracies[feature] += model.eval(mask * __fliplr_data(train_data), train_labels)[0]
            mask[feature] = 0.0

        accuracies = np.abs(np.max(accuracies) - accuracies)
        best_features = np.where(accuracies < 1e-4)[0]
        best_loss_index = np.argmin(losses[best_features])
        best_feature = best_features[best_loss_index]
        best_index = np.where(unselected_features == best_feature)[0][0]
        minimum_loss = losses[best_loss_index]

        if minimum_loss < last_loss and not blocked_features:
            features = rank
        else:
            blocked_features = True

        rank.append(best_feature)
        scores.append(minimum_loss)
        mask[best_feature] = 1.0
        accuracy.append(model.eval(mask * valid_data, valid_labels)[0])
        train_accuracy.append(model.eval(mask * train_data, train_labels)[0])
        del unselected_features[best_index]
        last_loss = minimum_loss

        if len(unselected_features) <= new_update:
            print('training models again')
            del models
            models = []
            for rep in range(reps):
                model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                model.fit(
                    (1.0 - mask) * train_data, train_labels, (1.0 - mask) * valid_data, valid_labels,
                    epochs=epochs, batch_size=batch_size
                )
                models.append(model)
            mask = np.zeros(train_data.shape[1:])
            new_update = int(new_update * factor)

    return {
        'features': features,
        'rank': rank,
        'score': scores,
        'valid_accuracy': accuracy,
        'train_accuracy': train_accuracy
    }


def decremental_get_features(train_data, train_labels, layers, global_opts, valid_data, valid_labels,
                             epochs=10000, batch_size=50,
                             reps=5, factor=0.9):
    mask = np.ones(train_data.shape[1:])

    input_size = list(train_data.shape[1:])
    total_elements = int(np.prod(input_size))
    models = []
    for rep in range(reps):
        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
        model.fit(train_data, train_labels, valid_data, valid_labels, epochs=epochs, batch_size=batch_size)
        models.append(model)
    unselected_features = list(range(int(np.prod(input_size))))
    rank = []
    scores = []
    train_accuracy = []
    accuracy = []
    last_loss = np.Inf
    blocked_features = False

    new_update = int(len(unselected_features) * factor)

    while len(unselected_features) > 0:

        n_components = len(rank)
        print('n_components =', n_components)
        print('best_nfeats =', len(rank))
        print('last loss =', last_loss)
        if len(accuracy) > 0:
            print('last train accuracy = ', train_accuracy[-1])
            print('last valid accuracy = ', accuracy[-1])
        losses = np.zeros(total_elements)
        accuracies = np.zeros(total_elements)

        for index, feature in enumerate(unselected_features):
            mask[feature] = 0.0
            for model in models:
                losses[feature] += model.eval(mask * train_data, train_labels, output_type='loss')[0]
                accuracies[feature] += model.eval(mask * train_data, train_labels)[0]
                # if __check_if_fliplr(model.layers):
                #     print('computing flipped version')
                #     custom[feature] += model.eval(mask * __fliplr_data(train_data), train_labels, output_type='loss')[0]
                #     accuracies[feature] += model.eval(mask * __fliplr_data(train_data), train_labels)[0]
            mask[feature] = 1.0

        accuracies = np.abs(np.max(accuracies) - accuracies)
        best_features = np.where(accuracies < 1e-4)[0]
        best_loss_index = np.argmin(losses[best_features])
        best_feature = best_features[best_loss_index]
        best_index = np.where(unselected_features == best_feature)[0][0]
        minimum_loss = losses[best_loss_index]

        if minimum_loss < last_loss and not blocked_features:
            features = rank
        else:
            blocked_features = True

        rank.append(best_feature)
        scores.append(minimum_loss)
        mask[best_feature] = 0.0
        accuracy.append(model.eval(mask * valid_data, valid_labels)[0])
        train_accuracy.append(model.eval(mask * train_data, train_labels)[0])
        del unselected_features[best_index]
        last_loss = minimum_loss

        if len(unselected_features) <= new_update:
            print('training models again')
            del models
            models = []
            for rep in range(reps):
                model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                model.fit(
                    (1.0 - mask) * train_data, train_labels, (1.0 - mask) * valid_data, valid_labels,
                    epochs=epochs, batch_size=batch_size
                )
                models.append(model)
            mask = np.zeros(train_data.shape[1:])
            new_update = int(new_update * factor)

    return {
        'features': features[::-1],
        'rank': rank[::-1],
        'score': scores[::-1],
        'valid_accuracy': accuracy[::-1],
        'train_accuracy': train_accuracy[::-1]
    }


def __check_if_fliplr(layers):
    for layer in layers:
        if 'random_flip_left_right' in layer['type']:
            return True
        elif 'layers' in layer:
            flag = __check_if_fliplr(layer['layers'])
            if flag:
                return True
    return False


def __fliplr_data(data):
    return np.array([np.fliplr(x) for x in data])

