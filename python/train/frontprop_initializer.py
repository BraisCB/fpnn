import numpy as np
import os
from python.train.vector_utils import get_index_2d, vector_cov, vector_2_mat, dict_merge
from python.train.cython.cutils import outer_sum
from multiprocessing import Pool
from itertools import repeat
from copy import deepcopy
from python.fpnn import FpNN


def init_model(fpnn_data, layers, data, label, limit_index=-1):
    new_layers = __init_recursive_model(fpnn_data, [], layers[:limit_index], data, label)
    return new_layers + layers[limit_index:]


def __init_recursive_model(fpnn_data, prev_layers, layers, data, label):
    new_layers = []
    for i in range(len(layers)):
        layer = layers[i]
        if layer['type'] in ['matmul', 'conv2d']:
            new_layer = __initialize_weights_and_bias(fpnn_data, prev_layers + new_layers, layer, data, label)
            if 'variables' not in new_layer:
                new_layer['variables'] = {}
            new_layer['variables']['trainable'] = False
            new_layers.append(new_layer)
        elif 'normalization' in layer['type']:
            new_layer = __initialize_normalization_layer(fpnn_data, prev_layers + new_layers, layer, data, label)
            if 'variables' not in new_layer:
                new_layer['variables'] = {}
            new_layer['variables']['trainable'] = False
            new_layers.append(new_layer)
        elif 'block' in layer['type']:
            fpnn_subset_data = deepcopy(fpnn_data)
            fpnn_subset_data['global_opts'] = dict_merge(fpnn_data['global_opts'], layer)
            new_block_layer = {
                'type': layer['type'],
            }
            if 'blocks' in layer:
                new_block_layer['blocks'] = []
                for block in layer['blocks']:
                    new_block_layer['blocks'].append(
                        __init_recursive_model(fpnn_subset_data, prev_layers + new_layers, block, data, label)
                    )
            elif 'layers' in layer:
                new_block_layer['layers'] = __init_recursive_model(
                    fpnn_subset_data, prev_layers + new_layers, layer['layers'], data, label
                )
            new_layers.append(new_block_layer)
        else:
            new_layers.append(layer)
    return new_layers


def __initialize_normalization_layer(fpnn_data, prev_layers, layer, data, label, bias=True):
    fpnn = FpNN(fpnn_data['input_size'], fpnn_data['model_type'], prev_layers,
                fpnn_data['global_opts'], fpnn_data['framework'])
    mean, var = get_moments(fpnn, data, label)
    del fpnn
    new_layer = deepcopy(layer)
    new_layer['data'] = {
        'mean': mean,
        'var': var,
        'offset': 1.0,
        'scale': 0.005
    }
    return new_layer


def __initialize_weights_and_bias(fpnn_data, prev_layers, layer, data, label, bias=True):
    fpnn = FpNN(fpnn_data['input_size'], fpnn_data['model_type'], prev_layers,
                fpnn_data['global_opts'], fpnn_data['framework'])
    weights = __initialize_weights(fpnn, layer, data, label)
    new_layer = deepcopy(layer)
    new_layer['data'] = {
        'weights': weights.tolist(),
    }
    if bias:
        bias_data = [0] * layer['opts']['shape'][-1]
        new_layer['data']['bias'] = bias_data
    fpnn.add_layer(new_layer)
    mean, var = get_moments(fpnn, data, label)
    stddev = np.sqrt(var)
    aug_block = dict_merge(fpnn.global_opts, fpnn.layers[-1])
    del fpnn
    offset = 0.05
    if 'frontprop' in aug_block and 'offset' in aug_block['frontprop']:
        offset = aug_block['frontprop']['offset']
    for i in range(weights.shape[-1]):
        if len(weights.shape) == 2:
            weights[:, i] /= stddev[i]
        else:
            weights[:, :, :, i] /= stddev[i]
        if bias:
            bias_data[i] = offset - mean[i]/stddev[i]
    new_layer['data']['weights'] = weights.tolist()
    if bias:
        new_layer['data']['bias'] = bias_data
    return new_layer


def get_moments(fpnn, data, label, bias=True):
    fpnn.build()
    feed_dict = fpnn.get_feed_dict(data=None, label=None, is_training=False, backprop_train=False)
    layer_shape = fpnn.model[-1]['operation'].get_shape().as_list()[1:]
    aug_block = dict_merge(fpnn.global_opts, fpnn.layers[-1])
    frontprop = {}
    if 'frontprop' in aug_block:
        frontprop = aug_block['frontprop']

    batch_size = 128
    if 'batch_size' in frontprop:
        batch_size = frontprop['batch_size']
    repetitions = 1
    if 'repetitions' in frontprop:
        repetitions = frontprop['repetitions']

    mean = np.zeros(layer_shape)
    squared_sum = np.zeros(layer_shape)
    cont_data = 0

    flips = ['normal']
    if 'flips' in frontprop:
        flips += frontprop['flips']

    perm = np.arange(len(data))
    for rep in range(repetitions):
        index = 0
        np.random.shuffle(perm)
        data = data[perm]
        label = label[perm]
        while index < len(data):
            new_index = min(len(data), index + batch_size)
            for flip in flips:
                data_batch = []
                for d in data[index:new_index]:
                    if flip == 'normal':
                        data_batch.append(d)
                    elif flip == 'left_right':
                        data_batch.append(np.fliplr(d))
                    elif flip == 'up_down':
                        data_batch.append(np.flipud(d))
                data_batch = np.array(data_batch)
                feed_dict[fpnn.model[0]['operation']] = data_batch
                out = fpnn.model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)
                out_2 = out * out
                for i in range(0, layer_shape[-1]):
                    if len(out.shape) > 2:
                        mean[i] += np.sum(out[:,:,:,i])
                        squared_sum[i] += np.sum(out_2[:,:,:,i])
                    else:
                        mean[i] += np.sum(out[:, i])
                        squared_sum[i] += np.sum(out_2[:, i])
                cont_data += np.prod(out.shape[:-1])
            index = new_index
    squared_sum /= cont_data
    mean /= cont_data
    if bias:
        var = squared_sum - mean * mean
    else:
        var = squared_sum
        mean = np.zeros(mean.shape)
    return mean, var


def __initialize_weights(fpnn, new_layer, data, label):
    if not fpnn.model:
        fpnn.build()
    aug_block = dict_merge(fpnn.global_opts, new_layer)
    frontprop = {}
    if 'frontprop' in aug_block:
        frontprop = aug_block['frontprop']

    batch_size = 50
    if 'batch_size' in frontprop:
        batch_size = frontprop['batch_size']
    repetitions = 1
    if 'repetitions' in frontprop:
        repetitions = frontprop['repetitions']

    scatter_matrices = __get_Sw_Sb(fpnn, new_layer, data, label, batch_size, repetitions, frontprop)

    techniques = [
        {'type': 'weighted-lda'}
    ]
    if 'frontprop' in aug_block and 'techniques' in aug_block['frontprop']:
        techniques = aug_block['frontprop']['techniques']

    basis = {'d': [], 'V': []}

    for technique in techniques:
        if technique['type'] == 'lda':
            new_basis = __get_LDA_basis(scatter_matrices['S_w'], scatter_matrices['S_b'], frontprop)
        elif technique['type'] == 'weighted-lda':
            new_basis = __get_LDA_basis(scatter_matrices['wS_w'], scatter_matrices['wS_b'], frontprop)
        elif technique['type'] == 'direct-lda':
            new_basis = __get_LDA_direct_basis(scatter_matrices['S_w'], scatter_matrices['S_b'], frontprop)
        elif technique['type'] == 'weighted-direct-lda':
            new_basis = __get_LDA_direct_basis(scatter_matrices['wS_w'], scatter_matrices['wS_b'], frontprop)
        elif technique['type'] == 'rda':
            new_basis = __get_PCA_basis(scatter_matrices['S_w'], frontprop)
        elif technique['type'] == 'pca':
            new_basis = __get_PCA_basis(scatter_matrices['S_w'] + scatter_matrices['S_b'], frontprop)
        basis['d'] += new_basis['d']
        basis['V'] += new_basis['V']

    weights = __get_weights(fpnn, basis, aug_block['opts'])
    return weights


def __get_Sw_Sb(fpnn, new_layer, data, label, b_size, repetitions, train_opts):

    flips = ['normal']
    if 'flips' in train_opts:
        flips += train_opts['flips']

    batch_size = int(b_size / len(flips))

    label_distances = __get_label_distances(fpnn, data, label, batch_size, flips)

    opts = new_layer['opts']
    unique_labels = np.unique(label)
    nlabels = len(unique_labels)

    layer_shape = fpnn.model[-1]['operation'].get_shape().as_list()[1:]

    conv_window = opts['shape'][:-1] + layer_shape[-1:]

    rows = get_index_2d(layer_shape, conv_window, train_opts)
    cov = np.zeros((int((np.prod(conv_window) + 1) * np.prod(conv_window) / 2.0)))
    w_cov = np.zeros((int((np.prod(conv_window) + 1) * np.prod(conv_window) / 2.0)))
    media = []

    nproc = os.cpu_count() or 1
    pool = Pool(nproc)

    print('flips', len(flips))
    feed_dict = fpnn.get_feed_dict(data=None, label=None, is_training=False, backprop_train=False)

    for i in range(nlabels):
        print('label', i, 'nelements', (label == unique_labels[i]).sum())
        data_label = data[label == unique_labels[i]]
        ndata_label = len(data_label)

        cov_label = np.zeros((int((np.prod(conv_window) + 1) * np.prod(conv_window) / 2.0)))
        media_label = np.zeros((int(np.prod(conv_window))))
        cont_label = 0
        for _ in range(repetitions):
            index = 0
            while index < ndata_label:
                data_model = []
                for i in range(nproc):
                    new_index = min(ndata_label, index + batch_size)
                    data_batch = []
                    for d in data_label[index:new_index]:
                        for flip in flips:
                            if flip == 'normal':
                                data_batch.append(d)
                            elif flip == 'left_right':
                                data_batch.append(np.fliplr(d))
                            elif flip == 'up_down':
                                data_batch.append(np.flipud(d))
                    data_batch = np.array(data_batch)
                    feed_dict[fpnn.model[0]['operation']] = data_batch
                    data_model.append(fpnn.model[-1]['operation'].eval(feed_dict=feed_dict).astype(float))
                    index = new_index
                    if new_index == ndata_label:
                        break
                results = pool.starmap(get_data, zip(data_model, repeat(rows)))
                for result in results:
                    cov_label += result['cov']
                    media_label += result['mean']
                    cont_label += result['cont']
        cov_label /= cont_label
        media_label /= cont_label
        cov += (cov_label - vector_cov(media_label))
        w_cov += label_distances[i] * (cov_label - vector_cov(media_label))
        media.append(media_label)

    pool.close()

    media = np.array(media)
    S_b = np.zeros((np.prod(conv_window), np.prod(conv_window)))
    wS_b = np.zeros((np.prod(conv_window), np.prod(conv_window)))
    c_b2 = 0
    c_b = 0
    # m_media = np.mean(media, axis=0)
    for i in range(nlabels):
        m = media[i]
        for j in range(i+1, nlabels):
            m2 = media[j]
            diff = m - m2
            factor = np.sqrt(diff @ diff.T)
            wS_b += factor * np.outer(diff, diff)
            c_b2 += factor
            S_b += np.outer(diff, diff)
            c_b += 1

    output = {'S_w': vector_2_mat(cov / nlabels), 'wS_w': vector_2_mat(w_cov / label_distances.sum()),
              'S_b': S_b / c_b, 'wS_b': wS_b / c_b2}

    return output


def __get_label_distances(fpnn, data, label, batch_size, flips):
    print('flips', len(flips))
    unique_labels = np.unique(label)
    nlabels = len(unique_labels)
    feed_dict = fpnn.get_feed_dict(data=None, label=None, is_training=False, backprop_train=False)
    layer_shape = fpnn.model[-1]['operation'].get_shape().as_list()[1:]
    w_s = np.zeros(nlabels)

    media = np.zeros([nlabels] + layer_shape)

    for i in range(nlabels):
        data_label = data[label == unique_labels[i]]
        ndata_label = len(data_label)
        index = 0
        while index < ndata_label:
            new_index = min(ndata_label, index + batch_size)
            data_batch = []
            for d in data_label[index:new_index]:
                for flip in flips:
                    if flip == 'normal':
                        data_batch.append(d)
                    elif flip == 'left_right':
                        data_batch.append(np.fliplr(d))
                    elif flip == 'up_down':
                        data_batch.append(np.flipud(d))
            data_batch = np.array(data_batch)
            feed_dict[fpnn.model[0]['operation']] = data_batch
            eval_data = fpnn.model[-1]['operation'].eval(feed_dict=feed_dict)
            media[i] += np.sum(eval_data, axis=0)
            index = new_index
        media[i] /= (ndata_label * len(flips))

    for i in range(nlabels):
        for j in range(nlabels):
            if i == j:
                continue
            diff = (media[i] - media[j]).ravel()
            w_s[i] += np.sqrt(diff @ diff.T)
        w_s[i] = 1.0 / w_s[i]
    return w_s


def __get_LDA_basis(S_w, S_b, opts):
    sigma = 2e-2
    if 'sigma' in opts:
        sigma = opts['sigma']
    M = np.linalg.pinv((1.0 - sigma)*S_w + sigma*np.trace(S_w)*np.eye(len(S_w))).dot(S_b)
    dim = np.linalg.matrix_rank(M)
    _, d, V = np.linalg.svd(M, full_matrices=True)
    d = np.real(d)
    return {
        'd': d[:dim].tolist(),
        'V': np.real(V[:dim]).tolist()
    }


def __get_PCA_basis(S_w, opts):
    sigma = 0 #1e-3
    if 'sigma' in opts:
        sigma = opts['sigma']
    _, d, V = np.linalg.svd(S_w + sigma*np.eye(len(S_w)), full_matrices=True)
    d = np.real(d)
    V = np.real(V)
    return {
        'd': d.tolist(),
        'V': np.real(V).tolist()
    }


def __get_LDA_direct_basis(S_w, S_b, opts):
    sigma = 0  # 1e-3
    if 'sigma' in opts:
        sigma = opts['sigma']
    _, ds_Sb, vs_Sb = np.linalg.svd(S_b)
    m_dim = np.linalg.matrix_rank(S_b)
    for i in range(1, len(ds_Sb)):
        ds_Sb[i] += ds_Sb[i - 1]
    vs_Sb = vs_Sb[:m_dim]
    ds_Sb = ds_Sb[:m_dim]
    print(ds_Sb)

    z = vs_Sb.T @ np.diag(1.0 / np.sqrt(ds_Sb))
    z_Sw = z.T @ (S_w + sigma*np.eye(len(S_w))) @ z
    _, ds_Sw, vs_Sw = np.linalg.svd(z_Sw)
    vs_dlda = np.diag(1.0 / np.sqrt(ds_Sw)) @ vs_Sw @ z.T

    for v in range(len(vs_dlda)):
        vs_dlda[v] /= np.linalg.norm(vs_dlda[v])

    return {
        'd': ds_Sw.tolist(),
        'V': np.real(vs_dlda).tolist()
    }


def __get_weights(fpnn, basis, opts, eps=1e-1):
    thresh = 1
    W, thresh = __recursive_get_weights(fpnn, basis, opts, eps, thresh)
    if thresh > 0:
        new_W = W
        while new_W is not None:
            thresh -= 1e-2
            W = new_W
            new_W, thresh = __recursive_get_weights(fpnn, basis, opts, eps, thresh)
    return W


def __recursive_get_weights(fpnn, basis, opts, eps=1e-2, thresh=0.5):
    layer_shape = fpnn.model[-1]['operation'].get_shape().as_list()[1:]
    conv_window = opts['shape'][:-1] + layer_shape[-1:]
    nchannels = opts['shape'][-1]

    d = np.array(basis['d'])
    d[d < 0] = 0
    V = np.array(basis['V'])

    W = np.zeros((nchannels, int(np.prod(conv_window))))
    m_corr = 0.0

    d_factor = np.reshape(d, [len(d), 1])

    nchannels_2 = int(nchannels/2)
    ndir = min(V.shape[0], nchannels_2)

    niters = 20000
    #W[:ndir] = V[:ndir]
    #W[ndir:2*ndir] = -1.0*V[:ndir]
    for i in range(nchannels):#range(2*V.shape[0], nchannels):
        found = False
        for _ in range(niters):
            sign = np.random.randn(len(d), 1)
            sign[sign < -eps] = -1
            sign[sign > eps] = 1
            sign[(-1 < sign) & (sign < 1)] = 0
            r_factor = sign * np.random.rand(len(d), 1) # * d_factor
            r_factor /= np.sqrt(np.dot(r_factor.T, r_factor))
            new_w = np.sum(r_factor * V, axis=0)
            new_w /= np.sqrt(np.dot(new_w, new_w.T))
            max_corr = np.max(np.dot(W, new_w))
            if max_corr <= thresh:
                m_corr = max(m_corr, max_corr)
                W[i] = new_w
                i += 1
                found = True
                break
        if not found:
            print('NOT Found weights for thresh =', thresh)
            return None, None

    print('Found weights for thresh =', thresh)

    if len(conv_window) == 3:
        W = np.reshape(W, [nchannels] + conv_window)
    W = np.swapaxes(W, -1, 0)

    return W, m_corr


def get_data(X, rows, ntries=1, percentage=1.0, only_mean=False):
    ntrain = int(len(X) * percentage)

    output = {}
    mean = np.zeros(rows.shape[-1])
    if not only_mean:
        scatter = np.zeros(int(rows.shape[-1] * (rows.shape[-1] + 1) / 2.0))
        for _ in range(ntries):
            pos = np.random.permutation(len(X))[:ntrain]
            for data in X[pos]:
                for row in rows:
                    mean += data.flat[row]
                    outer_sum(data.flat[row], scatter)
        output['cov'] = scatter
    else:
        for _ in range(ntries):
            pos = np.random.permutation(len(X))[:ntrain]
            for data in X[pos]:
                for row in rows:
                    mean += data.flat[row]
    output['mean'] = mean
    output['cont'] = ntrain * len(rows) * ntries
    return output
