import numpy as np
import os
from python.train.vector_utils import get_index_2d, vector_cov, vector_2_mat
from python.train.cython.cutils import outer_sum, outer_sum_with_factor
from multiprocessing import Pool
from itertools import repeat
from scipy.spatial.distance import cdist
from sklearn.neighbors import KernelDensity
from math import pi


def get_kde(X, xspace, kernel='gaussian', bandwidth=1.0):
    n_kdes = np.zeros(X.shape[-1])
    kdes = []
    for i in range(n_kdes):
        if len(X.shape) == 2:
            data = X[:, i].flatten()
        else:
            data = X[:, :, :, i].flatten()
        kde = KernelDensity(kernel=kernel, bandwidth=bandwidth).fit(data)
        kdes.append(np.exp(kde.score_samples(xspace)))
    return np.array(kdes)


def get_data(X, rows, ntries=1, percentage=1.0, only_mean=False):
    ntrain = int(len(X) * percentage)

    output = {}
    mean = np.zeros(rows.shape[-1])
    if not only_mean:
        scatter = np.zeros(int(rows.shape[-1] * (rows.shape[-1] + 1) / 2.0))
        for _ in range(ntries):
            pos = np.random.permutation(len(X))[:ntrain]
            for data in X[pos]:
                for row in rows:
                    mean += data.flat[row]
                    outer_sum(data.flat[row], scatter)
        output['cov'] = scatter
    else:
        for _ in range(ntries):
            pos = np.random.permutation(len(X))[:ntrain]
            for data in X[pos]:
                for row in rows:
                    mean += data.flat[row]
    output['mean'] = mean
    output['cont'] = ntrain * len(rows) * ntries
    return output


def get_distance(i_data, j_data, metric="euclidean"):
    return cdist(i_data, j_data, metric=metric)


class ClassificationTraining:

    def __init__(self):
        self.ds = None
        self.vs = None
        self.W = None
        self.omega = None

    def get_weights(self, model, feed_dict, opts, data, label, train_opts, augmented_label=None):
        self.ds = []
        self.vs = []
        batch_size = 50
        if 'batch_size' in opts:
            batch_size = opts['batch_size']
        repetitions = 1
        if 'repetitions' in opts:
            repetitions = opts['repetitions']

        max_dim = int(opts['shape'][-1])
        duplicate = True
        if 'duplicate' in opts:
            duplicate = opts['duplicate']
        if duplicate:
            max_dim = int(round(max_dim / 2))

        print('max_dim', max_dim)

        aug_label=None
        if augmented_label is not None:
            aug_label=augmented_label

        np_matrix = None
        kissme_matrix = None
        matrices = self.__get_Sw_Sb(
            model, feed_dict, opts, data, label, aug_label, batch_size,
            train_opts, repetitions, all_info=False
        )

        # max_dim = min(max_dim, int(nlabels * 2 / 3))
        train_options = [{'technique': 'lda', 'max_dim': max_dim}]
                          #{'technique': 'direct-lda', 'max_dim': max_dim},
                          #{'technique': 'weighted-lda', 'max_dim': max_dim},
                          #{'technique': 'weighted-direct-lda', 'max_dim': max_dim}]

        if 'train' in opts:
            train_options = opts['train']

        # if len(opts['shape']) == 2:
        #     train_options += [{'technique': '1vs1'}]
        #     kissme_matrix = self.__get_Sp_Sn(model, feed_dict, opts, data, label, batch_size, train_opts, repetitions)
        #     # np_matrix = self.__get_nonparametric_Sb(model, feed_dict, opts, data, label, batch_size, train_opts)
        #     np_dim = int(opts['shape'][-1] / 2)
        #     if duplicate:
        #         np_dim = int(round(np_dim / 2))
        #     # train_options.append({'technique': 'np-lda', 'max_dim': np_dim})
        #     # train_options.append({'technique': 'weighted-np-lda', 'max_dim': np_dim})
        #     train_options.append({'technique': 'kissme', 'max_dim': np_dim})

        #train_options += [{'technique': 'rda', 'max_dim': 2*max_dim}]
        #train_options += [{'technique': 'pca'}]

        for train_opt in train_options:
            if train_opt['technique'] == 'lda':
                self.__get_LDA_weights(matrices['S_w'], matrices['S_b'], train_opt)
            elif train_opt['technique'] == 'direct-lda':
                self.__get_LDA_direct_weights(matrices['S_w'], matrices['S_b'], train_opt)
            elif train_opt['technique'] == 'weighted-lda':
                self.__get_LDA_weights(matrices['wS_w'], matrices['wS_b'], train_opt)
            elif train_opt['technique'] == 'weighted-direct-lda':
                self.__get_LDA_direct_weights(matrices['wS_w'], matrices['wS_b'], train_opt)
            elif train_opt['technique'] == 'rda':
                self.__get_PCA_weights(matrices['S_w'], train_opt)
            elif train_opt['technique'] == 'pca':
                self.__get_PCA_weights(matrices['S_w'] + matrices['S_b'], train_opt)
            elif train_opt['technique'] == 'np-lda':
                self.__get_LDA_weights(matrices['S_w'], np_matrix['S_b'], train_opt)
            elif train_opt['technique'] == 'weighted-np-lda':
                self.__get_LDA_weights(matrices['wS_w'], np_matrix['S_b'], train_opt)
            elif train_opt['technique'] == 'kissme':
                self.__get_KISSME_weights(kissme_matrix['S_p'], kissme_matrix['S_n'], train_opt)
            elif train_opt['technique'] == '1vs1':
                for i in range(len(matrices['1vs1']['covs']) - 1):
                    for j in range(i+1, len(matrices['1vs1']['covs'])):
                        print('1vs1', i, j)
                        S_w = vector_2_mat(matrices['1vs1']['covs'][i] + matrices['1vs1']['covs'][j])
                        wS_w = vector_2_mat(matrices['1vs1']['dists'][i] * matrices['1vs1']['covs'][i] +
                                            matrices['1vs1']['dists'][j] * matrices['1vs1']['covs'][j])
                        diff = matrices['1vs1']['means'][i] - matrices['1vs1']['means'][j]
                        factor = diff @ diff.T
                        wS_b = factor * np.outer(diff, diff)
                        S_b = np.outer(diff, diff)
                        self.__get_LDA_weights(S_w, S_b, train_opt)
                        self.__get_LDA_direct_weights(S_w, S_b, train_opt)
                        self.__get_LDA_weights(wS_w, wS_b, train_opt)
                        self.__get_LDA_direct_weights(wS_w, wS_b, train_opt)

        self.__get_weights(opts)
        return self.W, self.omega

    def get_moments(self, model, feed_dict, opts, data, label, train_opts):
        feed_dict[model[0]['operation']] = data[:1]
        out = model[-1]['operation'].eval(feed_dict)
        layer_shape = out.shape[-1]

        batch_size = 128
        if 'batch_size' in opts:
            batch_size = opts['batch_size']
        repetitions = 1
        if 'repetitions' in opts:
            repetitions = opts['repetitions']

        mean_data = np.zeros(layer_shape)
        mean2_data = np.zeros(layer_shape)
        cont_data = 0

        flips = ['normal']
        if 'flips' in train_opts:
            flips += train_opts['flips']

        print('flips', len(flips))
        perm = np.arange(len(data))
        for rep in range(repetitions):
            index = 0
            np.random.shuffle(perm)
            data = data[perm]
            label = label[perm]
            while index < len(data):
                new_index = min(len(data), index + batch_size)
                for flip in flips:
                    data_batch = []
                    for d in data[index:new_index]:
                        if flip == 'normal':
                            data_batch.append(d)
                        elif flip == 'left_right':
                            data_batch.append(np.fliplr(d))
                        elif flip == 'up_down':
                            data_batch.append(np.flipud(d))
                    data_batch = np.array(data_batch)
                    feed_dict[model[0]['operation']] = data_batch
                    out = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)
                    out_2 = out * out
                    for i in range(0, layer_shape):
                        if len(out.shape) > 2:
                            mean_data[i] += np.sum(out[:,:,:,i])
                            mean2_data[i] += np.sum(out_2[:,:,:,i])
                        else:
                            mean_data[i] += np.sum(out[:, i])
                            mean2_data[i] += np.sum(out_2[:, i])
                    cont_data += np.prod(out.shape[:-1])
                index = new_index
        mean2_data /= cont_data
        mean_data /= cont_data
        desv = np.sqrt(mean2_data - mean_data * mean_data)
        return mean_data, desv

    def get_kde_mean(self, model, feed_dict, opts, data, label, train_opts):
        feed_dict[model[0]['operation']] = data[:1]
        out = model[-1]['operation'].eval(feed_dict)
        layer_shape = out.shape[-1]

        batch_size = 50
        if 'batch_size' in opts:
            batch_size = opts['batch_size']
        repetitions = 1
        if 'repetitions' in opts:
            repetitions = opts['repetitions']
        kde_points = 1000
        if 'kde_points' in opts:
            kde_points = opts['kde_points']
        bandwidth_points = 60
        if 'bandwidth_points' in opts:
            bandwidth_points = opts['bandwidth_points']
        kernel = 'gaussian'
        if 'kernel' in opts:
            kernel = opts['kernel']
        bandwidth = 0.2
        if 'bandwidth' in opts:
            bandwidth = opts['bandwidth']

        mean_data = np.zeros(layer_shape)

        flips = ['normal']
        if 'flips' in train_opts:
            flips += train_opts['flips']

        ulabels = np.unique(label)

        print('flips', len(flips))
        min_val = 10e10*np.ones(layer_shape)
        max_val = -10e10*np.ones(layer_shape)

        print('Establishing edges')
        for rep in range(repetitions):
            index = 0
            while index < len(data):
                new_index = min(len(data), index + batch_size)
                for flip in flips:
                    data_batch = []
                    for d in data[index:new_index]:
                        if flip == 'normal':
                            data_batch.append(d)
                        elif flip == 'left_right':
                            data_batch.append(np.fliplr(d))
                        elif flip == 'up_down':
                            data_batch.append(np.flipud(d))
                    data_batch = np.array(data_batch)
                    feed_dict[model[0]['operation']] = data_batch
                    out = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)
                    for i in range(0, layer_shape):
                        if len(out.shape) > 2:
                            min_val[i] = np.min([min_val[i], np.min(out[:, :, :, i].flatten())])
                            max_val[i] = np.max([max_val[i], np.max(out[:, :, :, i].flatten())])
                        else:
                            min_val[i] = np.min([min_val[i], np.min(out[:, i].flatten())])
                            max_val[i] = np.max([max_val[i], np.max(out[:, i].flatten())])
                index = new_index

        xspaces = []
        for i in range(0, layer_shape):
            xspaces.append(np.linspace(min_val[i], max_val[i], kde_points))
        xspaces = np.array(xspaces)
        label_frequencies = []

        norm_dist = 0.5/pi * np.exp(-0.5*np.linspace(-2.575, 2.575, 2*bandwidth_points+1)*np.linspace(
            -2.575, 2.575, 2*bandwidth_points+1))

        print('computing pdfs')
        for ulabel in ulabels:
            print('label', ulabel)
            frenquencies = np.zeros(shape=xspaces.shape)
            cont = 0
            data_label = data[label == ulabel]
            for rep in range(repetitions):
                index = 0
                while index < len(data_label):
                    new_index = min(len(data_label), index + batch_size)
                    for flip in flips:
                        data_batch = []
                        for d in data_label[index:new_index]:
                            if flip == 'normal':
                                data_batch.append(d)
                            elif flip == 'left_right':
                                data_batch.append(np.fliplr(d))
                            elif flip == 'up_down':
                                data_batch.append(np.flipud(d))
                        data_batch = np.array(data_batch)
                        feed_dict[model[0]['operation']] = data_batch
                        out = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)
                        lout = np.prod(out.shape[:-1])
                        cont += lout
                        for i in range(0, layer_shape):
                            if len(out.shape) > 2:
                                channel_data = np.around(
                                    (out[:, :, :, i].flatten() - min_val[i]) / (max_val[i] - min_val[i]) * (kde_points - 1)
                                ).astype(int)
                            else:
                                channel_data = np.around(
                                    (out[:, i].flatten() - min_val[i]) / (max_val[i] - min_val[i]) * (kde_points - 1)
                                ).astype(int)
                            for j in np.unique(channel_data):
                                nj = np.sum(channel_data == j)
                                frenquencies[i, max([0, j - bandwidth_points]):min(kde_points, j+bandwidth_points+1)] += \
                                    nj * norm_dist[max(0, bandwidth_points - j):min(
                                        kde_points - j + bandwidth_points, 2*bandwidth_points+1
                                    )]
                    index = new_index
            frenquencies /= cont
            label_frequencies.append(frenquencies)
        label_frequencies = np.array(label_frequencies) + 1e-8
        label_frequencies = np.transpose(label_frequencies, (1, 0, 2))
        log_label_frequencies = np.log(label_frequencies)

        print('computing entropy')
        for i in range(0, layer_shape):
            factor = np.sum(-1.0 * label_frequencies[i] * log_label_frequencies[i], axis=0)
            pos = np.argmax(factor)
            mean_data[i] = xspaces[i, pos]
            
        print('min_val', min_val)
        print('max_val', max_val)

        return mean_data

    def get_moments_2(self, model, feed_dict, opts, data, label, train_opts):
        feed_dict[model[0]['operation']] = data[:1]
        out = model[-1]['operation'].eval(feed_dict)
        layer_shape = out.shape[-1]

        mean_kde = self.get_kde_mean(model, feed_dict, opts, data, label, train_opts)

        batch_size = 50
        if 'batch_size' in opts:
            batch_size = opts['batch_size']
        repetitions = 1
        if 'repetitions' in opts:
            repetitions = opts['repetitions']

        mean_data = np.zeros(layer_shape)
        min_data = 10e10*np.ones(layer_shape)
        max_data = -10e10*np.ones(layer_shape)
        mean2_data = np.zeros(layer_shape)
        cont_data = 0

        flips = ['normal']
        if 'flips' in train_opts:
            flips += train_opts['flips']

        print('flips', len(flips))

        for rep in range(repetitions):
            index = 0
            while index < len(data):
                new_index = min(len(data), index + batch_size)
                for flip in flips:
                    data_batch = []
                    for d in data[index:new_index]:
                        if flip == 'normal':
                            data_batch.append(d)
                        elif flip == 'left_right':
                            data_batch.append(np.fliplr(d))
                        elif flip == 'up_down':
                            data_batch.append(np.flipud(d))
                    data_batch = np.array(data_batch)
                    feed_dict[model[0]['operation']] = data_batch
                    out = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float) - mean_kde
                    out_2 = out * out
                    for i in range(0, layer_shape):
                        if len(out.shape) > 2:
                            mean_data[i] += np.sum(out[:,:,:,i])
                            mean2_data[i] += np.sum(out_2[:,:,:,i])
                        else:
                            min_data[i] = np.min([min_data[i], np.min(out[:, i].flatten())])
                            max_data[i] = np.max([max_data[i], np.max(out[:, i].flatten())])
                            mean_data[i] += np.sum(out[:, i])
                            mean2_data[i] += np.sum(out_2[:, i])
                    cont_data += np.prod(out.shape[:-1])
                index = new_index
        mean2_data /= cont_data
        mean_data /= cont_data
        print('cont_data', cont_data)
        print('mean', mean_data)
        print('mean_kde', mean_kde)
        print('min_data', min_data)
        print('max_data', max_data)
        desv = np.sqrt(mean2_data)
        print('desv', desv)
        return mean_kde, desv

    def __get_Sw_Sb(self, model, feed_dict, opts, data, real_label, augmented_label, b_size, train_opts, repetitions=1, all_info=False):

        label = real_label
        if augmented_label is not None:
            label = augmented_label[0]

        flips = ['normal']
        if 'flips' in train_opts:
            flips += train_opts['flips']

        batch_size = int(b_size / len(flips))

        label_distances = self.__get_label_distances(model, feed_dict, data, label, batch_size, flips)

        unique_labels = np.unique(label)
        nlabels = len(unique_labels)
        conv_window = opts['shape']
        feed_dict[model[0]['operation']] = data[:1]
        out = model[-1]['operation'].eval(feed_dict=feed_dict)
        layer_shape = out.shape[1:]
        rows = get_index_2d(layer_shape, conv_window, train_opts)
        cov = np.zeros((int((np.prod(conv_window[:-1]) + 1) * np.prod(conv_window[:-1]) / 2.0)))
        w_cov = np.zeros((int((np.prod(conv_window[:-1]) + 1) * np.prod(conv_window[:-1]) / 2.0)))
        media = []

        nproc = os.cpu_count() or 1
        pool = Pool(nproc)

        print('flips', len(flips))

        if all_info:
            covs = []

        for i in range(nlabels):
            print('label', i)
            data_label = data[label == unique_labels[i]]
            ndata_label = len(data_label)

            cov_label = np.zeros((int((np.prod(conv_window[:-1]) + 1) * np.prod(conv_window[:-1]) / 2.0)))
            media_label = np.zeros((int(np.prod(conv_window[:-1]))))
            cont_label = 0
            for _ in range(repetitions):
                index = 0
                while index < ndata_label:
                    data_model = []
                    for i in range(nproc):
                        new_index = min(ndata_label, index + batch_size)
                        data_batch = []
                        for d in data_label[index:new_index]:
                            for flip in flips:
                                if flip == 'normal':
                                    data_batch.append(d)
                                elif flip == 'left_right':
                                    data_batch.append(np.fliplr(d))
                                elif flip == 'up_down':
                                    data_batch.append(np.flipud(d))
                        data_batch = np.array(data_batch)
                        feed_dict[model[0]['operation']] = data_batch
                        data_model.append(model[-1]['operation'].eval(feed_dict=feed_dict).astype(float))
                        index = new_index
                        if new_index == ndata_label:
                            break
                    results = pool.starmap(get_data, zip(data_model, repeat(rows)))
                    for result in results:
                        cov_label += result['cov']
                        media_label += result['mean']
                        cont_label += result['cont']
            cov_label /= cont_label
            media_label /= cont_label
            cov += (cov_label - vector_cov(media_label))
            w_cov += label_distances[i] * (cov_label - vector_cov(media_label))
            media.append(media_label)
            if all_info:
                covs.append(cov_label - vector_cov(media_label))

        pool.close()

        media = np.array(media)
        S_b = np.zeros((np.prod(conv_window[:-1]), np.prod(conv_window[:-1])))
        wS_b = np.zeros((np.prod(conv_window[:-1]), np.prod(conv_window[:-1])))
        c_b2 = 0
        c_b = 0
        # m_media = np.mean(media, axis=0)
        for i in range(nlabels):
            m = media[i]
            for j in range(i+1, nlabels):
                m2 = media[j]
                diff = m - m2
                factor = np.sqrt(diff @ diff.T)
                wS_b += factor * np.outer(diff, diff)
                c_b2 += factor
                S_b += np.outer(diff, diff)
                c_b += 1

        output = {'S_w': vector_2_mat(cov / nlabels), 'wS_w': vector_2_mat(w_cov / label_distances.sum()),
                  'S_b': S_b / c_b, 'wS_b': wS_b / c_b2}

        if all_info:
            output['1vs1'] = {'covs': np.array(covs), 'means': media, 'dists': label_distances}

        return output

    def __get_Sp_Sn(self, model, feed_dict, opts, data, label, batch_size, train_opts, repetitions=1):
        unique_labels = np.unique(label)
        nlabels = len(unique_labels)
        conv_window = opts['shape']
        feed_dict[model[0]['operation']] = data[:1]
        out = model[-1]['operation'].eval(feed_dict=feed_dict)
        layer_shape = out.shape[1:]
        rows = get_index_2d(layer_shape, conv_window, train_opts)
        cov_p = np.zeros((int((np.prod(conv_window[:-1]) + 1) * np.prod(conv_window[:-1]) / 2.0)))
        cov_n = np.zeros((int((np.prod(conv_window[:-1]) + 1) * np.prod(conv_window[:-1]) / 2.0)))
        cont_p = 0
        cont_n = 0

        positive_evaluations = 0.01
        if 'positive_evaluations' in opts:
            positive_evaluations = opts['positive_evaluations']
        negative_evaluations = positive_evaluations/(nlabels - 1.0)
        if 'negative_evaluations' in opts:
            negative_evaluations = opts['negative_evaluations']

        nproc = os.cpu_count() or 1
        pool = Pool(nproc)

        for i in range(nlabels):
            print('label', i)
            pos_labels = np.where(label == unique_labels[i])[0]
            neg_labels = np.where(label != unique_labels[i])[0]
            npos_labels = len(pos_labels)
            npos_comparisons = int(npos_labels*positive_evaluations)
            nneg_labels = len(neg_labels)
            nneg_comparisons = int(nneg_labels*negative_evaluations)
            for _ in range(repetitions):
                index = 0
                while index < npos_labels:
                    print('index', index)
                    new_index = min(npos_labels, index + batch_size)
                    feed_dict[model[0]['operation']] = data[pos_labels[index:new_index]]
                    batch_data = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)

                    data_model = []
                    data_aux = []
                    data_feed = []
                    d_index = 0
                    comparisons = 0
                    np.random.shuffle(pos_labels)
                    while d_index < len(batch_data):
                        while len(data_model) < nproc:
                            while True:
                                bsize = min(batch_size, batch_size - len(data_feed))
                                new_comparisons = min(npos_comparisons, comparisons + bsize)
                                data_aux += data[pos_labels[comparisons:new_comparisons]].tolist()
                                comparisons = new_comparisons
                                if comparisons == npos_comparisons:
                                    feed_dict[model[0]['operation']] = np.array(data_aux)
                                    data_feed += (batch_data[d_index] - model[-1]['operation'].eval(
                                        feed_dict=feed_dict).astype(float)
                                    ).tolist()
                                    data_aux = []
                                    comparisons = 0
                                    d_index += 1
                                    np.random.shuffle(pos_labels)
                                if d_index == len(batch_data) or len(data_feed) + len(data_aux) == batch_size:
                                    if len(data_aux):
                                        feed_dict[model[0]['operation']] = np.array(data_aux)
                                        data_feed += (batch_data[d_index] - model[-1]['operation'].eval(
                                            feed_dict=feed_dict).astype(float)
                                        ).tolist()
                                    data_model.append(np.array(data_feed))
                                    data_feed = []
                                    data_aux = []
                                    break
                            if d_index == len(batch_data):
                                break
                        results = pool.starmap(get_data, zip(data_model, repeat(rows)))
                        for result in results:
                            cov_p += result['cov']
                            cont_p += result['cont']
                        data_model = []

                    data_model = []
                    data_aux = []
                    d_index = 0
                    comparisons = 0
                    np.random.shuffle(neg_labels)
                    while d_index < len(batch_data):
                        while len(data_model) < nproc:
                            while True:
                                bsize = min(batch_size, batch_size - len(data_feed))
                                new_comparisons = min(nneg_comparisons, comparisons + bsize)
                                data_aux += data[neg_labels[comparisons:new_comparisons]].tolist()
                                comparisons = new_comparisons
                                if comparisons == nneg_comparisons:
                                    feed_dict[model[0]['operation']] = np.array(data_aux)
                                    data_feed += (batch_data[d_index] - model[-1]['operation'].eval(
                                        feed_dict=feed_dict).astype(float)
                                    ).tolist()
                                    data_aux = []
                                    comparisons = 0
                                    d_index += 1
                                    np.random.shuffle(neg_labels)
                                if d_index == len(batch_data) or len(data_feed) + len(data_aux) == batch_size:
                                    if len(data_aux):
                                        feed_dict[model[0]['operation']] = np.array(data_aux)
                                        data_feed += (batch_data[d_index] - model[-1]['operation'].eval(
                                            feed_dict=feed_dict).astype(float)
                                        ).tolist()
                                    data_model.append(np.array(data_feed))
                                    data_feed = []
                                    data_aux = []
                                    break
                            if d_index == len(batch_data):
                                break
                        results = pool.starmap(get_data, zip(data_model, repeat(rows)))
                        for result in results:
                            cov_n += result['cov']
                            cont_n += result['cont']
                        data_model = []
                    index = new_index

        return {'S_p': vector_2_mat(cov_p / cont_n), 'S_n': vector_2_mat(cov_n / cont_n)}

    def __get_nonparametric_Sb(self, model, feed_dict, opts, data, label, batch_size, train_opts):
        k = 4
        if 'k' in opts:
            k = opts['k']

        unique_labels = np.unique(label)
        nlabels = len(unique_labels)

        nn_indexes = np.zeros((nlabels-1, len(data), k), dtype=int)
        label_distances = np.zeros((len(data), nlabels-1))

        feed_dict[model[0]['operation']] = data[:1]
        out = model[-1]['operation'].eval(feed_dict=feed_dict)
        layer_shape = out.shape[1:]

        l_indexes = {}
        for i in range(nlabels):
            l_indexes[i] = np.where(label == i)[0]

        nproc = os.cpu_count() or 1
        pool = Pool(nproc)

        for i in range(nlabels):
            print('label', i)
            i_indexes = l_indexes[i]
            i_index = 0
            while i_index < len(i_indexes):
                new_i_index = min(len(i_indexes), i_index + batch_size)
                feed_dict[model[0]['operation']] = data[i_indexes[i_index:new_i_index]]
                i_data = np.reshape(model[-1]['operation'].eval(feed_dict=feed_dict), [-1, np.prod(layer_shape)])
                l_index = 0
                for j in range(nlabels):
                    if j==i:
                        continue
                    init_index = 0
                    j_index = 0
                    j_indexes = l_indexes[j]
                    distances = np.zeros((len(i_data), len(j_indexes)))
                    while j_index < len(j_indexes):
                        data_model = []
                        for pr in range(nproc):
                            if j_index == len(j_indexes):
                                break
                            new_j_index = min(len(j_indexes), j_index + batch_size)
                            feed_dict[model[0]['operation']] = data[j_indexes[j_index:new_j_index]]
                            data_model.append(np.reshape(model[-1]['operation'].eval(
                                feed_dict=feed_dict
                            ), [-1, np.prod(layer_shape)]))
                            j_index = new_j_index
                        results = pool.starmap(get_distance, zip(repeat(i_data), data_model))
                        for result in results:
                            distances[:, init_index:init_index + result.shape[1]] = result
                            init_index = init_index + result.shape[1]

                    nn_indexes[l_index, i_indexes[i_index:new_i_index], :] = np.argsort(distances)[:, :k]
                    label_distances[i_indexes[i_index:new_i_index], l_index] = np.amin(distances, axis=1, keepdims=False)
                    l_index += 1
                i_index = new_i_index

        i_index = 0
        cont = 0
        conv_window = opts['shape']
        rows = get_index_2d(layer_shape, conv_window, train_opts)
        cov = np.zeros((int((rows.shape[-1] + 1) * rows.shape[-1] / 2.0)))
        while i_index < len(data):
            print(i_index, 'of', len(data))
            new_i_index = min(len(data), i_index + batch_size)
            feed_dict[model[0]['operation']] = data[i_index:new_i_index]
            i_data = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)
            for i in range(len(i_data)):
                w = np.amax(label_distances[i_index + i, :]) / np.sum(label_distances[i_index + i, :])
                nn_index = nn_indexes[:, i, :].flat[:]
                feed_dict[model[0]['operation']] = data[nn_index]
                j_data = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)
                for j in range(nlabels - 1):
                    # mean_j_data = get_data(j_data[j * k:(j + 1) * k], rows, only_mean=True)
                    # mean_j_data['mean'] /= mean_j_data['cont']
                    mean_j_data = np.mean(j_data[j*k:(j+1)*k], axis=0)
                    # cov_i_data = get_data(i_data[i:i+1], rows)
                    # cov_i_data['cov'] /= cov_i_data['cont']
                    # cov += w*(cov_i_data['cov'] - vector_cov(mean_j_data['mean']))
                    diff = i_data[i].flat[:] - mean_j_data.flat[:]
                    outer_sum_with_factor(w, diff, cov)
                    cont += w
            i_index = new_i_index

        pool.close()

        return {'S_b': vector_2_mat(cov / cont)}

    def __get_nonparametric_Sb_random(self, model, feed_dict, opts, data, label, batch_size, train_opts):
        k = 4
        if 'k' in opts:
            k = opts['k']

        unique_labels = np.unique(label)
        nlabels = len(unique_labels)

        nn_indexes = np.zeros((nlabels-1, len(data), k), dtype=int)
        label_distances = np.zeros((len(data), nlabels-1))

        feed_dict[model[0]['operation']] = data[:1]
        out = model[-1]['operation'].eval(feed_dict=feed_dict)
        layer_shape = out.shape[1:]

        l_indexes = {}
        for i in range(nlabels):
            l_indexes[i] = np.where(label == i)[0]

        nproc = os.cpu_count() or 1
        pool = Pool(nproc)

        for i in range(nlabels):
            print('label', i)
            i_indexes = l_indexes[i]
            i_index = 0
            while i_index < len(i_indexes):
                new_i_index = min(len(i_indexes), i_index + batch_size)
                feed_dict[model[0]['operation']] = data[i_indexes[i_index:new_i_index]]
                i_data = np.reshape(model[-1]['operation'].eval(feed_dict=feed_dict), [-1, np.prod(layer_shape)])
                l_index = 0
                for j in range(nlabels):
                    if j==i:
                        continue
                    init_index = 0
                    j_index = 0
                    j_indexes = l_indexes[j]
                    distances = np.zeros((len(i_data), k))
                    np.random.shuffle(j_indexes)
                    while j_index < k:
                        data_model = []
                        for pr in range(nproc):
                            if j_index == k:
                                break
                            new_j_index = min(k, j_index + batch_size)
                            feed_dict[model[0]['operation']] = data[j_indexes[j_index:new_j_index]]
                            data_model.append(np.reshape(model[-1]['operation'].eval(
                                feed_dict=feed_dict
                            ), [-1, np.prod(layer_shape)]))
                            j_index = new_j_index
                        results = pool.starmap(get_distance, zip(repeat(i_data), data_model))
                        for result in results:
                            distances[:, init_index:init_index + result.shape[1]] = result
                            init_index = init_index + result.shape[1]

                    nn_indexes[l_index, i_indexes[i_index:new_i_index], :] = np.argsort(distances)[:, :k]
                    label_distances[i_indexes[i_index:new_i_index], l_index] = np.amin(distances, axis=1, keepdims=False)
                    l_index += 1
                i_index = new_i_index

        i_index = 0
        cont = 0
        conv_window = opts['shape']
        rows = get_index_2d(layer_shape, conv_window, train_opts)
        cov = np.zeros((int((rows.shape[-1] + 1) * rows.shape[-1] / 2.0)))
        while i_index < len(data):
            print(i_index, 'of', len(data))
            new_i_index = min(len(data), i_index + batch_size)
            feed_dict[model[0]['operation']] = data[i_index:new_i_index]
            i_data = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)
            for i in range(len(i_data)):
                w = np.amax(label_distances[i_index + i, :]) / np.sum(label_distances[i_index + i, :])
                nn_index = nn_indexes[:, i, :].flat[:]
                feed_dict[model[0]['operation']] = data[nn_index]
                j_data = model[-1]['operation'].eval(feed_dict=feed_dict).astype(float)
                for j in range(nlabels - 1):
                    mean_j_data = get_data(j_data[j * k:(j + 1) * k], rows, only_mean=True)
                    mean_j_data['mean'] /= mean_j_data['cont']
                    # mean_j_data = np.mean(j_data[j*k:(j+1)*k], axis=0)
                    cov_i_data = get_data(i_data[i:i+1], rows)
                    cov_i_data['cov'] /= cov_i_data['cont']
                    cov += w * (cov_i_data['cov'] - vector_cov(mean_j_data['mean']))
                    # diff = i_data[i].flat[:] - mean_j_data.flat[:]
                    # outer_sum_with_factor(w, diff, cov)
                    cont += w
            i_index = new_i_index

        pool.close()

        return {'S_b': vector_2_mat(cov / cont)}

    def __get_label_distances(self, model, feed_dict, data, label, batch_size, flips):
        print('flips', len(flips))
        unique_labels = np.unique(label)
        nlabels = len(unique_labels)
        feed_dict[model[0]['operation']] = data[:1]
        out = model[-1]['operation'].eval(feed_dict=feed_dict)
        layer_shape = list(out.shape[1:])
        w_s = np.zeros(nlabels)

        media = np.zeros([nlabels] + layer_shape)

        for i in range(nlabels):
            data_label = data[label == unique_labels[i]]
            ndata_label = len(data_label)
            index = 0
            while index < ndata_label:
                new_index = min(ndata_label, index + batch_size)
                data_batch = []
                for d in data_label[index:new_index]:
                    for flip in flips:
                        if flip == 'normal':
                            data_batch.append(d)
                        elif flip == 'left_right':
                            data_batch.append(np.fliplr(d))
                        elif flip == 'up_down':
                            data_batch.append(np.flipud(d))
                data_batch = np.array(data_batch)
                feed_dict[model[0]['operation']] = data_batch
                eval_data = model[-1]['operation'].eval(feed_dict=feed_dict)
                media[i] += np.sum(eval_data, axis=0)
                index = new_index
            media[i] /= (ndata_label * len(flips))

        for i in range(nlabels):
            for j in range(nlabels):
                if i == j:
                    continue
                diff = (media[i] - media[j]).ravel()
                w_s[i] += np.sqrt(diff @ diff.T)
            w_s[i] = 1.0 / w_s[i]
        return w_s

    def __get_LDA_weights(self, S_w, S_b, layer_info):
        sigma = 1e-4
        if 'sigma' in layer_info:
            sigma = layer_info['sigma']
        matrix = np.linalg.pinv(S_w + sigma*np.eye(len(S_w))).dot(S_b)
        m_dim = np.linalg.matrix_rank(matrix)
        ds_lda, vs_lda = np.linalg.eig(matrix)
        order = np.argsort(np.real(ds_lda))[::-1][:m_dim]
        ds_lda = np.real(ds_lda)[order]
        ds_lda = np.abs(ds_lda) / np.sum(np.abs(ds_lda))
        factor = 1.0 * ds_lda
        for i in range(1, len(ds_lda)):
            ds_lda[i] += ds_lda[i - 1]
        vs_lda = np.real(vs_lda)
        mi_dim = 0
        if 'min_dim' in layer_info:
            mi_dim = min(m_dim, layer_info['min_dim']) - 1
        ma_dim = m_dim - 1
        if 'max_dim' in layer_info:
            ma_dim = min(m_dim, layer_info['max_dim']) - 1
        thresh = 1.1
        if 'thresh' in layer_info:
            thresh = layer_info['thresh']
        n_thresh = min(max(ds_lda[mi_dim], thresh), ds_lda[ma_dim])

        vs_lda = vs_lda.T[order][ds_lda <= n_thresh]
        ds_lda = ds_lda[ds_lda <= n_thresh]
        print(ds_lda)
        factor = factor[:len(ds_lda)]

        self.ds += (1.0 * np.ones(shape=factor.shape)).tolist()
        self.vs += vs_lda.tolist()

        if len(ds_lda) < layer_info['max_dim']:
            n_lda = len(ds_lda)
            nvec = layer_info['max_dim'] - n_lda
            for _ in range(nvec):
                vec = np.zeros((vs_lda.shape[1]))
                for f, v in zip(factor, vs_lda):
                    nf = f*(np.random.randn()*0.2 + 1.0)
                    vec += nf * v
                vec /= (1.0/np.linalg.norm(vec))
                self.vs.append(vec.tolist())
            self.ds += (1.0 * np.ones((nvec))).tolist()
        #self.ds += factor.tolist()


    def __get_LDA_direct_weights(self, S_w, S_b, layer_info):
        sigma = 1e-4
        if 'sigma' in layer_info:
            sigma = layer_info['sigma']
        ds_Sb, vs_Sb = np.linalg.eig(S_b)
        m_dim = np.linalg.matrix_rank(S_b)
        print('matrix rank', m_dim)
        order = np.argsort(np.real(ds_Sb))[::-1][:m_dim]
        ds_Sb = np.real(ds_Sb)[order]
        ds_Sb = np.abs(ds_Sb) / np.sum(np.abs(ds_Sb))
        for i in range(1, len(ds_Sb)):
            ds_Sb[i] += ds_Sb[i - 1]
        vs_Sb = np.real(vs_Sb)
        mi_dim = 0
        if 'min_dim' in layer_info:
            mi_dim = min(m_dim, layer_info['min_dim']) - 1
        ma_dim = m_dim - 1
        if 'max_dim' in layer_info:
            ma_dim = min(m_dim, layer_info['max_dim']) - 1
        thresh = 1.1
        if 'thresh' in layer_info:
            thresh = layer_info['thresh']
        n_thresh = min(max(ds_Sb[mi_dim], thresh), ds_Sb[ma_dim])
        vs_Sb = vs_Sb.T[order][ds_Sb <= n_thresh].T
        ds_Sb = ds_Sb[ds_Sb <= n_thresh]
        print(ds_Sb)

        if len(ds_Sb):
            z = vs_Sb @ np.diag(1.0 / np.sqrt(ds_Sb))
            z_Sw = z.T @ (S_w + sigma*np.eye(len(S_w))) @ z
            ds_Sw, vs_Sw = np.linalg.eig(z_Sw)
            vs_dlda = np.diag(1.0 / np.sqrt(ds_Sw)) @ vs_Sw.T @ z.T
            order = np.argsort(1.0 / np.sqrt(ds_Sw))
            factor = np.abs(ds_Sw) / np.sum(np.abs(ds_Sw))
            # self.ds += (0.5 * factor).tolist()
            self.ds += (1.0 * np.ones(shape=factor.shape)).tolist()
            self.vs += vs_dlda[order].tolist()

    def __get_PCA_weights(self, S_w, layer_info):
        sigma = 0 #1e-3
        if 'sigma' in layer_info:
            sigma = layer_info['sigma']
        m_rank = np.linalg.matrix_rank(S_w + sigma*np.eye(len(S_w)))
        if 'max_dim' in layer_info:
            m_rank = min(m_rank, layer_info['max_dim'])
        print('matrix rank', m_rank)
        ds_pca, vs_pca = np.linalg.eig(S_w + sigma*np.eye(len(S_w)))
        order = np.argsort(np.real(ds_pca))[::-1][:m_rank]
        ds_pca = np.real(ds_pca)[order]
        ds_pca = np.abs(ds_pca) / np.sum(np.abs(ds_pca))
        # for i in range(1, len(ds_pca)):
        #     ds_pca[i] += ds_pca[i - 1]
        vs_pca = np.real(vs_pca)
        vs_pca = vs_pca.T[order]
        #self.ds += (0.25 * ds_pca).tolist()
        self.ds += (1.0 * np.ones(shape=ds_pca.shape)).tolist()
        self.vs += vs_pca.tolist()

    def __get_KISSME_weights(self, S_p, S_n, layer_info):
        zero = 1e-10
        M = np.linalg.pinv(S_p) - np.linalg.pinv(S_n)
        d_M, v_M = np.linalg.eig(M)
        if np.any(d_M < zero):
            d_M[d_M < zero] = zero
            M = v_M @ np.diag(d_M) @ v_M.T
            d_M, v_M = np.linalg.eig(M)

        order = np.argsort(np.real(d_M))[::-1][:np.linalg.matrix_rank(M)]
        d_M = np.real(d_M)[order]
        d_M = np.abs(d_M) / np.sum(np.abs(d_M))
        # for i in range(1, len(ds_pca)):
        #     ds_pca[i] += ds_pca[i - 1]
        v_M = np.real(v_M)
        v_M = v_M.T[order]
        self.ds += d_M.tolist()
        self.vs += v_M.tolist()

    def __get_weights(self, opts):
        conv_window = opts['shape']
        self.ds = np.array(self.ds)
        self.vs = np.array(self.vs)
        # order = np.argsort(self.ds)[::-1]
        # self.ds = self.ds[order]
        # self.vs = self.vs[order]
        self.W = np.zeros(conv_window)
        self.omega = np.zeros(conv_window[-1])
        cont = 0
        iter = 0

        duplicate = True
        if 'duplicate' in opts:
            duplicate = opts['duplicate']

        while cont < conv_window[-1]:
            for d, v in zip(self.ds, self.vs):
                print(d)
                if iter > 0:
                    z = v + 0.02*iter*np.random.randn(v.size)
                else:
                    z = v
                if len(conv_window) == 4:
                    self.W[:, :, :, cont] = np.reshape(z, conv_window[:-1])
                else:
                    self.W[:, cont] = z
                self.omega[cont] = d
                cont += 1
                if cont == conv_window[-1]:
                    break
                if duplicate:
                    if iter > 0:
                        z = -v + 0.02*iter*np.random.randn(v.size)
                    else:
                        z = -v
                    if len(conv_window) == 4:
                        self.W[:, :, :, cont] = np.reshape(z, conv_window[:-1])
                    else:
                        self.W[:, cont] = z
                    self.omega[cont] = d
                    cont += 1
                    if cont == conv_window[-1]:
                        break
            iter += 1

        if len(conv_window) == 4:
            norma = np.zeros((self.W.shape[-2]))
            cont = np.zeros((self.W.shape[-2]))
            for i in range(self.W.shape[-2]):
                print('dim', i)
                for c in range(self.W.shape[-1]):
                    norma[i] += self.omega[c] * np.linalg.norm(self.W[:, :, i, c].flatten())
                    cont[i] += self.omega[c]
                norma[i] /= cont[i]
            print(norma)
