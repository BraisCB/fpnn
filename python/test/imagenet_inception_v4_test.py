def conv2d_bn(nb_filter, num_row, num_col,
              padding='SAME', strides=[1, 1], use_bias=False):

    return {
        'type': 'block',
        'layers': [
            {
                'type': 'conv2d',
                'data': {
                    'shape': [num_row, num_col, nb_filter],
                    'strides': strides,
                    'padding': padding
                },
                'opts': {
                    'bias': use_bias
                },
                'regularization': [{'ord': 2, 'lambda': 0.00004}]
            },
            {
                'type': 'batch_normalization',
                'opts': {'decay': 0.9997}
            },
            {'type': 'relu'},
        ]
    }


def block_inception_a():
    return {
        'type': 'inception_block',
        'blocks': [
            conv2d_bn(96, 1, 1),
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(64, 1, 1),
                    conv2d_bn(96, 3, 3),
                ]
            },
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(64, 1, 1),
                    conv2d_bn(96, 3, 3),
                    conv2d_bn(96, 3, 3)
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {
                        'type': 'avg_pool',
                        'data': {
                            'ksize': [3, 3],
                            'strides': [1, 1],
                            'padding': 'SAME'
                        }
                    },
                    conv2d_bn(96, 1, 1)
                ]
            }
        ]
    }


def block_reduction_a():
    return {
        'type': 'inception_block',
        'blocks': [
            conv2d_bn(384, 3, 3, strides=[2, 2], padding='VALID'),
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(192, 1, 1),
                    conv2d_bn(224, 3, 3),
                    conv2d_bn(256, 3, 3, strides=[2, 2], padding='VALID')
                ]
            },
            {
                'type': 'max_pool',
                'data': {
                    'ksize': [3, 3],
                    'strides': [2, 2],
                    'padding': 'VALID'
                }
            }
        ]
    }


def block_inception_b():
    return {
        'type': 'inception_block',
        'blocks': [
            conv2d_bn(384, 1, 1),
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(192, 1, 1),
                    conv2d_bn(224, 1, 7),
                    conv2d_bn(256, 7, 1)
                ]
            },
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(192, 1, 1),
                    conv2d_bn(192, 7, 1),
                    conv2d_bn(224, 1, 7),
                    conv2d_bn(224, 7, 1),
                    conv2d_bn(256, 1, 7)
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {
                        'type': 'avg_pool',
                        'data': {
                            'ksize': [3, 3],
                            'strides': [1, 1],
                            'padding': 'SAME'
                        }
                    },
                    conv2d_bn(128, 1, 1)
                ]
            }
        ]
    }


def block_reduction_b():
    return {
        'type': 'inception_block',
        'blocks': [
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(192, 1, 1),
                    conv2d_bn(192, 3, 3, strides=[2, 2], padding='VALID')
                ]
            },
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(256, 1, 1),
                    conv2d_bn(256, 1, 7),
                    conv2d_bn(320, 7, 1),
                    conv2d_bn(320, 3, 3, strides=[2, 2], padding='VALID')
                ]
            },
            {
                'type': 'max_pool',
                'data': {
                    'ksize': [3, 3],
                    'strides': [2, 2],
                    'padding': 'VALID'
                }
            }
        ]
    }


def block_inception_c():
    return {
        'type': 'inception_block',
        'blocks': [
            conv2d_bn(256, 1, 1),
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(384, 1, 1),
                    {
                        'type': 'inception_block',
                        'layers': [
                            conv2d_bn(256, 1, 3),
                            conv2d_bn(256, 3, 1)
                        ]
                    }
                ]
            },
            {
                'type': 'block',
                'layers': [
                    conv2d_bn(384, 1, 1),
                    conv2d_bn(448, 3, 1),
                    conv2d_bn(512, 1, 3),
                    {
                        'type': 'inception_block',
                        'layers': [
                            conv2d_bn(256, 1, 3),
                            conv2d_bn(256, 3, 1)
                        ]
                    }
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {
                        'type': 'avg_pool',
                        'data': {
                            'ksize': [3, 3],
                            'strides': [1, 1],
                            'padding': 'SAME'
                        }
                    },
                    conv2d_bn(256, 1, 1)
                ]
            }
        ]
    }


def get_layers():
    layers = [
        conv2d_bn(32, 3, 3, strides=[2, 2], padding='VALID'),
        conv2d_bn(32, 3, 3, padding='VALID'),
        conv2d_bn(64, 3, 3),
        {
            'type': 'inception_block',
            'blocks': [
                {
                    'type': 'max_pool',
                    'data': {
                        'ksize': [3, 3],
                        'strides': [2, 2],
                        'padding': 'VALID'
                    }
                },
                conv2d_bn(96, 3, 3, strides=[2, 2], padding='VALID')
            ]
        },
        {
            'type': 'inception_block',
            'blocks': [
                {
                    'type': 'block',
                    'layers': [
                        conv2d_bn(64, 1, 1),
                        conv2d_bn(96, 3, 3, padding='valid'),
                    ]
                },
                {
                    'type': 'block',
                    'layers': [
                        conv2d_bn(64, 1, 1),
                        conv2d_bn(64, 1, 7),
                        conv2d_bn(64, 7, 1),
                        conv2d_bn(96, 3, 3, padding='VALID')
                    ]
                }
            ]
        },
        {
            'type': 'inception_block',
            'blocks': [
                conv2d_bn(192, 3, 3, strides=[2, 2], padding='VALID'),
                {
                    'type': 'max_pool',
                    'data': {
                        'ksize': [3, 3],
                        'strides': [2, 2],
                        'padding': 'VALID'
                    }
                }
            ]
        }
    ]

    # 35 x 35 x 384
    # 4 x Inception-A blocks
    for idx in range(4):
        layers.append(block_inception_a())

    # 35 x 35 x 384
    # Reduction-A block
    layers.append(block_reduction_a())

    # 17 x 17 x 1024
    # 7 x Inception-B blocks
    for idx in range(7):
        layers.append(block_inception_b())

    # 17 x 17 x 1024
    # Reduction-B block
    layers.append(block_reduction_b())

    # 8 x 8 x 1536
    # 3 x Inception-C blocks
    for idx in range(3):
        layers.append(block_inception_c())

    return layers

