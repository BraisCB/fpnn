from python.dataset_scripts.CIFAR_10 import load_data
from python import fpnn
import numpy as np
import matplotlib.pyplot as plt


model = fpnn.FpNN.load_from_file('./models/mnist/mnist_dropout_sqrt_bn_before.json')

mnist = load_data("./datasets/cifar-10/", flip_left_right=False)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

train_pos = np.where((mnist['train']['label'] == 4) | (mnist['train']['label'] == 9))[0]

test_pos = np.where((mnist['test']['label'] == 4) | (mnist['test']['label'] == 9))[0]

data = mnist['train']['data'][train_pos]
labels = mnist['train']['label'][train_pos]
labels[labels == 4] = 0
labels[labels == 9] = 1

test_data = mnist['test']['data'][test_pos]
test_labels = mnist['test']['label'][test_pos]
test_labels[test_labels == 4] = 0
test_labels[test_labels == 9] = 1

# perm = np.random.permutation(len(labels))
# data = test_data
# labels = test_labels

del mnist

test_pos = np.where(labels == 1)[0]
test_neg = np.where(labels == 0)[0]

test_features = model.get_deep_features(data)
print('score', model.eval(data, labels))

train_features = []
train_labels = []
nreps = 3
for i in range(nreps):
    print('iter', i)
    perm = np.random.permutation(len(labels))
    data = data[perm]
    labels = labels[perm]
    train_features += model.get_deep_features(data, is_training=True).tolist()
    train_labels += labels.tolist()
train_labels = np.array(train_labels)
train_features = np.array(train_features)

print('train min', np.min(train_features, axis=0))
print('train max', np.max(train_features, axis=0))
print('test min', np.min(test_features, axis=0))
print('test max', np.max(test_features, axis=0))

train_pos = np.where(train_labels == 1)[0]
train_neg = np.where(train_labels == 0)[0]
plt.ion()
plt.figure()
plt.plot(
    train_features[train_pos, 0], train_features[train_pos, 1], 'ko',
    train_features[train_neg, 0], train_features[train_neg, 1], 'g^',
    test_features[test_neg, 0], test_features[test_neg, 1], 'bs',
    test_features[test_pos, 0], test_features[test_pos, 1], 'rx',
)

plt.title('MNIST deep features distribution')
plt.legend(('label = 4 (training)', 'label = 9 (training)', 'label = 4 (testing)', 'label = 9 (testing)'))
plt.show()
plt.savefig('./images/mnist_dropout_sqrt_before_dispersion.png')