from tensorflow.examples.tutorials.mnist import input_data
from python import fpnn
from sklearn.cluster import KMeans
import numpy as np

mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)

input_size = [784]
train_labels = np.zeros(mnist.train.labels.shape, dtype=mnist.train.labels.dtype)
test_labels = np.zeros(mnist.test.labels.shape, dtype=mnist.test.labels.dtype)
for i in range(10):
    print('label', i)
    gmm = KMeans(n_clusters=10, init='k-means++', n_init=20, max_iter=500, tol=0.0001,
                 precompute_distances='auto', verbose=0, random_state=None, copy_x=True,
                 n_jobs=1, algorithm='auto')
    pos_train = np.where(mnist.train.labels == i)[0]
    pos_test = np.where(mnist.test.labels == i)[0]
    train_i = mnist.train.images[pos_train]
    test_i = mnist.test.images[pos_test]
    gmm.fit(train_i)
    train_labels[pos_train] = gmm.predict(train_i) + 10*i
    test_labels[pos_test] = gmm.predict(test_i) + 10*i


layers = [
    {'type': 'reshape', 'opts': {'shape': [28, 28, 1]}},
    {'type': 'conv2d', 'opts': {'shape': [5, 5, 1, 32]}},
    #{'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
    #{'type': 'dropout', 'opts': {'keep_prob': 0.75}},
    {'type': 'conv2d', 'opts': {'shape': [5, 5, 32, 64]}},
    #{'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
    {'type': 'reshape', 'opts': {'shape': [7*7*64]}},
    #{'type': 'dropout', 'opts': {'keep_prob': 0.5, 'precompute': True}},
    {'type': 'matmul', 'opts': {'shape': [7*7*64, 1024], 'batch_size': 500, 'repetitions': 1}},
    #{'type': 'batch_normalization', 'opts': {'precompute': True}},
    # {'type': 'matmul', 'opts': {'shape': [7*7*64, 1024], 'batch_size': 200, 'fine_tuning': True}},
    {'type': 'relu', 'opts': {'precompute': False}},
    {'type': 'dropout', 'opts': {'keep_prob': 0.5}},
    {'type': 'softmax', 'layers': [
        {'type': 'batch_normalization', 'opts': {'repetitions': 10}},
        # {'type': 'relu'},
        {'type': 'matmul', 'opts': {'shape': [1024, 100]}}
    ]}
]

# global_opts = {
#     'backprop_train': {'type': 'momentum', 'use_nesterov': True,
#                        'learning_decay': {'epochs': 10, 'factor': 0.5}}
# }

model = fpnn.FpNN(input_size=input_size, layers=layers)

# model.frontprop_train(mnist.train.images, train_labels, mnist.test.images, test_labels)

# model = fpnn.FpNN(input_size=input_size, layers=layers)
model.backprop_train(mnist.train.images, train_labels, mnist.test.images, test_labels)

