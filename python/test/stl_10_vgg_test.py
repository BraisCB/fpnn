import json
from python import fpnn
from python.dataset_scripts.STL_10 import load_data
from python.train.vector_utils import get_basis


stl_10 = load_data("./datasets/stl-10/")
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

#CiFAR-10
#no dropout = 91.21
#gd_none = 91.30
#gd_sqrt = 91.91
#d_sqrt = 91.68

#CiFAR-100
#no dropout = 66.45
#gd_sqrt = 68.82
#gd_none = 61.39
#d_sqrt = 67.12

input_size = [96, 96, 3]
dropout_layer = {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}}

output_losses = [
    # {'type': 'softmax'},
    # {'type': 'decoupled_plane_softmax', 'data': {'R': 0.0, 'C': 1.0}},
    # {'type': 'decoupled_softmax', 'data': {'R': 0.0, 'C': 1.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 0.5, 'C': 0.1}},
    # {'type': 'orthogonal_distance', 'data': {'R': 0.5, 'C': 1.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 0.5, 'C': 10.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 0.5, 'C': 100.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 1.0, 'C': 0.1}},
    # {'type': 'orthogonal_distance', 'data': {'R': 1.0, 'C': 1.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 1.0, 'C': 10.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 1.0, 'C': 100.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 2.0, 'C': 0.1}},
    # {'type': 'orthogonal_distance', 'data': {'R': 2.0, 'C': 1.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 2.0, 'C': 10.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 2.0, 'C': 100.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 4.0, 'C': 0.1}},
    # {'type': 'orthogonal_distance', 'data': {'R': 4.0, 'C': 1.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 4.0, 'C': 10.0}},
    # {'type': 'orthogonal_distance', 'data': {'R': 4.0, 'C': 100.0}},
    # {'type': 'orthogonal_plane_distance'},
    # {'type': 'orthogonal_hinge', 'data': {'R': 0.1, 'C': 0.1}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 0.1, 'C': 1.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 0.1, 'C': 10.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 0.1, 'C': 100.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 1.0, 'C': 0.1}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 1.0, 'C': 1.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 1.0, 'C': 10.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 1.0, 'C': 100.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 10.0, 'C': 0.1}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 10.0, 'C': 1.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 10.0, 'C': 10.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 10.0, 'C': 100.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 100.0, 'C': 0.1}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 100.0, 'C': 1.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 100.0, 'C': 10.0}},
    # {'type': 'orthogonal_hinge', 'data': {'R': 100.0, 'C': 100.0}},
    # {'type': 'orthogonal_plane_hinge'},
    # {'type': 'orthogonal', 'data': {'R': 0.5, 'C': 0.1}},
    # {'type': 'orthogonal', 'data': {'R': 0.5, 'C': 1.0}},
    # {'type': 'orthogonal', 'data': {'R': 0.5, 'C': 10.0}},
    # {'type': 'orthogonal', 'data': {'R': 0.5, 'C': 100.0}},
    # {'type': 'orthogonal', 'data': {'R': 1.0, 'C': 0.1}},
    # {'type': 'orthogonal', 'data': {'R': 1.0, 'C': 1.0}},
    # {'type': 'orthogonal', 'data': {'R': 1.0, 'C': 10.0}},
    # {'type': 'orthogonal', 'data': {'R': 1.0, 'C': 100.0}},
    # {'type': 'orthogonal', 'data': {'R': 2.0, 'C': 0.1}},
    # {'type': 'orthogonal', 'data': {'R': 2.0, 'C': 1.0}},
    # {'type': 'orthogonal', 'data': {'R': 2.0, 'C': 10.0}},
    # {'type': 'orthogonal', 'data': {'R': 2.0, 'C': 100.0}},
    # {'type': 'orthogonal', 'data': {'R': 4.0, 'C': 0.1}},
    # {'type': 'orthogonal', 'data': {'R': 4.0, 'C': 1.0}},
    # {'type': 'orthogonal', 'data': {'R': 4.0, 'C': 10.0}},
    # {'type': 'orthogonal', 'data': {'R': 4.0, 'C': 100.0}},
    # {'type': 'orthogonal_plane'},
    # {'type': 'orthogonal_softmax'}
]

basis = get_basis(1024, 10, 'augmented_orthogonal', 1.0)[None, None, :, :]
reps = 3

for output_loss in output_losses:
    print('loss =', output_loss['type'])
    for trainable in [False]:
        if 'orthogonal' in output_loss['type'] and trainable:
            continue
        for rep in range(reps):
            print(output_loss['type'] + '_R_' + str(output_loss['data']['R']) + '_C_' + str(output_loss['data']['C']))
            layers = [
                {'type': 'random_flip_left_right'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'max_pool', 'data': {'ksize': [3, 3], 'strides': [3, 3]}},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 128]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 128]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 1024], 'padding': 'VALID'}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'output', 'layers': [
                    {'type': 'conv2d', 'data': {'shape': [1, 1, 1024], 'padding': 'VALID'}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {
                        'type': 'conv2d',
                        'data': {
                            'shape': [1, 1, 10],
                            'padding': 'VALID',
                            'weights': basis,
                            'trainable': trainable
                        },
                    },
                    # {'type': 'batch_normalization'},
                    # dropout_layer,
                    # {'type': 'relu'},
                    {'type': 'avg_pool', 'data': {'ksize': [2, 2], 'padding': 'VALID'}},
                    {'type': 'reshape', 'data': {'shape': [10]}},
                    output_loss
                ]}
            ]

            global_opts = {
                # 'backprop_train': {'type': 'momentum', 'use_nesterov': True,
                #                    'learning_decay': {'epochs': 10, 'factor': 0.5}},
                'train': {'learning_rate': 1e-2, 'learning_rates': {'40': 1e-3, '80': 1e-4}}
            }


            model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
            # model.frontprop_train(stl_10['train']['data'], stl_10['train']['label'],
            #                       stl_10['test']['data'], stl_10['test']['label'])

            # model.save_to_file('./models/stl-10/stl_10_test_frontprop.json')

            info = model.fit(stl_10['train']['data'], stl_10['train']['label'],
                             stl_10['test']['data'], stl_10['test']['label'], epochs=120, batch_size=50)

            name = output_loss['type'] + '_R_' + str(output_loss['data']['R']) + '_C_' + str(output_loss['data']['C'])

            model.save_to_file('./models/stl-10/' + name + '.json')

            try:
                with open('./models/stl-10/losses_info_C_R.json') as outfile:
                    info_data = json.load(outfile)
            except:
                info_data = {}

            if name not in info_data:
                info_data[name] = []

            info_data[name].append(info)

            with open('./models/stl-10/losses_info_C_R.json', 'w') as outfile:
                json.dump(info_data, outfile)