from python import fpnn
import numpy as np
import json
from python.dataset_scripts.CIFAR_10 import load_data


mnist = load_data("./datasets/cifar-10/", flip_left_right=False)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

train_pos = np.where((mnist['train']['label'] == 4) | (mnist['train']['label'] == 9))[0]

test_pos = np.where((mnist['test']['label'] == 4) | (mnist['test']['label'] == 9))[0]

data = mnist['train']['data'][train_pos]
labels = mnist['train']['label'][train_pos]
labels[labels == 4] = 0
labels[labels == 9] = 1

test_data = mnist['test']['data'][test_pos]
test_labels = mnist['test']['label'][test_pos]
test_labels[test_labels == 4] = 0
test_labels[test_labels == 9] = 1
del mnist

input_size = [32, 32, 3]
epochs = 2000
batch_size = 100

dropout_layer = {
    'type': 'gaussian_dropout',
    'data': {'keep_prob': 0.5},
    'opts': {'mean_correction': False}
}

layers = [
    {'type': 'random_flip_left_right'},
    {'type': 'conv2d', 'data': {'shape': [3, 3, 16]}},
    {'type': 'batch_normalization'},
    dropout_layer,
    {'type': 'relu'},
    {'type': 'conv2d', 'data': {'shape': [3, 3, 16]}},
    {'type': 'batch_normalization'},
    dropout_layer,
    {'type': 'relu'},
    {'type': 'conv2d', 'data': {'shape': [3, 3, 16]}},
    {'type': 'batch_normalization'},
    dropout_layer,
    {'type': 'relu'},
    {'type': 'conv2d', 'data': {'shape': [3, 3, 32]}},
    {'type': 'batch_normalization'},
    dropout_layer,
    {'type': 'relu'},
    {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
    {'type': 'conv2d', 'data': {'shape': [3, 3, 32]}},
    {'type': 'batch_normalization'},
    dropout_layer,
    {'type': 'relu'},
    {'type': 'conv2d', 'data': {'shape': [3, 3, 32]}},
    {'type': 'batch_normalization'},
    dropout_layer,
    {'type': 'relu'},
    {'type': 'conv2d', 'data': {'shape': [3, 3, 32]}},
    {'type': 'batch_normalization'},
    dropout_layer,
    {'type': 'relu'},
    {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
    {'type': 'batch_normalization'},
    dropout_layer,
    {'type': 'relu'},
    {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
    {'type': 'reshape', 'data': {'shape': [8*8*64]}},
    {'type': 'matmul', 'data': {'shape': [2]}},
    {'type': 'batch_normalization'},
    {'type': 'relu'},
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [2]
                }
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-3, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
info = model.fit(data, labels, test_data, test_labels, epochs=epochs, batch_size=batch_size)

model.save_to_file('./models/mnist/mnist_gaussian_dropout_bn.json')

with open('./models/mnist/info_gaussian_dropout_bn.json', 'w') as outfile:
    json.dump(info, outfile)
#
# model.backprop_train(mnist['train']['data'], mnist['train']['label'], mnist['test']['data'], mnist['test']['label'])

