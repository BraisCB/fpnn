import json
from python import fpnn
from python.dataset_scripts.CIFAR_10 import load_data
from python.train.vector_utils import get_basis
import numpy as np


cifar_10 = load_data("./datasets/cifar-10/", flip_left_right=False)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

#CiFAR-10
#no dropout = 91.21
#gd_none = 91.30
#gd_sqrt = 91.91
#d_sqrt = 91.68

#CiFAR-100
#no dropout = 66.45
#gd_sqrt = 68.82
#gd_none = 61.39
#d_sqrt = 67.12

input_size = [32, 32, 3]
dropout_layer = {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}}

output_losses = [
    # {'type': 'softmax'},
    {'type': 'orthogonal_distance'},
    # {'type': 'orthogonal_plane_distance'},
    {'type': 'orthogonal_hinge'},
    # {'type': 'orthogonal_plane_hinge'},
    {'type': 'orthogonal'},
    # {'type': 'orthogonal_plane'},
    # {'type': 'orthogonal_softmax'}
]

basis = get_basis(1024, 10, 'orthogonal', 1.0)[None, None, :, :]
reps = 3
fixed_dimensions = 1014
fixed_dimensions_factor = 0.5
nfeatures = int(np.prod(cifar_10['train']['data'].shape[1:]))

for output_loss in output_losses:
    print('loss =', output_loss['type'])
    for trainable in [True, False]:
        if 'orthogonal' in output_loss and trainable:
            continue
        while True:
            for rep in range(reps):
                print('loss =', output_loss['type'])
                print('trainable =', trainable)
                print('fixed dimensions =', fixed_dimensions)
                factor = np.ones(10 + fixed_dimensions)
                factor[10:] = 100.0
                output_loss['data'] = {'factor': factor.tolist()}
                basis = get_basis(1024, 10 + fixed_dimensions, 'orthogonal', 1.0)[None, None, :, :]
                layers = [
                    {'type': 'random_flip_left_right'},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 128]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 128]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                    {'type': 'conv2d', 'data': {'shape': [3, 3, 1024], 'padding': 'VALID'}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {'type': 'output', 'layers': [
                        {'type': 'conv2d', 'data': {'shape': [1, 1, 1024], 'padding': 'VALID'}},
                        {'type': 'batch_normalization'},
                        dropout_layer,
                        {'type': 'relu'},
                        {
                            'type': 'conv2d',
                            'data': {
                                'shape': [1, 1, 10 + fixed_dimensions],
                                'padding': 'VALID',
                                'weights': basis,
                                'trainable': trainable
                            },
                        },
                        # {'type': 'batch_normalization'},
                        # dropout_layer,
                        # {'type': 'relu'},
                        {'type': 'avg_pool', 'data': {'ksize': [2, 2], 'padding': 'VALID'}},
                        {'type': 'reshape', 'data': {'shape': [10 + fixed_dimensions]}},
                        output_loss
                    ]}
                ]

                global_opts = {
                    # 'backprop_train': {'type': 'momentum', 'use_nesterov': True,
                    #                    'learning_decay': {'epochs': 10, 'factor': 0.5}},
                    'train': {'learning_rate': 1e-2, 'learning_rates': {'40': 1e-3, '80': 1e-4}}
                }


                model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
                # model.frontprop_train(cifar_10['train']['data'], cifar_10['train']['label'],
                #                       cifar_10['test']['data'], cifar_10['test']['label'])

                # model.save_to_file('./models/cifar-10/cifar_10_test_frontprop.json')

                info = model.fit(cifar_10['train']['data'], cifar_10['train']['label'],
                                 cifar_10['test']['data'], cifar_10['test']['label'], epochs=90, batch_size=100)

                name = output_loss['type'] + '_' + str(trainable)

                model.save_to_file('./models/cifar-10/' + name + '.json')

                try:
                    with open('./models/cifar-10/losses_info_fixed_dimensions.json') as outfile:
                        info_data = json.load(outfile)
                except:
                    info_data = {}

                if name not in info_data:
                    info_data[name] = {}

                if str(fixed_dimensions) not in info_data[name]:
                    info_data[name][str(fixed_dimensions)] = []

                info_data[name][str(fixed_dimensions)].append(info)

                with open('./models/cifar-10/losses_info_fixed_dimensions.json', 'w') as outfile:
                    json.dump(info_data, outfile)


            fixed_dimensions = int(fixed_dimensions * fixed_dimensions_factor)
            if fixed_dimensions == 0:
                break
