import json
from python import fpnn
from python.dataset_scripts.STL_10 import load_data
from python.train.vector_utils import get_basis
from python.train.preprocessing import get_cifar_preprocessing_layer
import python.train.wrn_utils as wrn_utils
import numpy as np



stl_10 = load_data("./datasets/stl-10/", normalize=True)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

#CiFAR-10
#no dropout = 91.21
#gd_none = 91.30
#gd_sqrt = 91.91
#d_sqrt = 91.68

#CiFAR-100
#no dropout = 66.45
#gd_sqrt = 68.82
#gd_none = 61.39
#d_sqrt = 67.12


num_classes = 10
batch_size = 100
input_size = [96, 96, 3]
dropout_layer = {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}}

output_losses = [
    {'type': 'softmax'},
    {'type': 'orthogonal_distance', 'data': {'R': 2.0, 'C': 1.0}},
    # {'type': 'orthogonal_plane_distance'},
    {'type': 'orthogonal_hinge', 'data': {'R': 1.0, 'C': 1.0}},
    # {'type': 'orthogonal_plane_hinge'},
    {'type': 'orthogonal', 'data': {'R': 1.0, 'C': 1.0}},
    # {'type': 'orthogonal_plane'},
    # {'type': 'orthogonal_softmax'}
]

basis = get_basis(640, num_classes, 'orthogonal', 1.0)
reps = 3


for output_loss in output_losses:
    print('loss =', output_loss['type'])
    for trainable in [True]:
        if 'orthogonal' in output_loss['type'] and trainable:
            continue
        for rep in range(reps):
            if 'data' in output_loss:
                if 'R' in output_loss['data']:
                    print('R:', str(output_loss['data']['R']))
                if 'C' in output_loss['data']:
                    print('C:', str(output_loss['data']['C']))
            layers = [
                get_cifar_preprocessing_layer(input_size),
                wrn_utils.wrn_conv_bn_relu(3, 16, 2),
                wrn_utils.wrn_residual_block(160, 2, 3),
                wrn_utils.wrn_residual_block(320, 2, 3),
                wrn_utils.wrn_residual_block(640, 2, 3),
                wrn_utils.wrn_bn(),
                {'type': 'relu'},
                # {'type': 'avg_pool', 'data': {'ksize': [8, 8], 'padding': 'VALID'}},
                {'type': 'reduce_mean', 'args': {'axis': [1, 2]}},
                {
                    'type': 'output',
                    'layers': [
                        {
                            'type': 'matmul',
                            'data': {
                                'shape': [num_classes],
                                'padding': 'VALID',
                                'initializer': 'random_normal',
                                'stddev': np.sqrt(1.0/num_classes),
                                'weights': basis.tolist(),
                                'trainable': trainable
                            },
                            'opts': {
                                'bias': output_loss['type'] == 'softmax' and trainable
                            }
                        },
                        output_loss
                    ]
                }
            ]

            global_opts = {
                # 'backprop_train': {'type': 'momentum', 'use_nesterov': True,
                #                    'learning_decay': {'epochs': 10, 'factor': 0.5}},
                'train': {'learning_rate': 1e-3, 'learning_rates': {'50': 1e-4}}
                # 'train': {
                #     'type': 'momentum',
                #     'momentum': 0.9,
                #     'learning_rate': {
                #         'type': 'exponential_decay',
                #         'opts': {
                #             'learning_rate': 0.1,
                #             'global_step': 0,
                #             'decay_steps': 50000,
                #             'decay_rate': 0.1,
                #             'staircase': True
                #         }
                #     }
                # }
            }


            model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
            # model.frontprop_train(stl_10['train']['data'], stl_10['train']['label'],
            #                       stl_10['test']['data'], stl_10['test']['label'])

            # model.save_to_file('./models/stl-10/stl_10_test_frontprop.json')

            info = model.fit(stl_10['train']['data'], stl_10['train']['label'],
                             stl_10['test']['data'], stl_10['test']['label'],
                             epochs=90, batch_size=batch_size)

            name = 'wrn_no_dropout_' + output_loss['type'] + '_' + str(trainable)
            if 'data' in output_loss:
                if 'R' in output_loss['data']:
                    name += '_R_' + str(output_loss['data']['R'])
                if 'C' in output_loss['data']:
                    name += '_C_' + str(output_loss['data']['C'])

            model.save_to_file('./models/stl-10/' + name + '.json')

            try:
                with open('./models/stl-10/wrn_losses_info_no_dropout.json') as outfile:
                    info_data = json.load(outfile)
            except:
                info_data = {}

            if name not in info_data:
                info_data[name] = []

            info_data[name].append(info)

            with open('./models/stl-10/wrn_losses_info_no_dropout.json', 'w') as outfile:
                json.dump(info_data, outfile)