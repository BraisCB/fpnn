import json
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats


dataset = 'stl-10'
basis = 'augmented_orthogonal'
file = './models/' + dataset + '/losses_info_C_R.json'
plot = False


with open(file) as outfile:
    info_data = json.load(outfile)

accuracies = {}
losses = {}
keys = info_data.keys()
keys = sorted(keys)
for method in keys:
    if 'hinge' in method:
        continue
    print('method =', method)
    best_acc = 0
    best_iter = -1
    accuracy = []
    loss = []
    for i, iter in enumerate(info_data[method]):
        accuracy.append(100 * iter['test']['accuracy'][-1][1])
        loss.append(iter['test']['loss'][-1][1])
        if accuracy[-1] > best_acc:
            best_acc = accuracy[-1]
            best_iter = i
    accuracies[method] = accuracy
    losses[method] = loss
    print('accuracy =', np.mean(accuracy), '+-', np.std(accuracy), 'min', np.min(accuracy), 'max', np.max(accuracy))
    print('loss =', np.mean(loss), '+-', np.std(loss))
    if plot:
        name = dataset + '_' + method
        train_acc = np.array(info_data[method][i]['train']['accuracy'])
        test_acc = np.array(info_data[method][i]['test']['accuracy'])
        plt.ion()
        plt.figure()
        plt.plot(
            train_acc[:, 0], train_acc[:, 1], 'b',
            test_acc[:, 0], test_acc[:, 1], 'r',
        )

        plt.title(name)
        plt.legend(('train', 'test'))
        plt.show()
        plt.savefig('./images/' + name + '_accuracy.png')

        train_loss = np.array(info_data[method][i]['train']['loss'])
        test_loss = np.array(info_data[method][i]['test']['loss'])
        plt.ion()
        plt.figure()
        plt.plot(
            train_loss[10:, 0], train_loss[10:, 1], 'b',
            test_loss[10:, 0], test_loss[10:, 1], 'r',
        )

        plt.title(name)
        plt.legend(('train', 'test'))
        plt.show()
        plt.savefig('./images/' + name + '_loss.png')

keys = list(accuracies.keys())
pvals = np.zeros((len(keys), len(keys)))
for i, key in enumerate(keys):
    print(key, accuracies[key])
    print(key, losses[key])
    for j in range(i, len(keys)):
        acc_i = accuracies[key]
        acc_j = accuracies[keys[j]]
        pvals[i,j] = pvals[j,i] = stats.f_oneway(acc_i, acc_j)[1]

print(keys)
print(pvals)
print(accuracies)
