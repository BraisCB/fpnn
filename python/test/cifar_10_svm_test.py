from python.fpnn import FpNN
from python.dataset_scripts.CIFAR_10 import load_data
from sklearn.svm import SVC, NuSVC, LinearSVC

print('obtaining data')
cifar_10 = load_data("./datasets/cifar-10/", flip_left_right=False)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

print('loading model')
model = FpNN.load_from_file('./models/cifar-10/cifar_10_test_frontprop.json')

print('computing data')
cifar_10['train']['data'] = model.eval(cifar_10['train']['data'])
cifar_10['test']['data'] = model.eval(cifar_10['test']['data'])

print('training svc')
svc = SVC(verbose=True, C=1e-4)
svc.fit(cifar_10['train']['data'], cifar_10['train']['label'])

train_pred = svc.predict(cifar_10['train']['data'])
test_pred = svc.predict(cifar_10['test']['data'])

train_acc = 1.0/len(train_pred)*(train_pred == cifar_10['train']['label']).sum()
test_acc = 1.0/len(test_pred)*(test_pred == cifar_10['test']['label']).sum()

print('train accuracy', train_acc)
print('test accuracy', test_acc)
