from tensorflow.examples.tutorials.mnist import input_data
from python import fpnn


mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

input_size = [784]

layers = [
    {'type': 'reshape', 'opts': {'shape': [28, 28, 1]}},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 1, 32]}},
    {'type': 'relu'},
    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
    {'type': 'residual_block', 'layers': [
        {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 32]}},
        {'type': 'relu'},
        {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 32]}},
    ]},
    {'type': 'relu'},
    {'type': 'residual_block', 'layers': [
        {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 32]}},
        {'type': 'relu'},
        {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 32]}},
    ]},
    {'type': 'relu'},
    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
    {'type': 'reshape', 'opts': {'shape': [7*7*32]}},
    # {'type': 'dropout', 'opts': {'keep_prob': 0.5, 'precompute': True}},
    {'type': 'matmul', 'opts': {'shape': [7*7*32, 1024], 'batch_size': 500, 'repetitions': 1}},
    # {'type': 'matmul', 'opts': {'shape': [7*7*64, 1024], 'batch_size': 200, 'fine_tuning': True}},
    {'type': 'relu', 'opts': {'precompute': False}},
    # {'type': 'dropout', 'opts': {'keep_prob': 0.5}},
    {'type': 'softmax', 'layers': [
        {'type': 'matmul', 'opts': {'shape': [1024, 10]}}
    ]}
]

global_opts = {
    # 'variables': {'weight_decay': 1e-4},
    'backprop_train': {#'type': 'momentum', 'use_nesterov': True,
                       'learning_decay': {'epochs': 10, 'factor': 0.5}},
                       #'learning_rate': 0.1},
    'frontprop_train': {'augmented_label': {}}
}

model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)

# model.frontprop_train(mnist.train.images, mnist.train.labels, mnist.test.images, mnist.test.labels)
model.backprop_train(mnist.train.images, mnist.train.labels, mnist.test.images, mnist.test.labels)

