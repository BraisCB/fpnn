import json
from python import fpnn
from python.dataset_scripts.CIFAR_100 import load_data
from python.train.vector_utils import get_basis
from python.train.preprocessing import get_cifar_preprocessing_layer
import python.train.wrn_utils as wrn_utils
import numpy as np


cifar_100 = load_data("./datasets/cifar-100/", normalize=True)

num_classes = 100
batch_size = 128
input_size = [32, 32, 3]
k = 8
l = 16
N = (16 - 4) // 6
dropout_layer = {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}}

output_losses = [
    {'type': 'softmax'},
    {'type': 'orthogonal_distance', 'data': {'R': 2.0, 'C': 1.0}},
    {'type': 'orthogonal_distance', 'data': {'R': 2.0, 'C': 0.1}},
    # {'type': 'orthogonal_plane_distance'},
    {'type': 'orthogonal_hinge', 'data': {'R': 1.0, 'C': 0.1}},
    # {'type': 'orthogonal_plane_hinge'},
    {'type': 'orthogonal', 'data': {'R': 1.0, 'C': 1.0}},
    {'type': 'orthogonal', 'data': {'R': 1.0, 'C': 0.1}},
    # {'type': 'orthogonal_plane'},
    # {'type': 'orthogonal_softmax'}
]

basis = get_basis(64 * k, num_classes, 'augmented_orthogonal', 1.0)
reps = 1

print('k =', k)
print('N =', N)
for output_loss in output_losses:
    for trainable in [True]:
        if 'orthogonal' in output_loss['type'] and trainable:
            continue
        for rep in range(reps):
            print('loss =', output_loss['type'])
            if 'data' in output_loss:
                if 'R' in output_loss['data']:
                    print('R:', str(output_loss['data']['R']))
                if 'C' in output_loss['data']:
                    print('C:', str(output_loss['data']['C']))
            layers = [
                get_cifar_preprocessing_layer(input_size),
                wrn_utils.conv(3, 16, 1),
                wrn_utils.block(16 * k, 1, N),
                wrn_utils.block(32 * k, 2, N),
                wrn_utils.block(64 * k, 2, N),
                wrn_utils.bn(),
                {'type': 'relu'},
                # {'type': 'avg_pool', 'data': {'ksize': [8, 8], 'padding': 'VALID'}},
                {'type': 'reduce_mean', 'args': {'axis': [1, 2]}},
                {
                    'type': 'output',
                    'layers': [
                        {
                            'type': 'matmul',
                            'data': {
                                'shape': [num_classes],
                                'padding': 'VALID',
                                'initializer': 'xavier',
                                # 'weights': basis.tolist(),
                                'trainable': trainable
                            },
                            'opts': {
                                'bias': output_loss['type'] == 'softmax' and trainable
                            }
                        },
                        output_loss
                    ]
                }
            ]

            global_opts = {
                # 'backprop_train': {'type': 'momentum', 'use_nesterov': True,
                #                    'learning_decay': {'epochs': 100, 'factor': 0.5}},
                'train': {
                    'type': 'momentum',
                    'use_nesterov': True,
                    'momentum': 0.9,
                    'learning_rate': 1e-1,
                    'learning_rates': {'60': 2e-2, '120': 4e-3, '160': 8e-4}
                }
                # 'train': {
                #     'type': 'momentum',
                #     'momentum': 0.9,
                #     'learning_rate': {
                #         'type': 'exponential_decay',
                #         'opts': {
                #             'learning_rate': 0.1,
                #             'global_step': 0,
                #             'decay_steps': 50000,
                #             'decay_rate': 0.1,
                #             'staircase': True
                #         }
                #     }
                # }
            }


            model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
            # model.frontprop_train(cifar_100['train']['data'], cifar_100['train']['label'],
            #                       cifar_100['test']['data'], cifar_100['test']['label'])

            # model.save_to_file('./models/cifar-100/cifar_100_test_frontprop.json')

            info = model.fit(cifar_100['train']['data'], cifar_100['train']['label'],
                             cifar_100['test']['data'], cifar_100['test']['label'],
                             epochs=200, batch_size=batch_size)

            name = 'wrn_' + str(l) + '_' + str(k) + '_' + output_loss['type'] + '_' + str(trainable) + '_no_dropout'
            if 'data' in output_loss:
                if 'R' in output_loss['data']:
                    name += '_R_' + str(output_loss['data']['R'])
                if 'C' in output_loss['data']:
                    name += '_C_' + str(output_loss['data']['C'])

            model.save_to_file('./models/cifar-100/' + name + '.json')

            losses_name = './models/cifar-100/wrn_' + str(l) + '_' + str(k) + '_' + 'losses_info_no_dropout.json'

            try:
                with open(losses_name) as outfile:
                    info_data = json.load(outfile)
            except:
                info_data = {}

            if name not in info_data:
                info_data[name] = []

            info_data[name].append(info)

            with open(losses_name, 'w') as outfile:
                json.dump(info_data, outfile)
