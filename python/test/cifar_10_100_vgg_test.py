import json
from python import fpnn
from python.dataset_scripts import CIFAR_100
from python.dataset_scripts import CIFAR_10
from python.train.vector_utils import get_basis
import numpy as np


cifar_100 = CIFAR_100.load_data("./datasets/cifar-100/", flip_left_right=False)
cifar_10 = CIFAR_10.load_data("./datasets/cifar-10/", flip_left_right=False)
train_data = np.concatenate((cifar_100['train']['data'], cifar_10['train']['data']), axis=0)
train_label = np.concatenate((cifar_100['train']['label'], cifar_10['train']['label']), axis=0)
test_data = np.concatenate((cifar_100['test']['data'], cifar_10['test']['data'] + 100), axis=0)
test_label = np.concatenate((cifar_100['test']['label'], cifar_10['test']['label'] + 100), axis=0)


# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

#CiFAR-10
#no dropout = 91.21
#gd_none = 91.30
#gd_sqrt = 91.91
#d_sqrt = 91.68

#CiFAR-100
#no dropout = 66.45
#gd_sqrt = 68.82
#gd_none = 61.39
#d_sqrt = 67.12

input_size = [32, 32, 3]
dropout_layer = {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}}

output_losses = [
    {'type': 'softmax'},
    {'type': 'orthogonal_distance'},
    # {'type': 'orthogonal_plane_distance'},
    {'type': 'orthogonal_hinge'},
    # {'type': 'orthogonal_plane_hinge'},
    {'type': 'orthogonal'},
    # {'type': 'orthogonal_plane'},
    # {'type': 'orthogonal_softmax'}
]

basis = get_basis(1024, 110, 'augmented_orthogonal', 1.0)[None, None, :, :]
reps = 5
factor = np.ones(110)
factor[100:] = 0.1

for output_loss in output_losses:
    print('loss =', output_loss['type'])
    for trainable in [True, False]:
        for rep in range(reps):
            layers = [
                {'type': 'random_flip_left_right'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 128]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 128]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
                {'type': 'conv2d', 'data': {'shape': [3, 3, 1024], 'padding': 'VALID'}},
                {'type': 'batch_normalization'},
                dropout_layer,
                {'type': 'relu'},
                {'type': 'output', 'layers': [
                    {'type': 'conv2d', 'data': {'shape': [1, 1, 1024], 'padding': 'VALID'}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                    {
                        'type': 'conv2d',
                        'data': {
                            'shape': [1, 1, 110],
                            'padding': 'VALID',
                            'weights': basis,
                            'factor': factor.tolist(),
                            'trainable': trainable
                        },
                    },
                    # {'type': 'batch_normalization'},
                    # dropout_layer,
                    # {'type': 'relu'},
                    {'type': 'avg_pool', 'data': {'ksize': [2, 2], 'padding': 'VALID'}},
                    {'type': 'reshape', 'data': {'shape': [110]}},
                    output_loss
                ]}
            ]

            global_opts = {
                # 'backprop_train': {'type': 'momentum', 'use_nesterov': True,
                #                    'learning_decay': {'epochs': 10, 'factor': 0.5}},
                'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}}
            }


            model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
            # model.frontprop_train(cifar_100['train']['data'], cifar_100['train']['label'],
            #                       cifar_100['test']['data'], cifar_100['test']['label'])

            # model.save_to_file('./models/cifar-100/cifar_100_test_frontprop.json')

            info = model.fit(train_data, train_label, test_data, test_label, epochs=100, batch_size=150)

            name = output_loss['type'] + '_' + str(trainable)

            model.save_to_file('./models/cifar-100/' + name + '.json')

            try:
                with open('./models/cifar-100/100_10_losses_info.json') as outfile:
                    info_data = json.load(outfile)
            except:
                info_data = {}

            if name not in info_data:
                info_data[name] = []

            info_data[name].append(info)

            with open('./models/cifar-100/100_10_losses_info.json', 'w') as outfile:
                json.dump(info_data, outfile)

            cifar_10_probs = model.eval(cifar_10['test']['data'], output_type='probabilities')[0, :, 100:]
            cifar_10_pred = np.argmax(cifar_10_probs, axis=-1)
            cifar_10_acc = (cifar_10_pred == cifar_10['test']['data']).sum() / len(cifar_10_pred)

            cifar_100_probs = model.eval(cifar_100['test']['data'], output_type='probabilities')[0, :, :100]
            cifar_100_pred = np.argmax(cifar_100_probs, axis=-1)
            cifar_100_acc = (cifar_100_pred == cifar_100['test']['data']).sum() / len(cifar_100_pred)

            try:
                with open('./models/cifar-100/100_10_accuracies.json') as outfile:
                    results = json.load(outfile)
            except:
                results = {}

            if name not in results:
                results[name] = {'cifar-10': [], 'cifar-100': []}

            results[name]['cifar-10'].append(cifar_10_acc)
            results[name]['cifar-100'].append(cifar_100_acc)

            print('cifar-10 accuracy =', cifar_10_acc)
            print('cifar-100 accuracy =', cifar_100_acc)

            with open('./models/cifar-100/100_10_accuracies.json', 'w') as outfile:
                json.dump(results, outfile)

            del model
