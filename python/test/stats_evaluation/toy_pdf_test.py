import numpy as np
from python.fpnn import FpNN
from tensorflow.examples.tutorials.mnist import input_data
from sklearn import decomposition
from matplotlib import pyplot as pl
import scipy.stats

n_components = 28*28

mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
pca = decomposition.PCA(n_components=n_components)

cnn = FpNN.load_from_file('./models/toy_test_tanh/toy_test_gaussian_noise_08.json')
cnn.build(trainable=False)

type_to_analyze = 'tanh'

layers = cnn.layers

batch_size = 128
treps = 5

source = mnist.train
data = pca.fit_transform(source.images)
# data = source.images
label = FpNN.dense_to_one_hot(source.labels)

diffs = {}
test = {}
train = {}

cont_p = 2

perm = np.arange(len(label))
feed_dict_training = cnn.get_feed_dict(is_training=True)
feed_dict_testing = cnn.get_feed_dict(is_training=False)

for i in range(treps):
    np.random.shuffle(perm)
    index = 0
    print(i, 'of', treps)
    while index < len(data):
        new_index = min(index + batch_size, len(data))
        data_batch = data[perm[index:new_index]]
        label_batch = label[perm[index:new_index]]
        index = new_index
        feed_dict_training[cnn.model[0]['operation']] = data_batch
        feed_dict_training[cnn.results[-1]] = label_batch
        feed_dict_testing[cnn.model[0]['operation']] = data_batch
        feed_dict_testing[cnn.results[-1]] = label_batch
        cont = 0
        for j in range(len(cnn.model)):
            model = cnn.model[j]
            if cont > cont_p:
                break
            if j < len(cnn.model) - 1 and cnn.model[j]['type'] == type_to_analyze:
                if cont == cont_p:
                    tr = model['operation'].eval(feed_dict=feed_dict_training)
                    te = model['operation'].eval(feed_dict=feed_dict_testing)
                    di = tr - te
                    if cont in train:
                        diffs[cont] += di.flatten().to_list()
                        train[cont] += tr.flatten().to_list()
                        test[cont] += te.flatten().to_list()
                    else:
                        diffs[cont] = di.flatten().to_list()
                        train[cont] = tr.flatten().to_list()
                        test[cont] = te.flatten().to_list()
                cont += 1
            elif 'block' in model['type']:
                for k in range(len(model['block_operations']) - 1):
                    block_model = model['block_operations'][k]
                    if model['block_operations'][k + 1]['type'] == type_to_analyze:
                        if cont == cont_p:
                            tr = block_model['operation'].eval(feed_dict=feed_dict_training)
                            te = block_model['operation'].eval(feed_dict=feed_dict_testing)
                            di = tr - te
                            if cont in train:
                                diffs[cont] += di.flatten().tolist()
                                train[cont] += tr.flatten().tolist()
                                test[cont] += te.flatten().tolist()
                            else:
                                diffs[cont] = di.flatten().tolist()
                                train[cont] = tr.flatten().tolist()
                                test[cont] = te.flatten().tolist()
                        cont += 1

print('train', len(train))
for cont in train:
    pl.figure(cont)
    train_hist, train_bins = np.histogram(train[cont], bins=200, normed=True)
    train_bin_centers = (train_bins[1:] + train_bins[:-1]) * 0.5
    #train_hist /= train_hist.sum()
    pl.plot(train_bin_centers, train_hist)

    test_hist, test_bins = np.histogram(test[cont], bins=200, normed=True)
    test_bin_centers = (test_bins[1:] + test_bins[:-1]) * 0.5
    #test_hist /= test_hist.sum()
    pl.plot(test_bin_centers, test_hist)

    diff_hist, diff_bins = np.histogram(diffs[cont], bins=200, normed=True)
    diff_bin_centers = (diff_bins[1:] + diff_bins[:-1]) * 0.5
    #diff_hist /= diff_hist.sum()
    pl.plot(diff_bin_centers, diff_hist)

    pl.legend(['train', 'test', 'diff'], loc='best')
    #pl.xlim((-10, 10))
    pl.show()
