from python.fpnn import FpNN
from tensorflow.examples.tutorials.mnist import input_data
from python.train.saliency_map import get_saliency_map
from python.train.vector_utils import get_basis, label_to_basis
from python.train.frontprop_initializer import init_model
from python.train.backprop_initializer import init_model_layer_by_layer
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from sklearn.mixture import GaussianMixture
from sklearn.decomposition import PCA



mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)

batch_size = 100

n_components = 28*28

source = mnist.train
test_source = mnist.test

data = source.images
labels = source.labels
test_data = test_source.images
test_labels = test_source.labels

input_size = [n_components]

basis = get_basis(50, 10, 'orthogonal', 1.0).tolist()
bias = 10*[0]
layers = [
    {'type': 'batch_normalization'},
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [50]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [70]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [50]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
            # {'type': 'l2_normalize'}
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'variables': {'trainable': False},
                'data': {
                    'shape': [10],
                    'weights': basis,
                    'bias': bias
                }
            },
            # {'type': 'tile', 'opts': {'multiples': [1, 10]}},
            # {'type': 'reshape', 'data': {'shape': [10, 50]}},
            # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
            # {'type': 'square'},
            # {'type': 'reduce_sum', 'opts': {'axis': -1}},
            # {'type': 'multiply', 'opts': {'y': -1}},
            {'type': 'hinge'}
        ]
    }
]


# dim = 1024
# basis = get_basis(dim, 10, 'ones', 1.0).tolist()
# bias = 10*[0]
# layers = [
#     {'type': 'reshape', 'data': {'shape': [28, 28, 1]}},
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'conv2d', 'data': {'shape': [5, 5, 32]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
#             {'type': 'relu'},
#             {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'conv2d', 'data': {'shape': [5, 5, 64]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
#             {'type': 'relu'},
#             {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
#             {'type': 'reshape', 'data': {'shape': [7*7*64]}},
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'data': {'shape': [1024]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
#             {'type': 'relu'},
#         ]
#     },
#     {
#         'type': 'output',
#         'layers': [
#             {
#                 'type': 'matmul',
#                 'variables': {'trainable': False},
#                 'data': {
#                     'shape': [10],
#                     'weights': basis,
#                     'bias': bias
#                 }
#             },
#             {'type': 'softmax', 'data': {'R': 1.0}}
#             # {'type': 'tile', 'opts': {'multiples': [1, 11]}},
#             # {'type': 'reshape', 'opts': {'shape': [11, 50]}},
#             # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
#             # {'type': 'square'},
#             # {'type': 'reduce_sum', 'opts': {'axis': -1}},
#             # {'type': 'distance'}
#         ]
#     }
# ]

global_opts = {
    'train': {'learning_rate': 1e-3, 'learning_decay': {'epochs': 7, 'factor': 0.1}},
    #'variables': {'l2_loss': {'lambda': 1e-2}}
}

fpnn_data = {
    'input_size': input_size,
    'model_type': 'classification',
    'global_opts': global_opts,
    'framework': 'tensorflow'
}

model = FpNN(input_size=input_size, layers=layers, global_opts=global_opts)

old_layers = deepcopy(model.layers)
stats = model.fit(data, labels, test_data, test_labels, epochs=7000, batch_size=batch_size)

print('accuracy = ', model.eval(test_data, test_labels))
maps = []

#plt.ion()
for i in range(10):
    i_data = data[labels == i]
    vector = np.zeros((1, 10))
    vector[:, i] = 1.0
    im_map = get_saliency_map(model, i_data, vector)
    maps.append(im_map)
    # image_map = np.reshape(im_map, (28, 28))
    #
    # plt.figure()
    # plt.imshow(image_map)
    # plt.savefig('./images/mnist_nofixed_' + str(i) + '.png')

maps = np.array(maps)
# maps /= np.max(maps)
# fig, axes = plt.subplots(2, 5)
# for i in range(10):
#     x = int(i/5)
#     y = i % 5
#     #axes[x, y].imshow(np.reshape(np.mean(data[labels == 0], axis=0), (28, 28)))
#     axes[x, y].imshow(np.reshape(maps[i], (28, 28)), vmax=1.0, vmin=0.0, cmap='plasma', alpha=1.0)
# plt.show()

max_maps = np.mean(maps, axis=0)

score_index = np.argsort(max_maps)[0]


for i in range(1, 600, 10):
    mask = np.ones(n_components)
    mask[score_index[:i]] = 0
    nelements = n_components - i - 1
    print('nelements', nelements, 'accuracy = ', model.eval(test_data*mask, test_labels))

del model
data = data[:, score_index[600:]]
test_data = test_data[:, score_index[600:]]

model = FpNN(input_size=[data.shape[-1]], layers=old_layers, global_opts=global_opts)

stats = model.fit(data, labels, test_data, test_labels, epochs=20000, batch_size=batch_size)


train_loss = np.array(stats['train']['loss'])
test_loss = np.array(stats['test']['loss'])
print('train loss', train_loss[-1, -1])
print('test loss', test_loss[-1, -1])
plt.figure()
plt.plot(train_loss[:, 0], train_loss[:, 1])
plt.plot(test_loss[:, 0], test_loss[:, 1])
plt.show()

