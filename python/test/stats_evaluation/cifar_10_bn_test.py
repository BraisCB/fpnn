import numpy as np
from python.fpnn import FpNN
from python.dataset_scripts.CIFAR_10 import load_data


cifar_10 = load_data("./datasets/cifar-10/", flip_left_right=False)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

cnn = FpNN.load_from_file('./models/cifar-10/cifar_10_test_bn_dropout_sqrt.json')
cnn.build(trainable=False)

input_size = [32, 32, 3]

layers = cnn.layers

batch_size = 128
treps = 10

source = cifar_10['test']
data = source['data']
label = FpNN.dense_to_one_hot(source['label'])

diffs = []
test = []
train = []
diffs_2 = []
test_2 = []
train_2 = []
corr = []
ndata = 0

perm = np.arange(len(label))
feed_dict_training = cnn.get_feed_dict(is_training=True, backprop_train=False)
feed_dict_testing = cnn.get_feed_dict(is_training=False, backprop_train=False)

for i in range(treps):
    np.random.shuffle(perm)
    index = 0
    print(i, 'of', treps)
    while index < len(data):
        print(index, 'of', len(data))
        new_index = min(index + batch_size, len(data))
        data_batch = data[perm[index:new_index]]
        label_batch = label[perm[index:new_index]]
        index = new_index
        feed_dict_training[cnn.model[0]['operation']] = data_batch
        feed_dict_training[cnn.results[-1]] = label_batch
        feed_dict_testing[cnn.model[0]['operation']] = data_batch
        feed_dict_testing[cnn.results[-1]] = label_batch
        cont = 0
        ndata += len(data_batch)
        for model in cnn.model:
            if model['type'] == 'gaussian_noise':
                tr = model['operation'].eval(feed_dict=feed_dict_training)
                te = model['operation'].eval(feed_dict=feed_dict_testing)
                di = tr - te
                rtrain = np.sum(tr, axis=0)
                rtest = np.sum(te, axis=0)
                rdiff = np.sum(di, axis=0)
                rtrain_2 = np.sum(tr*tr, axis=0)
                rtest_2 = np.sum(te*te, axis=0)
                rdiff_2 = np.sum(di*di, axis=0)
                rcorr = np.sum(tr * te, axis=0)
                if cont == len(diffs):
                    train.append(rtrain)
                    train_2.append(rtrain_2)
                    test.append(rtest)
                    test_2.append(rtest_2)
                    diffs.append(rdiff)
                    diffs_2.append(rdiff_2)
                    corr.append(rcorr)
                else:
                    train[cont] += rtrain
                    train_2[cont] += rtrain_2
                    test[cont] += rtest
                    test_2[cont] += rtest_2
                    diffs[cont] += rdiff
                    diffs_2[cont] += rdiff_2
                    corr[cont] += rcorr
                cont += 1

desv_train = []
desv_test = []
desv_diffs = []
mean_train = []
mean_desv_train = []
mean_test = []
mean_desv_test = []
mean_diffs = []
mean_desv_diffs = []
mean_corr = []
desv_corr = []

for cont in range(len(train)):
    train[cont] /= ndata
    train_2[cont] /= ndata
    test[cont] /= ndata
    test_2[cont] /= ndata
    diffs[cont] /= ndata
    diffs_2[cont] /= ndata
    desv_train.append(np.sqrt(train_2[cont] - train[cont]*train[cont]))
    desv_test.append(np.sqrt(test_2[cont] - test[cont]*test[cont]))
    desv_diffs.append(np.sqrt(diffs_2[cont] - diffs[cont]*diffs[cont]))
    dims = len(train[cont].shape)
    corr[cont] /= ndata
    desv_corr.append((corr[cont] - train[cont] * test[cont]) / (desv_train[cont] * desv_test[cont]))
    if dims == 1:
        mean_train.append(train[cont])
        mean_desv_train.append(desv_train[cont])
        mean_test.append(test[cont])
        mean_desv_test.append(desv_test[cont])
        mean_diffs.append(diffs[cont])
        mean_desv_diffs.append(desv_diffs[cont])
        mean_corr.append(desv_corr[cont])
    else:
        mean_train.append(np.mean(train[cont],axis=tuple(range(0, dims))))
        mean_desv_train.append(np.mean(desv_train[cont],axis=tuple(range(0, dims))))
        mean_test.append(np.mean(test[cont],axis=tuple(range(0, dims))))
        mean_desv_test.append(np.mean(desv_test[cont],axis=tuple(range(0, dims))))
        mean_diffs.append(np.mean(diffs[cont],axis=tuple(range(0, dims))))
        mean_desv_diffs.append(np.mean(desv_diffs[cont],axis=tuple(range(0, dims))))
        mean_corr.append(np.mean(desv_corr[cont], axis=tuple(range(0, dims))))
    print('')
    print('Layer', cont)
    print('Test distribution mean =', mean_test[cont], ', desv =', mean_desv_test[cont])
    print('Train distribution mean =', mean_train[cont], ', desv =', mean_desv_train[cont])
    print('Diff distribution mean =', mean_diffs[cont], ', desv =', mean_desv_diffs[cont])
    print('Correlation =', mean_corr[cont])
    print('')