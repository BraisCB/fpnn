from python.fpnn import FpNN
from tensorflow.examples.tutorials.mnist import input_data
from python.train.saliency_map import get_saliency_map
from python.train.vector_utils import get_basis, label_to_basis
from python.train.frontprop_initializer import init_model
from python.train.backprop_initializer import init_model_layer_by_layer
from python.train.activation_utils import inverse_elu, elu
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from sklearn.mixture import GaussianMixture
from sklearn.decomposition import PCA
from scipy.stats import ortho_group


mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)

batch_size = 100

n_components = 28*28

source = mnist.train
labels = source.labels

test_source = mnist.test

data = source.images
mean = np.mean(data, axis=0)
desv = np.std(data, axis=0)
desv[desv == 0] = 1.0
data = (data - mean) / desv

test_data = test_source.images
test_data = (test_data - mean) / desv
test_labels = test_source.labels

input_size = [n_components]

Y = -1.0*np.ones((len(labels), 10))
Y[np.arange(len(labels)), labels] = 1.0
Y = Y

# g(X*W_1)W_2 = Y

W_2 = get_basis(50, 10, 'ones', 1.0)
basis = W_2
X = data
Y = Y @ W_2.T
Y = Y @ np.linalg.pinv(W_2 @ W_2.T)
Y = inverse_elu(Y)
X_t_X = X.T @ X
Y = X.T @ Y
W_1 = np.linalg.pinv(X_t_X) @ Y

A_1 = elu(test_data @ W_1)
Y_p = np.argmax(A_1 @ W_2, axis=-1)

acc = (test_labels == Y_p).mean()
print('acc', acc)

bias = 10*[0]
layers = [
    {'type': 'batch_normalization'},
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [50]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
            {'type': 'elu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [70]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
            {'type': 'elu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [50]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
            {'type': 'elu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'variables': {'trainable': False},
                'data': {
                    'shape': [10],
                    'weights': basis,
                    'bias': bias
                }
            },
            # {'type': 'tile', 'opts': {'multiples': [1, 10]}},
            # {'type': 'reshape', 'opts': {'shape': [10, 50]}},
            # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
            # {'type': 'square'},
            # {'type': 'reduce_sum', 'opts': {'axis': -1}},
            #{'type': 'multiply', 'opts': {'y': -1}},
            {
                'type': 'softmax', 'data': {'R': 0.0}
            }
        ]
    }
]




global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 7, 'factor': 0.1}},
    #'variables': {'l2_loss': {'lambda': 1e-2}}
}

fpnn_data = {
    'input_size': input_size,
    'model_type': 'classification',
    'global_opts': global_opts,
    'framework': 'tensorflow'
}



model = FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
# model.build()
# model.extract_data()


old_layers = deepcopy(model.layers)

stats = model.fit(data, labels, test_data, test_labels, epochs=12000, batch_size=batch_size)

train_loss = np.array(stats['train']['loss'])
test_loss = np.array(stats['test']['loss'])
print('train loss', train_loss[-1, -1])
print('test loss', test_loss[-1, -1])
plt.figure()
plt.plot(train_loss[:, 0], train_loss[:, 1])
plt.plot(test_loss[:, 0], test_loss[:, 1])
plt.show()

