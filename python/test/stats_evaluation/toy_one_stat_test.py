import numpy as np
from python.fpnn import FpNN
from tensorflow.examples.tutorials.mnist import input_data
from sklearn import decomposition
import matplotlib.pyplot as plt
from python.dataset_scripts.CIFAR_100 import load_data

cifar_10 = load_data("./datasets/cifar-100/", flip_left_right=False)


batch_size = 128
treps = 1



source = cifar_10['train']
data = source['data']
# data = source.images
label = source['label']

diffs = []
test = []
train = []
diffs_2 = []
test_2 = []
train_2 = []
corr = []
dcov = []
ndata = []

test_layer = 3

perm = np.arange(len(label))

files = [('dropout', './models/toy-cifar-100/toy_test_bn_dropout_09.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_075.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_05.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_025.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_none_09.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_none_075.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_none_05.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_none_025.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_sqrt_09.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_sqrt_075.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_sqrt_05.json'),
         ('dropout', './models/toy-cifar-100/toy_test_bn_dropout_sqrt_025.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_09.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_075.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_05.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_025.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_none_09.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_none_075.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_none_05.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_none_025.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_sqrt_09.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_sqrt_075.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_sqrt_05.json'),
         ('gaussian_dropout', './models/toy-cifar-100/toy_test_bn_gaussian_dropout_sqrt_025.json'),
         ]

for v in files:
    file = v[1]
    evaluate = v[0]
    cnn = FpNN.load_from_file(file)
    cnn.build(trainable=False)
    feed_dict_training = cnn.get_feed_dict(is_training=True)
    feed_dict_testing = cnn.get_feed_dict(is_training=False)
    layers = cnn.layers

    for i in range(treps):
        np.random.shuffle(perm)
        index = 0
        print(i, 'of', treps)
        # while index < len(data):
        while index < 4000:
            new_index = min(index + batch_size, len(data))
            data_batch = data[perm[index:new_index]]
            label_batch = label[perm[index:new_index]]
            index = new_index
            feed_dict_training[cnn.model[0]['operation']] = data_batch
            feed_dict_training[cnn.results[-1]] = label_batch
            feed_dict_testing[cnn.model[0]['operation']] = data_batch
            feed_dict_testing[cnn.results[-1]] = label_batch
            cont = 0
            tlayer = 0
            for r in range(len(cnn.model)):
                model = cnn.model[r]
                if 'block' in model['type']:
                    for r in range(len(model['block_operations']) - 1):
                        if model['block_operations'][r+1]['type'] == evaluate:
                            tlayer += 1
                            if test_layer == tlayer:
                                block_model = model['block_operations'][r]
                                tr = block_model['operation'].eval(feed_dict=feed_dict_training)
                                te = block_model['operation'].eval(feed_dict=feed_dict_testing)
                                di = tr - te
                                rtrain = np.sum(tr)
                                rtest = np.sum(te)
                                rdiff = np.sum(di)
                                rtrain_2 = np.sum(tr * tr)
                                rtest_2 = np.sum(te * te)
                                rdiff_2 = np.sum(di * di)
                                rcorr = np.sum(tr * te)
                                if cont == len(diffs):
                                    train.append(rtrain)
                                    train_2.append(rtrain_2)
                                    test.append(rtest)
                                    test_2.append(rtest_2)
                                    diffs.append(rdiff)
                                    diffs_2.append(rdiff_2)
                                    corr.append(rcorr)
                                    ndata.append(np.prod(tr.shape))
                                    # dcov.append(np.outer(rdiff, rdiff))
                                else:
                                    train[cont] += rtrain
                                    train_2[cont] += rtrain_2
                                    test[cont] += rtest
                                    test_2[cont] += rtest_2
                                    diffs[cont] += rdiff
                                    diffs_2[cont] += rdiff_2
                                    corr[cont] += rcorr
                                    ndata[cont] += np.prod(tr.shape)
                                    # dcov[cont] += np.outer(rdiff, rdiff)
                                cont += 1

    print('File', file)
    corr_coef = []
    corr_coef2 = []
    for cont in range(len(train)):
        train[cont] /= ndata[cont]
        train_2[cont] /= ndata[cont]
        test[cont] /= ndata[cont]
        test_2[cont] /= ndata[cont]
        diffs[cont] /= ndata[cont]
        diffs_2[cont] /= ndata[cont]
        desv_train = np.sqrt(train_2[cont] - train[cont]*train[cont])
        desv_test = np.sqrt(test_2[cont] - test[cont]*test[cont])
        desv_diffs = np.sqrt(diffs_2[cont] - diffs[cont]*diffs[cont])
        corr[cont] /= ndata[cont]
        corr_coef.append( (corr[cont] - train[cont] * test[cont])/(desv_train*desv_test) )
        corr_coef2.append((corr[cont]) / (np.sqrt(train_2[cont]) * np.sqrt(test_2[cont])))
    print('Correlation =', corr_coef)
    print('diff mean', diffs[cont])
    print('diff stdev', desv_diffs)
    print('')
