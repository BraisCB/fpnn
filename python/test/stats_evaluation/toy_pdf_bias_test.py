import numpy as np
from python.fpnn import FpNN
from tensorflow.examples.tutorials.mnist import input_data
from sklearn import decomposition
from matplotlib import pyplot as pl
import scipy.stats

n_components = 20

mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
pca = decomposition.PCA(n_components=n_components)

type_to_analyze = 'relu'

files = [
    'toy_test_' + type_to_analyze + '_' + str(n_components) + '_gaussian_noise_09.json',
    'toy_test_' + type_to_analyze + '_' + str(n_components) + '_gaussian_noise_075.json',
    'toy_test_' + type_to_analyze + '_' + str(n_components) + '_gaussian_noise_05.json',
    'toy_test_' + type_to_analyze + '_' + str(n_components) + '_gaussian_noise_025.json'
]

legends = [
    '$GN(p = 0.1)$',
    '$GN(p = 0.25)$',
    '$GN(p = 0.5)$',
    '$GN(p = 0.75)$'
]

# files = [
#     'toy_test_' + type_to_analyze + '_' + str(n_components) + '_0005.json',
#     'toy_test_' + type_to_analyze + '_' + str(n_components) + '_005.json',
#     'toy_test_' + type_to_analyze + '_' + str(n_components) + '_01.json',
#     'toy_test_' + type_to_analyze + '_' + str(n_components) + '_015.json',
#     'toy_test_' + type_to_analyze + '_' + str(n_components) + '_02.json'
# ]
#
# legends = [
#     'b = 0.005',
#     'b = 0.05',
#     'b = 0.1',
#     'b = 0.15',
#     'b = 0.2'
# ]

batch_size = 128
treps = 1

source = mnist.train
pca.fit(source.images)
data = pca.transform(source.images)
# data = source.images
mean = np.mean(data, axis=0)
desv = np.std(data, axis=0)
data = (data - mean) / desv
label = source.labels

train = {}

cont_p = 2


perm = np.arange(len(label))

cont_cnn = 0
for file in files:
    cnn = FpNN.load_from_file('./models/toy_test_tanh/' + file)
    cnn.build(trainable=False)
    feed_dict = cnn.get_feed_dict(is_training=False)
    np.random.shuffle(perm)
    index = 0
    while index < len(data):
        new_index = min(index + batch_size, len(data))
        data_batch = data[perm[index:new_index]]
        label_batch = label[perm[index:new_index]]
        index = new_index
        feed_dict[cnn.model[0]['operation']] = data_batch
        feed_dict[cnn.results[-1]] = label_batch

        cont = 0
        for j in range(len(cnn.model)):
            model = cnn.model[j]
            if j < len(cnn.model) - 1 and cnn.model[j+1]['type'] == type_to_analyze:
                if cont == cont_p:
                    res = model['operation'].eval(feed_dict=feed_dict)
                    if cont_cnn in train:
                        train[cont_cnn] += res.flatten().tolist()
                    else:
                        train[cont_cnn] = res.flatten().tolist()
                    break
                cont += 1
            elif 'block' in model['type']:
                for k in range(len(model['block_operations']) - 1):
                    block_model = model['block_operations'][k]
                    if model['block_operations'][k + 1]['type'] == type_to_analyze:
                        if cont == cont_p:
                            res = block_model['operation'].eval(feed_dict=feed_dict)
                            if cont_cnn in train:
                                train[cont_cnn] += res.flatten().tolist()
                            else:
                                train[cont_cnn] = res.flatten().tolist()
                            break
                        cont += 1
    cont_cnn = cont_cnn + 1


print('train', len(train))
pl.figure()
for cont in train:
    train_hist, train_bins = np.histogram(train[cont], bins=200, normed=True)
    train_bin_centers = (train_bins[1:] + train_bins[:-1]) * 0.5
    #train_hist /= train_hist.sum()
    pl.plot(train_bin_centers, train_hist)

pl.title(type_to_analyze + ' layer #' + str(cont_p))
pl.legend(legends, loc='best')
pl.xlim((-10, 10))
pl.savefig('./images/mnist_' + type_to_analyze + '_gaussian_noise_layer ' + str(cont_p) + '.eps', bbox_inches='tight')
