import numpy as np
from python.fpnn import FpNN
from tensorflow.examples.tutorials.mnist import input_data
from sklearn import decomposition
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.svm import SVC

n_components = 20

mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
pca = decomposition.PCA(n_components=n_components)

cnn = FpNN.load_from_file('./models/mnist/toy_test_gn_12_layer.json')
cnn.build(trainable=False)

layers = cnn.layers

batch_size = 50

source = mnist.train
data = pca.fit_transform(source.images)
labels = source.labels

cnn_data = []
index = 0
feed_dict_testing = cnn.get_feed_dict(is_training=False)
while index < len(data):
    new_index = min(index + batch_size, len(data))
    feed_dict_testing[cnn.model[0]['operation']] = data[index:new_index]
    cnn_data += cnn.model[-1]['operation'].eval(feed_dict=feed_dict_testing).tolist()
    index = new_index
cnn_data = np.array(cnn_data)

test_source = mnist.test
test_data = pca.transform(test_source.images)
#test_data = (test_data - mean) / desv
test_labels = test_source.labels

cnn_test_data = []
index = 0
while index < len(test_data):
    new_index = min(index + batch_size, len(test_data))
    feed_dict_testing[cnn.model[0]['operation']] = test_data[index:new_index]
    cnn_test_data += cnn.model[-1]['operation'].eval(feed_dict=feed_dict_testing).tolist()
    index = new_index
cnn_test_data = np.array(cnn_test_data)

# clf = LinearDiscriminantAnalysis(solver='svd', n_components=9)
clf = SVC()

print('training model')
clf.fit(cnn_data, labels)
print('model trained')

predicted_train = clf.predict(cnn_data)

precision_train = (predicted_train == labels).sum() / len(predicted_train)
print('training precision =', precision_train)

predicted_test = clf.predict(cnn_test_data)
precision_test = (predicted_test == test_labels).sum() / len(predicted_test)
print('test precision =', precision_test)

