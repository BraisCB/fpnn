from python.fpnn import FpNN
from tensorflow.examples.tutorials.mnist import input_data
from python.train.vector_utils import get_basis, label_to_basis
from python.train.frontprop_initializer import init_model
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
import tensorflow as tf


mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)

batch_size = 150

n_components = 28*28

source = mnist.train
labels = source.labels

test_source = mnist.test

# pca = decomposition.PCA(n_components=n_components)
# pca.fit(source.images)
# data = pca.transform(source.images)
data = source.images
# mean = np.mean(data, axis=0)
# desv = np.std(data, axis=0)
# desv[desv == 0] = 1.0
# data = (data - mean) / desv

#test_data = pca.transform(test_source.images)
test_data = test_source.images
# test_data = (test_data - mean) / desv
test_labels = test_source.labels

input_size = [n_components]
#gain = 5.0
#basis = get_basis(10, 10, 'orthogonal', gain).tolist()
# layers = [
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'opts': {'shape': [50]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}},
#             {'type': 'relu'},
#             #{'type': 'leaky_relu', 'opts': {'alpha': 0.2}}
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'opts': {'shape': [70]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}},
#             {'type': 'relu'},
#             #{'type': 'leaky_relu', 'opts': {'alpha': 0.2}}
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'opts': {'shape': [50]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}},
#             {'type': 'relu'},
#         ]
#     },
#     {
#         'type': 'output',
#         'layers': [
#             {'type': 'matmul', 'opts': {'shape': [10]}},
#             #{'type': 'batch_normalization'},
#             #{'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}},
#             #{'type': 'relu'},
#             {'type': 'tile', 'opts': {'multiples': [1, 10]}},
#             {'type': 'reshape', 'opts': {'shape': [10, 10]}},
#             {'type': 'subtract', 'opts': {'y': basis}},
#             {'type': 'square'},
#             {'type': 'reduce_sum', 'opts': {'axis': -2}},
#             {'type': 'multiply', 'opts': {'y': -1}},
#             {'type': 'softmax'}
#         ]
#     },
#
# ]
dim = 50
basis = get_basis(dim, 11, 'orthogonal', 1.0).tolist()
bias = 11*[0]
layers = [
    {'type': 'batch_normalization'},
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [50]}},
            {'type': 'batch_normalization'},
            #{'type': 'gaussian_noise', 'data': {'keep_prob': 0.75}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [70]}},
            {'type': 'batch_normalization'},
            #{'type': 'gaussian_noise', 'data': {'keep_prob': 0.75}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [50]}},
            {'type': 'batch_normalization'},
            #{'type': 'gaussian_noise', 'data': {'keep_prob': 0.75}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'variables': {'trainable': False},
                'data': {
                    'weights': basis,
                    'bias': bias,
                    'shape': [11]
                }
            },
            # {'type': 'l2_normalize'},
            {'type': 'orthogonal_plane', 'data': {'C': 100.0, 'R': 4.0}}
            # {'type': 'tile', 'opts': {'multiples': [1, 11]}},
            # {'type': 'reshape', 'opts': {'shape': [11, 50]}},
            # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
            # {'type': 'square'},
            # {'type': 'reduce_sum', 'opts': {'axis': -1}},
            # {'type': 'distance'}
        ]
    }
]


# layers = [
#     {'type': 'reshape', 'opts': {'shape': [28, 28, 1]}},
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'conv2d', 'opts': {'shape': [5, 5, 3]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
#             {'type': 'relu'},
#             {'type': 'max_pool', 'opts': {'ksize': [2, 2], 'strides': [2, 2]}},
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'conv2d', 'opts': {'shape': [5, 5, 6]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
#             {'type': 'relu'},
#             {'type': 'max_pool', 'opts': {'ksize': [2, 2], 'strides': [2, 2]}},
#             {'type': 'reshape', 'opts': {'shape': [7*7*6]}},
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'opts': {'shape': [dim]}},
#             {'type': 'batch_normalization'},
#             {'type': 'gaussian_noise', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}},
#             {'type': 'relu'},
#         ]
#     },
#     {
#         'type': 'output',
#         'layers': [
#             {
#                 'type': 'matmul', 'opts': {'shape': [11]},
#                 'variables': {'trainable': False},
#                 'data': {
#                     'weights': basis,
#                     'bias': bias
#                 }
#             },
#             {'type': 'softmax'}
#             # {'type': 'tile', 'opts': {'multiples': [1, 11]}},
#             # {'type': 'reshape', 'opts': {'shape': [11, 50]}},
#             # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
#             # {'type': 'square'},
#             # {'type': 'reduce_sum', 'opts': {'axis': -1}},
#             # {'type': 'distance'}
#         ]
#     }
# ]


classifier_layers = layers[-1:]

global_opts = {
    'train': {'learning_rate': 1e-4, 'learning_decay': {'epochs': 40, 'factor': 0.5}},
    'variables': {'l2_loss': {'lambda': 100}}
}

fpnn_data = {
    'input_size': input_size,
    'model_type': 'classification',
    'global_opts': global_opts,
    'framework': 'tensorflow'
}

# layers = init_model(fpnn_data, layers, data, labels, limit_index=1)

deep_features = []

for i in range(9, -1, -1):
    print('label', i, 'nelements', (labels == i).sum())
    model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
    # model.build()
    # model.extract_data()
    # old_layers = deepcopy(model.layers)
    train_data = data[labels == i]
    train_labels = labels[labels == i]
    extra_data = data[labels != i]
    extra_labels = labels[labels != i]
    # extra_labels = np.array([10] * len(labels[labels != i]))
    # index_extra = np.where(labels != i)[0]
    # n_data = len(train_data)
    # n_extra = len(index_extra)
    # factor = int(n_extra/n_data)
    # np.random.shuffle(index_extra)
    # train_data = np.array(train_data.tolist() * factor + data[index_extra].tolist())
    # train_labels = np.array(train_labels.tolist() * factor + [10] * n_extra)
    # train_labels = np.array(train_labels.tolist() + labels[index_extra[:ndata]].tolist())
    # noise_input = np.random.randn(train_data.shape[0], train_data.shape[1])
    # noise_input = (noise_input - np.mean(noise_input, axis=0)) / np.std(noise_input, axis=0)
    # labels_input = [10] * train_data.shape[0]
    # train_data = np.array(train_data.tolist() + noise_input.tolist())
    # train_labels = np.array(train_labels.tolist() + labels_input)

    stats = model.fit(train_data, train_labels, test_data[test_labels == i], test_labels[test_labels == i],
                      epochs=20000, batch_size=batch_size, keep_variables=False,
                      extra_data=extra_data, extra_label=extra_labels)

    deep_features.append(model.get_deep_features(test_data))
    # sum_scores = np.matmul(deep_features[-1], np.array(basis)[:,:-1])
    # print('scores', model.eval(data[:1]))
    # print('probs', sum_scores[0])
    # print('max', np.max(sum_scores, axis=0))
    # print('min', np.min(sum_scores, axis=0))
    # print('loss', model.eval(train_data, train_labels, output_type='loss'))
    del model



deep_features = np.array(deep_features)
sum_deep_features = np.sum(deep_features, axis=0)


model = FpNN(input_size=[dim], layers=deepcopy(classifier_layers), global_opts=global_opts)
sum_scores = model.eval(sum_deep_features, output_type='probabilities')[-1, :, :-1]

specific_scores = np.zeros(sum_scores.shape)
for i in range(10):
    specific_scores[:,i] = model.eval(deep_features[i], output_type='probabilities')[-1, :, i]

# sum_scores = np.matmul(sum_deep_features, np.array(basis)[:, :-1])
#
# specific_scores = np.zeros(sum_scores.shape)
# for i in range(10):
#     specific_scores[:,i] = np.matmul(deep_features[i], np.array(basis)[:, i])

sum_labels = np.argmax(sum_scores, axis=-1)
specific_labels = np.argmax(specific_scores, axis=-1)

print('sum accuracy', (test_labels == sum_labels).sum()/len(test_labels))
print('specific accuracy', (test_labels == specific_labels).sum()/len(test_labels))

sum_labels = np.argmax(sum_scores*sum_scores, axis=-1)
specific_labels = np.argmax(specific_scores*specific_scores, axis=-1)

print('square sum accuracy', (test_labels == sum_labels).sum()/len(test_labels))
print('square specific accuracy', (test_labels == specific_labels).sum()/len(test_labels))