from python.fpnn import FpNN
from python.dataset_scripts.CIFAR_100 import load_data
from sklearn import decomposition
import numpy as np

# 76.29
cifar_10 = load_data("./datasets/cifar-100/", flip_left_right=False)

batch_size = 50

n_components = 32*32*3

source = cifar_10['train']
labels = source['label']

test_source = cifar_10['test']

#pca = decomposition.PCA(n_components=n_components)
#pca.fit(source.images)
#data = pca.transform(source.images)
data = source['data']
#mean = np.mean(data, axis=0)
#desv = np.std(data, axis=0)
#data = (data - mean) / desv

#test_data = pca.transform(test_source.images)
test_data = test_source['data']
#test_data = (test_data - mean) / desv
test_labels = test_source['label']

input_size = [32, 32, 3]

# layers = [
#     {'type': 'random_flip_left_right'},
#     {'type': 'reshape', 'opts': {'shape': [n_components]}},
#     #{'type': 'batch_normalization', 'opts': {'shape': [n_components]}},
#     #{'type': 'dropout', 'opts': {'keep_prob': 0.9}},
#     {'type': 'block', 'layers': [
#                                     {'type': 'matmul', 'opts': {'shape': [n_components, 50]}},
#                                     {'type': 'global_normalization', 'opts': {'shape': [50]}},
#                                     {'type': 'dropout', 'opts': {'keep_prob': 0.8}},
#                                     {'type': 'tanh'},
#                                 ]
#     },
#     {'type': 'block', 'layers': [
#                                     {'type': 'matmul', 'opts': {'shape': [50, 70]}},
#                                     {'type': 'global_normalization', 'opts': {'shape': [70]}},
#                                     {'type': 'dropout', 'opts': {'keep_prob': 0.8}},
#                                     {'type': 'tanh'},
#                                 ]
#     },
#     {'type': 'block', 'layers': [
#                                     {'type': 'matmul', 'opts': {'shape': [70, 50]}},
#                                     {'type': 'global_normalization', 'opts': {'shape': [50]}},
#                                     {'type': 'dropout', 'opts': {'keep_prob': 0.8}},
#                                     {'type': 'tanh'},
#                                 ]
#     },
#     {'type': 'softmax', 'layers': [
#         {'type': 'matmul', 'opts': {'shape': [50, 10]}}
#     ], 'opts': {'fine_tuning': False, 'epochs': 200000, 'batch_size': batch_size}}
# ]

keep_prob = 0.9
opts = [
    # ('toy_test_bn_gaussian_dropout_09',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_075',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.75}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_05',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.5}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_025',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.25}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_none_09',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_none_075',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.75}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_none_05',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_none_025',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.25}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_sqrt_09',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_sqrt_075',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.75}, 'opts': {'factor': 'sqrt'}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_sqrt_05',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': 'sqrt'}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_sqrt_025',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.25}, 'opts': {'factor': 'sqrt'}}
    #  ),
    # ('toy_test_bn_dropout_none_09',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_dropout_none_075',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.75}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_dropout_none_05',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_dropout_none_025',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.25}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_dropout_09',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.9}}
    #  ),
    # ('toy_test_bn_dropout_075',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.75}}
    #  ),
    # ('toy_test_bn_dropout_05',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.5}}
    #  ),
    # ('toy_test_bn_dropout_025',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.25}}
    #  ),
    ('toy_test_bn_dropout_sqrt_09',
     {'type': 'dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}}
     ),
    ('toy_test_bn_dropout_sqrt_075',
     {'type': 'dropout', 'data': {'keep_prob': 0.75}, 'opts': {'factor': 'sqrt'}}
     ),
    ('toy_test_bn_dropout_sqrt_05',
     {'type': 'dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': 'sqrt'}}
     ),
    ('toy_test_bn_dropout_sqrt_025',
     {'type': 'dropout', 'data': {'keep_prob': 0.25}, 'opts': {'factor': 'sqrt'}}
     ),
]


for opt in opts:
    #drop_layer = opt[1]
    print(opt[0])
    drop_file = './models/toy-cifar-100/' + opt[0] + '.json'
    model = FpNN.load_from_file(drop_file)
    # new_layers = []
    # for layer in model.layers:
    #     if layer['type'] in ['block', 'softmax']:
    #         new_layer = {'type': 'block', 'layers': []}
    #         for ilayer in layer['layers']:
    #             if ilayer['type'] == 'conv2d':
    #                 ilayer['opts']['shape'] = ilayer['opts']['shape'][:2] + ilayer['opts']['shape'][-1:]
    #             elif ilayer['type'] == 'matmul':
    #                 ilayer['opts']['shape'] = ilayer['opts']['shape'][-1:]
    #             elif ilayer['type'] == 'max_pool':
    #                 ilayer['opts']['ksize'] = ilayer['opts']['ksize'][1:-1]
    #                 ilayer['opts']['strides'] = ilayer['opts']['strides'][1:-1]
    #             new_layer['layers'].append(ilayer)
    #         new_layers.append(new_layer)
    #         if layer['type'] in ['softmax']:
    #             new_layers.append({'type': 'softmax'})
    # model.layers = new_layers

    print("test accuracy %g" % model.eval(test_data, test_labels)[0])
    del model
