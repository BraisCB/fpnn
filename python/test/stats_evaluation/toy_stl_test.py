from python.fpnn import FpNN
from python.dataset_scripts.STL_10 import load_data
import json

# 76.29
stl_10 = load_data("./datasets/stl-10/")

batch_size = 50

n_components = 32*32*3

source = stl_10['train']
labels = source['label']

test_source = stl_10['test']

#pca = decomposition.PCA(n_components=n_components)
#pca.fit(source.images)
#data = pca.transform(source.images)
data = source['data']
#mean = np.mean(data, axis=0)
#desv = np.std(data, axis=0)
#data = (data - mean) / desv

#test_data = pca.transform(test_source.images)
test_data = test_source['data']
#test_data = (test_data - mean) / desv
test_labels = test_source['label']

input_size = [96, 96, 3]

keep_prob = 0.9
opts = [
    # ('toy_test_bn_gaussian_dropout_09',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_075',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.75}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_05',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.5}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_025',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.25}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_none_09',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_none_075',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.75}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_none_05',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_gaussian_dropout_none_025',
    #  {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.25}, 'opts': {'factor': None}}
    #  ),
    ('toy_test_bn_gaussian_dropout_sqrt_09',
     {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}}
     ),
    ('toy_test_bn_gaussian_dropout_sqrt_075',
     {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.75}, 'opts': {'factor': 'sqrt'}}
     ),
    ('toy_test_bn_gaussian_dropout_sqrt_05',
     {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': 'sqrt'}}
     ),
    ('toy_test_bn_gaussian_dropout_sqrt_025',
     {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.25}, 'opts': {'factor': 'sqrt'}}
     ),
    # ('toy_test_bn_dropout_none_09',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_dropout_none_075',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.75}, 'opts': {'factor': None}}
    #  ),
    # ('toy_test_bn_dropout_none_05',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': None}}
    #  ),
    ('toy_test_bn_dropout_none_025',
     {'type': 'dropout', 'data': {'keep_prob': 0.25}, 'opts': {'factor': None}}
     ),
    # ('toy_test_bn_dropout_09',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.9}}
    #  ),
    # ('toy_test_bn_dropout_075',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.75}}
    #  ),
    # ('toy_test_bn_dropout_05',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.5}}
    #  ),
    ('toy_test_bn_dropout_025',
     {'type': 'dropout', 'data': {'keep_prob': 0.25}}
     ),
    # ('toy_test_bn_dropout_sqrt_09',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.9}, 'opts': {'factor': 'sqrt'}}
    #  ),
    # ('toy_test_bn_dropout_sqrt_075',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.75}, 'opts': {'factor': 'sqrt'}}
    #  ),
    # ('toy_test_bn_dropout_sqrt_05',
    #  {'type': 'dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': 'sqrt'}}
    #  ),
    ('toy_test_bn_dropout_sqrt_025',
     {'type': 'dropout', 'data': {'keep_prob': 0.25}, 'opts': {'factor': 'sqrt'}}
     ),
]

opts = [
    ('test_gaussian_dropout_095',
     {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.95}}
     ),
    ('test_gaussian_dropout_none_095',
     {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.95}, 'opts': {'factor': None}}
     ),
    ('test_gaussian_dropout_sqrt_095',
     {'type': 'gaussian_dropout', 'data': {'keep_prob': 0.95}, 'opts': {'factor': 'sqrt'}}
     ),
    ('test_dropout_none_095',
     {'type': 'dropout', 'data': {'keep_prob': 0.95}, 'opts': {'factor': None}}
     ),
    ('test_dropout_095',
     {'type': 'dropout', 'data': {'keep_prob': 0.95}}
     ),
    ('test_dropout_sqrt_095',
     {'type': 'dropout', 'data': {'keep_prob': 0.95}, 'opts': {'factor': 'sqrt'}}
     )
]

for opt in opts:
    dropout_layer = opt[1]
    print(opt[0])
    drop_file = './models/stl-10/' + opt[0] + '.json'
    info_file = './models/stl-10/' + opt[0] + '_info.json'
    layers = [
        {'type': 'random_flip_left_right'},
        {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
        {'type': 'batch_normalization'},
        dropout_layer,
        {'type': 'relu'},
        {'type': 'conv2d', 'data': {'shape': [3, 3, 64]}},
        {'type': 'batch_normalization'},
        dropout_layer,
        {'type': 'relu'},
        {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
        {'type': 'conv2d', 'data': {'shape': [3, 3, 128]}},
        {'type': 'batch_normalization'},
        dropout_layer,
        {'type': 'relu'},
        {'type': 'conv2d', 'data': {'shape': [3, 3, 128]}},
        {'type': 'batch_normalization'},
        dropout_layer,
        {'type': 'relu'},
        {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
        {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
        {'type': 'batch_normalization'},
        dropout_layer,
        {'type': 'relu'},
        {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
        {'type': 'batch_normalization'},
        dropout_layer,
        {'type': 'relu'},
        # {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
        # {'type': 'batch_normalization'},
        # dropout_layer,
        # {'type': 'relu'},
        # {'type': 'conv2d', 'data': {'shape': [3, 3, 256]}},
        # {'type': 'batch_normalization'},
        # dropout_layer,
        # {'type': 'relu'},
        {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
        {'type': 'conv2d', 'data': {'shape': [3, 3, 1024], 'padding': 'VALID'}},
        {'type': 'batch_normalization'},
        dropout_layer,
        {'type': 'relu'},
        {'type': 'output', 'layers': [
            {'type': 'conv2d', 'data': {'shape': [1, 1, 1024], 'padding': 'VALID'}},
            {'type': 'batch_normalization'},
            dropout_layer,
            {'type': 'relu'},
            {'type': 'conv2d', 'data': {'shape': [1, 1, 10], 'padding': 'VALID'}},
            {'type': 'batch_normalization'},
            dropout_layer,
            {'type': 'relu'},
            {'type': 'avg_pool', 'data': {'ksize': [2, 2], 'padding': 'VALID'}},
            {'type': 'reshape', 'data': {'shape': [10]}},
            {'type': 'softmax'}
        ]}
    ]

    global_opts = {
        'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
    }

    model = FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
    #model.build()
    #model.extract_data()

    # model.frontprop_train(data, labels, test_data, test_labels)
    info = model.fit(data, labels, test_data, test_labels, epochs=12000, batch_size=batch_size)
    model.save_to_file(drop_file)

    with open(info_file, 'w') as outfile:
        json.dump(info, outfile)
