from python.fpnn import FpNN
from python.dataset_scripts.nips_challenge import load_data
import json
import numpy as np
from python.train.feature_selection import get_relevant_features
from copy import deepcopy


def balance_data(data, labels):
    new_data = data.copy()
    new_labels = labels.copy()
    balance = int(np.round(len(labels[labels == 1]) / len(labels[labels == 0])))
    if balance > 1:
        print('augmenting neg values by', balance)
        data_pos = data[labels == 1]
        data_neg = np.tile(data[labels == 0], (balance, 1))
        label_pos = labels[labels == 1]
        label_neg = np.tile(labels[labels == 0], (balance))
        new_data = np.concatenate((data_pos, data_neg), axis=0)
        new_labels = np.concatenate((label_pos, label_neg), axis=0)
    elif balance == 0:
        balance = int(np.round(len(labels[labels == 0]) / len(labels[labels == 1])))
        if balance > 1:
            print('augmenting pos values by', balance)
            data_pos = np.tile(data[labels == 1], (balance, 1))
            data_neg = data[labels == 0]
            label_pos = np.tile(labels[labels == 1], (balance))
            label_neg = labels[labels == 0]
            new_data = np.concatenate((data_pos, data_neg), axis=0)
            new_labels = np.concatenate((label_pos, label_neg), axis=0)
    return new_data, new_labels


stats = []
keep_prob = 0.9
reps = 5
k_folds = 10

datasets = [
    'cina0',
    'cina1',
    'cina2',
    'marti0',
    'marti1',
    'marti2',
    'reged0',
    'reged1',
    'reged2',
    'sido0',
    'sido1',
    'sido2',
]

layers = [
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [300]}},
            {'type': 'batch_normalization'},
            {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [225]}},
            {'type': 'batch_normalization'},
            {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [150]}},
            {'type': 'batch_normalization'},
            {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [75]}},
            {'type': 'batch_normalization'},
            {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [2]
                }
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

for dataset in datasets:

    stats.append({
        'features': [],
        'rank': [],
        'score': 0.0
    })

    print('loading dataset', dataset)
    gisette = load_data('./datasets/' + dataset + '_text/' + dataset)
    print('data loaded. labels =', gisette['train']['data'].shape)

    batch_size = min(100, gisette['train']['data'].shape[0])
    epochs = (int(gisette['train']['data'].shape[0]/batch_size) + 1)*60

    source = gisette['train']
    test_source = gisette['test']

    train_labels = source['label']

    train_data = source['data']
    test_data = test_source['data']
    # mean_data = np.mean(data, axis=0)
    # std_data = np.std(data, axis=0)
    # std_data[std_data == 0.0] = 1e-8
    max_val = np.max(train_data, axis=0)
    print('max_val', max_val)
    non_zeros = np.where(max_val != 0.0)[0]
    if len(non_zeros) != train_data.shape[1]:
        print('Removing zeros')
        train_data = train_data[:, non_zeros]
        test_data = test_data[:, non_zeros]
        max_val = max_val[non_zeros]
    # data = (data - mean_data) / std_data
    train_data /= max_val
    train_data, train_labels = balance_data(train_data, train_labels)

    test_data /= max_val
    # test_data = (test_data - mean_data) / std_data

    n_components = gisette['train']['data'].shape[-1]
    input_size = [n_components]

    perm = np.random.permutation(len(labels))
    step = len(perm) / k_folds
    train_data = train_data[perm]
    train_labels = train_labels[perm]

    for k_fold in range(k_folds):
        start = int(k_fold * step)
        finish = int((k_fold + 1) * step)
        data = np.concatenate((train_data[:start], train_data[finish:]), axis=0)
        labels = np.concatenate((train_labels[:start], train_labels[finish:]), axis=0)
        valid_data = train_data[start:finish]
        valid_labels = train_labels[start:finish]

        feat_info = get_relevant_features(layers, global_opts, data, labels, valid_data, valid_labels,
                                          epochs=epochs, batch_size=batch_size, reps=reps)
        print(data.shape)
        data = data[:, feat_info['features']]
        valid_data = valid_data[:, feat_info['features']]
        test_data = test_data[:, feat_info['features']]
        best_score = 0
        for nr in (range(reps)):
            model = FpNN(input_size=list(data.shape[1:]), layers=deepcopy(layers), global_opts=global_opts)
            model.fit(data, labels, valid_data, valid_labels, epochs=epochs, batch_size=batch_size)
            n_components = data.shape[-1]
            step = max(int(n_components / 100), 1)
            mask = np.ones(n_components)
            for i in range(0, n_components, step):
                mask = np.ones(n_components)
                mask[:i] = 0.0
                mask = mask[::-1]
                nelements = n_components - i
                score = model.eval(valid_data * mask, valid_labels)[0]
                print('nelements', nelements, 'accuracy = ', score)
                if score > best_score:
                    best_prob = 2.0 * model.eval(test_data * mask, output_type='probabilities')[0, :, 1] - 1.0
                    best_score = score
                    feat_info['features'] = feat_info['features'][:nelements]

            del model

        np.savetxt('./models/feature_selection/' + dataset + '_test.predict', best_prob)
        feat_info['dataset'] = dataset
        if stats[-1]['score'] < feat_info['score']:
            stats[-1] = feat_info

with open('./models/feature_selection/info.json', 'w') as outfile:
    json.dump(stats, outfile)
