import numpy as np
from python.fpnn import FpNN
from tensorflow.examples.tutorials.mnist import input_data
from sklearn import decomposition
import matplotlib.pyplot as plt
from python.dataset_scripts.CIFAR_10 import load_data

cifar_10 = load_data("./datasets/cifar-10/", flip_left_right=False)

cnn = FpNN.load_from_file('./models/toy-cifar-10/toy_test_bn_dropout_09.json')
cnn.build(trainable=False)

layers = cnn.layers

batch_size = 128
treps = 50

evaluate = 'dropout'

source = cifar_10['train']
data = source['data']
# data = source.images
label = source['label']

diffs = []
test = []
train = []
diffs_2 = []
test_2 = []
train_2 = []
corr = []
dcov = []
ndata = 0

perm = np.arange(len(label))
feed_dict_training = cnn.get_feed_dict(is_training=True)
feed_dict_testing = cnn.get_feed_dict(is_training=False)

for i in range(treps):
    np.random.shuffle(perm)
    index = 0
    print(i, 'of', treps)
    while index < len(data):
        new_index = min(index + batch_size, len(data))
        data_batch = data[perm[index:new_index]]
        label_batch = label[perm[index:new_index]]
        index = new_index
        feed_dict_training[cnn.model[0]['operation']] = data_batch
        feed_dict_training[cnn.results[-1]] = label_batch
        feed_dict_testing[cnn.model[0]['operation']] = data_batch
        feed_dict_testing[cnn.results[-1]] = label_batch
        cont = 0
        ndata += len(data_batch)
        for r in range(len(cnn.model)):
            model = cnn.model[r]
            if r < len(cnn.model) - 1 and cnn.model[r + 1]['type'] == evaluate:
                tr = model['operation'].eval(feed_dict=feed_dict_training)
                te = model['operation'].eval(feed_dict=feed_dict_testing)
                di = tr - te
                rtrain = np.sum(tr, axis=0)
                rtest = np.sum(te, axis=0)
                rdiff = np.sum(di, axis=0)
                rtrain_2 = np.sum(tr*tr, axis=0)
                rtest_2 = np.sum(te*te, axis=0)
                rdiff_2 = np.sum(di*di, axis=0)
                rcorr = np.sum(tr*te, axis=0)
                if cont == len(diffs):
                    train.append(rtrain)
                    train_2.append(rtrain_2)
                    test.append(rtest)
                    test_2.append(rtest_2)
                    diffs.append(rdiff)
                    diffs_2.append(rdiff_2)
                    corr.append(rcorr)
                    dcov.append(np.outer(rdiff, rdiff))
                else:
                    train[cont] += rtrain
                    train_2[cont] += rtrain_2
                    test[cont] += rtest
                    test_2[cont] += rtest_2
                    diffs[cont] += rdiff
                    diffs_2[cont] += rdiff_2
                    corr[cont] += rcorr
                    dcov[cont] += np.outer(rdiff, rdiff)
                cont += 1
            elif 'block' in model['type']:
                for r in range(len(model['block_operations']) - 1):
                    if model['block_operations'][r+1]['type'] == evaluate:
                        block_model = model['block_operations'][r]
                        tr = block_model['operation'].eval(feed_dict=feed_dict_training)
                        te = block_model['operation'].eval(feed_dict=feed_dict_testing)
                        di = tr - te
                        rtrain = np.sum(tr)
                        rtest = np.sum(te, axis=0)
                        rdiff = np.sum(di, axis=0)
                        rtrain_2 = np.sum(tr * tr, axis=0)
                        rtest_2 = np.sum(te * te, axis=0)
                        rdiff_2 = np.sum(di * di, axis=0)
                        rcorr = np.sum(tr * te, axis=0)
                        if cont == len(diffs):
                            train.append(rtrain)
                            train_2.append(rtrain_2)
                            test.append(rtest)
                            test_2.append(rtest_2)
                            diffs.append(rdiff)
                            diffs_2.append(rdiff_2)
                            corr.append(rcorr)
                            dcov.append(np.outer(rdiff, rdiff))
                        else:
                            train[cont] += rtrain
                            train_2[cont] += rtrain_2
                            test[cont] += rtest
                            test_2[cont] += rtest_2
                            diffs[cont] += rdiff
                            diffs_2[cont] += rdiff_2
                            corr[cont] += rcorr
                            dcov[cont] += np.outer(rdiff, rdiff)
                        cont += 1

desv_train = []
desv_test = []
desv_diffs = []
mean_train = []
mean_desv_train = []
mean_test = []
mean_desv_test = []
mean_diffs = []
mean_desv_diffs = []
mean_corr = []
desv_corr = []
cov_diffs = []

for cont in range(len(train)):
    train[cont] /= ndata
    train_2[cont] /= ndata
    test[cont] /= ndata
    test_2[cont] /= ndata
    diffs[cont] /= ndata
    diffs_2[cont] /= ndata
    dcov[cont] /= ndata
    desv_train.append(np.sqrt(train_2[cont] - train[cont]*train[cont]))
    desv_test.append(np.sqrt(test_2[cont] - test[cont]*test[cont]))
    desv_diffs.append(np.sqrt(diffs_2[cont] - diffs[cont]*diffs[cont]))
    cov_diffs.append(dcov[cont] - np.outer(diffs[cont], diffs[cont]))
    dims = len(train[cont].shape)
    corr[cont] /= ndata
    diag = np.sqrt(np.diag(cov_diffs[cont]))
    cov_diffs[cont] /= np.outer(diag, diag)
    desv_corr.append((corr[cont] - train[cont] * test[cont])/(desv_train[cont]*desv_test[cont]))
    if dims == 1:
        mean_train.append(train[cont])
        mean_desv_train.append(desv_train[cont])
        mean_test.append(test[cont])
        mean_desv_test.append(desv_test[cont])
        mean_diffs.append(diffs[cont])
        mean_desv_diffs.append(desv_diffs[cont])
        mean_corr.append(desv_corr[cont])
    else:
        mean_train.append(np.mean(train[cont],axis=tuple(range(0, dims-1))))
        mean_desv_train.append(np.mean(desv_train[cont],axis=tuple(range(0, dims-1))))
        mean_test.append(np.mean(test[cont],axis=tuple(range(0, dims-1))))
        mean_desv_test.append(np.mean(desv_test[cont],axis=tuple(range(0, dims-1))))
        mean_diffs.append(np.mean(diffs[cont],axis=tuple(range(0, dims-1))))
        mean_desv_diffs.append(np.mean(desv_diffs[cont],axis=tuple(range(0, dims-1))))
        mean_corr.append(np.mean(desv_corr[cont], axis=tuple(range(0, dims - 1))))
    print('')
    print('Layer', cont)
    print('Test distribution mean =', mean_test[cont], ', desv =', mean_desv_test[cont])
    print('Train distribution mean =', mean_train[cont], ', desv =', mean_desv_train[cont])
    print('Diff distribution mean =', mean_diffs[cont], ', desv =', mean_desv_diffs[cont])
    print('Correlation =', mean_corr[cont])
    print('Correlation matrix =', cov_diffs[cont])
    print('')
    plt.imshow(cov_diffs[cont]);
    plt.colorbar()
    plt.show()