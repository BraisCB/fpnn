from tensorflow.examples.tutorials.mnist import input_data
from sklearn.mixture import GaussianMixture
from scipy.spatial.distance import cdist
from sklearn.cluster import KMeans
import numpy as np


mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

data_0 = mnist.train.images[mnist.train.labels == 1]
tdata_0 = mnist.test.images[mnist.test.labels == 1]

# gmm = GaussianMixture(n_components=10, covariance_type='full', tol=0.001,
#                      reg_covar=1e-06, max_iter=1000, n_init=1, init_params='kmeans',
#                      weights_init=None, means_init=None, precisions_init=None,
#                      random_state=None, warm_start=False, verbose=0, verbose_interval=10)

gmm = KMeans(n_clusters=10, init='k-means++', n_init=10, max_iter=300, tol=0.0001,
             precompute_distances='auto', verbose=0, random_state=None, copy_x=True,
             n_jobs=1, algorithm='auto')

gmm.fit(data_0)

pred_label = gmm.predict(tdata_0)

for i in range(10):
    print('cluster', i, 'n =', np.sum(pred_label == i))

dist = cdist(gmm.cluster_centers_, gmm.cluster_centers_)
print(dist)

print(pred_label)
