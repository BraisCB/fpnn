import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from sklearn.neighbors import KernelDensity
from python import fpnn

from python.dataset_scripts.CIFAR_10 import load_data
from python.train.classification_utils import ClassificationTraining

input_size = [32, 32, 3]
cifar_10 = load_data("./datasets/cifar-10/", flip_left_right=False)

ct = ClassificationTraining()

layers = [
    {'type': 'random_flip_left_right'},
]

global_opts = {
    # 'variables': {'weight_decay': 1e-4},
    'backprop_train': {#'type': 'momentum', 'use_nesterov': True,
                       'learning_decay': {'epochs': 40, 'factor': 0.5}},
                       #'learning_rate': 0.1},
    'frontprop_train': {'flips': ['left_right']}
}

model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)
model.build(trainable=False)

mean = ct.get_kde_mean(model.model, model.get_feed_dict(is_training=False, backprop_train=True), {}, cifar_10['test']['data'], cifar_10['test']['label'], {})

print(mean)