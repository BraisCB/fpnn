from python import fpnn
from python.dataset_scripts.CIFAR_10 import load_data

# 76.29
cifar_10 = load_data("./datasets/cifar-10/", flip_left_right=False)


input_size = [32, 32, 3]

layers = [
    {'type': 'random_flip_left_right'},
    {'type': 'batch_normalization', 'opts': {'shape': [32, 32, 3]}},
    {'type': 'gaussian_dropout', 'opts': {'keep_prob': 0.9}},
    {'type': 'block', 'layers': [
                                    {'type': 'conv2d', 'opts': {'shape': [5, 5, 3, 32]}},
                                    {'type': 'batch_normalization', 'opts': {'shape': [32, 32, 32]}},
                                    {'type': 'gaussian_dropout', 'opts': {'keep_prob': 0.9}},
                                    {'type': 'relu'},
                                    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
                                ]
    },
    {'type': 'block', 'layers': [
                                    {'type': 'conv2d', 'opts': {'shape': [5, 5, 32, 64]}},
                                    {'type': 'batch_normalization', 'opts': {'shape': [16, 16, 64]}},
                                    {'type': 'gaussian_dropout', 'opts': {'keep_prob': 0.9}},
                                    {'type': 'relu'},
                                    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
                                    {'type': 'reshape', 'opts': {'shape': [8*8*64]}},
                                ]
    },
    {'type': 'block', 'layers': [
                                    {'type': 'matmul', 'opts': {'shape': [8*8*64, 1024]}},
                                    {'type': 'batch_normalization', 'opts': {'shape': [1024]}},
                                    {'type': 'gaussian_dropout', 'opts': {'keep_prob': 0.9}},
                                    {'type': 'relu'},
                                ]
    },
    {'type': 'softmax', 'layers': [
        {'type': 'matmul', 'opts': {'shape': [1024, 10]}}
    ]}
]

global_opts = {
    # 'backprop_train': {'type': 'momentum', 'use_nesterov': True,
    #                    'learning_decay': {'epochs': 10, 'factor': 0.5}},
    'backprop_train': {'learning_rate': 5e-4, 'learning_decay': {'epochs': 25, 'factor': 0.75}},
    'frontprop_train': {}
}

cnn = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)

cnn.backprop_train(cifar_10['train']['data'], cifar_10['train']['label'],
                     cifar_10['test']['data'], cifar_10['test']['label'],
                     total_training=False, layer_by_layer=False, epochs=20000)

cnn.save_to_file('./models/cifar-10/cifar_10_test.json')
