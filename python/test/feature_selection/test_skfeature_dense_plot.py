import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.nips_challenge import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np
import os, errno
from matplotlib import pyplot as pl

# datasets = ['arcene', 'gisette', 'madelon', 'dexter', 'dorothea']

datasets = ['dorothea']

def main():
    # load data
    cont = 0
    for dataset in datasets:
        print(dataset)
        pl.figure(cont)
        with open('./python/test/feature_selection/dataset_ranking/' + dataset + '_results_dense.json') as datafile:
            results = json.load(datafile)
        methods = list(results.keys())
        for method in methods:
            keys = list(results[method]['accuracy'].keys())
            x_axis = np.sort([int(k) for k in keys])
            y_axis = np.zeros_like(x_axis).astype(np.float32)
            auc = 0
            y_axis[0] = results[method]['accuracy'][str(x_axis[0])]
            for i in range(1, len(x_axis)):
                y_axis[i] = results[method]['accuracy'][str(x_axis[i])]
                auc += 0.5 * (y_axis[i] + y_axis[i-1]) * (x_axis[i] - x_axis[i-1])
            auc /= np.max(x_axis)
            pl.plot(x_axis[:20], y_axis[:20])
            best = np.argmax(y_axis)
            print('method =', method, ', mean =', y_axis[best], ', nfeat =', x_axis[best], ', auc =', auc)
        pl.title(dataset)
        pl.legend(methods, loc='best')
        pl.show()
        cont += 1


if __name__ == '__main__':
    main()