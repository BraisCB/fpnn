from python.fpnn import FpNN
from python.dataset_scripts.nips_challenge import load_data
from python.train.saliency_map import get_saliency_map
from python.train.vector_utils import get_basis
import json
import numpy as np
from copy import deepcopy

datasets = [
    'arcene',
    'dexter',
    'dorothea',
    'gisette',
    'madelon'
]

stats = []
basis_type = 'eye'
keep_prob = 0.5

for dataset in datasets:

    print('loading dataset', dataset)
    gisette = load_data('./datasets/' + dataset + '/' + dataset)
    print('data loaded. labels =', gisette['train']['data'].shape)

    batch_size = min(100, gisette['train']['data'].shape[0])
    epochs = (int(gisette['train']['data'].shape[0]/batch_size) + 1)*60

    n_components = gisette['train']['data'].shape[-1]

    source = gisette['train']
    labels = source['label']

    test_source = gisette['validation']

    data = source['data']
    max_val = np.max(data)
    data /= max_val

    # balance = round(len(labels[labels == 1]) / len(labels[labels == 0]))
    # if balance > 1:
    #     print('augmenting neg values by', balance)
    #     data_pos = data[labels == 1]
    #     data_neg = np.tile(data[labels == 0], (balance, 1))
    #     label_pos = labels[labels == 1]
    #     label_neg = np.tile(labels[labels == 0], (balance))
    #     data = np.concatenate((data_pos, data_neg), axis=0)
    #     labels = np.concatenate((label_pos, label_neg), axis=0)
    # elif balance == 0:
    #     balance = round(len(labels[labels == 0]) / len(labels[labels == 1]))
    #     if balance > 1:
    #         print('augmenting pos values by', balance)
    #         data_pos = np.tile(data[labels == 1], (balance, 1))
    #         data_neg = data[labels == 0]
    #         label_pos = np.tile(labels[labels == 1], (balance))
    #         label_neg = labels[labels == 0]
    #         data = np.concatenate((data_pos, data_neg), axis=0)
    #         labels = np.concatenate((label_pos, label_neg), axis=0)

    # mean = np.mean(data, axis=0)
    # desv = np.std(data, axis=0)
    # desv[desv == 0.0] = 1e-8
    # data = (data - mean) / desv

    test_data = test_source['data']
    # test_data = (test_data - mean) / desv
    test_data /= max_val
    test_labels = test_source['label']

    input_size = [n_components]

    real_test_data = gisette['test']['data']
    real_test_data /= max_val

    best_indexes = np.arange(n_components).astype(int)
    indexes = best_indexes.copy()
    best_prob = None
    best_n_components = n_components
    best_score = 0
    best_features = None
    thresh = 0.1
    limit = int(n_components*0.01)
    print('limit =', limit)
    counter = 0

    basis = get_basis(75, 2, basis_type, 1.0).tolist()
    bias = 2 * [0]
    layers = [
        {'type': 'batch_normalization'},
        {
            'type': 'block',
            'layers': [
                {'type': 'matmul', 'data': {'shape': [300]}},
                {'type': 'batch_normalization'},
                {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                {'type': 'relu'},
            ]
        },
        {
            'type': 'block',
            'layers': [
                {'type': 'matmul', 'data': {'shape': [225]}},
                {'type': 'batch_normalization'},
                {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                {'type': 'relu'},
            ]
        },
        {
            'type': 'block',
            'layers': [
                {'type': 'matmul', 'data': {'shape': [150]}},
                {'type': 'batch_normalization'},
                {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                {'type': 'relu'},
            ]
        },
        {
            'type': 'block',
            'layers': [
                {'type': 'matmul', 'data': {'shape': [75]}},
                {'type': 'batch_normalization'},
                {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                {'type': 'relu'},
            ]
        },
        {
            'type': 'output',
            'layers': [
                {
                    'type': 'matmul',
                    'variables': {'trainable': False},
                    'data': {
                        'shape': [2],
                        'weights': basis,
                        'bias': bias
                    }
                },
                # {'type': 'tile', 'opts': {'multiples': [1, 10]}},
                # {'type': 'reshape', 'opts': {'shape': [10, 50]}},
                # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
                # {'type': 'square'},
                # {'type': 'reduce_sum', 'opts': {'axis': -1}},
                # {'type': 'multiply', 'opts': {'y': -1}},
                {'type': 'softmax', 'data': {'R': 0.0}}
            ]
        }
    ]

    global_opts = {
        'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
        # 'variables': {'l2_loss': {'lambda': 1e-2}}
    }

    fpnn_data = {
        'input_size': input_size,
        'model_type': 'classification',
        'global_opts': global_opts,
        'framework': 'tensorflow'
    }

    step = max(int(n_components / 100), 1)
    print('n_features =', n_components)
    print('best_score =', best_score)
    print('best_n_features =', best_n_components)
    input_size = [n_components]

    model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)

    info = model.fit(data, labels, test_data, test_labels, epochs=epochs, batch_size=batch_size)

    maps = []
    label_probs = 2.0 * model.eval(test_data, output_type='probabilities', is_training=True)[0, :, 1] - 1.0
    label_pred = label_probs.copy()
    label_pred[label_pred >= 0.0] = 1.0
    label_pred[label_pred < 0.0] = 0.0
    label_pred.astype(int)
    # label_pred = model.eval(test_data, output_type='labels')[0]
    for i in range(2):
        vector = 0.0 * np.ones(2)
        vector[i] = 1.0
        neg_vector = 0.0 * np.ones(2)
        neg_vector[i] = 1.0
        tp = np.where((test_labels == i) & (label_pred == i))[0]
        fp = np.where((test_labels != i) & (label_pred == i))[0]
        pos_saliency = np.zeros(n_components)
        if len(tp) > 0:
            pos_saliency = np.mean(
                np.abs(label_probs[tp])[:, None] * get_saliency_map(model, vector, data=test_data[tp]),
                axis=0
            )
            pos_saliency /= pos_saliency.sum() + 1e-6
        neg_saliency = np.zeros(n_components)
        if len(fp) > 0:
            neg_saliency = np.mean(
                np.abs(label_probs[fp])[:, None] * get_saliency_map(model, neg_vector, data=test_data[fp]),
                axis=0
            )
            neg_saliency /= neg_saliency.sum() + 1e-6
        im_map = pos_saliency - neg_saliency
        maps.append(im_map)

    maps = np.array(maps)
    max_maps = np.mean(maps, axis=0)
    init = int((max_maps < 0.0).sum())
    max_maps[max_maps < 0.0] = 0.0
    max_maps /= np.sum(max_maps)

    score_index = np.argsort(max_maps)
    cumscore = 1.0 - np.cumsum(max_maps[score_index])

    # if init > 0:
    #     print('found bad features', init)
    #     print('removing previous results')
    #     best_score = 0
    del model

    old_i = 0.0
    best_mean_score = 0.0
    ntries = 3
    for i in np.arange(0.025, 1.0, 0.025):
        updated = False
        feats = np.where(cumscore <= i)[0]
        new_data = data[:, feats]
        new_test_data = test_data[:, feats]
        new_real_test_data = real_test_data[:, feats]
        nelements = len(feats)
        mean_score = 0
        p_best_score = 0
        for j in range(ntries):
            print('best_score', best_score)
            print('best_mean_score', best_mean_score)
            print('best nfeats', best_n_components)
            model = FpNN(input_size=[nelements], layers=deepcopy(layers), global_opts=global_opts)

            model.fit(new_data, labels, new_test_data, test_labels, epochs=epochs, batch_size=batch_size, verbose=False)
            score = model.eval(new_test_data, test_labels)[0]
            mean_score += score
            print(i, 'nelements', nelements, 'explained', i, 'accuracy = ', score)
            if score > 1.005*p_best_score or (score > p_best_score and nelements == p_best_n_components):
                p_best_prob = 2.0 * model.eval(new_real_test_data, output_type='probabilities')[0, :, 1] - 1.0
                p_best_score = score
                p_best_n_components = nelements
                p_best_features = feats
                updated = True
            del model
        mean_score /= ntries
        if updated and mean_score > best_mean_score:
            best_mean_score = mean_score
            best_prob = p_best_prob
            best_score = p_best_score
            best_n_components = p_best_n_components
            best_features = p_best_features
        else:
            cumscore[(cumscore <= i) & (cumscore > old_i)] = 1.0
        old_i = i

    stats.append({
        'dataset': dataset,
        'nfeats': best_n_components,
        'val_score': best_score,
        'feats': best_features.tolist()
    })
    np.savetxt('./models/feature_selection/' + dataset + '_test.predict', best_prob)

with open('./models/feature_selection/info_' + basis_type + '.json', 'w') as outfile:
    json.dump(stats, outfile)
