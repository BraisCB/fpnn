from python.fpnn import FpNN
from python.dataset_scripts.nips_challenge import load_data
from python.train.feature_selection import get_features
from python.train.saliency_map import get_saliency_map
from python.train.vector_utils import get_basis
import json
import numpy as np
from copy import deepcopy

datasets = [
    'arcene',
    'dexter',
    'dorothea',
    'gisette',
    'madelon'
]

stats = []
basis_type = 'eye'
keep_prob = 0.5





for dataset in datasets:

    print('loading dataset', dataset)
    gisette = load_data('./datasets/' + dataset + '/' + dataset)
    print('data loaded. labels =', gisette['train']['data'].shape)

    batch_size = min(100, gisette['train']['data'].shape[0])
    epochs = (int(gisette['train']['data'].shape[0]/batch_size) + 1)*60

    n_components = gisette['train']['data'].shape[-1]

    source = gisette['train']
    labels = source['label']

    test_source = gisette['validation']

    data = source['data']
    max_val = np.max(data, axis=0)
    max_val[max_val == 0.0] = 1e-8
    data /= max_val

    # balance = round(len(labels[labels == 1]) / len(labels[labels == 0]))
    # if balance > 1:
    #     print('augmenting neg values by', balance)
    #     data_pos = data[labels == 1]
    #     data_neg = np.tile(data[labels == 0], (balance, 1))
    #     label_pos = labels[labels == 1]
    #     label_neg = np.tile(labels[labels == 0], (balance))
    #     data = np.concatenate((data_pos, data_neg), axis=0)
    #     labels = np.concatenate((label_pos, label_neg), axis=0)
    # elif balance == 0:
    #     balance = round(len(labels[labels == 0]) / len(labels[labels == 1]))
    #     if balance > 1:
    #         print('augmenting pos values by', balance)
    #         data_pos = np.tile(data[labels == 1], (balance, 1))
    #         data_neg = data[labels == 0]
    #         label_pos = np.tile(labels[labels == 1], (balance))
    #         label_neg = labels[labels == 0]
    #         data = np.concatenate((data_pos, data_neg), axis=0)
    #         labels = np.concatenate((label_pos, label_neg), axis=0)

    # mean = np.mean(data, axis=0)
    # desv = np.std(data, axis=0)
    # desv[desv == 0.0] = 1e-8
    # data = (data - mean) / desv

    test_data = test_source['data']
    # test_data = (test_data - mean) / desv
    test_data /= max_val
    test_labels = test_source['label']

    input_size = [n_components]

    real_test_data = gisette['test']['data']
    real_test_data /= max_val

    layers = [
        {'type': 'batch_normalization'},
        {
            'type': 'block',
            'layers': [
                {'type': 'matmul', 'data': {'shape': [300]}},
                {'type': 'batch_normalization'},
                {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                {'type': 'relu'},
            ]
        },
        {
            'type': 'block',
            'layers': [
                {'type': 'matmul', 'data': {'shape': [225]}},
                {'type': 'batch_normalization'},
                {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                {'type': 'relu'},
            ]
        },
        {
            'type': 'block',
            'layers': [
                {'type': 'matmul', 'data': {'shape': [150]}},
                {'type': 'batch_normalization'},
                {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                {'type': 'relu'},
            ]
        },
        {
            'type': 'block',
            'layers': [
                {'type': 'matmul', 'data': {'shape': [75]}},
                {'type': 'batch_normalization'},
                {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                {'type': 'relu'},
            ]
        },
        {
            'type': 'output',
            'layers': [
                {
                    'type': 'matmul',
                    # 'variables': {'trainable': False},
                    'data': {
                        'shape': [2],
                        # 'weights': basis,
                        # 'bias': bias
                    }
                },
                # {'type': 'tile', 'opts': {'multiples': [1, 10]}},
                # {'type': 'reshape', 'opts': {'shape': [10, 50]}},
                # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
                # {'type': 'square'},
                # {'type': 'reduce_sum', 'opts': {'axis': -1}},
                # {'type': 'multiply', 'opts': {'y': -1}},
                {'type': 'softmax', 'data': {'R': 0.0}}
            ]
        }
    ]

    global_opts = {
        'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
        # 'variables': {'l2_loss': {'lambda': 1e-2}}
    }

    info = get_features(layers, global_opts, data, labels, test_data, test_labels,
                        thresh=0.001, epochs=epochs, batch_size=batch_size,
                        reps=5, feat_type='wrapper', reduce_data=True)
    print(info)
    stats.append(info)
    np.savetxt('./models/feature_selection/' + dataset + '_test.predict', best_prob)

with open('./models/feature_selection/info_' + basis_type + '.json', 'w') as outfile:
    json.dump(stats, outfile)
