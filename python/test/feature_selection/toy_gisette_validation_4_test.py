from python.fpnn import FpNN
from python.dataset_scripts.nips_challenge import load_data
from python.train.saliency_map import get_saliency_map
from python.train.vector_utils import get_basis
import json
import numpy as np
from copy import deepcopy

datasets = [
    # 'arcene',
    # 'dexter',
    # 'dorothea',
    # 'gisette',
    'madelon'
]

stats = []
basis_type = 'augmented_eye'
keep_prob = 0.9
train_tries = 5
test_tries = 10
thresh = 0.01
min_percentage = 0.5
dropout_layer = {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}}

for dataset in datasets:

    print('loading dataset', dataset)
    gisette = load_data('./datasets/' + dataset + '/' + dataset)
    print('data loaded. labels =', gisette['train']['data'].shape)

    batch_size = min(int(10000000/gisette['train']['data'].shape[1]), gisette['train']['data'].shape[0])
    epochs = (int(gisette['train']['data'].shape[0]/batch_size) + 1)*60

    n_components = gisette['train']['data'].shape[-1]

    source = gisette['train']
    labels = source['label']

    test_source = gisette['validation']

    data = source['data']
    max_val = np.max(data)
    data /= max_val

    test_data = test_source['data']
    test_data /= max_val
    test_labels = test_source['label']

    real_test_data = gisette['test']['data']
    real_test_data /= max_val

    balance = round(len(labels[labels == 1]) / len(labels[labels == 0]))
    if balance > 1:
        print('augmenting neg values by', balance)
        data_pos = data[labels == 1]
        data_neg = np.tile(data[labels == 0], (balance, 1))
        label_pos = labels[labels == 1]
        label_neg = np.tile(labels[labels == 0], (balance))
        data = np.concatenate((data_pos, data_neg), axis=0)
        labels = np.concatenate((label_pos, label_neg), axis=0)
    elif balance == 0:
        balance = round(len(labels[labels == 0]) / len(labels[labels == 1]))
        if balance > 1:
            print('augmenting pos values by', balance)
            data_pos = np.tile(data[labels == 1], (balance, 1))
            data_neg = data[labels == 0]
            label_pos = np.tile(labels[labels == 1], (balance))
            label_neg = labels[labels == 0]
            data = np.concatenate((data_pos, data_neg), axis=0)
            labels = np.concatenate((label_pos, label_neg), axis=0)

    balance = round(len(test_labels[test_labels == 1]) / len(test_labels[test_labels == 0]))
    if balance > 1:
        print('augmenting test neg values by', balance)
        data_pos = test_data[test_labels == 1]
        data_neg = np.tile(test_data[test_labels == 0], (balance, 1))
        label_pos = test_labels[test_labels == 1]
        label_neg = np.tile(test_labels[test_labels == 0], (balance))
        test_data = np.concatenate((data_pos, data_neg), axis=0)
        test_labels = np.concatenate((label_pos, label_neg), axis=0)
    elif balance == 0:
        balance = round(len(test_labels[test_labels == 0]) / len(test_labels[test_labels == 1]))
        if balance > 1:
            print('augmenting test pos values by', balance)
            data_pos = np.tile(test_data[test_labels == 1], (balance, 1))
            data_neg = test_data[test_labels == 0]
            label_pos = np.tile(test_labels[test_labels == 1], (balance))
            label_neg = test_labels[test_labels == 0]
            test_data = np.concatenate((data_pos, data_neg), axis=0)
            test_labels = np.concatenate((label_pos, label_neg), axis=0)

    best_indexes = np.arange(n_components).astype(int)
    indexes = best_indexes.copy()
    best_prob = None
    best_n_components = n_components
    best_score = 0
    best_features = None
    counter = 0
    while counter < 8 or n_components / best_n_components > 0.02:

        basis = get_basis(75, 2, basis_type, 1.0).tolist()
        bias = 2 * [0]
        layers = [
            {'type': 'batch_normalization'},
            {'type': 'dropout', 'data': {'keep_prob': 0.5}, 'opts': {'factor': 'sqrt'}},
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [300]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [225]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [150]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [75]}},
                    {'type': 'batch_normalization'},
                    dropout_layer,
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'output',
                'layers': [
                    {
                        'type': 'matmul',
                        'variables': {'trainable': False},
                        'data': {
                            'shape': [2],
                            'weights': basis,
                            'bias': bias
                        }
                    },
                    # {'type': 'tile', 'opts': {'multiples': [1, 10]}},
                    # {'type': 'reshape', 'opts': {'shape': [10, 50]}},
                    # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
                    # {'type': 'square'},
                    # {'type': 'reduce_sum', 'opts': {'axis': -1}},
                    # {'type': 'multiply', 'opts': {'y': -1}},
                    {'type': 'softmax', 'data': {'R': 0.0}}
                ]
            }
        ]

        global_opts = {
            'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
            # 'variables': {'l2_loss': {'lambda': 1e-2}}
        }


        input_size = [n_components]

        fpnn_data = {
            'input_size': input_size,
            'model_type': 'classification',
            'global_opts': global_opts,
            'framework': 'tensorflow'
        }

        step = max(int(n_components / 100), 1)
        print('n_features =', n_components)
        print('best_score =', best_score)
        print('best_n_features =', best_n_components)

        maps = []
        for it in range(train_tries):

            model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)

            info = model.fit(data, labels, test_data, test_labels, epochs=epochs, batch_size=batch_size)

            label_probs = 2.0 * model.eval(test_data, output_type='probabilities')[0, :, 1] - 1.0
            # label_pred = label_probs.copy()
            # label_pred[label_pred >= 0.0] = 1.0
            # label_pred[label_pred < 0.0] = 0.0
            # label_pred.astype(int)
            label_pred = model.eval(test_data, output_type='labels')[0]
            for i in range(2):
                vector = 0.0 * np.ones(2)
                vector[i] = 1.0
                neg_vector = 0.0 * np.ones(2)
                neg_vector[i] = 1.0
                tp = np.where((test_labels == i) & (label_pred == i))[0]
                fp = np.where((test_labels != i) & (label_pred == i))[0]
                pos_saliency = np.zeros(n_components)
                if len(tp) > 0:
                    pos_saliency = np.mean(
                        np.abs(label_probs[tp])[:, None] * get_saliency_map(model, vector, data=test_data[tp]),
                        axis=0
                    )
                    pos_saliency /= pos_saliency.sum() + 1e-6
                neg_saliency = np.zeros(n_components)
                if len(fp) > 0:
                    neg_saliency = np.mean(
                        np.abs(label_probs[fp])[:, None] * get_saliency_map(model, neg_vector, data=test_data[fp]),
                        axis=0
                    )
                    neg_saliency /= neg_saliency.sum() + 1e-6
                im_map = pos_saliency - neg_saliency
                maps.append(im_map)

        maps = np.array(maps)
        max_maps = np.mean(maps, axis=0)
        nnegative = int((max_maps <= 0.0).sum())
        print('negatives values', nnegative)
        # max_maps = np.abs(max_maps)
        score_index = np.argsort(max_maps)
        max_maps[max_maps < 0.0] = 0
        max_maps /= np.sum(max_maps)

        cumscore = np.cumsum(max_maps[score_index])

        # if init > 0:
        #     print('found bad features', init)
        #     print('removing previous results')
        #     best_score = 0

        for i in range(0, 1, step):
            mask = np.ones(n_components)
            mask[score_index[:i]] = 0
            nelements = n_components - i
            score = 0
            test_score = 0
            for j in range(test_tries):
                perm = np.random.permutation(len(data))
                test_perm = np.random.permutation(len(test_data))
                score_labels = model.eval(
                    data[perm]*mask, is_training=True, output_type='labels', batch_size=batch_size
                )[0]
                test_score_labels = model.eval(
                    test_data[test_perm] * mask, is_training=True, output_type='labels', batch_size=batch_size
                )[0]
                score += np.sqrt(
                    ((score_labels == 1) & (labels[perm] == 1)).sum() / (labels == 1).sum() *
                    ((score_labels == 0) & (labels[perm] == 0)).sum() / (labels == 0).sum()
                )
                test_score += np.sqrt(
                    ((test_score_labels == 1) & (test_labels[test_perm] == 1)).sum() / (test_labels == 1).sum() *
                    ((test_score_labels == 0) & (test_labels[test_perm] == 0)).sum() / (test_labels == 0).sum()
                )
            score /= test_tries
            test_score /= test_tries
            score = np.sqrt(
                min(score, model.eval(data * mask, labels, is_training=False)[0]) *
                min(test_score, model.eval(test_data * mask, test_labels, is_training=False)[0])
            )
            print('nelements', nelements, 'explained', 1.0 - np.sum(max_maps[score_index[:i]]), 'accuracy = ', score,
                  'test_accuracy', test_score)
            if score > best_score or (np.abs(best_score - score) < 1e-5 and best_n_components > nelements):
                best_prob = 2.0 * model.eval(real_test_data*mask, output_type='probabilities')[0, :, 1] - 1.0
                best_score = score
                best_mask = mask.copy()
                best_indexes = indexes.copy()
                best_n_components = nelements
                best_features = best_indexes[score_index[i:]]
                # new_indexes = score_index[i:]
        del model
        # if len(new_indexes) == n_components:
        #     new_indexes = list(range(nnegative, n_components))
        # else:
        new_indexes = np.where(cumscore > thresh)[0]
        if len(new_indexes) < best_n_components < n_components:
            new_indexes = list(range(best_n_components, n_components))
        if len(new_indexes) < min_percentage * n_components:
            new_indexes = list(range(int((1.0 - min_percentage)*n_components), n_components))
        if len(new_indexes) == n_components:
            new_indexes = score_index[1:]
        # elif len(new_indexes) < 0.75*n_components:
        #     new_indexes = score_index[-int(0.75*n_components):]
        n_components = len(new_indexes)
        data = data[:, score_index[new_indexes]]
        test_data = test_data[:, score_index[new_indexes]]
        real_test_data = real_test_data[:, score_index[new_indexes]]
        indexes = indexes[ score_index[new_indexes] ]
        counter += 1

    stats.append({
        'dataset': dataset,
        'nfeats': best_n_components,
        'val_score': best_score,
        'feats': best_features.tolist()
    })
    np.savetxt('./models/feature_selection/' + dataset + '_test.predict', best_prob)

with open('./models/feature_selection/info_' + basis_type + '.json', 'w') as outfile:
    json.dump(stats, outfile)
