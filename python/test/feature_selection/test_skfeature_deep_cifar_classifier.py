import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.loader import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np
import os, errno


layers = [
    {'type': 'random_flip_left_right'},
    # {'type': 'batch_normalization'},
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': [{'ord': 1, 'lambda': 0.5}]
    #     }
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'conv2d', 'data': {'shape': [3,3,32]}},
            {'type': 'batch_normalization'},
            {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'conv2d', 'data': {'shape': [3,3,32]}},
            {'type': 'batch_normalization'},
            {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
            {'type': 'relu'},
        ]
    },
    {'type': 'reshape', 'data': {'shape': [8*8*32]}},
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [1024]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [10]
                },
                'opts': {'bias': False}
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 6, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

reps = 1
factor = 0.5
per = 1.0

datasets = [
    # ('./datasets/mnist/', 'mnist'),
    ('./datasets/cifar-10/', 'cifar-10'),
    # ('./datasets/cifar-100/', 'cifar-100')
]


def main():
    # load data
    for dataset in datasets:
        print('dataset =', dataset)
        dataset_json = load_data(dataset[0], dataset[1])
        train_data = dataset_json['train']['data']  # data
        train_labels = dataset_json['train']['label']  # label
        test_data = dataset_json['test']['data']
        test_labels = dataset_json['test']['label']

        mean = np.mean(train_data, axis=0)
        std = np.std(train_data, axis=0) + 1e-6

        train_data = (train_data - mean) / std
        # valid_data /= max_val
        test_data = (test_data - mean) / std
        with open('./results/' + dataset[1] + '/' + dataset[1] + '_feature_ranking.json') as datafile:
            rankings = json.load(datafile)
        for method in rankings:
            print('method =', method)
            rank = rankings[method]

            n_features = int(np.prod(train_data.shape[1:]))

            stats = {
                # 'validation': {'accuracy': {}, 'best_nfeatures': n_features},
                'test': {'accuracy': {}, 'best_nfeatures': n_features}
            }
            # best_conf = None
            # best_resu = None
            best_feats = None
            best_predictions = None
            best_score = 0
            best_n_features = 1e10
            while n_features > 0:
                print('n_features', n_features)
                print('best_score', best_score)
                print('best_features', best_n_features)
                valid_score = []
                input_size = list(train_data.shape[1:])
                batch_size = min(50, train_data.shape[0])
                epochs = (int(dataset_json['train']['data'].shape[0] / batch_size) + 1) * 14
                print('epochs', epochs)
                mask = np.zeros(train_data.shape[1:])
                mask.flat[rank[:n_features]] = 1.0
                for i in range(reps):
                    print('rep', i)
                    model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                    info = model.fit(mask * train_data, train_labels,
                                     mask * test_data, test_labels,
                                     epochs=epochs, batch_size=batch_size, verbose=False)
                    valid_score.append(info['test']['accuracy'][-1][1])
                    print('score =', valid_score)
                    mean_score = valid_score[-1]
                    j = n_features
                    if j not in stats['test']['accuracy']:
                        stats['test']['accuracy'][j] = -1.0
                    if stats['test']['accuracy'][j] < mean_score:
                        stats['test']['accuracy'][j] = mean_score
                    if (mean_score > per*best_score and j < best_n_features) or mean_score > best_score:
                        print('score =', mean_score, ', n_elements =', j)
                        if mean_score > best_score:
                            best_score = mean_score
                        if j < best_n_features or per*mean_score > best_score:
                            # eval = model.eval(mask*test_data[:, rank[:n_features]], output_type='probabilities')[0]
                            # eval_l = np.argmax(eval, axis=-1)
                            # best_resu = (2.0 * eval_l - 1.0).astype(int)
                            # best_conf = 2.0 * ((1 - eval_l) * eval[:, 0] + eval_l*eval[:, 1] - 0.5)
                            best_feats = rank[:j]
                            best_n_features = j
                    del model

                if n_features == int(n_features * factor):
                    break
                n_features = int(n_features * factor)

            stats['best_nfeatures'] = best_n_features
            try:
                with open('./results/' + dataset[1] + '/' + dataset[1] + '_results_dense.json') as datafile:
                    results = json.load(datafile)
            except:
                results = {}

            results[method] = stats

            with open('./results/' + dataset[1] + '/' + dataset[1] + '_results_dense.json', 'w') as outfile:
                json.dump(results, outfile)

            # try:
            #     os.makedirs('./results/' + dataset[1] + '/' + method + '_predict_dense')
            # except OSError as e:
            #     pass

            # np.savetxt(
            #     './results/' + dataset[1] + '/' + method + '_predict_dense/' + dataset + '.feat',
            #     (np.array(best_feats) + 1).astype(int), fmt='%i'
            # )
            #
            # np.savetxt(
            #     './results/' + dataset[1] + '/' + method + '_predict_dense/' + dataset + '_test.predict',
            #     best_predictions, fmt='%2.5f'
            # )


if __name__ == '__main__':
    main()