import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.nips_challenge import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np
import os, errno
from matplotlib import pyplot as pl

# datasets = ['arcene', 'gisette', 'madelon', 'dexter', 'dorothea']

datasets = ['gisette']

def main():
    # load data
    cont = 0
    for dataset in datasets:
        print(dataset)
        pl.figure(cont)
        with open('./python/test/feature_selection/dataset_ranking/' + dataset + '_results.json') as datafile:
            results = json.load(datafile)
        methods = list(results.keys())
        for method in methods:
            x_axis = np.array(results[method]['nfeatures'])
            y_axis = np.array(results[method]['accuracy'])
            y_axis_mean = np.mean(y_axis, axis=-1)
            y_axis_std = np.std(y_axis, axis=-1)
            auc = np.sum(0.5 * (y_axis_mean[:-1] + y_axis_mean[1:]) * (x_axis[:-1] - x_axis[1:])) / (np.max(x_axis) - 1)
            pl.plot(x_axis, y_axis_mean)
            pl.fill_between(x_axis, y_axis_mean - y_axis_std, y_axis_mean + y_axis_std, alpha=.1)
            best = np.argmax(y_axis_mean)
            print('method =', method, ', mean =', y_axis_mean[best], ', std = ', y_axis_std[best],
                  ', nfeat =', x_axis[best], ', auc =', auc)
        pl.title(dataset)
        pl.legend(methods, loc='best')
        pl.show()
        cont += 1


if __name__ == '__main__':
    main()