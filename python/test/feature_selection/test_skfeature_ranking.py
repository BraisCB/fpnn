import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.nips_challenge import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np
import os, errno

layers = [
    # {'type': 'batch_normalization'},
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-4}
    #     }
    # },
    # {
    #     'type': 'block',
    #     'layers': [
    #         {'type': 'matmul', 'data': {'shape': [300]}},
    #         {'type': 'batch_normalization'},
    #         # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
    #         {'type': 'relu'},
    #     ]
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [225]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [150]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [75]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [2]
                },
                'opts': {'bias': True}
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

reps = 5
factor = 0.9

# datasets = ['arcene', 'gisette', 'madelon', 'dexter', 'dorothea']

datasets = ['arcene']

def main():
    # load data
    for dataset in datasets:
        print('dataset =', dataset)
        with open('./python/test/feature_selection/dataset_ranking/' + dataset + '.json') as datafile:
            rankings = json.load(datafile)
        for method in rankings:
            print('method =', method)
            rank = rankings[method]
            dataset_json = load_data('./datasets/' + dataset + '/' + dataset)
            train_data = dataset_json['train']['data']    # data
            train_labels = dataset_json['train']['label']    # label

            valid_data = dataset_json['validation']['data']
            valid_labels = dataset_json['validation']['label']

            test_data = dataset_json['test']['data']

            max_val = np.max(train_data, axis=0)
            max_val[max_val == 0] = 1e-6
            train_data /= max_val
            valid_data /= max_val
            test_data /= max_val

            n_features = int(np.prod(train_data.shape[1:]))

            stats = {'nfeatures': [], 'accuracy': [], 'best_nfeatures': n_features}
            best_predictions = None
            best_score = 0
            best_n_features = -1
            while n_features > 0:
                print('n_features', n_features)
                print('best_score', best_score)
                print('best_features', best_n_features)
                valid_score = []
                input_size = [n_features]
                batch_size = min(100, train_data.shape[0])
                epochs = (int(train_data.shape[0] / batch_size) + 1) * 60
                test_predictions = np.zeros(len(test_data))
                mean_score = 0
                for i in range(reps):
                    print('rep', i)
                    model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                    info = model.fit(train_data[:, rank[:n_features]], train_labels,
                                     valid_data[:, rank[:n_features]], valid_labels,
                                     epochs=epochs, batch_size=batch_size, verbose=False)
                    valid_score.append(info['test']['accuracy'][-1][1])
                    print('score =', valid_score[-1])
                    mean_score += valid_score[-1]
                    test_predictions += (
                        2.0 * model.eval(test_data[:, rank[:n_features]], output_type='probabilities')[0, :, 1] - 1.0
                    )
                    del model
                stats['nfeatures'].append(n_features)
                stats['accuracy'].append(valid_score)
                mean_score /= reps
                test_predictions /= reps
                if mean_score > best_score:
                    best_score = mean_score
                    best_predictions = test_predictions.copy()
                    best_n_features = n_features

                if n_features == int(n_features * factor):
                    break
                n_features = int(n_features * factor)

            stats['best_nfeatures'] = best_n_features
            try:
                with open('./python/test/feature_selection/dataset_ranking/' + dataset + '_results.json') as datafile:
                    results = json.load(datafile)
            except:
                results = {}

            results[method] = stats

            with open('./python/test/feature_selection/dataset_ranking/' + dataset + '_results.json', 'w') as outfile:
                json.dump(results, outfile)

            try:
                os.makedirs('./python/test/feature_selection/dataset_ranking/' + method + '_predict')
            except OSError as e:
                pass

            np.savetxt(
                './python/test/feature_selection/dataset_ranking/' + method + '_predict/' + dataset + '_test.predict',
                best_predictions
            )


if __name__ == '__main__':
    main()