from python.fpnn import FpNN
from python.dataset_scripts.nips_challenge import load_data
from python.train.saliency_map import get_saliency_map
from python.train.vector_utils import get_basis
import json
import numpy as np
from copy import deepcopy

datasets = [
    'arcene',
    'dexter',
    'dorothea',
    # 'gisette',
    # 'madelon'
]

stats = []
basis_type = 'eye'
keep_prob = 0.9

for dataset in datasets:

    print('loading dataset', dataset)
    gisette = load_data('./datasets/' + dataset + '/' + dataset)
    print('data loaded. labels =', gisette['train']['data'].shape)

    batch_size = min(100, gisette['train']['data'].shape[0])
    epochs = (int(gisette['train']['data'].shape[0]/batch_size) + 1)*60

    n_components = gisette['train']['data'].shape[-1]

    source = gisette['train']
    labels = source['label']

    test_source = gisette['validation']

    data = source['data']
    max_val = np.max(data)
    data /= max_val

    balance = round(len(labels[labels == 1]) / len(labels[labels == 0]))
    if balance > 1:
        print('augmenting neg values by', balance)
        data_pos = data[labels == 1]
        data_neg = np.tile(data[labels == 0], (balance, 1))
        label_pos = labels[labels == 1]
        label_neg = np.tile(labels[labels == 0], (balance))
        data = np.concatenate((data_pos, data_neg), axis=0)
        labels = np.concatenate((label_pos, label_neg), axis=0)
    elif balance == 0:
        balance = round(len(labels[labels == 0]) / len(labels[labels == 1]))
        if balance > 1:
            print('augmenting pos values by', balance)
            data_pos = np.tile(data[labels == 1], (balance, 1))
            data_neg = data[labels == 0]
            label_pos = np.tile(labels[labels == 1], (balance))
            label_neg = labels[labels == 0]
            data = np.concatenate((data_pos, data_neg), axis=0)
            labels = np.concatenate((label_pos, label_neg), axis=0)

    # mean = np.mean(data, axis=0)
    # desv = np.std(data, axis=0)
    # desv[desv == 0.0] = 1e-8
    # data = (data - mean) / desv

    test_data = test_source['data']
    # test_data = (test_data - mean) / desv
    test_data /= max_val
    test_labels = test_source['label']

    input_size = [n_components]

    real_test_data = gisette['test']['data']
    real_test_data /= max_val

    best_indexes = np.arange(n_components).astype(int)
    indexes = best_indexes.copy()
    best_prob = None
    best_n_components = n_components
    best_score = 0
    best_features = None
    thresh = 0.01
    limit = int(n_components*0.01)
    print('limit =', limit)
    counter = 0
    while counter < 8 or n_components / best_n_components > 0.5:

        basis = get_basis(75, 2, basis_type, 1.0).tolist()
        bias = 2 * [0]
        layers = [
            {'type': 'batch_normalization'},
            {
                'type': 'mask',
                'opts': {
                    'regularization': {'ord': 1, 'lambda': 1e-3}
                }
            },
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [300]}},
                    {'type': 'batch_normalization'},
                    {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [225]}},
                    {'type': 'batch_normalization'},
                    {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [150]}},
                    {'type': 'batch_normalization'},
                    {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [75]}},
                    {'type': 'batch_normalization'},
                    {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'output',
                'layers': [
                    {
                        'type': 'matmul',
                        'variables': {'trainable': True},
                        'data': {
                            'shape': [2],
                            #'weights': basis,
                            #'bias': bias
                        },
                        'opts': {
                            'bias': True
                        }
                    },
                    # {'type': 'tile', 'opts': {'multiples': [1, 10]}},
                    # {'type': 'reshape', 'opts': {'shape': [10, 50]}},
                    # {'type': 'subtract', 'opts': {'y': np.array(basis).T.tolist()}},
                    # {'type': 'square'},
                    # {'type': 'reduce_sum', 'opts': {'axis': -1}},
                    # {'type': 'multiply', 'opts': {'y': -1}},
                    {'type': 'softmax', 'data': {'R': 0.0}}
                ]
            }
        ]

        global_opts = {
            'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
            # 'variables': {'l2_loss': {'lambda': 1e-2}}
        }

        fpnn_data = {
            'input_size': input_size,
            'model_type': 'classification',
            'global_opts': global_opts,
            'framework': 'tensorflow'
        }

        step = max(int(n_components / 100), 1)
        print('n_features =', n_components)
        print('best_score =', best_score)
        print('best_n_features =', best_n_components)
        input_size = [n_components]
        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)

        info = model.fit(data, labels, test_data, test_labels, epochs=epochs, batch_size=batch_size)

        maps = []
        for i in range(2):
            vector = 0.0 * np.ones(2)
            vector[i] = 1.0
            label_maps = get_saliency_map(model, vector, data=test_data[test_labels == i])
            label_probs = 2.0 * model.eval(test_data[test_labels == i], output_type='probabilities')[0, :, 1] - 1.0
            label_real = 2.0 * test_labels[test_labels == i] - 1.0
            im_map = np.mean((label_probs * label_real)[:, None] * label_maps, axis=0)
            maps.append(im_map)

        maps = np.array(maps)
        max_maps = np.mean(maps, axis=0)
        max_maps[max_maps < 0.0] = 0.0
        max_maps /= np.sum(max_maps)

        score_index = np.argsort(max_maps)
        cumscore = np.cumsum(max_maps[score_index])

        for i in range(0, n_components, step):
            mask = np.ones(n_components)
            mask[score_index[:i]] = 0
            nelements = n_components - i - 1
            score = model.eval(test_data*mask, test_labels)[0]
            print('nelements', nelements, 'explained', 1.0 - np.sum(max_maps[score_index[:i]]), 'accuracy = ', score)
            if score > best_score or (np.abs(best_score - score) < 1e-5 and best_n_components > nelements):
                best_prob = 2.0 * model.eval(real_test_data*mask, output_type='probabilities')[0, :, 1] - 1.0
                best_score = score
                best_mask = mask.copy()
                best_indexes = indexes.copy()
                best_n_components = nelements
                best_features = best_indexes[score_index[i:]]
        del model
        new_indexes = np.where(cumscore > thresh)[0]
        if n_components - step < len(new_indexes):
            print('changing thresh to', 0.05)
            thresh = 0.05
            new_indexes = np.where(cumscore > thresh)[0]
        if len(new_indexes) == n_components:
            new_indexes = score_index[1:]
        n_components = len(new_indexes)
        data = data[:, score_index[new_indexes]]
        test_data = test_data[:, score_index[new_indexes]]
        real_test_data = real_test_data[:, score_index[new_indexes]]
        indexes = indexes[ score_index[new_indexes] ]
        counter += 1

    stats.append({
        'dataset': dataset,
        'nfeats': best_n_components,
        'val_score': best_score,
        'feats': best_features.tolist()
    })
    np.savetxt('./models/feature_selection/' + dataset + '_test.predict', best_prob)

with open('./models/feature_selection/info_' + basis_type + '.json', 'w') as outfile:
    json.dump(stats, outfile)
