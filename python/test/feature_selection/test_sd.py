import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.loader import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np
import os, errno
from sklearn.model_selection import KFold

keep_prob = 0.75
layers = [
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': [{'ord': 1, 'lambda': 1e-3}]
    #     }
    # },
    # {
    #     'type': 'block',
    #     'layers': [
    #         {'type': 'matmul', 'data': {'shape': [300]}},
    #         {'type': 'batch_normalization'},
    #         # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
    #         {'type': 'relu'},
    #     ]
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [225]}, 'opts': {'bias': True}},
            # {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [150]}, 'opts': {'bias': True}},
            # {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    # {'type': 'dropout', 'data': {'keep_prob': 0.5}},
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [75]}, 'opts': {'bias': True}},
            # {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    # {'type': 'dropout', 'data': {'keep_prob': 0.5}},
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [3]
                },
                'opts': {'bias': False}
            },
            {'type': 'hinge'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-3, 'learning_decay': {'epochs': 120, 'factor': 0.1}},
    'regularization': [{'ord': 2, 'lambda': 1}]
}

reps = 3
factor = 0.9
per = 0.999
kfold = KFold(n_splits=5, shuffle=True)

kwargs = {
    'layers': layers,
    'global_opts': global_opts,
    'reps': reps,
    'factor': factor,
    'reduce_data': True
}

nepochs = 240
datasets = ['sd1', 'sd2', 'sd3']
methods = ['ITdeepFS_simonyan', 'deepFS_simonyan', 'ITdeepFS', 'deepFS']
# methods = [
#     'deepLASSO', 'deepFS_lasso', 'ITdeepFS_lasso', 'ITdeepLASSO', 'deepFS_lasso_simonyan', 'ITdeepFS_lasso_simonyan'
# ]

def main():
    results = {}
    # load data
    for dataset in datasets:
        print('dataset =', dataset)
        dataset_json = load_data('./datasets/sd/',  dataset)
        data = dataset_json['train']['data']  # data
        labels = dataset_json['train']['label']  # label
        for train_index, test_index in kfold.split(data):
            train_data, test_data = data[train_index].copy(), data[test_index].copy()
            me = np.mean(train_data, axis=0)
            st = np.std(train_data, axis=0)
            train_data = (train_data - me) / st
            test_data = (test_data - me) / st
            train_labels, test_labels = labels[train_index].copy(), labels[test_index].copy()

            for method in methods:

                if method not in results:
                    results[method] = []

                results[method].append({'score': [], 'nfeats': []})

                print('method =', method)

                kwargs['valid_data'] = test_data
                kwargs['valid_labels'] = test_labels
                kwargs['batch_size'] = train_data.shape[0]
                kwargs['epochs'] = nepochs

                n_features = int(np.prod(train_data.shape[1:]))

                print('obtaining indexes', method)
                info = get_feature_indexes(train_data, train_labels, method, **kwargs)
                rank = info['rank']
                print('indexes obtained')

                best_score = 0
                best_test_score = 0
                best_n_features = 1e10
                while n_features > 0:
                    print('n_features', n_features)
                    print('best_score', best_score)
                    print('best_test_score', best_test_score)
                    print('best_features', best_n_features)
                    valid_score = []
                    input_size = [n_features]
                    batch_size = train_data.shape[0]
                    epochs = nepochs
                    for i in range(reps):
                        print('rep', i)
                        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                        info = model.fit(train_data[:, rank[:n_features]], train_labels,
                                         test_data[:, rank[:n_features]], test_labels,
                                         epochs=epochs, batch_size=batch_size, verbose=False)
                        valid_score.append(info['test']['accuracy'][-1][1])
                        step = -max(1, int(n_features/1000))
                        print('score =', valid_score)
                        for j in range(n_features, 0, step):
                            mask = np.zeros(n_features)
                            mask[:j] = 1.0
                            mean_score = model.eval(mask * train_data[:, rank[:n_features]], train_labels)[0]
                            if (mean_score >= per*best_score and j < best_n_features) or mean_score > best_score:
                                if j < best_n_features or per*mean_score > best_score:
                                    best_test_score = model.eval(mask*test_data[:, rank[:n_features]], test_labels)[0]
                                    best_n_features = j
                                    print('score =', mean_score, ', n_elements =', j, ', test =', best_test_score)
                                if mean_score > best_score:
                                    best_score = mean_score

                        del model
                    n_features = int(n_features * factor)
                results[method][-1]['score'].append(best_test_score)
                results[method][-1]['nfeats'].append(best_n_features)

        filename = './results/' + dataset + '_results.json'
        with open(filename, 'w') as outfile:
            json.dump(results, outfile)


if __name__ == '__main__':
    main()