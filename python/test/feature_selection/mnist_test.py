from python.train.feature_selection import get_features
from tensorflow.examples.tutorials.mnist import input_data
import json
import numpy as np
from copy import deepcopy

mnist_struct = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
mnist = {
    'train': {'data': mnist_struct.train.images, 'label': mnist_struct.train.labels},
    'validation': {'data': mnist_struct.validation.images, 'label': mnist_struct.validation.labels},
    'test': {'data': mnist_struct.test.images, 'label': mnist_struct.test.labels},
}
del mnist_struct

batch_size = min(100, mnist['train']['data'].shape[0])
epochs = 10000
keep_prob = 0.9

n_components = mnist['train']['data'].shape[-1]

source = mnist['train']
labels = source['label']

validation_source = mnist['validation']
test_source = mnist['test']

data = source['data']
max_val = np.max(data, axis=0)
max_val[max_val == 0.0] = 1e-8
data /= max_val

validation_data = validation_source['data']
validation_data /= max_val
validation_labels = validation_source['label']

test_data = test_source['data']
test_data /= max_val
test_labels = test_source['label']

input_size = [n_components]

layers = [
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-3}
    #     }
    # },
    {'type': 'batch_normalization'},
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [300]}}, #, 'opts': {'regularization': {'ord': 1, 'lambda': 1e-4}}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [225]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [150]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [75]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [10],
                }
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 10, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

info = get_features(layers, global_opts, data, labels, validation_data, validation_labels, test_data, test_labels,
                    thresh=0.001, epochs=epochs, batch_size=batch_size,
                    reps=1, feat_type='wrapper', reduce_data=True, ranking=True)

with open('./models/feature_selection/info_fs_mnist_wrapper_test.json', 'w') as outfile:
    json.dump(info, outfile)
