from python.dataset_scripts.loader import load_data
from python.dataset_scripts.arff_parser import to_arff


datasets = [
    'arcene',
    'dexter',
    'dorothea',
    'gisette',
    'madelon'
]

datasets = [
    # ('./datasets/mnist/', 'mnist'),
    ('./datasets/cifar-10/', 'cifar-10'),
    ('./datasets/cifar-100/', 'cifar-100')
]


for dataset in datasets:
    print('loading dataset', dataset[1])
    dataset_json = load_data(dataset[0], dataset[1])
    print('data loaded. labels =', dataset_json['train']['data'].shape)
    to_arff(dataset_json, dataset[0], dataset[1])
