import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.loader import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np
import os, errno
from sklearn.model_selection import KFold

keep_prob = 0.5

global_opts = {
    'train': {'learning_rate': 1e-3, 'learning_decay': {'epochs': 120, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-3}}
}

reps = 3
factor = 0.9
per = 0.98
kfold = KFold(n_splits=4, shuffle=True)

datasets = [
    'Leukemia1', 'Leukemia2', '9_Tumors', '11_Tumors',
    '14_Tumors', 'Brain_Tumor1', 'Brain_Tumor2', 'Cll',
    'Gcm', 'Gla', 'Lung_Cancer', 'Lymphoma',
    'MLL', 'SRBCT', 'Tox'
]
methods = ['ITdeepFS_simonyan', 'deepFS_simonyan', 'ITdeepFS', 'deepFS']
# methods = [
#     'deepLASSO', 'deepFS_lasso', 'ITdeepFS_lasso', 'ITdeepLASSO', 'deepFS_lasso_simonyan', 'ITdeepFS_lasso_simonyan'
# ]

def main():
    results = {}
    # load data
    for dataset in datasets:
        print('dataset =', dataset)
        dataset_json = load_data('./datasets/microarray/',  dataset)
        data = dataset_json['train']['data']  # data
        data_mean = np.mean(data, axis=0)
        data_std = np.std(data, axis=0)
        labels = dataset_json['train']['label']  # label
        nlabels = len(np.unique(labels))
        layers = [
            # {
            #     'type': 'mask',
            #     'opts': {
            #         'regularization': [{'ord': 1, 'lambda': 1e-3}]
            #     }
            # },
            # {'type': 'batch_normalization'},
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [225]}, 'opts': {'bias': True}},
                    # {'type': 'batch_normalization'},
                    # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                    {'type': 'relu'},
                ]
            },
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [150]}, 'opts': {'bias': True}},
                    # {'type': 'batch_normalization'},
                    # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                    {'type': 'relu'},
                ]
            },
            # {'type': 'dropout', 'data': {'keep_prob': 0.5}},
            {
                'type': 'block',
                'layers': [
                    {'type': 'matmul', 'data': {'shape': [75]}, 'opts': {'bias': True}},
                    # {'type': 'batch_normalization'},
                    # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
                    {'type': 'relu'},
                ]
            },
            # {'type': 'dropout', 'data': {'keep_prob': 0.5}},
            {
                'type': 'output',
                'layers': [
                    {
                        'type': 'matmul',
                        'data': {
                            'shape': [nlabels]
                        },
                        'opts': {'bias': False}
                    },
                    {'type': 'softmax'}
                ]
            }
        ]

        kwargs = {
            'layers': layers,
            'global_opts': global_opts,
            'reps': reps,
            'factor': factor,
            'reduce_data': True
        }

        test_labels = dataset_json['test']['label']
        for train_index, valid_index in kfold.split(data):
            train_data, valid_data = data[train_index].copy(), data[valid_index].copy()
            train_labels, valid_labels = labels[train_index].copy(), labels[valid_index].copy()

            # mean = np.mean(train_data, axis=0)
            # std = np.std(train_data, axis=0)
            train_data = (train_data - data_mean) / data_std
            valid_data = (valid_data - data_mean)  / data_std
            test_data = (dataset_json['test']['data'] - data_mean) / data_std
            for method in methods:
                print('method =', method)

                kwargs['valid_data'] = valid_data
                kwargs['valid_labels'] = valid_labels
                kwargs['batch_size'] = train_data.shape[0]
                kwargs['epochs'] = 240

                n_features = int(np.prod(train_data.shape[1:]))

                print('obtaining indexes', method)
                info = get_feature_indexes(train_data, train_labels, method, **kwargs)
                rank = info['rank']
                print('indexes obtained')

                if method not in results:
                    results[method] = []

                results[method].append({'score': 0, 'nfeats': 0})

                best_score = 0
                best_test_score = 0
                best_n_features = 1e10
                while n_features > 0:
                    print('n_features', n_features)
                    print('best_score', best_score)
                    print('best_test_score', best_test_score)
                    print('best_features', best_n_features)
                    valid_score = []
                    test_score = []
                    input_size = [n_features]
                    batch_size = train_data.shape[0]
                    epochs = 240
                    for i in range(reps):
                        print('rep', i)
                        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                        info = model.fit(train_data[:, rank[:n_features]], train_labels,
                                         valid_data[:, rank[:n_features]], valid_labels,
                                         epochs=epochs, batch_size=batch_size, verbose=False)
                        valid_score.append(info['test']['accuracy'][-1][1])
                        test_score.append(model.eval(test_data[:, rank[:n_features]], test_labels)[0])

                        print('score =', valid_score)
                        print('test_score =', test_score)

                        # step = -max(1, int(n_features / 100000))
                        # for j in range(n_features, 0, step):
                        #     mask = np.zeros(n_features)
                        #     mask[:j] = 1.0
                        #     mean_score = model.eval(mask*valid_data[:, rank[:n_features]], valid_labels)[0]
                        #     if (mean_score > per*best_score and j < best_n_features) or mean_score > best_score:
                        #         print('score =', mean_score, ', n_elements =', j)
                        #         print('test_score =', model.eval(mask*test_data[:, rank[:n_features]], test_labels)[0])
                        #         if j < best_n_features or per*mean_score > best_score:
                        #             best_test_score = model.eval(mask*test_data[:, rank[:n_features]], test_labels)[0]
                        #             best_n_features = j
                        #         if mean_score > best_score:
                        #             best_score = mean_score
                        del model

                    score = np.prod(valid_score)
                    if score >= per * best_score:
                        best_test_score = np.prod(test_score)
                        best_n_features = n_features
                        if score > best_score:
                            best_score = score

                    n_features = int(n_features * factor)
                scores = []
                for i in range(reps):
                    model = FpNN(input_size=[best_n_features], layers=deepcopy(layers), global_opts=global_opts)
                    info = model.fit(data[:, rank[:best_n_features]], labels,
                                     test_data[:, rank[:best_n_features]], test_labels,
                                     epochs=epochs, batch_size=batch_size, verbose=False)
                    scores.append(info['test']['accuracy'][-1][1])
                results[method][-1]['score'] = np.mean(scores)
                results[method][-1]['nfeats'] = best_n_features

        filename = './results/' + dataset + '_results.json'
        with open(filename, 'w') as outfile:
            json.dump(results, outfile)


if __name__ == '__main__':
    main()