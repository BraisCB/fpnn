from python.dataset_scripts.augmented_dataset import read_dataset
import json
import numpy as np
from python.fpnn import FpNN
from copy import deepcopy
from skfeature.function.statistical_based import CFS

mnist = read_dataset('./datasets/mnist/', dataset='fs_mnist')

batch_size = min(100, mnist['train']['data'].shape[0])
epochs = 10000
keep_prob = 0.9

n_components = mnist['train']['data'].shape[-1]

source = mnist['train']
labels = source['label']

validation_source = mnist['validation']
test_source = mnist['test']

data = source['data']
shape = list(data.shape[1:])
# data = np.reshape(data, [-1, int(np.prod(shape))])
mean = np.mean(data, axis=0)
std = np.std(data, axis=0)
print('zeros', n_components - np.count_nonzero(std))
std[std == 0.0] = 1e-8
data = (data - mean) / std

validation_data = validation_source['data']
# validation_data = np.reshape(validation_data, [-1, int(np.prod(shape))])
validation_data = (validation_data - mean) / std
validation_labels = validation_source['label']

test_data = test_source['data']
# test_data = np.reshape(test_data, [-1, int(np.prod(shape))])
test_data = (test_data - mean) / std
test_labels = test_source['label']

input_size = shape

print('evaluating')
idx = CFS.cfs(np.reshape(data, [-1, int(np.prod(shape))]), labels)
print('evaluated', print(len(idx)))

with open('./models/feature_selection/fs_mnist_cfs_rank.json', 'w') as outfile:
    json.dump({'rank': idx.tolist()}, outfile)


# mask = np.zeros(shape)
# mask.flat[idx[:400]] = 1.0
#
# data *= mask
# validation_data *= mask
# test_data *= mask
#
# layers = [
#     {'type': 'batch_normalization'},
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'data': {'shape': [300]}},
#             {'type': 'batch_normalization'},
#             {'type': 'relu'},
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'data': {'shape': [225]}},
#             {'type': 'batch_normalization'},
#             {'type': 'relu'},
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'data': {'shape': [150]}},
#             {'type': 'batch_normalization'},
#             {'type': 'relu'},
#         ]
#     },
#     {
#         'type': 'block',
#         'layers': [
#             {'type': 'matmul', 'data': {'shape': [75]}},
#             {'type': 'batch_normalization'},
#             {'type': 'relu'},
#         ]
#     },
#     {
#         'type': 'output',
#         'layers': [
#             {
#                 'type': 'matmul',
#                 'data': {
#                     'shape': [10],
#                 }
#             },
#             {'type': 'softmax'}
#         ]
#     }
# ]
#
# global_opts = {
#     'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 10, 'factor': 0.1}},
#     # 'variables': {'l2_loss': {'lambda': 1e-2}}
# }
#
# model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
# info = model.fit(data, labels, test_data, test_labels, epochs=epochs, batch_size=batch_size)
#
# with open('./models/feature_selection/info_correlation.json', 'w') as outfile:
#     json.dump(info, outfile)
