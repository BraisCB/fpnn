import os
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.loader import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np


layers = [
    {'type': 'random_flip_left_right'},
    {
        'type': 'mask',
        'opts': {
            'regularization': [{'ord': 1, 'lambda': 1e-4}]
        }
    },
    {'type': 'reshape', 'data': {'shape': [32*32*3]}},
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [500]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [400]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [300]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [200]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    # {
    #     'type': 'block',
    #     'layers': [
    #         {'type': 'conv2d', 'data': {'shape': [3,3,32]}},
    #         {'type': 'batch_normalization'},
    #         {'type': 'relu'},
    #         # {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
    #     ]
    # },
    # {
    #     'type': 'block',
    #     'layers': [
    #         {'type': 'conv2d', 'data': {'shape': [3,3,32]}},
    #         {'type': 'batch_normalization'},
    #         {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
    #         {'type': 'relu'},
    #         # {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
    #     ]
    # },
    # # {
    # #     'type': 'block',
    # #     'layers': [
    # #         {'type': 'conv2d', 'data': {'shape': [3,3,64]}},
    # #         {'type': 'batch_normalization'},
    # #         {'type': 'relu'},
    # #         # {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
    # #     ]
    # # },
    # {
    #     'type': 'block',
    #     'layers': [
    #         {'type': 'conv2d', 'data': {'shape': [3,3,32]}},
    #         {'type': 'batch_normalization'},
    #         {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
    #         {'type': 'relu'},
    #         # {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
    #     ]
    # },
    # {'type': 'reshape', 'data': {'shape': [8*8*32]}},
    # {
    #     'type': 'block',
    #     'layers': [
    #         {'type': 'matmul', 'data': {'shape': [1024]}},
    #         {'type': 'batch_normalization'},
    #         {'type': 'relu'},
    #     ]
    # },
    # # {
    # #     'type': 'block',
    # #     'layers': [
    # #         {'type': 'matmul', 'data': {'shape': [1024]}},
    # #         {'type': 'batch_normalization'},
    # #         {'type': 'relu'},
    # #     ]
    # # },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [10],
                },
                'opts': {'bias': False}
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 6, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

reps = 3
factor = 0.75

kwargs = {
    'layers': layers,
    'global_opts': global_opts,
    'reps': reps,
    'factor': factor,
    'reduce_data': False
}

# methods = ['reliefF', 'MIM', 'MRMR', 'FCBF']
datasets = [
    # ('./datasets/mnist/', 'mnist'),
    ('./datasets/cifar-10/', 'cifar-10'),
    # ('./datasets/cifar-100/', 'cifar-100')
]

methods = ['ITdeepFS', 'deepFS', 'ITdeepFS_simonyan', 'deepFS_simonyan']
# methods = [
#     'deepLASSO', 'deepFS_lasso', 'ITdeepFS_lasso', 'ITdeepLASSO', 'deepFS_lasso_simonyan', 'ITdeepFS_lasso_simonyan'
# ]
# datasets = ['dorothea']


def main():
    # load data
    for dataset in datasets:
        for method in methods:
            dataset_json = load_data(dataset[0], dataset[1])
            X = dataset_json['train']['data']  # data
            # X = np.reshape(X, (len(X), -1))
            # X = X.astype(float)
            y = dataset_json['train']['label']    # label
            # X, y = balance_data(X, y)
            # n_samples, n_features = X.shape    # number of samples and number of features

            stats = {}
            # split data into 10 folds
                # obtain the index of selected features on training set
            # max_val = np.max(X, axis=0)
            # max_val[max_val == 0] = 1e-6

            if 'deep' in method:
                mean = np.mean(X, axis=0)
                X = X.astype(float) - mean
                std = np.std(X, axis=0) + 1e-6
                X /= std
                # perm = np.random.permutation(len(dataset_json['validation']['data']))
                # len_valid = int(len(perm)/2)
                if 'validation' in dataset_json:
                    kwargs['valid_data'] = dataset_json['validation']['data']# [:len_valid]
                    # kwargs['valid_data'] = np.reshape(kwargs['valid_data'], (len(kwargs['valid_data']), -1))
                    kwargs['valid_labels'] = dataset_json['validation']['label']# [:len_valid]
                    kwargs['valid_data'] = (kwargs['valid_data'] - mean) / std

                # kwargs['valid_data'], kwargs['valid_labels'] = balance_data(
                #     dataset_json['validation']['data'], dataset_json['validation']['label']
                # )
                if 'test' in dataset_json:
                    if 'validation' not in dataset_json:
                        kwargs['valid_data'] = dataset_json['test']['data']
                        # kwargs['valid_data'] = np.reshape(kwargs['valid_data'], (len(kwargs['valid_data']), -1))
                        kwargs['valid_data'] = (kwargs['valid_data'] - mean) / std
                        kwargs['valid_labels'] = dataset_json['test']['label']
                    else:
                        kwargs['test_data'] = dataset_json['test']['data']
                        # kwargs['test_data'] = np.reshape(kwargs['valid_data'], (len(kwargs['test_data']), -1))
                        kwargs['test_data'] = (kwargs['test_data'] - mean) / std
                        kwargs['test_labels'] = dataset_json['test']['label']

                # kwargs['test_data'] = dataset_json['validation']['data'][len_valid:]
                # kwargs['test_labels'] = dataset_json['validation']['label'][len_valid:]
                # kwargs['valid_data'] /= max_val
                kwargs['batch_size'] = min(50, X.shape[0])
                kwargs['epochs'] = (int(X.shape[0] / kwargs['batch_size']) + 1) * 14
                print('epochs', kwargs['epochs'])

            print('obtaining indexes', method)
            info = get_feature_indexes(X, y, method, **kwargs)
            print('indexes obtained')
            n_features = X.shape[1]

            # while n_features > 0:
            #     print('n_features', n_features)
            #     if n_features not in stats:
            #         stats[n_features] = {
            #             'features': [],
            #             'info': {
            #                 'train': {'accuracy': [], 'loss': []},
            #                 'test': {'accuracy': [], 'loss': []}
            #             }
            #         }
            #     train_score = []
            #     test_score = []
            #     train_loss = []
            #     test_loss = []
            #     input_size = [n_features]
            #     data = X[:, idx[:n_features]]
            #     train_data = data[train, :]
            #     test_data = data[test, :]
            #     batch_size = min(100, data.shape[0])
            #     epochs = (int(data.shape[0] / batch_size) + 1) * 60
            #     for i in range(reps):
            #         print('rep', i)
            #         model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
            #         info = model.fit(train_data, y[train], test_data, y[test], epochs=epochs,
            #                          batch_size=batch_s\ze, verbose=False)
            #         test_score.append(info['test']['accuracy'][-1][1])
            #         train_score.append(info['train']['accuracy'][-1][1])
            #         test_loss.append(info['test']['loss'][-1][1])
            #         train_loss.append(info['train']['loss'][-1][1])
            #         print('train = ', train_score[-1], ', test', test_score[-1])
            #     stats[n_features]['info']['train']['loss'].append(train_loss)
            #     stats[n_features]['info']['train']['accuracy'].append(train_score)
            #     stats[n_features]['info']['test']['loss'].append(test_loss)
            #     stats[n_features]['info']['test']['accuracy'].append(test_score)
            #     stats[n_features]['features'].append(idx[:n_features])
            #
            #     if n_features == int(n_features * factor):
            #         break
            #     n_features = int(n_features * factor)

            try:
                os.makedirs('./results/' + dataset[1] + '/')
            except OSError as e:
                pass

            try:
                with open('./results/' + dataset[1] + '/' + dataset[1] + '_feature_ranking.json') as datafile:
                    json_info = json.load(datafile)
            except:
                json_info = {}

            try:
                json_info[method] = info['rank'].tolist()
            except:
                json_info[method] = info['rank']

            with open('./results/' + dataset[1] + '/' + dataset[1] + '_feature_ranking.json', 'w') as outfile:
                json.dump(json_info, outfile)

            try:
                with open('./results/' + dataset[1] + '/' + dataset[1] + '_info.json') as datafile:
                    results = json.load(datafile)
            except:
                results = {}

            results[method] = info

            with open('./results/' + dataset[1] + '/' + dataset[1] + '_info.json', 'w') as outfile:
                json.dump(results, outfile)


if __name__ == '__main__':
    main()