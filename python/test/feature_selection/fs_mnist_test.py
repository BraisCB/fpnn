from python.train.feature_selection import get_features
from python.dataset_scripts.augmented_dataset import read_dataset
import json
import numpy as np
from copy import deepcopy

mnist = read_dataset('./datasets/mnist/', dataset='fs_mnist')

batch_size = min(100, mnist['train']['data'].shape[0])
epochs = 10000
keep_prob = 0.9

n_components = np.prod(mnist['train']['data'].shape[1:])

source = mnist['train']
labels = source['label']

validation_source = mnist['validation']
test_source = mnist['test']

data = source['data']
mean = np.mean(data, axis=0)
std = np.std(data, axis=0)
print('zeros', n_components - np.count_nonzero(std))
std[std == 0.0] = 1e-8
data = (data - mean) / std

validation_data = validation_source['data']
validation_data = (validation_data - mean) / std
validation_labels = validation_source['label']

test_data = test_source['data']
test_data = (test_data - mean) / std
test_labels = test_source['label']

input_size = list(data.shape[1:])

layers = [
    {
        'type': 'mask',
        'opts': {
            'regularization': {'ord': 1, 'lambda': 1e-4}
        }
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'conv2d', 'data': {'shape': [3, 3, 32]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
            {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
        ]
    },
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-4}
    #     }
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'conv2d', 'data': {'shape': [3,3,32]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
            {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
        ]
    },
    {'type': 'reshape', 'data': {'shape': [7*7*32]}},
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-4}
    #     }
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [1024]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
        ]
    },
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-4}
    #     }
    # },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [10],
                }
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 10, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

info = get_features(layers, global_opts, data, labels, validation_data, validation_labels, test_data, test_labels,
                    thresh=0.1, epochs=epochs, batch_size=batch_size,
                    reps=5, feat_type='filter_valid', reduce_data=False, ranking=False)

with open('./models/feature_selection/info_fs_mnist_total_lasso_test.json', 'w') as outfile:
    json.dump(info, outfile)
