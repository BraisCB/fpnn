import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.nips_challenge import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np


def balance_data(pdata, plabels):
    new_data = pdata.copy()
    new_labels = plabels.copy()
    balance = int(np.round(len(plabels[plabels == 1]) / len(plabels[plabels == 0])))
    if balance > 1:
        print('augmenting neg values by', balance)
        data_pos = pdata[plabels == 1]
        data_neg = np.tile(pdata[plabels == 0], (balance, 1))
        label_pos = plabels[plabels == 1]
        label_neg = np.tile(plabels[plabels == 0], balance)
        new_data = np.concatenate((data_pos, data_neg), axis=0)
        new_labels = np.concatenate((label_pos, label_neg), axis=0)
    elif balance == 0:
        balance = int(np.round(len(plabels[plabels == 0]) / len(plabels[plabels == 1])))
        if balance > 1:
            print('augmenting pos values by', balance)
            data_pos = np.tile(pdata[plabels == 1], (balance, 1))
            data_neg = pdata[plabels == 0]
            label_pos = np.tile(plabels[plabels == 1], balance)
            label_neg = plabels[plabels == 0]
            new_data = np.concatenate((data_pos, data_neg), axis=0)
            new_labels = np.concatenate((label_pos, label_neg), axis=0)
    perm = np.random.permutation(len(new_labels))
    new_data = new_data[perm]
    new_labels = new_labels[perm]
    return new_data, new_labels


layers = [
    # {'type': 'batch_normalization'},
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-3}
    #     }
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [300]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [225]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [150]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [75]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [2]
                },
                'opts': {'bias': False}
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-1, 'learning_decay': {'epochs': 80, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

reps = 1
factor = 0.75

kwargs = {
    'layers': layers,
    'global_opts': global_opts,
    'reps': reps,
    'factor': factor,
    'reduce_data': True
}

# methods = ['reliefF', 'MIM', 'MRMR', 'FCBF']
datasets = ['arcene', 'madelon', 'dexter', 'gisette', 'dorothea']

methods = ['ITdeepFS', 'ITdeepFS_ranking', 'deepFS', 'deepFS_ranking']
# methods = [
#     'deepLASSO', 'deepFS_lasso', 'deepLASSO_ranking', 'deepFS_lasso_ranking',
#     'ITdeepFS_lasso_ranking', 'ITdeepLASSO',
#     'ITdeepLASSO_ranking', 'ITdeepFS_lasso',
# ]
# datasets = ['dorothea']


def main():
    # load data
    for dataset in datasets:
        for method in methods:
            dataset_json = load_data('./datasets/' + dataset + '/' + dataset)
            X = dataset_json['train']['data']    # data
            #X = X.astype(float)
            y = dataset_json['train']['label']    # label
            # X, y = balance_data(X, y)
            n_samples, n_features = X.shape    # number of samples and number of features

            stats = {}
            # split data into 10 folds
                # obtain the index of selected features on training set
            max_val = np.max(X, axis=0)
            max_val[max_val == 0] = 1e-6

            if 'deep' in method:
                X /= max_val
                perm = np.random.permutation(len(dataset_json['validation']['data']))
                # len_valid = int(len(perm)/2)
                kwargs['valid_data'] = dataset_json['validation']['data']# [:len_valid]
                kwargs['valid_labels'] = dataset_json['validation']['label']# [:len_valid]
                # kwargs['valid_data'], kwargs['valid_labels'] = balance_data(
                #     dataset_json['validation']['data'], dataset_json['validation']['label']
                # )
                # kwargs['test_data'] = dataset_json['validation']['data'][len_valid:]
                # kwargs['test_labels'] = dataset_json['validation']['label'][len_valid:]
                kwargs['valid_data'] /= max_val
                # kwargs['valid_data'] /= max_val
                kwargs['batch_size'] = min(50, X.shape[0])
                kwargs['epochs'] = (int(X.shape[0] / kwargs['batch_size']) + 1) * 120

            print('obtaining indexes', method)
            idx = get_feature_indexes(X, y, method, **kwargs)
            print('indexes obtained')
            n_features = X.shape[1]

            # while n_features > 0:
            #     print('n_features', n_features)
            #     if n_features not in stats:
            #         stats[n_features] = {
            #             'features': [],
            #             'info': {
            #                 'train': {'accuracy': [], 'loss': []},
            #                 'test': {'accuracy': [], 'loss': []}
            #             }
            #         }
            #     train_score = []
            #     test_score = []
            #     train_loss = []
            #     test_loss = []
            #     input_size = [n_features]
            #     data = X[:, idx[:n_features]]
            #     train_data = data[train, :]
            #     test_data = data[test, :]
            #     batch_size = min(100, data.shape[0])
            #     epochs = (int(data.shape[0] / batch_size) + 1) * 60
            #     for i in range(reps):
            #         print('rep', i)
            #         model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
            #         info = model.fit(train_data, y[train], test_data, y[test], epochs=epochs,
            #                          batch_size=batch_s\ze, verbose=False)
            #         test_score.append(info['test']['accuracy'][-1][1])
            #         train_score.append(info['train']['accuracy'][-1][1])
            #         test_loss.append(info['test']['loss'][-1][1])
            #         train_loss.append(info['train']['loss'][-1][1])
            #         print('train = ', train_score[-1], ', test', test_score[-1])
            #     stats[n_features]['info']['train']['loss'].append(train_loss)
            #     stats[n_features]['info']['train']['accuracy'].append(train_score)
            #     stats[n_features]['info']['test']['loss'].append(test_loss)
            #     stats[n_features]['info']['test']['accuracy'].append(test_score)
            #     stats[n_features]['features'].append(idx[:n_features])
            #
            #     if n_features == int(n_features * factor):
            #         break
            #     n_features = int(n_features * factor)

            try:
                with open('./python/test/feature_selection/dataset_ranking/' + dataset + '.json') as datafile:
                    json_info = json.load(datafile)
            except:
                json_info = {}

            json_info[method] = idx.tolist()

            with open('./python/test/feature_selection/dataset_ranking/' + dataset + '.json', 'w') as outfile:
                json.dump(json_info, outfile)


if __name__ == '__main__':
    main()