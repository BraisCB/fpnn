import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.loader import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np
import os, errno


layers = [
    # {'type': 'batch_normalization'},
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': [{'ord': 1, 'lambda': 0.5}]
    #     }
    # },
    # {
    #     'type': 'block',
    #     'layers': [
    #         {'type': 'matmul', 'data': {'shape': [300]}},
    #         {'type': 'batch_normalization'},
    #         # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
    #         {'type': 'relu'},
    #     ]
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [225]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [150]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    # {'type': 'dropout', 'data': {'keep_prob': 0.5}},
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [75]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    # {'type': 'dropout', 'data': {'keep_prob': 0.5}},
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [10]
                },
                'opts': {'bias': False}
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

reps = 3
factor = 0.75
per = 1.0

datasets = [
    # ('./datasets/mnist/', 'mnist'),
    # ('./datasets/cifar-10/', 'cifar-10'),
    # ('./datasets/cifar-100/', 'cifar-100')
]


def main():
    # load data
    for dataset in datasets:
        print('dataset =', dataset)
        with open('./results/' + dataset[1] + '/' + dataset[1] + '_feature_ranking.json') as datafile:
            rankings = json.load(datafile)
        for method in rankings:
            print('method =', method)
            rank = rankings[method]
            if method == 'MIM':
                rank = (np.array(rank).astype(int) - 1).tolist()
            dataset_json = load_data(dataset[0], dataset[1])
            train_data = dataset_json['train']['data']    # data
            train_labels = dataset_json['train']['label']    # label

            valid_data = dataset_json['validation']['data']
            valid_labels = dataset_json['validation']['label']
            # valid_data, valid_labels = balance_data(valid_data, valid_labels)

            test_data = dataset_json['test']['data']
            test_labels = dataset_json['test']['label']

            max_val = np.max(train_data, axis=0)
            max_val[max_val == 0] = 1e-6
            train_data /= max_val
            valid_data /= max_val
            test_data /= max_val

            n_features = int(np.prod(train_data.shape[1:]))

            stats = {
                'validation': {'accuracy': {}, 'best_nfeatures': n_features},
                'test': {'accuracy': {}, 'best_nfeatures': n_features}
            }
            # best_conf = None
            # best_resu = None
            best_feats = None
            best_predictions = None
            best_score = 0
            best_test_score = 0
            best_n_features = 1e10
            while n_features > 0:
                print('n_features', n_features)
                print('best_score', best_score)
                print('best_test_score', best_test_score)
                print('best_features', best_n_features)
                valid_score = []
                input_size = [n_features]
                batch_size = min(100, train_data.shape[0])
                epochs = (int(dataset_json['train']['data'].shape[0] / batch_size) + 1) * 60
                for i in range(reps):
                    print('rep', i)
                    model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
                    info = model.fit(train_data[:, rank[:n_features]], train_labels,
                                     valid_data[:, rank[:n_features]], valid_labels,
                                     epochs=epochs, batch_size=batch_size, verbose=False)
                    valid_score.append(info['test']['accuracy'][-1][1])
                    step = -max(1, int(n_features/100))
                    print('score =', valid_score)
                    for j in range(n_features, 0, step):
                        mask = np.zeros(n_features)
                        mask[:j] = 1.0
                        mean_score = model.eval(mask*valid_data[:, rank[:n_features]], valid_labels)[0]
                        if j not in stats['validation']['accuracy']:
                            stats['validation']['accuracy'][j] = -1.0
                        if stats['validation']['accuracy'][j] < mean_score:
                            stats['validation']['accuracy'][j] = mean_score
                            if j not in stats['test']['accuracy']:
                                stats['test']['accuracy'][j] = -1.0
                            stats['test']['accuracy'][j] = model.eval(mask*test_data[:, rank[:n_features]], test_labels)[0]
                        if (mean_score > per*best_score and j < best_n_features) or mean_score > best_score:
                            print('score =', mean_score, ', n_elements =', j)
                            if mean_score > best_score:
                                best_score = mean_score
                            if j < best_n_features or per*mean_score > best_score:
                                # eval = model.eval(mask*test_data[:, rank[:n_features]], output_type='probabilities')[0]
                                # eval_l = np.argmax(eval, axis=-1)
                                # best_resu = (2.0 * eval_l - 1.0).astype(int)
                                # best_conf = 2.0 * ((1 - eval_l) * eval[:, 0] + eval_l*eval[:, 1] - 0.5)
                                best_feats = rank[:j]
                                best_test_score = model.eval(mask*test_data[:, rank[:n_features]], test_labels)[0]
                                best_n_features = j
                    del model

                if n_features == int(n_features * factor):
                    break
                n_features = int(n_features * factor)

            stats['best_nfeatures'] = best_n_features
            try:
                with open('./results/' + dataset[1] + '/' + dataset[1] + '_results_dense.json') as datafile:
                    results = json.load(datafile)
            except:
                results = {}

            results[method] = stats

            with open('./results/' + dataset[1] + '/' + dataset[1] + '_results_dense.json', 'w') as outfile:
                json.dump(results, outfile)

            # try:
            #     os.makedirs('./results/' + dataset[1] + '/' + method + '_predict_dense')
            # except OSError as e:
            #     pass

            # np.savetxt(
            #     './results/' + dataset[1] + '/' + method + '_predict_dense/' + dataset + '.feat',
            #     (np.array(best_feats) + 1).astype(int), fmt='%i'
            # )
            #
            # np.savetxt(
            #     './results/' + dataset[1] + '/' + method + '_predict_dense/' + dataset + '_test.predict',
            #     best_predictions, fmt='%2.5f'
            # )


if __name__ == '__main__':
    main()