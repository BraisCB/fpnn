import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.nips_challenge import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np

keep_prob = 0.75
layers = [
    # {'type': 'batch_normalization'},
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-3}
    #     }
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [300]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [225]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [150]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [75]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt', 'mean_correction': True}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [2]
                },
                'opts': {'bias': False}
            },
            {'type': 'softmax', 'data': {'factor': [0.1, 0.9]}}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 80, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

reps = 10
factor = 0.9

kwargs = {
    'layers': layers,
    'global_opts': global_opts,
    'reps': reps,
    'factor': factor,
    'reduce_data': True
}

# methods = ['CFS', 'reliefF', 'MIM', 'MRMR', 'FCBF']
datasets = ['arcene', 'gisette', 'madelon', 'dexter', 'dorothea']

methods = ['ITdeepFS_wrapper', 'ITdeepFS_wrapper_ranking', 'deepFS_wrapper', 'deepFS_wrapper_ranking']
# methods = [
#     'deepLASSO', 'deepFS_lasso',
#     'deepLASSO_ranking', 'deepFS_lasso_ranking','ITdeepFS_lasso_ranking'
#     'ITdeepLASSO', 'ITdeepLASSO_ranking', 'ITdeepFS_lasso',
# ]
datasets = ['dorothea']


def main():
    # load data
    for dataset in datasets:
        for method in methods:
            dataset_json = load_data('./datasets/' + dataset + '/' + dataset)
            X = dataset_json['train']['data']    # data
            #X = X.astype(float)
            y = dataset_json['train']['label']    # label
            n_samples, n_features = X.shape    # number of samples and number of features

            stats = {}
            # split data into 10 folds
                # obtain the index of selected features on training set
            max_val = np.max(X, axis=0)
            max_val[max_val == 0] = 1e-6

            X /= max_val

            if 'deep' in method:
                perm = np.random.permutation(len(dataset_json['validation']['data']))
                len_valid = int(len(perm)/2)
                kwargs['valid_data'] = dataset_json['validation']['data'][:len_valid]
                kwargs['valid_labels'] = dataset_json['validation']['label'][:len_valid]
                kwargs['test_data'] = dataset_json['validation']['data'][len_valid:]
                kwargs['test_labels'] = dataset_json['validation']['label'][len_valid:]
                kwargs['valid_data'] /= max_val
                kwargs['test_data'] /= max_val
                kwargs['batch_size'] = min(100, dataset_json['train']['data'].shape[0])
                kwargs['epochs'] = (int(dataset_json['train']['data'].shape[0] / kwargs['batch_size']) + 1) * 60

            print('obtaining indexes', method)
            idx = get_feature_indexes(X, y, method, **kwargs)
            print('indexes obtained')
            n_features = X.shape[1]

            # while n_features > 0:
            #     print('n_features', n_features)
            #     if n_features not in stats:
            #         stats[n_features] = {
            #             'features': [],
            #             'info': {
            #                 'train': {'accuracy': [], 'loss': []},
            #                 'test': {'accuracy': [], 'loss': []}
            #             }
            #         }
            #     train_score = []
            #     test_score = []
            #     train_loss = []
            #     test_loss = []
            #     input_size = [n_features]
            #     data = X[:, idx[:n_features]]
            #     train_data = data[train, :]
            #     test_data = data[test, :]
            #     batch_size = min(100, data.shape[0])
            #     epochs = (int(data.shape[0] / batch_size) + 1) * 60
            #     for i in range(reps):
            #         print('rep', i)
            #         model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
            #         info = model.fit(train_data, y[train], test_data, y[test], epochs=epochs,
            #                          batch_size=batch_size, verbose=False)
            #         test_score.append(info['test']['accuracy'][-1][1])
            #         train_score.append(info['train']['accuracy'][-1][1])
            #         test_loss.append(info['test']['loss'][-1][1])
            #         train_loss.append(info['train']['loss'][-1][1])
            #         print('train = ', train_score[-1], ', test', test_score[-1])
            #     stats[n_features]['info']['train']['loss'].append(train_loss)
            #     stats[n_features]['info']['train']['accuracy'].append(train_score)
            #     stats[n_features]['info']['test']['loss'].append(test_loss)
            #     stats[n_features]['info']['test']['accuracy'].append(test_score)
            #     stats[n_features]['features'].append(idx[:n_features])
            #
            #     if n_features == int(n_features * factor):
            #         break
            #     n_features = int(n_features * factor)

            try:
                with open('./python/test/feature_selection/dataset_ranking/' + dataset + '.json') as datafile:
                    json_info = json.load(datafile)
            except:
                json_info = {}

            json_info[method] = idx.tolist()

            with open('./python/test/feature_selection/dataset_ranking/' + dataset + '.json', 'w') as outfile:
                json.dump(json_info, outfile)


if __name__ == '__main__':
    main()