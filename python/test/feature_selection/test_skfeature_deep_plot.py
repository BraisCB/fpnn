import scipy.io
from sklearn import model_selection
from python.train.skfeature import get_feature_indexes
from python.dataset_scripts.nips_challenge import load_data
from python.fpnn import FpNN
from copy import deepcopy
import json
import numpy as np
import os, errno
from matplotlib import pyplot as pl

# datasets = ['arcene', 'gisette', 'madelon', 'dexter', 'dorothea']

datasets = [
    ('./datasets/mnist/', 'mnist', 0.75),
    ('./datasets/cifar-10/', 'cifar-10', 0.5),
    # ('./datasets/cifar-100/', 'cifar-100')
]

factor = 0.5



def main():
    # load data
    cont = 0
    for dataset in datasets:
        print(dataset)
        pl.figure(cont)
        factor = dataset[2]
        with open('./results/' + dataset[1] + '/' + dataset[1] + '_results_dense.json') as datafile:
            results = json.load(datafile)
        methods = list(results.keys())
        legends = []
        for subset in ['test']:
            for method in methods:
                if 'ranking' in method: # or 'IT' not in method:
                    continue
                keys = list(results[method][subset]['accuracy'].keys())
                x_axis = np.sort([int(k) for k in keys])
                max_x = np.max(x_axis)
                x_axis = []
                while max_x > 0:
                    x_axis.append(max_x)
                    max_x = int(max_x * factor)
                x_axis = np.array(x_axis)[::-1]
                y_axis = np.zeros_like(x_axis).astype(np.float32)
                auc = 0
                y_axis[0] = results[method][subset]['accuracy'][str(x_axis[0])]
                for i in range(1, len(x_axis)):
                    y_axis[i] = results[method][subset]['accuracy'][str(x_axis[i])]
                    auc += 0.5 * (y_axis[i] + y_axis[i-1]) * (x_axis[i] - x_axis[i-1])
                auc /= np.max(x_axis)
                pl.plot(x_axis[4:16], y_axis[4:16])
                best = np.argmax(y_axis)
                legends.append(method)
                print('subset =', subset, 'method =', method, ', mean =', y_axis[best],
                      ', nfeat =', x_axis[best], ', auc =', auc, ', full_mean =', y_axis[-1])
        pl.title(dataset[1])
        pl.legend(legends, loc='best')
        pl.show()
        cont += 1


if __name__ == '__main__':
    main()