from python.fpnn import FpNN
from python.dataset_scripts.nips_challenge import load_data
import json
import numpy as np
from python.train.feature_selection import get_features, get_relevant_features
from copy import deepcopy


def balance_data(pdata, plabels):
    new_data = pdata.copy()
    new_labels = plabels.copy()
    balance = int(np.round(len(plabels[plabels == 1]) / len(plabels[plabels == 0])))
    if balance > 1:
        print('augmenting neg values by', balance)
        data_pos = pdata[plabels == 1]
        data_neg = np.tile(pdata[plabels == 0], (balance, 1))
        label_pos = plabels[plabels == 1]
        label_neg = np.tile(plabels[plabels == 0], balance)
        new_data = np.concatenate((data_pos, data_neg), axis=0)
        new_labels = np.concatenate((label_pos, label_neg), axis=0)
    elif balance == 0:
        balance = int(np.round(len(plabels[plabels == 0]) / len(plabels[plabels == 1])))
        if balance > 1:
            print('augmenting pos values by', balance)
            data_pos = np.tile(pdata[plabels == 1], (balance, 1))
            data_neg = pdata[plabels == 0]
            label_pos = np.tile(plabels[plabels == 1], balance)
            label_neg = plabels[plabels == 0]
            new_data = np.concatenate((data_pos, data_neg), axis=0)
            new_labels = np.concatenate((label_pos, label_neg), axis=0)
    perm = np.random.permutation(len(new_labels))
    new_data = new_data[perm]
    new_labels = new_labels[perm]
    return new_data, new_labels


stats = []
keep_prob = 0.9
reps = 5

datasets = [
    'arcene',
    'dexter',
    'dorothea',
    'gisette',
    'madelon'
]

layers = [
    {'type': 'batch_normalization'},
    {
        'type': 'mask',
        'opts': {
            'regularization': {'ord': 1, 'lambda': 1e-4}
        }
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [300]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [225]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [150]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [75]}},
            {'type': 'batch_normalization'},
            # {'type': 'gaussian_dropout', 'data': {'keep_prob': keep_prob}, 'opts': {'factor': 'sqrt'}},
            {'type': 'relu'},
        ]
    },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [2]
                },
                'opts': {'bias': True}
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 40, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

for dataset in datasets:

    print('loading dataset', dataset)
    gisette = load_data('./datasets/' + dataset + '/' + dataset)
    print('data loaded. labels =', gisette['train']['data'].shape)

    source = gisette['train']
    valid_source = gisette['validation']
    test_source = gisette['test']

    labels = source['label']
    valid_labels = valid_source['label']

    data = source['data']
    valid_data = valid_source['data']
    test_data = test_source['data']
    # mean_data = np.mean(data, axis=0)
    # std_data = np.std(data, axis=0)
    # std_data[std_data == 0.0] = 1e-8
    max_val = np.max(np.concatenate((data, valid_data), axis=0), axis=0)
    # max_val = np.max(data, axis=0)
    # max_val = np.max(data)
    print('max_val', max_val)
    non_zeros = np.where(max_val != 0.0)[0]
    if len(non_zeros) != data.shape[1]:
        print('Removing zeros')
        data = data[:, non_zeros]
        valid_data = valid_data[:, non_zeros]
        test_data = test_data[:, non_zeros]
        max_val = max_val[non_zeros]
    # data = (data - mean_data) / std_data
    data /= max_val
    data, labels = balance_data(data, labels)


    valid_data /= max_val
    # valid_data = (valid_data - mean_data) / std_data
    valid_data, valid_labels = balance_data(valid_data, valid_labels)

    test_data /= max_val
    # test_data = (test_data - mean_data) / std_data

    batch_size = min(100, data.shape[0])
    epochs = (int(data.shape[0] / batch_size) + 1) * 60
    n_components = data.shape[-1]
    input_size = [n_components]

    # feat_info = get_relevant_features(layers, global_opts, data, labels, valid_data, valid_labels,
    #                                   epochs=epochs, batch_size=batch_size, reps=reps)

    feat_info = get_features(layers, global_opts, data, labels, valid_data, valid_labels,
                             thresh=0.01, epochs=epochs, batch_size=batch_size,
                             reps=reps, feat_type='wrapper', reduce_data=True, ranking=False)

    print(data.shape)
    data = data[:, feat_info['features']]
    valid_data = valid_data[:, feat_info['features']]
    test_data = test_data[:, feat_info['features']]
    best_score = 0
    for nr in (range(reps)):
        model = FpNN(input_size=list(data.shape[1:]), layers=deepcopy(layers), global_opts=global_opts)
        model.fit(data, labels, valid_data, valid_labels, epochs=epochs, batch_size=batch_size)
        n_components = data.shape[-1]
        step = max(int(n_components / 100), 1)
        mask = np.ones(n_components)
        for i in range(0, n_components, step):
            mask = np.ones(n_components)
            mask[:i] = 0.0
            mask = mask[::-1]
            nelements = n_components - i
            score = model.eval(valid_data * mask, valid_labels)[0]
            print('nelements', nelements, 'accuracy = ', score)
            if score > best_score:
                best_prob = 2.0 * model.eval(test_data * mask, output_type='probabilities')[0, :, 1] - 1.0
                best_score = score
                feat_info['features'] = feat_info['features'][:nelements]

        del model

    np.savetxt('./models/feature_selection/' + dataset + '_test.predict', best_prob)
    feat_info['dataset'] = dataset
    stats.append(feat_info)

with open('./models/feature_selection/info.json', 'w') as outfile:
    json.dump(stats, outfile)
