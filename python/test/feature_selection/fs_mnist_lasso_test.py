from python.train.feature_selection import get_features
from python.dataset_scripts.augmented_dataset import read_dataset
from python.fpnn import FpNN
import json
import numpy as np
from copy import deepcopy

mnist = read_dataset('./datasets/mnist/', dataset='fs_mnist')

batch_size = min(100, mnist['train']['data'].shape[0])
epochs = 10000
keep_prob = 0.9
reps = 5

n_components = np.prod(mnist['train']['data'].shape[1:])

source = mnist['train']
labels = source['label']

validation_source = mnist['validation']
test_source = mnist['test']

data = source['data']
mean = np.mean(data, axis=0)
std = np.std(data, axis=0)
print('zeros', n_components - np.count_nonzero(std))
std[std == 0.0] = 1e-8
data = (data - mean) / std

validation_data = validation_source['data']
validation_data = (validation_data - mean) / std
validation_labels = validation_source['label']

test_data = test_source['data']
test_data = (test_data - mean) / std
test_labels = test_source['label']

input_size = list(data.shape[1:])

layers = [
    {
        'type': 'mask',
        'opts': {
            'regularization': {'ord': 1, 'lambda': 1e-4}
        }
    },
    {
        'type': 'block',
        'layers': [
            {'type': 'conv2d', 'data': {'shape': [3, 3, 32]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
            {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
        ]
    },
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-4}
    #     }
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'conv2d', 'data': {'shape': [3,3,32]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
            {'type': 'max_pool', 'data': {'ksize': [2, 2], 'strides': [2, 2]}},
        ]
    },
    {'type': 'reshape', 'data': {'shape': [7*7*32]}},
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-4}
    #     }
    # },
    {
        'type': 'block',
        'layers': [
            {'type': 'matmul', 'data': {'shape': [1024]}},
            {'type': 'batch_normalization'},
            {'type': 'relu'},
        ]
    },
    # {
    #     'type': 'mask',
    #     'opts': {
    #         'regularization': {'ord': 1, 'lambda': 1e-4}
    #     }
    # },
    {
        'type': 'output',
        'layers': [
            {
                'type': 'matmul',
                'data': {
                    'shape': [10],
                }
            },
            {'type': 'softmax'}
        ]
    }
]

global_opts = {
    'train': {'learning_rate': 1e-2, 'learning_decay': {'epochs': 10, 'factor': 0.1}},
    # 'variables': {'l2_loss': {'lambda': 1e-2}}
}

model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
info = model.fit(data, labels, validation_data, validation_labels, epochs=epochs, batch_size=batch_size)

model.extract_data()
lasso_mask = np.array(model.layers[0]['data']['weights'])

order = np.argsort(np.abs(lasso_mask.flatten()))[::-1]

factor = 0.9
n_features = len(order)
lasso_stats = []
while n_features > 0:
    print()
    print('n_features', n_features)
    mask = np.zeros_like(lasso_mask)
    mask.flat[order[:n_features]] = 1.0
    score = []
    train_score = []
    test_score = []
    loss = []
    train_loss = []
    test_loss = []
    for i in range(reps):
        print('rep', i)
        model = FpNN(input_size=input_size, layers=deepcopy(layers), global_opts=global_opts)
        info = model.fit(mask*data, labels, mask*validation_data, validation_labels, epochs=epochs, batch_size=batch_size)
        score.append(info['test']['accuracy'][-1][1])
        train_score.append(info['train']['accuracy'][-1][1])
        loss.append(info['test']['loss'][-1][1])
        train_loss.append(info['train']['loss'][-1][1])
        print('train = ', train_score[-1], ', validation', score[-1])
        if test_data is not None:
            test_score.append(model.eval(test_data, test_labels)[0])
            test_loss.append(model.eval(test_data, test_labels, output_type='loss')[0])
            print('test = ', test_score[-1])
    stats = {
        'map': np.where(mask.flatten() > 0)[0].tolist(),
        'stats': {
            'train': {'accuracy': train_score, 'loss': train_loss},
            'validation': {'accuracy': score, 'loss': loss},
            'test': {'accuracy': test_score, 'loss': test_loss}
        }
    }
    lasso_stats.append({
        'info': stats,
        'features': np.where(mask.flatten() > 0)[0].tolist(),
        'nfeatures': np.count_nonzero(mask.flatten())
    })

    if n_features == int(n_features*factor):
        break
    n_features = int(n_features*factor)

with open('./models/feature_selection/info_fs_mnist_lasso_test.json', 'w') as outfile:
    json.dump(lasso_stats, outfile)
