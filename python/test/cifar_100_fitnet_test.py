from tensorflow.examples.tutorials.mnist import input_data
from python import fpnn
from python.dataset_scripts.CIFAR_100 import load_data


cifar_100 = load_data("./datasets/cifar-100/", flip_left_right=True)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop


input_size = [32, 32, 3]

layers = [
    # {'type': 'random_flip_left_right'},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 3, 16]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 16, 16]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 16, 16]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 16, 32]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 32]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 32]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 48]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 48, 48]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 48, 64]}},
    # {'type': 'batch_normalization'},
    {'type': 'relu'},
    {'type': 'reshape', 'opts': {'shape': [8*8*64]}},
    {'type': 'matmul', 'opts': {'shape': [8 * 8 * 64, 500], 'batch_size': 500, 'repetitions': 1}},
    {'type': 'relu', 'opts': {'precompute': True}},
    #{'type': 'dropout', 'opts': {'keep_prob': 0.5, 'precompute': False}},
    # {'type': 'dropout', 'opts': {'keep_prob': 0.5}},
    {'type': 'softmax', 'layers': [

        # {'type': 'matmul', 'opts': {'shape': [7*7*64, 1024], 'batch_size': 200, 'fine_tuning': True}},
        # {'type': 'batch_normalization', 'opts': {'precompute': False}},
        {'type': 'matmul', 'opts': {'shape': [500, 100]}}
    ]}
]

global_opts = {
    # 'variables': {'weight_decay': 1e-4},
    'backprop_train': {#'type': 'momentum', 'use_nesterov': True,
                       'learning_decay': {'epochs': 40, 'factor': 0.5}},
                       #'learning_rate': 0.1},
    'frontprop_train': {}#'flips': ['left_right']}#'augmented_label': {}}
}

model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)

# model.frontprop_train(cifar_100['train']['data'], cifar_100['train']['label'],
#                       cifar_100['test']['data'], cifar_100['test']['label'])
#
# model.save_to_file('./models/cifar-100/cifar_100_test_frontprop.json')

model.backprop_train(cifar_100['train']['data'], cifar_100['train']['label'],
                     cifar_100['test']['data'], cifar_100['test']['label'], epochs=30000)

model.save_to_file('./models/cifar-100/cifar_100_test_finetuning.json')
