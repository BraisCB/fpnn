import json
import numpy as np


def eval_info(info_file):
    with open(info_file) as outfile:
        info = json.load(outfile)

        for name in info:
            data = info[name]
            print(name)
            for subset in ['test']: # ['train', 'test']:
                accuracy = []
                loss = []
                for example in data:
                    accuracy.append(
                        example[subset]['accuracy'][-1][-1]
                    )
                    loss.append(
                        example[subset]['loss'][-1][-1]
                    )
                print(subset, 'stats')
                print('loss', np.mean(loss), '+-', np.std(loss))
                print('accuracy', np.mean(accuracy), '+-', np.std(accuracy))
                print('list', accuracy)


file = './python/test/stable_dropout/info/cifar10_wrn_16_4_bn.json'
eval_info(file)
