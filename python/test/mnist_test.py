from tensorflow.examples.tutorials.mnist import input_data
from python import fpnn


mnist = input_data.read_data_sets("./datasets/mnist/", one_hot=False)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

input_size = [784]

layers = [
    {'type': 'reshape', 'opts': {'shape': [28, 28, 1]}},
    {'type': 'batch_normalization', 'opts': {'shape': [28, 28, 1]}},
    {'type': 'gaussian_dropout', 'opts': {'keep_prob': 0.9}},
    {'type': 'block', 'layers': [
                                    {'type': 'conv2d', 'opts': {'shape': [5, 5, 1, 32]}},
                                    {'type': 'batch_normalization', 'opts': {'shape': [28, 28, 32]}},
                                    {'type': 'gaussian_dropout', 'opts': {'keep_prob': 0.9}},
                                    {'type': 'relu'},
                                    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
                                ]
    },
    {'type': 'block', 'layers': [
                                    {'type': 'conv2d', 'opts': {'shape': [5, 5, 32, 64]}},
                                    {'type': 'batch_normalization', 'opts': {'shape': [14, 14, 64]}},
                                    {'type': 'gaussian_dropout', 'opts': {'keep_prob': 0.9}},
                                    {'type': 'relu'},
                                    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
                                    {'type': 'reshape', 'opts': {'shape': [7*7*64]}},
                                ]
    },
    {'type': 'block', 'layers': [
                                    {'type': 'matmul', 'opts': {'shape': [7*7*64, 1024]}},
                                    {'type': 'batch_normalization', 'opts': {'shape': [1024]}},
                                    {'type': 'gaussian_dropout', 'opts': {'keep_prob': 0.9}},
                                    {'type': 'relu'},
                                ]
    },
    {'type': 'softmax', 'layers': [
        {'type': 'matmul', 'opts': {'shape': [1024, 10]}}
    ]}
]

global_opts = {
    # 'backprop_train': {'type': 'momentum', 'use_nesterov': True,
    #                    'learning_decay': {'epochs': 10, 'factor': 0.5}},
    'backprop_train': {'learning_rate': 5e-4, 'learning_decay': {'epochs': 25, 'factor': 0.1}},
    'frontprop_train': {}
}

model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)

#model.frontprop_train(mnist.train.images, mnist.train.labels, mnist.test.images, mnist.test.labels)
model.backprop_train(mnist.train.images, mnist.train.labels, mnist.test.images, mnist.test.labels,
                     total_training=True, layer_by_layer=True, epochs=7000)

