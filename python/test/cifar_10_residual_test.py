from tensorflow.examples.tutorials.mnist import input_data
from python import fpnn
from python.dataset_scripts.CIFAR_10 import load_data


cifar_10 = load_data("./datasets/cifar-10/", flip_left_right=True)
# mnist = input_data.read_data_sets("/Users/brais/Documents/bitbucket/fpnn/datasets/mnist/", one_hot=True) # backprop

# 73.07 en backprop
input_size = [32, 32, 3]

layers = [
    {'type': 'conv2d', 'opts': {'shape': [3, 3, 3, 32]}},
    {'type': 'relu'},
    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
    {'type': 'residual_block', 'layers': [
        {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 32]}},
        {'type': 'relu'},
        {'type': 'conv2d', 'opts': {'shape': [3, 3, 32, 32]}},
    ]},
    {'type': 'relu'},
    {'type': 'max_pool', 'opts': {'ksize': [1, 2, 2, 1], 'strides': [1, 2, 2, 1]}},
    {'type': 'reshape', 'opts': {'shape': [8*8*32]}},
    # {'type': 'dropout', 'opts': {'keep_prob': 0.5, 'precompute': True}},
    {'type': 'matmul', 'opts': {'shape': [8*8*32, 1024], 'batch_size': 500, 'repetitions': 1}},
    # {'type': 'matmul', 'opts': {'shape': [8*8*64, 1024], 'batch_size': 200, 'fine_tuning': True}},
    {'type': 'relu', 'opts': {'precompute': True}},
    # {'type': 'dropout', 'opts': {'keep_prob': 0.5}},
    {'type': 'softmax', 'layers': [
        {'type': 'matmul', 'opts': {'shape': [1024, 10]}}
    ]}
]

global_opts = {
    # 'variables': {'weight_decay': 1e-4},
    'backprop_train': {#'type': 'momentum', 'use_nesterov': True,
                       'learning_decay': {'epochs': 40, 'factor': 0.5}},
                       #'learning_rate': 0.1},
    'frontprop_train': {'flips': ['left_right']}
}

model = fpnn.FpNN(input_size=input_size, layers=layers, global_opts=global_opts)

model.frontprop_train(cifar_10['train']['data'], cifar_10['train']['label'],
                     cifar_10['test']['data'], cifar_10['test']['label'])

model.save_to_file('./models/cifar-10/cifar_10_residual_test_frontprop.json')

model.backprop_train(cifar_10['train']['data'], cifar_10['train']['label'],
                     cifar_10['test']['data'], cifar_10['test']['label'], epochs=20000)

model.save_to_file('./models/cifar-10/cifar_10_residual_test_finetuning.json')
