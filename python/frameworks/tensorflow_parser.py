import tensorflow as tf
import numpy as np
from python.train.vector_utils import dict_merge
from copy import deepcopy
import math


def get_deep_features_tf(fpnn, data, batch_size=50, is_training=False, layer=-1):
    if not fpnn.model:
        build_tf(fpnn)
    result = []
    index = 0
    while index < len(data):
        new_index = min(len(data), index + batch_size)
        feed_dict = get_feed_dict_tf(fpnn, data[index:new_index], is_training=is_training)
        index = new_index
        result += fpnn.model[layer]['operation'].eval(feed_dict=feed_dict).tolist()
    result = np.array(result)
    return result


def get_op_features_tf(fpnn, operation, data, batch_size=50, is_training=False, output_type=None):
    if not fpnn.model:
        build_tf(fpnn)
    result = []
    index = 0
    while index < len(data):
        new_index = min(len(data), index + batch_size)
        feed_dict = get_feed_dict_tf(fpnn, data[index:new_index], is_training=is_training)
        index = new_index
        if output_type is None or output_type == 'features':
            aux = fpnn.sess.run(operation, feed_dict=feed_dict)
            if not result:
                result = aux
            else:
                if isinstance(aux, list):
                    for i, v in enumerate(aux):
                        result[i] = np.concatenate((result[i], v), axis=0)
                else:
                    result = np.concatenate((result, aux), axis=0)
        elif output_type == 'mean':
            if isinstance(result, list):
                result = np.sum(fpnn.sess.run(operation, feed_dict=feed_dict).tolist(), axis=0)
            else:
                result += np.sum(fpnn.sess.run(operation, feed_dict=feed_dict).tolist(), axis=0)
    if output_type == 'mean':
        result /= len(data)
    return result


def eval_tf(fpnn, data, label=None, batch_size=50, is_training=False, output_type=None):
    if not fpnn.model:
        build_tf(fpnn)
    result = []
    if not fpnn.outputs or output_type == 'deep_features':
        index = 0
        while index < len(data):
            new_index = min(len(data), index + batch_size)
            feed_dict = get_feed_dict_tf(fpnn, data[index:new_index], is_training=is_training)
            index = new_index
            result += fpnn.sess.run(fpnn.model[-1]['operation'], feed_dict=feed_dict).tolist()
    else:
        for o in fpnn.outputs:
            if output_type == 'loss' or (output_type is None and label is not None) or output_type == 'accuracy':
                new_result = 0
            else:
                new_result = []
            index = 0
            cont = 0
            ind = 0
            while index < len(data):
                new_index = min(len(data), index + batch_size)
                if label is not None:
                    feed_dict = get_feed_dict_tf(
                        fpnn, data[index:new_index], label[index:new_index], is_training=is_training
                    )
                else:
                    feed_dict = get_feed_dict_tf(
                        fpnn, data[index:new_index], is_training=is_training
                    )
                if (output_type is None and label is not None) or output_type == 'accuracy':
                    new_result += fpnn.sess.run(o['validation']['accuracy'], feed_dict=feed_dict) * (new_index - index)
                elif output_type == 'loss':
                    new_result += np.sum(fpnn.sess.run(o['train']['loss'], feed_dict=feed_dict)) * (new_index - index)
                elif output_type == 'labels':
                    new_result += np.argmax(fpnn.sess.run(o['operation'], feed_dict=feed_dict), 1).tolist()
                elif output_type == 'probabilities' or label is None:
                    new_result += fpnn.sess.run(o['operation'], feed_dict=feed_dict).tolist()
                cont += (new_index - index)
                ind += 1
                index = new_index
            if output_type in [None, 'accuracy', 'loss']:
                new_result /= cont
            result.append(new_result)

    result = np.array(result)
    return result


def build_tf(fpnn, trainable=True):
    if fpnn.sess is not None:
        fpnn.sess.close()
    tf.reset_default_graph()
    fpnn.sess = tf.InteractiveSession()
    fpnn.model = []
    fpnn.outputs = []
    fpnn.is_testing = tf.placeholder(tf.bool)
    fpnn.is_backprop_train = tf.placeholder(tf.bool)
    fpnn.regularization['custom'] = []
    fpnn.extra_ops = {}
    fpnn.model.append({'type': 'placeholder', 'operation': tf.placeholder(tf.float32, shape=[None] + fpnn.input_size)})
    if len(fpnn.layers) == 0:
        fpnn.model.append({'operation': tf.reshape(fpnn.model[-1]['operation'], [-1] + fpnn.input_size)})
    index = {'cont': 0}
    for i in range(len(fpnn.layers)):
        layer = fpnn.layers[i]
        layer_opts = dict_merge(fpnn.global_opts, layer)
        build_layer = __build_model_layer(fpnn.model[-1], layer_opts, trainable, fpnn.is_testing,
                                          fpnn.is_backprop_train, index, fpnn.regularization)
        if layer_opts['type'] in ['output']:
            fpnn.outputs.append(build_layer)
        else:
            fpnn.model.append(build_layer)
        index['cont'] += 1
    fpnn.sess.run(tf.global_variables_initializer())


def __build_model_layer(last_layer, layer, trainable, test, backprop_train, index, regularization):
    last_operation = last_layer['operation']
    if layer['type'] == 'output':
        model_layer = __build_tf_output_layer(last_layer, layer, trainable, test, backprop_train, index, regularization)
    elif layer['type'] == 'conv2d':
        model_layer = __build_tf_conv2d_layer(last_operation, layer, trainable=trainable, index=index, regularization=regularization)
    elif 'pool' in layer['type']:
        model_layer = __build_tf_pool_layer(last_operation, layer)
    elif layer['type'] == 'signlog':
        model_layer = {'operation': tf.sign(last_operation)*tf.log(1.0 + 10*tf.square(last_operation))}
    elif layer['type'] == 'tanhlog':
        model_layer = {'operation': tf.tanh(last_operation)*tf.log(1.0 + tf.abs(last_operation))}
    elif layer['type'] == 'random_rotate':
        model_layer = __build_tf_random_rotate_layer(last_operation, layer)
    elif layer['type'] == 'softmax':
        model_layer = __build_tf_softmax_layer(last_operation, layer)
    elif layer['type'] == 'decoupled_softmax':
        model_layer = __build_tf_decoupled_softmax_layer(last_operation, layer)
    elif layer['type'] == 'decoupled_plane_softmax':
        model_layer = __build_tf_decoupled_plane_softmax_layer(last_operation, layer)
    elif layer['type'] == 'hinge':
        model_layer = __build_tf_hinge_layer(last_operation, layer)
    elif layer['type'] == 'orthogonal_distance':
        model_layer = __build_tf_orthogonal_distance_layer(last_operation, layer)
    elif layer['type'] == 'orthogonal_plane_distance':
        model_layer = __build_tf_orthogonal_plane_distance_layer(last_operation, layer)
    elif layer['type'] == 'orthogonal_hinge':
        model_layer = __build_tf_orthogonal_hinge_layer(last_operation, layer)
    elif layer['type'] == 'orthogonal_plane_hinge':
        model_layer = __build_tf_orthogonal_plane_hinge_layer(last_operation, layer)
    elif layer['type'] == 'orthogonal':
        model_layer = __build_tf_orthogonal_layer(last_operation, layer)
    elif layer['type'] == 'orthogonal_plane':
        model_layer = __build_tf_orthogonal_plane_layer(last_operation, layer)
    elif layer['type'] == 'orthogonal_softmax':
        model_layer = __build_tf_orthogonal_softmax_layer(last_operation, layer)
    elif layer['type'] == 'matmul':
        model_layer = __build_tf_matmul_layer(last_operation, layer, trainable=trainable, index=index, regularization=regularization)
    elif layer['type'] == 'mask':
        model_layer = __build_tf_mask_layer(last_operation, layer, trainable=trainable, index=index, regularization=regularization)
    elif layer['type'] in ['dropout', 'gaussian_dropout', 'gaussian_noise']:
        model_layer = __build_tf_dropout_layer(last_layer, layer, test)
    elif layer['type'] == 'reshape':
        model_layer = {'operation': tf.reshape(last_operation, [-1] + layer['data']['shape'])}
    elif layer['type'] == 'flatten':
        model_layer = {'operation': tf.reshape(last_operation, [-1] + [int(np.prod(last_operation.get_shape().as_list()[1:]))])}
    elif layer['type'] == 'l2_normalize':
        model_layer = __build_tf_l2_normalize_layer(last_operation, layer)
    elif layer['type'] in ['batch_normalization']:
        model_layer = __build_tf_batch_normalization_layer(last_operation, layer, trainable, test, index)
    elif layer['type'] in ['block', 'residual_block']:
        model_layer = __build_tf_block_layer(last_layer, layer, trainable, test, backprop_train, index, regularization=regularization)
    elif layer['type'] == 'train_test_option':
        model_layer = __build_tf_train_test_option_layer(last_layer, layer, trainable, test, backprop_train, index, regularization=regularization)
    elif layer['type'] == 'inception_block':
        model_layer = __build_tf_inception_block_layer(last_layer, layer, trainable, test, backprop_train, index, regularization=regularization)
    else:
        args = {}
        if 'args' in layer:
            args = layer['args']
        try:
            functions = layer['type'].split('.')
            op = tf
            for func in functions:
                op = getattr(op, func)
            if 'map_fn' in layer and layer['map_fn']:
                model_layer = {
                    'operation': tf.map_fn(lambda map_op: op(map_op, **args), last_operation)
                }
            else:
                model_layer = {'operation': op(last_operation, **args)}
        except:
            model_layer = {'operation': getattr(tf.nn, layer['type'])(last_operation, **args)}
    model_layer['type'] = layer['type']
    model_layer['info'] = {
        'shape': {
            'in': last_operation.get_shape().as_list()[1:],
            'out': model_layer['operation'].get_shape().as_list()[1:]
        }
    }
    return model_layer


def __initialize_weights_and_bias(layer, trainable, index, regularization, channels):
    if 'data' in layer and 'trainable' in layer['data']:
        trainable = layer['data']['trainable']
    initializer = 'truncated_normal'
    if 'data' in layer and 'initializer' in layer['data']:
        initializer = layer['data']['initializer']
    output = {'variables': {}}
    w_shape = layer['data']['shape'][:-1] + channels + layer['data']['shape'][-1:]
    bias = 'opts' in layer and 'bias' in layer['opts'] and layer['opts']['bias']
    if 'data' not in layer or 'weights' not in layer['data']:
        stddev = 0.1
        if 'data' in layer and 'stddev' in layer['data']:
            stddev = layer['data']['stddev']
        if initializer == 'truncated_normal':
            initializer_function = tf.truncated_normal_initializer(stddev=stddev)
        elif initializer == 'orthogonal':
            initializer_function = tf.orthogonal_initializer(gain=stddev)
        elif initializer == 'random_normal':
            initializer_function = tf.random_normal_initializer(stddev=stddev)
        elif initializer == 'random_uniform':
            initializer_function = tf.random_uniform_initializer(minval=-stddev, maxval=stddev)
        elif initializer == 'xavier':
            initializer_function = tf.contrib.layers.xavier_initializer()
        else:
            raise Exception('Weight initialization function not supported')
        W = tf.get_variable("W" + str(index['cont']), shape=w_shape, trainable=True,
                            dtype=tf.float32, initializer=initializer_function)
    else:
        print('trainable', trainable)
        W = tf.get_variable("W" + str(index['cont']), shape=w_shape, trainable=trainable,
                            dtype=tf.float32, initializer=tf.constant_initializer(layer['data']['weights']))
    output['variables']['weights'] = W
    output['variables']['shape'] = layer['data']['shape']

    if bias:
        if 'data' not in layer or 'bias' not in layer['data']:
            bias_factor = 0.005
            if 'data' in layer and 'bias_factor' in layer['data']:
                bias_factor = layer['data']['bias_factor']
            b = tf.get_variable("b" + str(index['cont']), shape=layer['data']['shape'][-1:], trainable=True,
                                dtype=tf.float32, initializer=tf.constant_initializer(bias_factor))
        else:
            b = tf.get_variable("b" + str(index['cont']), shape=layer['data']['shape'][-1:], trainable=trainable,
                                dtype=tf.float32, initializer=tf.constant_initializer(layer['data']['bias']))
        output['variables']['bias'] = b

    if 'regularization' in layer:
        for reg in layer['regularization']:
            ord = reg['ord']
            lamda = 1.0
            if 'lambda' in reg:
                lamda = float(reg['lambda'])
            print('adding regularization', reg['ord'], 'lambda', lamda)
            regularization['custom'].append(tf.multiply(lamda, tf.norm(W, ord=ord)))
    return output


def __build_tf_conv2d_layer(last_operation, layer, trainable, index, regularization):
    shape = last_operation.get_shape().as_list()
    output = __initialize_weights_and_bias(layer, trainable, index, regularization, shape[-1:])
    strides = [1, 1]
    if 'strides' in layer['data']:
        strides = layer['data']['strides']
    padding = 'SAME'
    if 'padding' in layer['data']:
        padding = layer['data']['padding']
    output['operation'] = tf.nn.conv2d(
        last_operation, output['variables']['weights'], strides=[1] + strides + [1], padding=padding
    )
    if 'bias' in output['variables']:
        output['operation'] += output['variables']['bias']
    output['variables']['strides'] = strides
    output['variables']['padding'] = padding
    return output


def __build_tf_matmul_layer(last_operation, layer, trainable, index, regularization):
    shape = last_operation.get_shape().as_list()
    output = __initialize_weights_and_bias(layer, trainable, index, regularization, shape[-1:])
    output['operation'] =  tf.matmul(last_operation, output['variables']['weights'])
    if 'bias' in output['variables']:
        output['operation'] += output['variables']['bias']
    return output


def __build_tf_mask_layer(last_operation, layer, trainable, index, regularization):
    shape = last_operation.get_shape().as_list()
    if 'data' not in layer:
        layer['data'] = {}
    layer['data']['shape'] = shape[1:]
    layer['data']['weights'] = np.ones(layer['data']['shape']).tolist()
    output = __initialize_weights_and_bias(layer, trainable, index, regularization, [])
    output['operation'] = tf.multiply(last_operation, np.abs(output['variables']['weights']))
    return output


def __build_tf_pool_layer(last_operation, layer):
    if 'fractional' not in layer['type']:
        ksize = [2, 2]
        if 'data' in layer and 'ksize' in layer['data']:
            ksize = layer['data']['ksize']
        strides = [2, 2]
        if 'data' in layer and 'strides' in layer['data']:
            strides = layer['data']['strides']
        padding = 'SAME'
        if 'data' in layer and 'padding' in layer['data']:
            padding = layer['data']['padding']
        pool_func = tf.nn.max_pool
        if layer['type'] == 'avg_pool':
            pool_func = tf.nn.avg_pool
        output = {
            'operation': pool_func(last_operation, ksize=[1] + ksize + [1],
                                   strides=[1] + strides + [1], padding=padding),
            'variables': {'ksize': ksize, 'strides': strides, 'padding': padding}
        }
    else:
        pooling_ratio = [1.44, 1.44]
        if 'data' in layer and 'pooling_ratio' in layer['data']:
            pooling_ratio = layer['data']['pooling_ratio']
        pool_func = tf.nn.fractional_max_pool
        if layer['type'] == 'fractional_avg_pool':
            pool_func = tf.nn.fractional_avg_pool
        output = {
            'operation': pool_func(last_operation, pooling_ratio=[1.0] + pooling_ratio + [1.0])[0],
            'variables': {'pooling_ratio': pooling_ratio}
        }
    return output


def __build_tf_dropout_layer(last_layer, layer, test):
    last_operation = last_layer['operation']
    keep_prob = 0.5
    if 'data' in layer and 'keep_prob' in layer['data']:
        keep_prob = layer['data']['keep_prob']

    dropout_layer = {'placeholders': {'keep_prob': tf.placeholder(tf.float32, shape=())},
                     'variables': {'keep_prob': keep_prob}}

    def if_train():
        operation = last_operation
        is_bn = 'opts' in layer and 'mean_correction' in layer['opts'] and layer['opts']['mean_correction']
        if is_bn:
            shape = last_operation.get_shape()
            dims = list(range(len(shape[1:])))
            batch_mean, _ = tf.nn.moments(last_operation, dims)
            operation = operation - batch_mean
        if layer['type'] == 'dropout':
            operation = tf.nn.dropout(operation, keep_prob=dropout_layer['placeholders']['keep_prob'])
            if 'opts' in layer and 'factor' in layer['opts']:
                if layer['opts']['factor'] is None:
                    operation = tf.multiply(dropout_layer['placeholders']['keep_prob'], operation)
                elif layer['opts']['factor'] == 'sqrt':
                    operation = tf.multiply(tf.sqrt(dropout_layer['placeholders']['keep_prob']), operation)
        elif 'gaussian' in layer['type']:
            stddev = tf.sqrt((1.0 - dropout_layer['placeholders']['keep_prob']) / dropout_layer['placeholders']['keep_prob'])
            if layer['type'] == 'gaussian_dropout':
                noise = tf.random_normal(shape=tf.shape(operation), mean=1.0, stddev=stddev, dtype=tf.float32)
                if 'opts' in layer and 'factor' in layer['opts']:
                    if layer['opts']['factor'] == 'sqrt':
                        operation = tf.multiply(tf.sqrt(dropout_layer['placeholders']['keep_prob']), operation)
                    elif layer['opts']['factor'] is None:
                        operation = tf.multiply(dropout_layer['placeholders']['keep_prob'], operation)
                operation = tf.multiply(noise, operation)
            else:
                noise = tf.random_normal(shape=tf.shape(operation), mean=0.0, stddev=stddev, dtype=tf.float32)
                operation = tf.add(noise, operation)
        else:
            raise Exception('Dropout operation not supported')
        if is_bn:
            operation = operation + batch_mean
        return operation

    def if_test():
        if 'opts' in layer and 'factor' in layer['opts'] and layer['opts']['factor'] is None:
            return tf.multiply(dropout_layer['placeholders']['keep_prob'], last_operation)
        return tf.identity(last_operation)

    dropout_layer['operation'] = tf.cond(test, if_test, if_train)
    return dropout_layer


def __build_tf_batch_normalization_layer(last_operation, layer, trainable, test, index):
    if 'variables' in layer and 'trainable' in layer['variables']:
        trainable = layer['variables']['trainable']

    if 'opts' in layer and 'momentum' in layer['opts']:
        print('changing momentum')
        momentum = layer['opts']['momentum']
    else:
        momentum = 0.95
    epsilon = 1e-5
    shape = last_operation.get_shape()
    if 'opts' in layer and 'epsilon' in layer['opts']:
        epsilon = layer['opts']['epsilon']
    if 'data' not in layer or 'mean' not in layer['data']:
        trainable = True
        mean = tf.get_variable("mean" + str(index['cont']), shape=shape[-1:], trainable=False,
                               dtype=tf.float32, initializer=tf.constant_initializer(0.0))
    else:
        mean = tf.get_variable("mean" + str(index['cont']), shape=shape[-1:], trainable=False,
                               dtype=tf.float32, initializer=tf.constant_initializer(layer['data']['mean']))
    if 'data' not in layer or 'var' not in layer['data']:
        trainable = True
        var = tf.get_variable("var" + str(index['cont']), shape=shape[-1:], trainable=False,
                               dtype=tf.float32, initializer=tf.constant_initializer(1.0))
    else:
        var = tf.get_variable("var" + str(index['cont']), shape=shape[-1:], trainable=False,
                               dtype=tf.float32, initializer=tf.constant_initializer(layer['data']['var']))
    if 'data' not in layer or 'scale' not in layer['data']:
        scale = tf.get_variable("scale" + str(index['cont']), shape=shape[-1:], trainable=trainable,
                               dtype=tf.float32, initializer=tf.constant_initializer(1.0))
    else:
        scale = tf.get_variable("scale" + str(index['cont']), shape=shape[-1:], trainable=trainable,
                               dtype=tf.float32, initializer=tf.constant_initializer(layer['data']['scale']))
    if 'data' not in layer or 'offset' not in layer['data']:
        offset = tf.get_variable("offset" + str(index['cont']), shape=shape[-1:], trainable=trainable,
                               dtype=tf.float32, initializer=tf.constant_initializer(0.0))
    else:
        offset = tf.get_variable("offset" + str(index['cont']), shape=shape[-1:], trainable=trainable,
                                 dtype=tf.float32, initializer=tf.constant_initializer(layer['data']['offset']))

    def if_train():
        dims = list(range(len(shape[1:])))
        batch_mean, batch_var = tf.nn.moments(last_operation, dims)
        if trainable:
            train_mean = tf.assign(mean, mean * momentum + batch_mean * (1.0 - momentum))
            train_var = tf.assign(var, var * momentum + batch_var * (1.0 - momentum))
            with tf.control_dependencies([train_mean, train_var]):
                return tf.nn.batch_normalization(
                    last_operation, batch_mean, batch_var, offset, scale, epsilon
                )
        else:
            return tf.nn.batch_normalization(
                last_operation, batch_mean, batch_var, offset, scale, epsilon
            )

    def if_test():
        return tf.nn.batch_normalization(
            last_operation, mean, var, offset, scale, epsilon
        )

    return {'operation': tf.cond(test, if_test, if_train),
            'variables': {'mean': mean, 'var': var, 'scale': scale, 'offset': offset}}


def __build_tf_l2_normalize_layer(last_operation, layer):
    shape = np.array(last_operation.get_shape().as_list()[1:])
    normalization_layer = {}
    if len(shape) > 2:
        normalization_layer['reshape'] = tf.reshape(last_operation, [-1] + [np.prod(shape)])
        normalization_layer['normalization'] = tf.nn.l2_normalize(normalization_layer['reshape'], dim=1)
        normalization_layer['operation'] = tf.reshape(normalization_layer['normalization'], [-1] + shape.tolist())
    else:
        normalization_layer['operation'] = tf.nn.l2_normalize(last_operation, dim=-1)
    return normalization_layer


def __build_tf_random_rotate_layer(last_operation, layer):
    rotation_range = 10
    if 'opts' in layer and 'rotation_range' in layer['opts']:
        rotation_range = layer['opts']['rotation_range']
    interpolation = 'NEAREST'
    if 'opts' in layer and 'interpolation' in layer['opts']:
        interpolation = layer['opts']['interpolation']
    random_degree = tf.random_uniform(
        [tf.shape(last_operation)[0]],
        minval=-rotation_range * math.pi / 180.0,
        maxval=rotation_range * math.pi / 180.0,
    )
    return {
        'operation': tf.contrib.image.rotate(last_operation, random_degree, interpolation)
    }


def __build_tf_train_test_option_layer(last_operation, layer, trainable, test, backprop_train, index, regularization):
    shared_dict = {}
    if 'opts' in layer:
        shared_dict['opts'] = layer['opts']
    if 'regularization' in layer:
        shared_dict['regularization'] = layer['regularization']
    train_layer = dict_merge(shared_dict, layer['train'])
    train_model = __build_model_layer(
        last_operation, train_layer, trainable, test, backprop_train, index, regularization
    )
    test_layer = dict_merge(shared_dict, layer['test'])
    test_model = __build_model_layer(
        last_operation, test_layer, trainable, test, backprop_train, index, regularization
    )

    def if_test():
        return test_model['operation']

    def if_train():
        return train_model['operation']

    return {
        'operation': tf.cond(test, if_test, if_train),
        'block_operations': [train_model, test_model]
    }


def __build_tf_block_layer(last_operation, layer, trainable, test, backprop_train, index, regularization):
    shared_dict = {}
    if 'opts' in layer:
        shared_dict['opts'] = layer['opts']
    if 'regularization' in layer:
        shared_dict['regularization'] = layer['regularization']
    res_layer = dict_merge(shared_dict, layer['layers'][0])
    residual_layers = [
        __build_model_layer(last_operation, res_layer, trainable, test,
                            backprop_train, index, regularization)
    ]
    index['cont'] += 1
    for residual_layer in layer['layers'][1:]:
        res_layer = dict_merge(shared_dict, residual_layer)
        residual_layers.append(
            __build_model_layer(residual_layers[-1], res_layer, trainable, test, backprop_train, index, regularization)
        )
        index['cont'] += 1
    if layer['type'] == 'residual_block':
        print('residual block')
        block_layer = {'operation': tf.add(last_operation['operation'], residual_layers[-1]['operation']),
                       'block_operations': residual_layers}
    elif layer['type'] == 'block':
        block_layer = {'operation': tf.identity(residual_layers[-1]['operation']),
                       'block_operations': residual_layers}
    return block_layer


def __build_tf_inception_block_layer(last_operation, layer, trainable, test, backprop_train, index, regularization):
    shared_dict = {}
    if 'opts' in layer:
        shared_dict['opts'] = layer['opts']
    if 'regularization' in layer:
        shared_dict['regularization'] = layer['regularization']
    inception_branches = []
    inception_ops = []
    index['cont'] += 1
    for inception_branch in layer['branches']:
        inc_layer = dict_merge(shared_dict, inception_branch)
        inception_branches.append(
            __build_model_layer(last_operation, inc_layer, trainable, test, backprop_train, index, regularization)
        )
        inception_ops.append(inception_branches[-1]['operation'])
        index['cont'] += 1
    merge = 'concat'
    if 'merge' in layer:
        merge = layer['merge']
    if merge == 'concat':
        output = {'operation': tf.concat(inception_ops, axis=-1), 'block_operations': inception_branches}
    elif merge == 'add':
        print('adding branches')
        output = {'operation': tf.add_n(inception_ops), 'block_operations': inception_branches}
    return output


def __build_tf_softmax_layer(last_operation, layer):

    exp_last_op = tf.exp(last_operation)
    R = 0.0  # Con 4.0 obtuve buenos resultados
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'operation': tf.nn.softmax(last_operation)
    }
    if R > 1e-6:
        output['operation'] = tf.divide(
            exp_last_op,
            tf.maximum(R, tf.reduce_sum(exp_last_op, axis=-1, keep_dims=True))
        )
    nlabels = last_operation.get_shape().as_list()[-1]
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    # output['cost'] = -tf.reduce_sum(
    #     (one_hot_labels * tf.log(tf.maximum(1e-8, output['operation'])) +
    #     (1.0 - one_hot_labels) * tf.log(tf.maximum(1e-8, 1.0 - output['operation']))) * factor,
    #     axis=-1
    # )
    output['cost'] = tf.nn.sparse_softmax_cross_entropy_with_logits(
        labels=output['labels'], logits=last_operation
    )
    return output


def __build_tf_decoupled_softmax_layer(last_operation, layer):
    exp_last_op = tf.exp(last_operation)
    R = 0.0  # Con 4.0 obtuve buenos resultados
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    C = 1.0  # Con 4.0 obtuve buenos resultados
    if 'data' in layer and 'C' in layer['data']:
        C = layer['data']['C']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'operation': tf.nn.softmax(last_operation)
    }
    if R > 1e-6:
        output['operation'] = tf.divide(
            exp_last_op,
            tf.maximum(R, tf.reduce_sum(exp_last_op, axis=-1, keep_dims=True))
        )
    nlabels = last_operation.get_shape().as_list()[-1]
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = tf.reduce_sum(
        -(one_hot_labels * tf.log(1.0 - tf.exp(R - tf.sign(last_operation)*tf.square(last_operation))) +
        C * (1.0 - one_hot_labels) * tf.square(last_operation)) * factor,
        axis=-1
    )
    return output


def __build_tf_decoupled_plane_softmax_layer(last_operation, layer):
    exp_last_op = tf.square(last_operation)
    R = 0.0  # Con 4.0 obtuve buenos resultados
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    C = 1.0  # Con 4.0 obtuve buenos resultados
    if 'data' in layer and 'C' in layer['data']:
        C = layer['data']['C']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'operation': tf.nn.softmax(last_operation)
    }
    if R > 1e-6:
        output['operation'] = tf.divide(
            exp_last_op,
            tf.maximum(R, tf.reduce_sum(exp_last_op, axis=-1, keep_dims=True))
        )
    nlabels = last_operation.get_shape().as_list()[-1]
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = tf.reduce_sum(
        (one_hot_labels * -tf.log(tf.maximum(1e-8, 1.0 - tf.exp(R - tf.square(last_operation)))) +
        C * (1.0 - one_hot_labels) * tf.square(last_operation)) * factor,
        axis=-1
    )
    return output


def __build_tf_hinge_layer(last_operation, layer):
    nlabels = last_operation.get_shape().as_list()[-1]
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'operation': last_operation
    }
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = tf.reduce_sum(
        (one_hot_labels * tf.maximum(0.0, 1.0 - output['operation']) +
        (1.0 - one_hot_labels) * tf.maximum(0.0, 1.0 + output['operation'])) * factor,
        axis=-1
    )
    return output


def __build_tf_orthogonal_layer(last_operation, layer):
    nlabels = last_operation.get_shape().as_list()[-1]
    C = 1.0  # Con 100.0 obtuve buenos resultados
    if 'data' in layer and 'C' in layer['data']:
        C = layer['data']['C']
    R = 1.0  # Con 4.0 obtuve buenos resultados
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'variables': {'C': C, 'R': R},
        'operation': tf.nn.softmax(last_operation)
    }
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    output['cost'] = tf.reduce_sum(
        (one_hot_labels * tf.exp(R - last_operation) +
        C * (1.0 - one_hot_labels) * tf.square(last_operation)) * factor,
        axis=-1
    )
    return output


def __build_tf_orthogonal_hinge_layer(last_operation, layer):
    nlabels = last_operation.get_shape().as_list()[-1]
    C = 1.0
    if 'data' in layer and 'C' in layer['data']:
        C = layer['data']['C']
    R = 1.0
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'variables': {'C': C, 'R': R},
        'operation': tf.nn.softmax(last_operation)
    }
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = tf.reduce_sum(
        (one_hot_labels * tf.maximum(0.0, R - last_operation) +
        C * (1.0 - one_hot_labels) * tf.square(last_operation)) * factor,
        axis=-1
    )

    return output


def __build_tf_orthogonal_plane_hinge_layer(last_operation, layer):
    nlabels = last_operation.get_shape().as_list()[-1]
    square_last_op = tf.square(last_operation)
    C = 1.0
    if 'data' in layer and 'C' in layer['data']:
        C = layer['data']['C']
    R = 1.0
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'variables': {'C': C, 'R': R},
        'operation': tf.divide(
            square_last_op,
            tf.maximum(R, tf.reduce_sum(square_last_op, axis=-1, keep_dims=True))
        )
    }
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = tf.reduce_sum(
        (one_hot_labels * tf.maximum(0.0, R - square_last_op) +
        C * (1.0 - one_hot_labels) * square_last_op) * factor,
        axis=-1
    )

    return output


def __build_tf_orthogonal_distance_layer(last_operation, layer):
    nlabels = last_operation.get_shape().as_list()[-1]
    C = 1.0  # Con 10.0 obtuve buenos resultados
    if 'data' in layer and 'C' in layer['data']:
        print('changing C')
        C = layer['data']['C']
    R = 1.0  # Con 2.0 obtuve buenos resultados
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'variables': {'C': C, 'R': R},
        'operation': tf.nn.softmax(last_operation)
    }
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = tf.reduce_sum(
        (one_hot_labels * tf.square(R - last_operation) +
        C * (1.0 - one_hot_labels) * tf.square(last_operation)) * factor,
        axis=-1
    )
    return output


def __build_tf_orthogonal_plane_distance_layer(last_operation, layer):
    nlabels = last_operation.get_shape().as_list()[-1]
    square_last_op = tf.square(last_operation)
    C = 1.0  # Con 10.0 obtuve buenos resultados
    if 'data' in layer and 'C' in layer['data']:
        C = layer['data']['C']
    R = 1.0  # Con 2.0 obtuve buenos resultados
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'variables': {'C': C, 'R': R},
        'operation': tf.divide(
            square_last_op,
            tf.maximum(R, tf.reduce_sum(square_last_op, axis=-1, keep_dims=True))
        )
    }
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = tf.reduce_sum(
        (one_hot_labels * tf.square(R - square_last_op) +
        C * (1.0 - one_hot_labels) * square_last_op) * factor,
        axis=-1
    )
    return output


def __build_tf_orthogonal_plane_layer(last_operation, layer):
    nlabels = last_operation.get_shape().as_list()[-1]
    square_last_op = tf.square(last_operation)
    C = 1.0 # Con 100.0 obtuve buenos resultados
    if 'data' in layer and 'C' in layer['data']:
        C = layer['data']['C']
    R = 1.0 # Con 4.0 obtuve buenos resultados
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'variables': {'C': C, 'R': R},
        'operation': tf.divide(
            square_last_op,
            tf.maximum(R, tf.reduce_sum(square_last_op, axis=-1, keep_dims=True))
        )
    }
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = tf.reduce_sum(
        (one_hot_labels * tf.exp(R - square_last_op) +
        C * (1.0 - one_hot_labels) * square_last_op) * factor,
        axis=-1
    )

    return output


def __build_tf_orthogonal_softmax_layer(last_operation, layer):
    nlabels = last_operation.get_shape().as_list()[-1]
    square_last_op = tf.square(last_operation)
    R = 1.0
    if 'data' in layer and 'R' in layer['data']:
        R = layer['data']['R']
    output = {
        'labels': tf.placeholder(tf.int32, shape=None),
        'variables': {'R': R},
        'operation': tf.divide(
            square_last_op,
            tf.maximum(R, tf.reduce_sum(square_last_op, axis=-1, keep_dims=True))
        )
    }
    one_hot_labels = tf.one_hot(output['labels'], nlabels)
    factor = tf.ones(nlabels, dtype=tf.float32)
    if 'data' in layer and 'factor' in layer['data']:
        print('adding factor to cost function')
        factor = tf.convert_to_tensor(layer['data']['factor'])
    output['cost'] = -tf.reduce_sum(
        (one_hot_labels * tf.log(tf.maximum(1e-8, output['operation'])) +
        (1.0 - one_hot_labels) * tf.log(tf.maximum(1e-8, 1.0 - output['operation']))) * factor,
        axis=-1
    )
    return output


def __build_tf_output_layer(last_layer, layer, trainable, test, backprop_train, index, regularization):
    output_layer = deepcopy(layer)
    output_layer['type'] = 'block'
    output = __build_model_layer(last_layer, output_layer, trainable, test, backprop_train, index, regularization)
    output['operation'] = output['block_operations'][-1]['operation']
    output['validation'] = {
        'correct_prediction': tf.equal(
            tf.argmax(output['operation'], 1, output_type=tf.int32),
            output['block_operations'][-1]['labels']
        )
    }
    try:
        output['train'] = {
            'loss': tf.reduce_mean(output['block_operations'][-1]['cost'])
        }
        __build_tf_training_layer(output, layer, regularization)
    except:
        print('WARNING: this model cannot be trained. Only eval() and get_deep_features() functions can be used')

    return output


def __build_tf_training_layer(output, layer, regularization):
    # adding regularization parameters
    if regularization['custom']:
        for loss in regularization['custom']:
            output['train']['loss'] = tf.add(output['train']['loss'], loss)

    learning_rate = 1e-4
    if 'train' in layer and 'learning_rate' in layer['train'] and \
        isinstance(layer['train']['learning_rate'], dict):
            print('using momentum modified learning rate')
            learning_rate_tensor = getattr(
                tf.train, layer['train']['learning_rate']['type']
            )(**layer['train']['learning_rate']['opts'])
    else:
        learning_rate = layer['train']['learning_rate']
        learning_rate_tensor = tf.placeholder(tf.float32)
        output['placeholders'] = {'learning_rate': learning_rate_tensor}
    train_type = 'adam'
    if 'train' in layer and 'type' in layer['train']:
        train_type = layer['train']['type']
    if train_type == 'adam':
        epsilon = 1e-8
        if 'train' in layer and 'epsilon' in layer['train']:
            epsilon = layer['train']['epsilon']
        beta1 = 0.9
        if 'train' in layer and 'beta1' in layer['train']:
            beta1 = layer['train']['beta1']
        beta2 = 0.999
        if 'train' in layer and 'beta2' in layer['train']:
            beta2 = layer['train']['beta2']
        output['train']['step'] = tf.train.AdamOptimizer(
            learning_rate=learning_rate_tensor, beta1=beta1, beta2=beta2, epsilon=epsilon
        ).minimize(
            output['train']['loss']
        )
        output['variables'] = {
            'epsilon': epsilon,
            'beta1': beta1, 'beta2': beta2
        }
    elif train_type == 'momentum':
        momentum = 0.9
        if 'train' in layer and 'momentum' in layer['train']:
            momentum = layer['train']['momentum']
        use_nesterov = False
        if 'train' in layer and 'use_nesterov' in layer['train']:
            use_nesterov = layer['train']['use_nesterov']
        use_locking = False
        if 'train' in layer and 'use_locking' in layer['train']:
            use_locking = layer['train']['use_locking']
        output['train']['step'] = tf.train.MomentumOptimizer(
            learning_rate=learning_rate_tensor, momentum=momentum, use_nesterov=use_nesterov, use_locking=use_locking
        ).minimize(
            output['train']['loss']
        )
        output['variables'] = {
            'momentum': momentum,
            'use_nesterov': use_nesterov, 'use_locking': use_locking
        }
    if 'placeholders' in output:
        output['variables']['learning_rate'] = learning_rate
    output['validation']['accuracy'] = tf.reduce_mean(
        tf.cast(output['validation']['correct_prediction'], tf.float32)
    )


def extract_data_tf(fpnn, layers=None, model=None):
    if layers is None:
        layers = fpnn.layers
    if model is None:
        model = fpnn.model
        model_index = 1
    else:
        model_index = 0
    output_index = 0
    for i in range(len(layers)):
        if layers[i]['type'] == 'output':
            extract_data_tf(fpnn, layers=layers[i]['layers'], model=fpnn.outputs[output_index]['block_operations'])
            if 'train' not in layers[i]:
                layers[i]['train'] = {}
            for v in fpnn.outputs[output_index]['variables']:
                layers[i]['train'][v] = fpnn.outputs[output_index]['variables'][v]
            layers[i]['info'] = fpnn.outputs[output_index]['info']
            output_index += 1
            continue
        layers[i]['info'] = model[model_index]['info']
        if 'variables' in model[model_index]:
            if 'data' not in layers[i]:
                layers[i]['data'] = {}
            for key in model[model_index]['variables']:
                try:
                    layers[i]['data'][key] = model[model_index]['variables'][key].eval().tolist()
                except:
                    layers[i]['data'][key] = model[model_index]['variables'][key]
        if layers[i]['type'] == 'inception_block':
            extract_data_tf(fpnn, layers=layers[i]['branches'], model=model[model_index]['block_operations'])
            # for j in range(len(layers[i]['branches'])):
            #     extract_data_tf(fpnn, layers=layers[i]['blocks'][j]['layers'],
            #                     model=model[model_index]['block_operations'][j]['block_operations'])
        elif 'block' in layers[i]['type']:
            extract_data_tf(fpnn, layers=layers[i]['layers'], model=model[model_index]['block_operations'])
        elif layers[i]['type'] == 'train_test_option':
            extract_data_tf(
                fpnn, layers=[layers[i]['train'], layers[i]['test']], model=model[model_index]['block_operations']
            )

        model_index += 1


def fit_tf(fpnn, data, label, test_data=None, test_label=None, batch_size=50, epochs=60,
           is_training=True,
           extra_data=None, extra_label=None, extra_factor=0.5, verbose=True):
    fpnn.build(trainable=True)
    perm = np.arange(len(label))
    np.random.shuffle(perm)
    backprop_opts = fpnn.global_opts
    learning_iter = epochs
    extra_batch_size = None
    if extra_factor is not None and extra_data is not None and extra_label is not None:
        extra_batch_size = int(extra_factor*batch_size)
        batch_size -= extra_batch_size
        extra_perm = np.arange(len(extra_label))
        np.random.shuffle(extra_perm)
        if verbose:
            print('batch size for training data =', batch_size)
            print('batch size for extra data =', extra_batch_size)
    output = {
        'train': {'loss': [], 'accuracy': []},
        'test': {'loss': [], 'accuracy': []},
    }
    start = 0
    niter = 0
    extra_start = 0
    extra_niter = 0
    i = -1
    while niter < epochs:
        i += 1
        new_start = start + batch_size
        if new_start > len(perm):
            np.random.shuffle(perm)
            start = 0
            new_start = batch_size
            niter += 1
            if verbose:
                print('new_iter', niter, 'epochs', learning_iter)
            if 'train' in backprop_opts and 'learning_rates' in backprop_opts['train'] and \
                    str(niter) in backprop_opts['train']['learning_rates']:
                fpnn.outputs[-1]['variables']['learning_rate'] = backprop_opts['train']['learning_rates'][str(niter)]
                if verbose:
                    print('learning rate', fpnn.outputs[-1]['variables']['learning_rate'])
                # fpnn.extract_data()
                # if force_init:
                #     fpnn.build(trainable=is_training)
        batch_data = data[perm[start:new_start]]
        batch_label = label[perm[start:new_start]]
        if extra_batch_size is not None:
            new_extra_start = extra_start + extra_batch_size
            if new_extra_start > len(extra_perm):
                np.random.shuffle(extra_perm)
                extra_start = 0
                new_extra_start = extra_batch_size
                extra_niter += 1
                if verbose:
                    print('new_extra_iter', extra_niter, 'epochs', learning_iter)
            batch_extra_data = extra_data[extra_perm[extra_start:new_extra_start]]
            batch_extra_label = extra_label[extra_perm[extra_start:new_extra_start]]
            batch_data = np.concatenate((batch_data, batch_extra_data), axis=0)
            batch_label = np.concatenate((batch_label, batch_extra_label), axis=0)
            extra_start = new_extra_start
        feed_dict = get_feed_dict_tf(fpnn, batch_data, batch_label, is_training=True)
        fpnn.outputs[-1]['train']['step'].run(feed_dict=feed_dict)
        if i % 100 == 0:
            train_accuracy = fpnn.eval(batch_data, batch_label)[-1]
            train_loss = fpnn.eval(batch_data, batch_label, output_type='loss')[-1]
            output['train']['loss'].append([i, train_loss])
            output['train']['accuracy'].append([i, train_accuracy])
            # print('probs', self.eval(batch_data[:1], output_type='probabilities'))
            if verbose:
                print("step %d, training accuracy %g, training_loss %g" % (i, train_accuracy, train_loss))
            # fpnn.extract_data()
        if i % 1000 == 0 and test_data is not None and test_label is not None:
            test_accuracy = fpnn.eval(test_data, test_label)[-1]
            test_loss = fpnn.eval(test_data, test_label, output_type='loss')[-1]
            if verbose:
                print("test accuracy %g, test loss %g" % (test_accuracy, test_loss))
            output['test']['loss'].append([i, test_loss])
            output['test']['accuracy'].append([i, test_accuracy])
        start = new_start

    # fpnn.extract_data()
    # fpnn.build()
    if output['train']['loss'][-1][0] < epochs:
        train_accuracy = fpnn.eval(data, label)[-1]
        train_loss = fpnn.eval(data, label, output_type='loss')[-1]
        output['train']['loss'].append([epochs, train_loss])
        output['train']['accuracy'].append([epochs, train_accuracy])
    if output['test']['loss'][-1][0] < epochs:
        test_accuracy = fpnn.eval(test_data, test_label)[-1]
        test_loss = fpnn.eval(test_data, test_label, output_type='loss')[-1]
        output['test']['loss'].append([epochs, test_loss])
        output['test']['accuracy'].append([epochs, test_accuracy])

    if verbose:
        print(
            "train accuracy %g, train loss %g" % (
                output['train']['accuracy'][-1][1], output['train']['loss'][-1][1]
            )
        )
        print(
            "test accuracy %g, test loss %g" % (
                output['test']['accuracy'][-1][1], output['test']['loss'][-1][1]
            )
        )
    return output


def __fill_feed_dict_tf(fpnn, feed_dict, layer, is_training=False):
    if 'placeholders' in layer:
        for key in layer['placeholders']:
            feed_dict[layer['placeholders'][key]] = layer['variables'][key]
    if 'block_operations' in layer:
        for i in range(len(layer['block_operations'])):
            __fill_feed_dict_tf(fpnn, feed_dict, layer['block_operations'][i], is_training)


def get_feed_dict_tf(fpnn, data=None, label=None, is_training=False, backprop_train=True):
    feed_dict = {}
    if data is not None:
        feed_dict[fpnn.model[0]['operation']] = data
    if label is not None:
        feed_dict[fpnn.outputs[-1]['block_operations'][-1]['labels']] = label
    feed_dict[fpnn.is_testing] = not is_training
    if not is_training:
        feed_dict[fpnn.is_backprop_train] = False
    else:
        feed_dict[fpnn.is_backprop_train] = backprop_train
    for i in range(len(fpnn.model)):
        __fill_feed_dict_tf(fpnn, feed_dict, fpnn.model[i], is_training)
    for i in range(len(fpnn.outputs)):
        __fill_feed_dict_tf(fpnn, feed_dict, fpnn.outputs[i], is_training)
    return feed_dict