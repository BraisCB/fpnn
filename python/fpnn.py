import numpy as np
from python.frameworks import tensorflow_parser
import json


class FpNN:
    def __init__(self, input_size, model_type='classification', layers=None, global_opts={}, framework='tensorflow'):
        if layers:
            self.layers = layers
        else:
            self.layers = []
        self.input_size = input_size
        self.sess = None
        self.model = None
        self.outputs = None
        self.regularization = {'custom': []}
        self.global_opts = global_opts
        self.__index = 0
        self.__framework = framework
        self.is_testing = None
        self.is_backprop_train = None
        self.model_type = model_type
        self.extra_ops = {}

    def __del__(self):
        if self.sess is not None:
            self.sess.close()
        del self.layers, self.outputs, self.model, self.regularization, self.global_opts

    def get_framework(self):
        return self.__framework

    def set_framework(self, framework, trainable=True):
        self.__framework = framework
        self.build(trainable=trainable)

    def save_to_file(self, file):
        self.extract_data()
        data = {'input_size': self.input_size, 'layers': self.layers, 'model_type': self.model_type,
                'global_opts': self.global_opts, 'framework': self.__framework}
        with open(file, 'w') as outfile:
            json.dump(data, outfile)

    @classmethod
    def load_from_file(cls, file):
        with open(file) as data_file:
            data = json.load(data_file)
        return cls(input_size=data['input_size'], model_type=data['model_type'], layers=data['layers'],
                   global_opts=data['global_opts'], framework=data['framework'])

    def add_layer(self, layer, trainable=True):
        self.layers.append(layer)
        self.build(trainable=trainable)

    def set_layers(self, layers, input_size=None):
        if input_size:
            self.input_size = input_size
        self.__init__(self.input_size, self.model_type, layers, self.global_opts, self.__framework)

    def build(self, trainable=True):
        if self.__framework == 'tensorflow':
            tensorflow_parser.build_tf(self, trainable)
        else:
            raise Exception('Framework not supported')

    def get_deep_features(self, data, batch_size=50, is_training=False, layer=-1):
        if self.__framework == 'tensorflow':
            return tensorflow_parser.get_deep_features_tf(self, data, batch_size, is_training, layer)

    def get_op_features(self, operation, data, batch_size=50, is_training=False, output_type=None):
        if self.__framework == 'tensorflow':
            return tensorflow_parser.get_op_features_tf(self, operation, data, batch_size, is_training, output_type)

    def eval(self, data, label=None, batch_size=50, is_training=False, output_type=None):
        if self.__framework == 'tensorflow':
            return tensorflow_parser.eval_tf(self, data, label, batch_size, is_training, output_type)

    def fit(self, data, label, test_data, test_label, keep_variables=False, batch_size=50,
            epochs=60, extra_data=None, extra_label=None, extra_factor=0.5, verbose=True):
        if self.__framework == 'tensorflow':
            return tensorflow_parser.fit_tf(
                self, data, label, test_data, test_label,
                batch_size=batch_size, epochs=epochs, is_training=not keep_variables,
                extra_data=extra_data, extra_label=extra_label, extra_factor=extra_factor, verbose=verbose
            )

    def extract_data(self, layers=None, model=None):
        if self.__framework == 'tensorflow':
            return tensorflow_parser.extract_data_tf(self, layers, model)
