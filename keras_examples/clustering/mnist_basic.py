import numpy as np
import tensorflow as tf
import sklearn.metrics as metrics
from keras.backend.tensorflow_backend import set_session
from keras.regularizers import l2
from keras.datasets import mnist
import keras.callbacks as callbacks
import keras.utils.np_utils as kutils
from keras_examples.custom import losses
from keras_examples.custom import metrics as custom_metrics
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.layers import Activation, Dense, MaxPooling2D, Flatten, Convolution2D, Input, BatchNormalization
from keras import backend as K
from keras import optimizers
import pickle
import json


batch_size = 100
nb_epoch = 1
img_rows, img_cols = 32, 32
nb_classes = 10
regularization = 0.0005
channel_axis = 1 if K.image_data_format() == "channels_first" else -1

output_losses = [
    {'type': 'unsupervised_cross_entropy_loss', 'data': {'num_classes': nb_classes}},
]

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
set_session(tf.Session(config=config))

reps = 1

trainable = True

(trainX, trainY), (testX, testY) = mnist.load_data()
trainX = np.expand_dims(trainX, axis=-1)
testX = np.expand_dims(testX, axis=-1)

trainX = trainX.astype('float32')
trainX_mean = trainX.mean(axis=0)
trainX_std = trainX.std(axis=0)
trainX_std[trainX_std == 0] = 1e-6
trainX = (trainX - trainX_mean) / trainX_std
testX = testX.astype('float32')
testX = (testX - trainX_mean) / trainX_std

trainY = kutils.to_categorical(trainY)
testY_cat = kutils.to_categorical(testY)

generator = ImageDataGenerator(width_shift_range=5./28,
                               height_shift_range=5./28)

init_shape = (1, 28, 28) if K.image_dim_ordering() == 'th' else (28, 28, 1)
input = Input(shape=init_shape)

def get_deep_features(input):
    x = Convolution2D(32, (3, 3), padding='same', kernel_initializer='he_normal',
        use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None
    )(input)
    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((2, 2))(x)
    x = Convolution2D(32, (3, 3), padding='same', kernel_initializer='he_normal',
        use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None
    )(x)
    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((2, 2))(x)
    x = Flatten()(x)
    x = Dense(1024, use_bias=False, kernel_initializer=kernel_initializer,
              kernel_regularizer=l2(regularization) if regularization > 0.0 else None
    )(x)
    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    x = Activation('relu')(x)
    return x


# For WRN-16-8 put N = 2, k = 8
# For WRN-28-10 put N = 4, k = 10
# For WRN-40-4 put N = 6, k = 4
for drop in [0.0]:
    for i in range(reps):
        for output_loss in output_losses:
            for trainable in [False, True]:
                if 'orthogonal' in output_loss['type'] and trainable:
                    continue
                print('loss =', output_loss['type'], ', trainable = ', trainable)
                if 'data' in output_loss:
                    if 'C' in output_loss['data']:
                        print('C =', output_loss['data']['C'])
                    if 'R' in output_loss['data']:
                        print('R =', output_loss['data']['R'])
                kernel_initializer = 'he_normal'
                regularization = 0.0
                deep_features = get_deep_features(input)

                kernel_initializer = 'orthogonal' if not trainable else 'he_normal'
                classifier = Dense(nb_classes, use_bias=trainable, kernel_initializer=kernel_initializer)
                classifier.trainable = trainable

                output = classifier(deep_features)

                model = Model(input, output)

                model.summary()

                def scheduler(epoch):
                    if epoch < 40:
                        return .01
                    elif epoch < 80:
                        return .001
                    else:
                        return .0001

                change_lr = callbacks.LearningRateScheduler(scheduler)

                name = 'mnist_unsupervised_' + output_loss['type'] + '_' + str(drop) + '_' + str(trainable)
                if 'data' in output_loss:
                    if 'C' in output_loss['data']:
                        name += '_C_' + str(output_loss['data']['C'])
                    if 'R' in output_loss['data']:
                        name += '_R_' + str(output_loss['data']['R'])

                # plot_model(model, name + '.png', show_shapes=False)

                optimizer = optimizers.adam(lr=0.01)

                loss = losses.unsupervised_cross_entropy_loss(num_classes=nb_classes)

                metric = 'acc' # if output_loss['type'] == 'softmax' else ortho.softmax_accuracy

                model.compile(loss=loss, optimizer=optimizer, metrics=[metric])
                print("Finished compiling")

                #model.load_weights("weights/WRN-16-8 Weights.h5")
                print("Model loaded.")

                history = model.fit_generator(
                    generator.flow(trainX, trainY, batch_size=batch_size),
                    steps_per_epoch=len(trainX) // batch_size, epochs=nb_epoch,
                    callbacks=[
                        # callbacks.ModelCheckpoint(
                        #     './keras_examples/weights/' + name + '_Weights.h5',
                        #     monitor="val_acc",
                        #     save_best_only=True,
                        #     verbose=1
                        # ),
                        callbacks.LearningRateScheduler(scheduler)
                    ],
                    validation_data=(testX, testY_cat),
                    validation_steps=testX.shape[0] // batch_size,
                    verbose=2
                )

                # with open('./keras_examples/weights/cifar10_' + name + '_history_' + str(i) + '.h5', 'wb') as file_pi:
                #     pickle.dump(history.history, file_pi)

                yPreds = model.predict(testX)
                yPred = np.argmax(yPreds, axis=1)
                yPred_cat = kutils.to_categorical(yPred, num_classes=nb_classes)
                yTrue_cat = testY_cat

                accuracy = metrics.accuracy_score(yTrue_cat, yPred_cat) * 100
                error = 100 - accuracy
                print("Accuracy : ", accuracy)
                print("Error : ", error)

                confusion_matrix = np.zeros((nb_classes, nb_classes))
                for i in range(nb_classes):
                    for j in range(nb_classes):
                        confusion_matrix[i, j] = ((yPred == j) & (testY == i)).sum() / (testY == i).sum()

                acc_name = './keras_examples/info/json_files/mnist_unsupervised_info.json'

                try:
                    with open(acc_name) as outfile:
                        info_data = json.load(outfile)
                except:
                    info_data = {}

                if name not in info_data:
                    info_data[name] = []

                info_data[name].append(confusion_matrix.tolist())

                with open(acc_name, 'w') as outfile:
                    json.dump(info_data, outfile)