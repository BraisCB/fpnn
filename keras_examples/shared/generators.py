from keras.utils import Sequence
import numpy as np
import threading


class SparseGenerator(Sequence):
    # Class is a dataset wrapper for better training performance
    def __init__(self, x_set, y_set, batch_size=256, shuffle=True, sparse=False):
        self.x, self.y = x_set, y_set
        self.batch_size = batch_size
        self.indices = np.arange(self.x.shape[0]).astype(int)
        self.shuffle = shuffle
        self.sparse = sparse

    def __len__(self):
        return self.x.shape[0] // self.batch_size

    def __getitem__(self, idx):
        inds = self.indices[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_x = self.x[inds]
        # if self.sparse:
        #     batch_x = batch_x.toarray()
        batch_y = self.y[inds]
        return [batch_x], [batch_y]

    def on_epoch_begin(self):
        if self.shuffle:
            np.random.shuffle(self.indices)


class FromDiskGenerator(Sequence):
    # Class is a dataset wrapper for better training performance
    def __init__(self, x_set, y_set, load_data_func=None, batch_size=256, memory_batch_size=200, shuffle=True):
        self.x, self.y = x_set, y_set
        self.batch_size = batch_size
        self.memory_batch_size = memory_batch_size
        self.indices = np.arange(self.x.shape[0]).astype(int)
        self.shuffle = shuffle
        self.load_data_func = load_data_func
        self.memory_batch = {}
        self.lock = threading.Lock()

    def __len__(self):
        return self.x.shape[0] // self.batch_size

    def __getitem__(self, idx):
        if idx not in self.memory_batch:
            self.lock.acquire()
            if idx not in self.memory_batch:
                memory_batch_idx = idx // self.memory_batch_size
                self.__load_batch_data(memory_batch_idx)
            self.lock.release()
        output = self.memory_batch[idx]
        del self.memory_batch[idx]
        return output

    def __load_batch_data(self, memory_batch_idx):
        inds = self.indices[
            memory_batch_idx * self.memory_batch_size * self.batch_size:
            min(self.x.shape[0], (memory_batch_idx + 1) * self.memory_batch_size * self.batch_size)
        ]
        memory_batch_x = self.x[inds] is self.load_data_func if None else self.load_data_func(self.x[inds])
        memory_batch_y = self.y[inds]

        number_of_batches = inds.shape[0] // self.batch_size
        for idx in range(number_of_batches):
            indices = range(idx * self.batch_size, (idx+1)*self.batch_size)
            self.memory_batch[memory_batch_idx*self.memory_batch_size + idx] = memory_batch_x[indices], memory_batch_y[indices]

    def on_epoch_begin(self):
        if self.shuffle:
            np.random.shuffle(self.indices)
