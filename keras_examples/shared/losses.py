from keras import backend as K
import keras.utils.np_utils as kutils

_EPSILON = K.epsilon()


def unsupervised_cross_entropy(num_classes=None, augmented_index=None):
    def loss(y_true, y_pred):
        y_pred_neg = K.max(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        winner = K.argmax(y_pred_neg, axis=-1)
        winner = K.one_hot(winner, num_classes)
        softmax_y_pred = K.softmax(y_pred_neg)
        softmax_y_pred = K.clip(softmax_y_pred, _EPSILON, 1.0 - _EPSILON)
        out = -winner * K.log(softmax_y_pred)
        return K.sum(out, axis=-1)
    return loss


def confusion_loss(mask=None, norm='frob'):
    def loss(y_true, y_pred):
        y_true /= K.maximum(1.0, K.sum(y_true, axis=0))
        confusion_matrix = K.dot(K.transpose(y_true), y_pred)
        diagonal_true_pred = K.sum(y_true * y_pred, axis=0) * K.eye(K.int_shape(y_pred)[-1])
        # diagonal_true_true = K.eye(K.int_shape(y_pred)[-1]) * K.sum(y_true, axis=0)
        # confusion_matrix += (1. * diagonal_true_true - (1. + 1.) * diagonal_true_pred)
        confusion_matrix -= diagonal_true_pred
        # confusion_matrix /= K.cast(K.shape(y_pred)[0], 'float32')
        confusion_matrix = K.clip(confusion_matrix, _EPSILON, 1.0 - _EPSILON)
        confusion_matrix = - K.log(1. - confusion_matrix)
        # confusion_matrix = K.tf.Print(confusion_matrix, [confusion_matrix])
        # if norm in ['frob', 'frobenius', 'frob_2', 'frobenius_2']:
        #     confusion_matrix *= confusion_matrix
        # else:
        #     confusion_matrix = K.abs(confusion_matrix)
        if mask is not None:
            confusion_matrix *= mask
        if norm in ['frob', 'frobenius', 'frob_2', 'frobenius_2']:
            cost = K.sum(confusion_matrix)
            if '2' not in norm:
                cost = K.sqrt(cost + 1e-8)
        elif norm in ['1', '1_2']:
            cost = K.max(K.sum(confusion_matrix, axis=0))
            if '2' in norm:
                cost = K.square(cost)
        elif norm in ['Inf', 'Inf_2']:
            cost = K.max(K.sum(confusion_matrix, axis=-1))
            if '2' in norm:
                cost = K.square(cost)
        else:
            raise Exception('norm not valid')
        return cost / K.int_shape(y_pred)[-1] # / K.cast(K.shape(y_pred)[0], 'float32')

    return loss


# def unsupervised_cross_entropy(num_clases=None, augmented=False):
#     def loss(y_true, y_pred):
#         y_pred_neg = K.max(y_pred, axis=-1) if augmented_index is not None else y_pred
#         max_val = K.max(y_pred_neg, axis=-1)
#         out = K.exp(-max_val)
#         return out
#     return loss


def cross_entropy(reduction_function='max', augmented_index=None):
    f = getattr(K, reduction_function)
    def loss(y_true, y_pred):
        y_pred_pos = f(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        softmax_y_pred = K.softmax(y_pred_pos)
        softmax_y_pred = K.clip(softmax_y_pred, _EPSILON, 1.0 - _EPSILON)
        out = - y_true * K.log(softmax_y_pred)
        return K.sum(out, axis=-1)
    return loss


def exponential_orthogonal(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
                           augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)
    
    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(K.square(y_pred), axis=augmented_index) if augmented_index is not None else K.square(y_pred)
        out = f_pos(y_true * K.exp(R - y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * y_pred_neg, axis=-1)
        return out
    return loss


def hinge_orthogonal(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
                     augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(K.square(y_pred), axis=augmented_index) if augmented_index is not None else K.square(y_pred)
        out = f_pos(y_true * K.square(K.relu(R - y_pred_pos)), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * y_pred_neg, axis=-1)
        return out

    return loss


def trench_orthogonal(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
                      augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(K.square(y_pred), axis=augmented_index) if augmented_index is not None else K.square(y_pred)
        out = f_pos(y_true * K.square(R - y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * y_pred_neg, axis=-1)
        return out

    return loss


def softplus_orthogonal(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
                        augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(K.square(y_pred), axis=augmented_index) if augmented_index is not None else K.square(y_pred)
        out = f_pos(y_true * K.square(K.softplus(R - y_pred_pos)), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * y_pred_neg, axis=-1)
        return out

    return loss


def softplus(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
             augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        out = f_pos(y_true * K.softplus(R - y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.softplus(y_pred_neg + R), axis=-1)
        return out

    return loss


def hinge(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
          augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        out = f_pos(y_true * K.relu(R - y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.relu(y_pred_neg + R), axis=-1)
        return out

    return loss


def square_softplus(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
             augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        out = f_pos(y_true * K.square(K.softplus(R - y_pred_pos)), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.square(K.softplus(y_pred_neg + R)), axis=-1)
        return out

    return loss


def square_softplus_2(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
             augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred = K.clip(y_pred, -5, 5)
        y_softplus_pos = K.softplus(R - y_pred)
        y_softplus_neg = K.softplus(R + y_pred)
        y_pred_pos = f_aug_pos(y_softplus_pos, axis=augmented_index) if augmented_index is not None else y_softplus_pos
        y_pred_neg = f_aug_neg(y_softplus_neg, axis=augmented_index) if augmented_index is not None else y_softplus_neg
        out = f_pos(y_true * K.square(y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.square(y_pred_neg), axis=-1)
        return out

    return loss


def square_hinge_3(R, lamda=1.0, reduction_function='sum', mode=None):
    f = getattr(K, reduction_function)

    def loss(y_true, y_pred):
        if mode is None:
            out = f(y_true * K.square(K.relu(R - y_pred)), axis=-1) + \
                  lamda * f((1.0 - y_true) * K.square(K.relu(y_pred + R)), axis=-1)
        elif mode=='positive':
            out = lamda * f(y_true * K.square(K.relu(R - y_pred)), axis=-1)
        elif mode == 'negative':
            out = lamda * f((1.0 - y_true) * K.square(K.relu(y_pred + R)), axis=-1)
        return out
    return loss


def square_hinge(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
          augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        out = f_pos(y_true * K.square(K.relu(R - y_pred_pos)), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.square(K.relu(y_pred_neg + R)), axis=-1)
        return out

    return loss


def square_hinge_2(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
          augmented_function_pos='mean', augmented_function_neg='mean', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_relu_pos = K.relu(R - y_pred)
        y_relu_neg = K.relu(R + y_pred)
        y_pred_pos = f_aug_pos(y_relu_pos, axis=augmented_index) if augmented_index is not None else y_relu_pos
        y_pred_neg = f_aug_neg(y_relu_neg, axis=augmented_index) if augmented_index is not None else y_relu_neg
        out = f_pos(y_true * K.square(y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.square(y_pred_neg), axis=-1)
        return out

    return loss


def trench(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
           augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        out = f_pos(y_true * K.square(R - y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.square(y_pred_neg + R), axis=-1)
        return out

    return loss


def exponential(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
                augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred_pos = f_aug_pos(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        y_pred_neg = f_aug_neg(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        out = f_pos(y_true * K.exp(R - y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.exp(y_pred_neg + R), axis=-1)
        return out

    return loss


def square_orthogonal_softplus_2(R, lamda, reduction_function_pos='sum', reduction_function_neg='sum',
             augmented_function_pos='max', augmented_function_neg='max', augmented_index=None):
    f_pos = getattr(K, reduction_function_pos)
    f_neg = getattr(K, reduction_function_neg)
    f_aug_pos = getattr(K, augmented_function_pos)
    f_aug_neg = getattr(K, augmented_function_neg)

    def loss(y_true, y_pred):
        y_pred = K.clip(y_pred, -5, 5)
        y_softplus_pos = K.softplus(R - y_pred)
        y_softplus_neg = K.square(y_pred)
        y_pred_pos = f_aug_pos(y_softplus_pos, axis=augmented_index) if augmented_index is not None else y_softplus_pos
        y_pred_neg = f_aug_neg(y_softplus_neg, axis=augmented_index) if augmented_index is not None else y_softplus_neg
        out = f_pos(y_true * K.square(y_pred_pos), axis=-1) + \
              lamda * f_neg((1.0 - y_true) * K.square(y_pred_neg), axis=-1)
        return out

    return loss
