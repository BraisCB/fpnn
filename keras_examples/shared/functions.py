from keras import backend as K

_EPSILON = 1e-6


def sigmoid_old(epsilon=1e-2, bias=0.):
    def func(x):
        return 0.5 * ((x - bias) / (K.abs(x - bias) + epsilon) + 1.)
    return func


def sigmoid(factor=30.):
    # @K.tf.custom_gradient
    # def func(x):
    #     # x_clip = K.clip(x - bias, -2., 2.)
    #     s = K.sigmoid(factor * x)
    #     step = K.round(s)
    #
    #     def grad(dy):
    #         return dy * s * (1. - s)
    #
    #     return step, grad
    #
    # return func
    def func(x):
        return K.sigmoid(factor * x)
    #     return K.switch(K.less(sig, .1), K.zeros_like(sig), sig)
    return func


def step(bias=0.):
    def func(x):
        return K.round(x - bias)
    return func


def softstep(bias=0., factor=3.):
    @K.tf.custom_gradient
    def func(x):
        # x_clip = K.clip(x - bias, -2., 2.)
        s = K.sigmoid(factor*(x - bias))

        def grad(dy):
            return dy * factor * s * (1. - s)

        return K.round(s), grad
    return func


def softstep_for_fs(axis=0, bias=0., factor=10.):
    @K.tf.custom_gradient
    def func(x):
        argmax = K.argmax(x, axis=axis)
        one_hot = K.one_hot(argmax, K.int_shape(x)[axis])
        if axis == 0:
            one_hot = K.transpose(one_hot)
        s = K.sigmoid(factor * (x - bias))
        h = one_hot * K.round(s)

        def grad(dy):
            # epsilon = 1e-3
            # return one_hot * dy * s * (1. - s) + dy * epsilon
            # return (one_hot + .0) * dy * s * (1. - s) + .01 * (1. - one_hot) * dy * x
            # return one_hot * dy * s * (1. - s) # + (1. - one_hot) * K.random_normal(K.shape(s), 0.0, 0.001)
            epsilon = 0.
            return ((1. - epsilon) * one_hot + epsilon) * dy * factor * s * (1. - s)

        return h, grad

    return func


def matrix_norm(x, norm):
    is_squared = False
    if isinstance(norm, str):
        norm = norm.lower()
        is_squared = '_2' in norm
    norm = norm.split('_')[0]
    if norm in ['frob', 'frobenius', 'frob_2', 'frobenius_2']:
        result = K.sum(K.square(x))
        if not is_squared:
            result = K.sqrt(result + _EPSILON)
    else:
        abs_x = K.abs(x)
        if ',' in norm:
            p, q = (int(factor) for factor in norm.split(','))
            result = abs_x
            if p != 1:
                result = K.pow(result, p)
            result = K.sum(result, axis=0)
            if p != q:
                result = K.pow(result, q/p)
            result = K.sum(result)
            if q != 1:
                result = K.pow(K.sum(result), 1./q)
        else:
            if norm in [1, '1', '1_2']:
                axis = 0
            elif norm in ['inf', 'infinity', 'inf_2', 'infinity_2']:
                axis = 1
            else:
                raise Exception('norm not supported')
            result = K.max(K.sum(abs_x, axis=axis))
        if is_squared:
            result = K.square(result)
    return result

