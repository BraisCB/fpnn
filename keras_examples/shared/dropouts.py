from keras import backend as K
from keras.engine import Layer
from keras.legacy.interfaces import generate_legacy_interface


legacy_stabledropout_support = generate_legacy_interface(
    allowed_positional_args=['rate', 'noise_shape', 'seed', 'mean'],
    conversions=[('p', 'rate')])

legacy_stablegaussiandropout_support = generate_legacy_interface(
    allowed_positional_args=['rate', 'mean'],
    conversions=[('p', 'rate')])


class StableDropout(Layer):
    """Applies Dropout to the input.

    Dropout consists in randomly setting
    a fraction `rate` of input units to 0 at each update during training time,
    which helps prevent overfitting.

    # Arguments
        rate: float between 0 and 1. Fraction of the input units to drop.
        noise_shape: 1D integer tensor representing the shape of the
            binary dropout mask that will be multiplied with the input.
            For instance, if your inputs have shape
            `(batch_size, timesteps, features)` and
            you want the dropout mask to be the same for all timesteps,
            you can use `noise_shape=(batch_size, 1, features)`.
        seed: A Python integer to use as random seed.

    # References
        - [Dropout: A Simple Way to Prevent Neural Networks from Overfitting](http://www.cs.toronto.edu/~rsalakhu/papers/srivastava14a.pdf)
    """
    @legacy_stabledropout_support
    def __init__(self, rate, mean_stabilization=True, factor_correction=True, noise_shape=None, seed=None, **kwargs):
        super(StableDropout, self).__init__(**kwargs)
        self.rate = min(1., max(0., rate))
        self.noise_shape = noise_shape
        self.seed = seed
        self.supports_masking = True
        self.mean_stabilization = mean_stabilization
        self.factor_correction = factor_correction

    def _get_noise_shape(self, inputs):
        if self.noise_shape is None:
            return self.noise_shape

        symbolic_shape = K.shape(inputs)
        noise_shape = [symbolic_shape[axis] if shape is None else shape
                       for axis, shape in enumerate(self.noise_shape)]
        return tuple(noise_shape)

    def call(self, inputs, training=None):
        if 0. < self.rate < 1.:
            noise_shape = self._get_noise_shape(inputs)

            def dropped_inputs():
                mean = None
                if self.mean_stabilization:
                    input_shape = K.int_shape(inputs)
                    # Prepare broadcasting shape.
                    ndim = len(input_shape) - 1
                    reduction_axes = list(range(ndim))
                    mean = K.mean(inputs, axis=reduction_axes)
                    outputs = inputs - mean
                else:
                    outputs = inputs
                outputs = K.dropout(outputs, self.rate, noise_shape,
                                    seed=self.seed)
                if self.factor_correction:
                    outputs = K.sqrt(1.0 - K.constant(self.rate, dtype=inputs.dtype)) * outputs
                if self.mean_stabilization:
                    outputs = outputs + mean
                return outputs
            return K.in_train_phase(dropped_inputs, inputs,
                                    training=training)
        return inputs

    def get_config(self):
        config = {'rate': self.rate,
                  'noise_shape': self.noise_shape,
                  'seed': self.seed}
        base_config = super(StableDropout, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return input_shape


class StableGaussianDropout(Layer):
    """Apply multiplicative 1-centered Gaussian noise.

    As it is a regularization layer, it is only active at training time.

    # Arguments
        rate: float, drop probability (as with `Dropout`).
            The multiplicative noise will have
            standard deviation `sqrt(rate / (1 - rate))`.

    # Input shape
        Arbitrary. Use the keyword argument `input_shape`
        (tuple of integers, does not include the samples axis)
        when using this layer as the first layer in a model.

    # Output shape
        Same shape as input.

    # References
        - [Dropout: A Simple Way to Prevent Neural Networks from Overfitting Srivastava, Hinton, et al. 2014](http://www.cs.toronto.edu/~rsalakhu/papers/srivastava14a.pdf)
    """

    @legacy_stablegaussiandropout_support
    def __init__(self, rate, mean_stabilization=True, factor_correction=True, **kwargs):
        super(StableGaussianDropout, self).__init__(**kwargs)
        self.supports_masking = True
        self.rate = rate
        self.mean_stabilization = mean_stabilization
        self.factor_correction = factor_correction

    def call(self, inputs, training=None):
        if 0 < self.rate < 1:
            def noised():
                mean = None
                k_rate = K.constant(self.rate, dtype=inputs.dtype)
                stddev = K.sqrt(k_rate / (1.0 - k_rate))
                if self.mean_stabilization:
                    input_shape = K.int_shape(inputs)
                    # Prepare broadcasting shape.
                    ndim = len(input_shape) - 1
                    reduction_axes = list(range(ndim))
                    mean = K.mean(inputs, axis=reduction_axes)
                    outputs = inputs - mean
                else:
                    outputs = inputs
                outputs = outputs * K.random_normal(shape=K.shape(outputs),
                                                    mean=1.0,
                                                    stddev=stddev)
                if self.factor_correction:
                    outputs = K.sqrt(1.0 - k_rate) * outputs
                if self.mean_stabilization:
                    outputs = outputs + mean
                return outputs
            return K.in_train_phase(noised, inputs, training=training)
        return inputs

    def get_config(self):
        config = {'rate': self.rate}
        base_config = super(StableGaussianDropout, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return input_shape