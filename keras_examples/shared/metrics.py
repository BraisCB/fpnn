from keras import backend as K


def categorical_accuracy(y_true, y_pred, framework=K):
    return framework.mean(framework.equal(framework.argmax(y_true, axis=-1), framework.argmax(y_pred, axis=-1)))


def orthogonal_probabilities(y_pred, P, framework=K):
    relu_y_pred = framework.maximum(0.0, y_pred)
    probs = relu_y_pred / framework.maximum(P, framework.sum(relu_y_pred, axis=-1))
    return probs


def max_accuracy(augmented_index=None, reduction_function='max', framework=K):
    f = getattr(framework, reduction_function)
    def accuracy(y_true, y_pred):
        y_pred_max = f(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        return categorical_accuracy(y_true, y_pred_max)
    return accuracy


def softmax_accuracy(augmented_index=None, reduction_function='max', framework=K):
    f = getattr(framework, reduction_function)

    def accuracy(y_true, y_pred):
        y_pred_max = f(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        softmax_y_pred = y_pred_max / framework.sum(y_pred_max, axis=-1, keepdims=True)
        return categorical_accuracy(y_true, softmax_y_pred)
    return accuracy


def orthogonal_accuracy(P=0.05, augmented_index=None, reduction_function='max', framework=K):
    f = getattr(framework, reduction_function)

    def accuracy(y_true, y_pred):
        y_pred_max = f(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        orthogonal_y_pred = orthogonal_probabilities(y_pred_max, P)
        return categorical_accuracy(y_true, orthogonal_y_pred)
    return accuracy


def unsupervised_accuracy(y_test, augmented_index=None, framework=K):
    def accuracy(y_true, y_pred):
        y_pred_max = framework.max(y_pred, axis=augmented_index) if augmented_index is not None else y_pred
        return categorical_accuracy(y_test, y_pred_max)
    return accuracy


def precision(y_true, y_pred, reduce=True, framework=K):
    prec = framework.sum(y_true * y_pred, axis=0) / K.sum(y_pred, axis=0)
    if reduce:
        prec = framework.mean(prec)
    return prec


def recall(y_true, y_pred, reduce=True, framework=K):
    rec = framework.sum(y_true * y_pred, axis=0) / K.sum(y_true, axis=0)
    if reduce:
        rec = framework.mean(rec)
    return rec


def f1_score(y_true, y_pred, reduce=True, framework=K):
    prec = precision(y_true, y_pred, False, framework)
    rec = recall(y_true, y_pred, False, framework)
    f1 = 2. * (prec * rec) / (prec + rec)
    if reduce:
        f1 = framework.mean(f1)
    return f1
