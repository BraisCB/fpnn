from keras.regularizers import Regularizer
from keras_examples.shared.functions import matrix_norm


class RegularizerList(Regularizer):

    def __init__(self, regularizer_list):
        super(RegularizerList, self).__init__()
        self.regularizer_list = regularizer_list

    def __call__(self, x):
        cost = 0.
        for regularizer in self.regularizer_list:
            cost += regularizer(x)
        return cost

    def get_config(self):
        config = {
            'regularizer_list': []
        }
        for regularizer in self.regularizer_list:
            config['regularizer_list'].append(regularizer.get_config())
        return config


class MatrixNorm(Regularizer):

    def __init__(self, factor, norm, activation=None):
        super(MatrixNorm, self).__init__()
        self.norm = norm
        self.factor = factor
        self.activation = activation

    def __call__(self, x):
        if self.activation is not None:
            x = self.activation(x)
        return self.factor * matrix_norm(x, self.norm)

    def get_config(self):
        config = {
            'factor': self.factor,
            'norm': self.norm,
            'activation': self.activation
        }
        return config
