from copy import deepcopy
import numpy as np
from keras import backend as K


def get_cross_validation_params(model_func, data, label, fixed_params=None, variable_params=None,
                                valid_data=None, valid_label=None, fit_kwargs=None, evaluate_kwargs=None, best_max=True):
    valid_data = data if valid_data is None else valid_data
    valid_label = label if valid_label is None else valid_label
    fit_kwargs = {} if fit_kwargs is None else fit_kwargs
    evaluate_kwargs = {} if evaluate_kwargs is None else evaluate_kwargs
    fixed_params = {} if fixed_params is None else fixed_params
    variable_params = {} if variable_params is None else variable_params
    loss, score, score_params = __recursive_get_cv_params(
        model_func, data, label, fixed_params, variable_params, valid_data, valid_label, fit_kwargs, evaluate_kwargs
    )
    best_score = np.max(score) if best_max else np.min(score)
    index = np.where(score == best_score)[0]
    worst_loss = np.argmax(np.array(loss)[index])
    return score_params[index[worst_loss]]


def __recursive_get_cv_params(
        model_func, data, label, fixed_params, variable_params, valid_data, valid_label, fit_kwargs, evaluate_kwargs
):
    if len(variable_params):
        loss, scores, params = [], [], []
        key = list(variable_params.keys())[0]
        variable_params_aux = deepcopy(variable_params)
        fixed_params_aux = deepcopy(fixed_params)
        values = variable_params[key]
        del variable_params_aux[key]
        for value in values:
            fixed_params_aux[key] = value
            new_loss, new_scores, new_params = __recursive_get_cv_params(
                model_func, data, label, fixed_params_aux, variable_params_aux, valid_data, valid_label, fit_kwargs,
                evaluate_kwargs
            )
            loss += new_loss
            scores += new_scores
            params += new_params
        return loss, scores, params
    else:
        n_features = (data.shape[-1], )
        model = model_func(n_features, **fixed_params)
        model.fit(data, label, **fit_kwargs)
        evaluation = model.evaluate(valid_data, valid_label, **evaluate_kwargs)
        # print('conf : ', fixed_params)
        # print('result : ', evaluation)
        del model
        K.clear_session()
        return [evaluation[0]], [evaluation[1]], [fixed_params]
