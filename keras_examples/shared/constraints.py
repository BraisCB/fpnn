from keras.constraints import Constraint
from keras import backend as K


class ConstraintList(Constraint):

    def __init__(self, constraint_list, activation=None):
        super(ConstraintList, self).__init__()
        self.constraint_list = constraint_list
        self.activation = activation

    def __call__(self, w):
        new_w = w if self.activation is None else self.activation(w)
        for constraint in self.constraint_list:
            new_w = constraint(new_w)
        return new_w

    def get_config(self):
        config = {
            'constraint_list': []
        }
        for constraint in self.constraint_list:
            config['constraint_list'].append(constraint.get_config())
        return config


class UnitNorm(Constraint):
    """Constrains the weights incident to each hidden unit to have unit norm.

    # Arguments
        axis: integer, axis along which to calculate weight norms.
            For instance, in a `Dense` layer the weight matrix
            has shape `(input_dim, output_dim)`,
            set `axis` to `0` to constrain each weight vector
            of length `(input_dim,)`.
            In a `Conv2D` layer with `data_format="channels_last"`,
            the weight tensor has shape
            `(rows, cols, input_depth, output_depth)`,
            set `axis` to `[0, 1, 2]`
            to constrain the weights of each filter tensor of size
            `(rows, cols, input_depth)`.
    """

    def __init__(self, axis=0, norm='2'):
        self.axis = axis
        self.norm = str(norm).lower()

    def __call__(self, w):
        norm = self.norm.split('_')[0]
        if 'inf' in norm:
            factor = K.epsilon() + K.max(K.abs(w), axis=self.axis, keepdims=True)
        elif norm == '1':
            factor = K.epsilon() + K.sum(K.abs(w), axis=self.axis, keepdims=True)
        elif norm == '2':
            factor = K.sqrt(K.epsilon() + K.sum(K.square(w), axis=self.axis, keepdims=True))
        else:
            factor = K.pow(K.epsilon() + K.sum(K.pow(w, int(norm)), axis=self.axis, keepdims=True), 1./int(norm))
        return w / factor

    def get_config(self):
        return {'axis': self.axis, 'norm': self.norm}


class MinMaxNorm(Constraint):
    """Constrains the weights incident to each hidden unit to have unit norm.

    # Arguments
        axis: integer, axis along which to calculate weight norms.
            For instance, in a `Dense` layer the weight matrix
            has shape `(input_dim, output_dim)`,
            set `axis` to `0` to constrain each weight vector
            of length `(input_dim,)`.
            In a `Conv2D` layer with `data_format="channels_last"`,
            the weight tensor has shape
            `(rows, cols, input_depth, output_depth)`,
            set `axis` to `[0, 1, 2]`
            to constrain the weights of each filter tensor of size
            `(rows, cols, input_depth)`.
    """

    def __init__(self, min_val=0., max_val=None):
        self.min_val = 0. if max_val is None else min_val
        self.max_val = min_val if max_val is None else max_val

    def __call__(self, w):
        return K.clip(w, self.min_val, self.max_val)

    def get_config(self):
        return {'min_val': self.min_val, 'max_val': self.max_val}
