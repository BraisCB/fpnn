import numpy as np


def load_data(source):

    file = source + '/train_y.bin'
    with open(file, 'rb') as f:
        trainY = np.fromfile(f, dtype=np.uint8) - 1

    file = source + '/train_X.bin'
    with open(file, 'rb') as f:
        images = np.fromfile(f, dtype=np.uint8)
        images = np.reshape(images, (-1, 3, 96, 96))
        trainX = np.transpose(images, (0, 3, 2, 1))

    file = source + '/test_y.bin'
    with open(file, 'rb') as f:
        testY = np.fromfile(f, dtype=np.uint8) - 1

    file = source + '/test_X.bin'
    with open(file, 'rb') as f:
        images = np.fromfile(f, dtype=np.uint8)
        images = np.reshape(images, (-1, 3, 96, 96))
        testX = np.transpose(images, (0, 3, 2, 1))

    return (trainX, trainY), (testX, testY)
