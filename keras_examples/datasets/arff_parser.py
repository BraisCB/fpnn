import numpy as np


def to_arff(dataset, directory, name, subsets=None):
    print('Formatting data')
    subsets = dataset.keys() if subsets is None else subsets

    for subset in subsets:
        filename = directory + '/' + name + '_' + subset + '.arff'
        data = np.asarray(dataset[subset]['data'])
        label = np.asarray(dataset[subset]['label'])
        if len(data.shape) > 2:
            data = np.reshape(data, [-1, np.prod(data.shape[1:])])
        nfeatures = data.shape[-1]
        if 'label' in dataset[subset]:
            data = np.concatenate((data, label[:, None]), axis=1)
            nlabels = np.unique(label).tolist()
        data = data.astype(int)
        with open(filename, 'w') as fs:
            fs.write('@RELATION %s\n\n' % name)
            for feat in range(nfeatures):
                fs.write('@ATTRIBUTE attr%d NUMERIC\n' % feat)
            if 'label' in dataset[subset]:
                fs.write('@ATTRIBUTE class {%s}\n\n' % ','.join(str(x) for x in nlabels))
            fs.write('@DATA\n')
            for row in data:
                fs.write('%s\n' % ','.join(str(x) for x in row))
            fs.write('\n')

    print('Data formatted')

