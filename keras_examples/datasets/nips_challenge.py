import numpy as np


def load_data(source, normalize=True):
    info = {
        'train': {}, 'validation': {}, 'test': {}
    }

    file = source + '_train.labels'
    info['train']['label'] = np.loadtxt(file, dtype=np.int16)
    info['train']['label'][info['train']['label'] < 0] = 0

    file = source + '_train.data'
    try:
        info['train']['data'] = np.loadtxt(file, dtype=np.int16).astype(np.float32)
    except:
        if 'dexter' in source:
            info['train']['data'] = __load_dexter_data(file)
        else:
            info['train']['data'] = __load_dorothea_data(file)

    file = source + '_test.data'
    try:
        info['test']['data'] = np.loadtxt(file, dtype=np.int16).astype(np.float32)
    except:
        if 'dexter' in source:
            info['test']['data'] = __load_dexter_data(file)
        else:
            info['test']['data'] = __load_dorothea_data(file)

    file = source + '_valid.labels'
    info['validation']['label'] = np.loadtxt(file, dtype=np.int16)
    info['validation']['label'][info['validation']['label'] < 0] = 0

    file = source + '_valid.data'
    try:
        info['validation']['data'] = np.loadtxt(file, dtype=np.int16).astype(np.float32)
    except:
        if 'dexter' in source:
            info['validation']['data'] = __load_dexter_data(file)
        else:
            info['validation']['data'] = __load_dorothea_data(file)

    if normalize:
        mean = np.mean(info['train']['data'], axis=0)
        std = np.std(info['train']['data'], axis=0)
        std[std == 0.0] = 1e-5
        info['train']['data'] = (info['train']['data'] - mean) / std
        info['validation']['data'] = (info['validation']['data'] - mean) / std
        info['test']['data'] = (info['test']['data'] - mean) / std

    return info


def __load_dexter_data(source):
    """
    A function that reads in the original dexter data in sparse form of feature:value
    and transform them into matrix form.
    # Arguments:
    filename: the url to either the dexter_train.data or dexter_valid.data
    mode: either 'text' for unpacked file; 'gz' for .gz file; or 'online' to download from the UCI repo
    # Return:
    the dexter data in matrix form.
    """
    with open(source) as f:
        readin_list = f.readlines()

    def to_dense_sparse(string_array):
        n = len(string_array)
        inds = np.zeros(n, dtype='int32')
        vals = np.zeros(n, dtype='int32')
        ret = np.zeros(20000, dtype='int32')
        for i in range(n):
            this_split = string_array[i].split(':')
            inds[i] = int(this_split[0]) - 1
            vals[i] = int(this_split[1])
        ret[inds] = vals
        return ret

    N = len(readin_list)
    dat = [None]*N

    for i in range(N):
        dat[i] = to_dense_sparse(readin_list[i].split(' ')[0:-1])[None, :]

    dat = np.concatenate(dat, axis=0).astype('float32')
    return dat


def __load_dorothea_data(filename):
    """
    A function that reads in the original dorothea data in sparse form of feature:value
    and transform them into matrix form.
    # Arguments:
    filename: the url to either the dorothea_train.data or dorothea_valid.data
    mode: either 'text' for unpacked file; 'gz' for .gz file; or 'online' to download from the UCI repo
    # Return:
    the dexter data in matrix form.
    """
    with open(filename) as f:
        readin_list = f.readlines()

    def to_dense_dorothea(string_array):
        n = len(string_array)
        inds = np.zeros(n, dtype='int32')
        ret = np.zeros(100001, dtype='int32')
        for i in range(n):
            this_split = string_array[i].split(' ')
            inds[i] = int(this_split[0])
        ret[inds] = 1
        return ret

    N = len(readin_list)
    dat = [None]*N

    for i in range(N):
        dat[i] = to_dense_dorothea(readin_list[i].split(' ')[1:-1])[None, :]

    dat = np.concatenate(dat, axis=0).astype('float32')
    return dat[:, 1::]  # the first column all zero