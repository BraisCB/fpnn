import numpy as np


def load_data(source):
    info = {
        'train': {}, 'validation': {}, 'test': {}
    }

    file = source + '.csv'
    info['train']['data'] = np.genfromtxt(file, delimiter=',')
    info['train']['label'] = info['train']['data'][:, -1].astype(int) - 1
    info['train']['data'] = info['train']['data'][:, :-1]

    return info
