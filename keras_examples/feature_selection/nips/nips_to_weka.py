from keras_examples.feature_selection.nips.datasets import save_weka_dataset


dataset_names = [
    'arcene',
    'dexter',
    'dorothea',
    'gisette',
    'madelon',
]

subsets = ['train']

datasets_directory = './keras_examples/feature_selection/nips/datasets/'


if __name__ == '__main__':
    for dataset_name in dataset_names:
        print('loading dataset', dataset_name)
        dataset = save_weka_dataset(dataset_name, directory=datasets_directory, normalize=True, subsets=subsets)
        print('data saved')
