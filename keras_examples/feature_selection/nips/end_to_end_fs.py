from keras import backend as K, callbacks
import numpy as np
import json
import os
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from keras.utils import to_categorical
from keras_examples.feature_selection.nips import network_models, datasets as dt
from keras_examples.shared.generators import SparseGenerator
from keras_examples.shared.functions import matrix_norm


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

datasets = ['madelon']

factor = 4
reps = 1
regularization = 1e-3
epochs = 800 * factor
batch_size = 100


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
set_session(tf.Session(config=config))


def scheduler(epoch):
    if epoch < 200*factor:
        return 0.01
    elif epoch < 400*factor:
        return 0.002
    elif epoch < 600*factor:
        return 0.0004
    else:
        return 0.00008

if __name__ == '__main__':
    os.chdir('../../../')
    features = [5, 10, 15] # , 200, 250, 300, 350, 400, 450, 500, 750, 1000] #, 1500, 2000, 5000, 10000, data.shape[-1]]

    datasets_directory = './keras_examples/feature_selection/nips/datasets/'
    directory = './keras_examples/feature_selection/nips/info/'

    network_names = ['dense_hard', 'dense_soft', ]

    for dataset_name in datasets:
        dataset = dt.load_dataset(dataset_name, directory=datasets_directory, normalize=True)

        train_data = dataset['train']['data']
        train_result = to_categorical(dataset['train']['label'])

        valid_data = dataset['validation']['data']
        valid_result = to_categorical(dataset['validation']['label'])

        nnz = np.count_nonzero(train_result[:, -1]) / train_result.shape[0]
        class_weight=[1./(1. - nnz), 1./nnz]
        class_weight /= np.max(class_weight)
        print('pos values : ', nnz)
        print('neg values : ', 1. - nnz)
        print('class_weight : ', class_weight)

        output_directory = directory + dataset_name + '/'

        for network_name in network_names:

            print('NETWORK : ', network_name)
            result_filename = output_directory + network_name + '_results.json'

            new_results = {}
            for n_features in features:
                print('nfeatures : ', n_features)
                r_predictions = []

                for rep in range(reps):
                    fs_create_model_func = getattr(network_models, network_name)
                    input_shape = train_data.shape[1:]
                    fs_model = fs_create_model_func(
                        input_shape=input_shape, regularization=regularization, nfeatures=n_features
                    )
                    # model.fit(data_min, train_result, batch_size=batch_size, epochs=epochs, verbose=0, callbacks=[
                    #     callbacks.LearningRateScheduler(scheduler)], class_weight=class_weight
                    # )
                    fs_model.fit_generator(
                        SparseGenerator(train_data, train_result, batch_size=batch_size, sparse=False),
                        steps_per_epoch=train_data.shape[0] // batch_size, epochs=epochs,
                        callbacks=[
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        validation_data=(valid_data, valid_result),
                        validation_steps=valid_data.shape[0] // batch_size,
                        class_weight=class_weight,
                        verbose=2
                    )
                    # fs_matrix = K.eval(K.sigmoid(fs_model.fs_layer.kernel))
                    kernel = K.eval(fs_model.fs_layer.kernel)
                    fs_matrix = K.eval(fs_model.fs_layer.step_function(fs_model.fs_layer.kernel)) if fs_model.fs_layer.step_function is not None else kernel
                    fs_matrix_norm_1 = K.eval(matrix_norm(fs_matrix, '1'))
                    # fs_matrix = K.eval(0.5 * (K.sign(fs_model.fs_layer.kernel) + 1.))
                    r_predictions.append(fs_model.evaluate(valid_data, valid_result, verbose=0)[-1])
                    print('result : ', fs_model.evaluate(valid_data, valid_result, verbose=0)[-1])
                    del fs_model
                    K.clear_session()

                new_results[n_features] = r_predictions


            # results = np.array(np.mean(results, axis=-1))
            # nfeats = np.array(features)
            # roc = 0.5 * (results[1:] + results[:-1]) * (nfeats[1:] - nfeats[:-1])
            # roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
            # print('ROC : ', roc)

            try:
                with open(result_filename) as outfile:
                    results = json.load(outfile)
            except:
                results = {}

            for new_result in new_results:
                if new_result not in results:
                    results[new_result] = []
                results[new_result] += new_results[new_result]

            with open(result_filename, 'w') as outfile:
                json.dump(results, outfile)
