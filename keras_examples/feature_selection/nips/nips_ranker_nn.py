from keras_examples.feature_selection.nips.datasets import load_data
from keras_examples.feature_selection.custom.feature_selection import get_rank
from keras_examples.feature_selection.custom.layers import Mask
from keras_examples.feature_selection.custom import saliencies
from keras_examples.shared.utils import balance_data
from keras.layers import Activation, Dense, Input, Dropout, BatchNormalization
from keras.models import Model
from keras import optimizers
from keras.regularizers import l2, l1
import keras.utils.np_utils as kutils
import keras.backend as K
import numpy as np
import json
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

dataset_names = [
    'arcene',
    'dexter',
    'dorothea',
    'gisette',
    'madelon'
]

gamma = 0.0
b_size = 100
epochs = 100


def create_model(input_shape, nclasses=2, layer_dims=None, bn=True, kernel_initializer='he_normal',
                 dropout=0.0, lasso=0.0, regularization=0.0, loss_weights=None):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape)
    if layer_dims is None:
        layer_dims = [150, 100, 50]
    x = Mask(kernel_regularizer=l1(lasso))(input) if lasso > 0 else input

    for layer_dim in layer_dims:
        x = Dense(layer_dim, use_bias=not bn, kernel_initializer=kernel_initializer,
                  kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)

    x = Dense(nclasses, use_bias=True, kernel_initializer=kernel_initializer,
              kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    output = Activation('softmax')(x)

    model = Model(input, output)

    optimizer = optimizers.adam(lr=1e-2)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    model.saliency = saliencies.get_saliency('categorical_crossentropy', model)

    return model


for dataset_name in dataset_names:

    fs_filename = './keras_examples/feature_selection/nips/info/' + dataset_name + '_nn_ranks.json'

    print('loading dataset', dataset_name)
    dataset = load_data('./datasets/' + dataset_name + '/' + dataset_name, normalize=True)
    print('data loaded. labels =', dataset['train']['data'].shape)
    input_shape = dataset['train']['data'].shape[-1:]
    batch_size = min(len(dataset['train']['data']), b_size)

    uclasses = np.unique(dataset['train']['label'])
    nclasses = len(uclasses)

    data = dataset['train']['data']
    label = dataset['train']['label']
    data, label = balance_data(data, label)
    print('data size after balance : ', data.shape)
    label = kutils.to_categorical(label, nclasses)

    for fs_mode in ['simonyan', 'lasso']:
        for lasso in [0.0, 1e-3]:
            if lasso == 0.0 and fs_mode == 'lasso' or lasso > 0.0 and fs_mode == 'simonyan':
                continue
            reps = 1
            # for gamma in np.arange(0.0, 1.0, 0.3):
            while reps <= 32:
                print('reps : ', reps)
                for regularization in [1e-3]:
                    name = dataset_name + '_' + fs_mode + '_l_' + str(lasso) + '_g_' + str(gamma) + \
                           '_r_' + str(regularization)
                    print(name)
                    kwargs = {
                        'lasso': lasso,
                        'regularization': regularization
                    }
                    rank = get_rank(fs_mode, data=data, label=label, create_model_func=create_model,
                                    gamma=gamma, reps=reps,
                                    batch_size=batch_size, epochs=epochs, **kwargs)

                    try:
                        with open(fs_filename) as outfile:
                            info_data = json.load(outfile)
                    except:
                        info_data = {}

                    if fs_mode not in info_data:
                        info_data[fs_mode] = []

                    info_data[fs_mode].append(
                        {
                            'lasso': lasso,
                            'gamma': gamma,
                            'regularization': regularization,
                            'rank': rank.tolist(),
                            'reps': reps
                        }
                    )

                    with open(fs_filename, 'w') as outfile:
                        json.dump(info_data, outfile)

                    del rank
                    reps *= 2


