from keras_examples.datasets.nips_challenge import load_data
from keras_examples.custom.feature_selection import get_rank
import sklearn.metrics as metrics
from keras.layers import Activation, Dense, Input, Dropout, BatchNormalization
from keras.models import Model
from keras import optimizers
from keras.regularizers import l2
import keras.utils.np_utils as kutils
import keras.backend as K
import numpy as np
import json
from copy import deepcopy


dataset_names = [
    'arcene',
    'dexter',
    'gisette',
    'madelon',
    'dorothea'
]

reps = 6
b_size = 50
epochs = 100
regularization = 1e-3
dropout = 0.0
limit = 0.5


def mu(n_features, max_features):
    factor = n_features / max_features
    if factor < 0.1:
        return 0.9
    elif factor < 0.25:
        return 0.75
    else:
        return 0.6


def create_model(input_shape, nclasses=2, layer_dims=None, bn=True, kernel_initializer='he_normal',
                 dropout=0.0, regularization=0.0):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape)
    if layer_dims is None:
        layer_dims = [150, 75]

    x = input
    for layer_dim in layer_dims:
        x = Dense(layer_dim, use_bias=not bn, kernel_initializer=kernel_initializer,
                  kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('tanh')(x)

    x = Dense(nclasses, use_bias=True, kernel_initializer=kernel_initializer,
              kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    output = Activation('softmax')(x)

    model = Model(input, output)

    optimizer = optimizers.adam(lr=1e-2)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    return model


for dataset_name in dataset_names:
    fs_filename = './keras_examples/info/feature_selection/' + dataset_name
    print('loading dataset', dataset_name)
    dataset = load_data('./datasets/' + dataset_name + '/' + dataset_name, normalize=True)
    print('data loaded. labels =', dataset['train']['data'].shape)
    input_shape = dataset['train']['data'].shape[-1:]
    batch_size = min(len(dataset['train']['data']), b_size)

    nclasses = len(np.unique(dataset['train']['label']))

    data = dataset['train']['data']
    label = kutils.to_categorical(dataset['train']['label'], nclasses)

    valid_data = dataset['validation']['data']
    valid_label = kutils.to_categorical(dataset['validation']['label'], nclasses)

    for weka in [True, False]:
        filename = fs_filename + '_weka.json' if weka else fs_filename + '_ranks.json'
        output_filename = fs_filename + '_results.json'
        try:
            with open(filename) as outfile:
                ranks = json.load(outfile)
        except:
            continue

        for method in ranks:
            print('METHOD : ', method)
            rank_list = ranks[method]
            for example in rank_list:
                print(example)
                output = deepcopy(example)
                nfeats = []
                accuracies = []
                rank = np.array(example['rank']).astype(int) - int(weka)
                n_features = 1
                max_features = int(limit * data.shape[-1])
                while n_features <= max_features:
                    print('n_features : ', n_features)
                    accuracy = 0.0
                    data_min = data[:, rank[:n_features]]
                    for rep in range(reps):
                        model = create_model((n_features, ), nclasses, dropout=dropout, regularization=regularization)
                        model.fit(data_min, label, batch_size=batch_size, epochs=epochs, verbose=0)

                        yPreds = model.predict(valid_data[:, rank[:n_features]])
                        yPred_label = np.argmax(yPreds, axis=1)
                        yPred = kutils.to_categorical(yPred_label, nclasses)

                        try:
                            accuracy += metrics.accuracy_score(valid_label, yPred) * 100
                        except:
                            pass

                        del model
                        K.clear_session()
                    accuracy /= reps
                    print("Accuracy : ", accuracy)
                    print("Error : ", 100 - accuracy)
                    nfeats.append(n_features)
                    accuracies.append(accuracy)
                    if n_features == max_features:
                        break
                    n_features = min(int(n_features / mu(n_features, max_features)) + 1, max_features)
                output['classification'] = {
                    'n_features': nfeats,
                    'accuracy': accuracies
                }

                try:
                    with open(output_filename) as outfile:
                        results = json.load(outfile)
                except:
                    results = {}

                if method not in results:
                    results[method] = []

                results[method].append(output)

                with open(output_filename, 'w') as outfile:
                    json.dump(results, outfile)
