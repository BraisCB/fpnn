from skfeature.function.statistical_based import CFS
from skfeature.function.information_theoretical_based import MIM, MRMR, FCBF
from skfeature.function.similarity_based import reliefF
from skfeature.function.wrapper import svm_backward


def get_feature_indexes(X, y, method):
    output = {}
    if method == 'CFS':
        output['rank'] = CFS.cfs(X, y).tolist()
    elif method == 'MIM':
        output['rank'], _, _ = MIM.mim(X, y)
        output['rank'] = output['rank'].tolist()
    elif method == 'reliefF':
        score = reliefF.reliefF(X, y)
        # rank features in descending order according to score
        output['rank'] = reliefF.feature_ranking(score).tolist()
    elif method == 'MRMR':
        output['rank'], _, _ = MRMR.mrmr(X, y)
        output['rank'] = output['rank'].tolist()
    elif method == 'FCBF':
        output['rank'], _ = FCBF.fcbf(X, y).tolist()
        output['rank'] = output['rank'].tolist()
    elif method == 'svm-rfe':
        output['rank'] = svm_backward.svm_backward(X, y).tolist()
    return output
