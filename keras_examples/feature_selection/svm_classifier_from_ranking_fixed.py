from keras_examples.datasets.nips_challenge import load_data
from keras_examples.custom.utils import balance_data
# from sklearn.svm import SVC
from keras_examples.custom.svm import SVC
import numpy as np
import json
from copy import deepcopy


dataset_names = [
    ('arcene', 7000),
    ('dexter', 9947),
    ('gisette', 2500),
    ('madelon', 20),
    ('dorothea', 50000)
]

reps = 10
limit = 1.0


def mu(n_features, max_features):
    factor = n_features / max_features
    if factor < 0.1:
        return 0.9
    elif factor < 0.25:
        return 0.75
    else:
        return 0.6


def get_scores(rank, data, label, valid_data, valid_label, max_features=None, class_func=None, class_args=None):
    output = {'rank': rank}
    nfeats = []
    accuracies = []
    n_features = 1
    best_acc = 0
    best_feat = 0
    max_features = int(data.shape[-1]) if max_features is None else max_features
    total_features = max_features
    class_args = {} if class_args is None else class_args
    class_func = SVC if class_func is None else class_func
    while n_features <= max_features:
        # print('n_features : ', n_features)
        r_accuracy = []
        train_accuracy = 0.0
        data_min = data[:, rank[:n_features]]
        for rep in range(reps):
            model = class_func(**class_args)
            model.fit(data_min, label)
            train_accuracy += (model.predict(data_min) == label).sum() / len(label) * 100
            yPred = model.predict(valid_data[:, rank[:n_features]])
            r_accuracy.append((yPred == valid_label).sum() / len(yPred) * 100)
            del model
        accuracy = np.mean(r_accuracy)
        train_accuracy /= reps
        if accuracy > best_acc:
            # print("Accuracy : ", accuracy)
            best_acc = accuracy
            best_feat = n_features
        nfeats.append(n_features)
        accuracies.append(r_accuracy)
        if n_features == max_features:
            break
        n_features = min(int(n_features / mu(n_features, total_features)) + 1, max_features)
    output['classification'] = {
        'n_features': nfeats,
        'accuracy': accuracies
    }
    print('best : ', best_acc, ' , feats : ', best_feat)
    accuracies = np.array(np.mean(accuracies, axis=-1))
    nfeats = np.array(nfeats)
    roc = 0.5 * (accuracies[1:] + accuracies[:-1]) * (nfeats[1:] - nfeats[:-1])
    roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
    print('ROC : ', roc)
    output['percentiles'] = []
    for per in [0.1, 0.25, 0.5, 1.0]:
        n_features = int(max_features * per)
        r_accuracy = []
        data_min = data[:, rank[:n_features]]
        for rep in range(reps):
            model = SVC()
            model.fit(data_min, label)

            yPred = model.predict(valid_data[:, rank[:n_features]])
            r_accuracy.append((yPred == valid_label).sum() / len(yPred) * 100)

            del model
        accuracy = np.mean(r_accuracy)
        print("Percentile : ", per, "Accuracy : ", accuracy)
        output['percentiles'].append(
            {
                'percentile': per,
                'n_features': n_features,
                'accuracy': r_accuracy
            }
        )

    return output

for dataset_stats in dataset_names:
    dataset_name, max_features = dataset_stats
    print('loading dataset', dataset_name)
    dataset = load_data('./datasets/' + dataset_name + '/' + dataset_name, normalize=True)
    print('data loaded. labels =', dataset['train']['data'].shape)
    input_shape = dataset['train']['data'].shape[-1:]

    nclasses = len(np.unique(dataset['train']['label']))

    data = dataset['train']['data']
    data_mean = np.mean(data, axis=0)
    data_std = np.maximum(1e-6, np.std(data, axis=0))
    data = (data - data_mean) / data_std

    label = dataset['train']['label']

    data, label = balance_data(data, label)

    valid_data = dataset['validation']['data']
    valid_data = (valid_data - data_mean) / data_std
    valid_label = dataset['validation']['label']

    total_features = data.shape[-1]

    for weka in [False]:
        fs_filename = './keras_examples/info/feature_selection/'
        filename = fs_filename + dataset_name + '_weka.json' if weka else \
            fs_filename + 'nn_fixed/' + dataset_name + '_nn_ranks.json'
        output_filename = fs_filename + dataset_name + '_weka_results.json' if weka\
            else fs_filename + 'nn_fixed/' + dataset_name + '_nn_results.json'
        try:
            with open(filename) as outfile:
                ranks = json.load(outfile)
        except:
            continue

        for method in ranks:
            print('METHOD : ', method)
            rank_list = ranks[method]
            for example in rank_list[::-1]:
                if 'regularization' in example:
                    print('regularization : ', example['regularization'])
                if 'gamma' in example:
                    print('gamma : ', example['gamma'])
                output = deepcopy(example)
                nfeats = []
                accuracies = []
                rank = np.array(example['rank']).astype(int) - int(weka)
                n_features = 1
                best_acc = 0
                best_feat = 0
                # max_features = int(limit * data.shape[-1])
                while n_features <= max_features:
                    # print('n_features : ', n_features)
                    r_accuracy = []
                    train_accuracy = 0.0
                    data_min = data[:, rank[:n_features]]
                    for rep in range(reps):
                        model = SVC()
                        model.fit(data_min, label)
                        train_accuracy += (model.predict(data_min) == label).sum() / len(label) * 100
                        yPred = model.predict(valid_data[:, rank[:n_features]])
                        r_accuracy.append((yPred == valid_label).sum() / len(yPred) * 100)
                        del model
                    accuracy = np.mean(r_accuracy)
                    train_accuracy /= reps
                    if accuracy > best_acc:
                        # print("Accuracy : ", accuracy)
                        best_acc = accuracy
                        best_feat = n_features
                    nfeats.append(n_features)
                    accuracies.append(r_accuracy)
                    if n_features == max_features:
                        break
                    n_features = min(int(n_features / mu(n_features, total_features)) + 1, max_features)
                output['classification'] = {
                    'n_features': nfeats,
                    'accuracy': accuracies
                }
                print('best : ', best_acc, ' , feats : ', best_feat)
                accuracies = np.array(np.mean(accuracies, axis=-1))
                nfeats = np.array(nfeats)
                roc = 0.5 * (accuracies[1:] + accuracies[:-1]) * (nfeats[1:] - nfeats[:-1])
                roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
                print('ROC : ', roc)
                output['percentiles'] = []
                for per in [0.1, 0.25, 0.5, 1.0]:
                    n_features = int(max_features * per)
                    r_accuracy = []
                    data_min = data[:, rank[:n_features]]
                    for rep in range(reps):
                        model = SVC()
                        model.fit(data_min, label)

                        yPred = model.predict(valid_data[:, rank[:n_features]])
                        r_accuracy.append((yPred == valid_label).sum() / len(yPred) * 100)

                        del model
                    accuracy = np.mean(r_accuracy)
                    print("Percentile : ", per)
                    print("Accuracy : ", accuracy)
                    print("Error : ", 100 - accuracy)
                    output['percentiles'].append(
                        {
                            'percentile': per,
                            'n_features': n_features,
                            'accuracy': r_accuracy
                        }
                    )

                try:
                    with open(output_filename) as outfile:
                        results = json.load(outfile)
                except:
                    results = {}

                if method not in results:
                    results[method] = []

                results[method].append(output)

                with open(output_filename, 'w') as outfile:
                    json.dump(results, outfile)
                # break
