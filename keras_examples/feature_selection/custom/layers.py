from keras import backend as K
from keras import initializers, constraints, regularizers
from keras.layers import Layer, Dense
from .functions import fs_add_to_model
from .regularizers import FeatureSelectionRegularizer
from keras_examples.shared.functions import softstep_for_fs, sigmoid, softstep
from keras_examples.shared.regularizers import RegularizerList, MatrixNorm
from keras.regularizers import l2, l1
from numpy import prod as numpy_prod
from keras_examples.shared.constraints import ConstraintList, UnitNorm, MinMaxNorm
from keras.constraints import NonNeg


class GaussianNoise(Layer):
    """Apply additive zero-centered Gaussian noise.

    This is useful to mitigate overfitting
    (you could see it as a form of random data augmentation).
    Gaussian Noise (GS) is a natural choice as corruption process
    for real valued inputs.

    As it is a regularization layer, it is only active at training time.

    # Arguments
        stddev: float, standard deviation of the noise distribution.

    # Input shape
        Arbitrary. Use the keyword argument `input_shape`
        (tuple of integers, does not include the samples axis)
        when using this layer as the first layer in a model.

    # Output shape
        Same shape as input.
    """

    def __init__(self, stddev, min_value=0., max_value=1.,**kwargs):
        super(GaussianNoise, self).__init__(**kwargs)
        self.supports_masking = True
        self.stddev = stddev
        self.min_value = min_value
        self.max_value = max_value

    def call(self, inputs, training=None):
        def noised():
            noise_inputs = inputs + K.random_normal(shape=K.shape(inputs),
                                                  mean=0.,
                                                  stddev=self.stddev)
            return K.maximum(K.minimum(noise_inputs, self.max_value), self.min_value)
        return K.in_train_phase(noised, inputs, training=training)

    def get_config(self):
        config = {'stddev': self.stddev, 'max_value': self.max_value, 'min_value': self.min_value}
        base_config = super(GaussianNoise, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return input_shape


class Mask(Layer):

    def __init__(self,
                 kernel_initializer='ones',
                 kernel_regularizer=None,
                 kernel_constraint=None,
                 **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(Mask, self).__init__(**kwargs)
        self.kernel_initializer = initializers.get(kernel_initializer)
        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.kernel_constraint = constraints.get(kernel_constraint)
        self.supports_masking = True

    def build(self, input_shape):
        input_dim = input_shape[1:]
        self.kernel = self.add_weight(shape=input_dim,
                                      initializer=self.kernel_initializer,
                                      name='kernel',
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)
        self.built = True

    def call(self, inputs, **kwargs):
        output = inputs * self.kernel
        return output

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        config = {
            'kernel_initializer': initializers.serialize(self.kernel_initializer),
            'kernel_regularizer': regularizers.serialize(self.kernel_regularizer),
            'kernel_constraint': constraints.serialize(self.kernel_constraint),
        }
        base_config = super(Mask, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


# class SoftFS(Layer):
#
#     def __init__(self, units,
#                  kernel_initializer=initializers.random_normal(mean=0.5, stddev=0.05),
#                  factor=0.,
#                  factor_gain=1.0001,
#                  norm='Inf',
#                  row_norm=None,
#                  row_factor=None,
#                  row_factor_gain=None,
#                  column_norm=None,
#                  column_factor=None,
#                  column_factor_gain=None,
#                  step_function=None,
#                  reduce_input=True,
#                  dropout=0.,
#                  **kwargs):
#         if 'input_shape' not in kwargs and 'input_dim' in kwargs:
#             kwargs['input_shape'] = (kwargs.pop('input_dim'),)
#         super(SoftFS, self).__init__()
#         self.units = units
#         self.kernel_initializer = kernel_initializer
#         self.row_norm = norm if row_norm is None else row_norm
#         self.row_factor = factor if row_factor is None else row_factor
#         self.row_factor_gain = factor_gain if row_factor_gain is None else row_factor_gain
#         self.column_norm = norm if column_norm is None else column_norm
#         self.column_factor = factor if column_factor is None else column_factor
#         self.column_factor_gain = factor_gain if column_factor_gain is None else column_factor_gain
#         self.kernel_constraint = ConstraintList(
#             [
#                 MinMaxNorm(min_val=0., max_val=1.)
#             ]
#         )
#         self.supports_masking = True
#         self.step_function = step_function
#         self.kernel = None
#         self.reduce_input = reduce_input
#         self.dropout = dropout
#
#     def build(self, input_shape):
#         assert len(input_shape) >= 2
#         input_dim = numpy_prod(input_shape[1:])
#         self.row_moving_factor = self.add_weight(
#             shape=(),
#             name='row_moving_factor',
#             initializer=initializers.constant(self.row_factor),
#             trainable=False)
#         self.column_moving_factor = self.add_weight(
#             shape=(),
#             name='column_moving_factor',
#             initializer=initializers.constant(self.column_factor),
#             trainable=False)
#         self.kernel_regularizer = FeatureSelectionRegularizer(
#             row_norm=self.row_norm, row_factor=self.row_moving_factor,
#             column_norm=self.column_norm, column_factor=self.column_moving_factor, step_function=self.step_function
#         )
#         self.kernel = self.add_weight(shape=(input_dim, self.units),
#                                       initializer=self.kernel_initializer,
#                                       name='kernel',
#                                       regularizer=self.kernel_regularizer,
#                                       constraint=self.kernel_constraint)
#         self.reduce_input = self.reduce_input if len(input_shape) == 2 else False
#         self.stateful = False
#         self.built = True
#
#     def call(self, inputs, training=None, **kwargs):
#
#         def dropped_kernel():
#             if not self.dropout:
#                 return self.kernel
#             else:
#                 random_uniform = K.random_uniform(K.int_shape(self.kernel))
#                 return K.switch(K.less(random_uniform, self.dropout), -1e6 * K.ones_like(self.kernel), self.kernel)
#
#         kernel = K.in_train_phase(dropped_kernel, self.kernel, training=training)
#
#         if not self.reduce_input:
#             kernel = K.sum(kernel, axis=-1)
#             kernel = K.reshape(kernel, shape=self.input_shape[1:])
#         sigmoid_kernel = self.step_function(kernel) if self.step_function is not None else kernel
#         output = K.dot(inputs, sigmoid_kernel) if self.reduce_input else inputs * sigmoid_kernel
#
#         updates = [
#             (self.row_moving_factor, self.row_moving_factor * self.row_factor_gain),
#             (self.column_moving_factor, self.column_moving_factor * self.column_factor_gain),
#         ]
#         self.add_update(updates, inputs)
#         return output
#
#     def add_to_model(self, model, input_shape, activation=None):
#         return fs_add_to_model(self, model, input_shape, activation)
#
#     def compute_output_shape(self, input_shape):
#         if self.reduce_input:
#             assert input_shape and len(input_shape) >= 2
#             assert input_shape[-1]
#             output_shape = list(input_shape)
#             output_shape[-1] = self.units
#             return tuple(output_shape)
#         else:
#             return input_shape


class SoftFS(Layer):

    def __init__(self, units,
                 kernel_initializer=initializers.random_normal(mean=0.5, stddev=0.05),
                 factor=0.,
                 factor_gain=1.0001,
                 norm='Inf',
                 row_norm=None,
                 row_factor=None,
                 row_factor_gain=None,
                 column_norm=None,
                 column_factor=None,
                 column_factor_gain=None,
                 step_function=None,
                 reduce_input=True,
                 dropout=0.,
                 **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(SoftFS, self).__init__()
        self.units = units
        self.kernel_initializer = kernel_initializer
        self.row_norm = norm if row_norm is None else row_norm
        self.row_factor = factor if row_factor is None else row_factor
        self.row_factor_gain = factor_gain if row_factor_gain is None else row_factor_gain
        self.column_norm = norm if column_norm is None else column_norm
        self.column_factor = factor if column_factor is None else column_factor
        self.column_factor_gain = factor_gain if column_factor_gain is None else column_factor_gain
        self.kernel_constraint = ConstraintList(
            [
                NonNeg(),
                UnitNorm(axis=0, norm='infinity')
            ]
        )
        self.supports_masking = True
        self.step_function = step_function
        self.kernel = None
        self.reduce_input = reduce_input
        self.dropout = dropout

    def build(self, input_shape):
        assert len(input_shape) >= 2
        input_dim = numpy_prod(input_shape[1:])
        self.row_moving_factor = self.add_weight(
            shape=(),
            name='row_moving_factor',
            initializer=initializers.constant(self.row_factor),
            trainable=False)
        self.column_moving_factor = self.add_weight(
            shape=(),
            name='column_moving_factor',
            initializer=initializers.constant(self.column_factor),
            trainable=False)
        self.kernel_regularizer = FeatureSelectionRegularizer(
            row_norm=self.row_norm, row_factor=self.row_moving_factor,
            column_norm=self.column_norm, column_factor=self.column_moving_factor, step_function=self.step_function
        )
        self.kernel = self.add_weight(shape=(input_dim, self.units),
                                      initializer=self.kernel_initializer,
                                      name='kernel',
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)
        self.reduce_input = self.reduce_input if len(input_shape) == 2 else False
        self.stateful = False
        self.built = True

    def call(self, inputs, training=None, **kwargs):

        def dropped_kernel():
            if not self.dropout:
                return self.kernel
            else:
                random_uniform = K.random_uniform(K.int_shape(self.kernel))
                return K.switch(K.less(random_uniform, self.dropout), -1e6 * K.ones_like(self.kernel), self.kernel)

        kernel = K.in_train_phase(dropped_kernel, self.kernel, training=training)

        if not self.reduce_input:
            kernel = K.sum(kernel, axis=-1)
            kernel = K.reshape(kernel, shape=self.input_shape[1:])
        sigmoid_kernel = self.step_function(kernel) if self.step_function is not None else kernel
        output = K.dot(inputs, sigmoid_kernel) if self.reduce_input else inputs * sigmoid_kernel

        updates = [
            (self.row_moving_factor, self.row_moving_factor * self.row_factor_gain),
            (self.column_moving_factor, self.column_moving_factor * self.column_factor_gain),
        ]
        self.add_update(updates, inputs)
        return output

    def add_to_model(self, model, input_shape, activation=None):
        return fs_add_to_model(self, model, input_shape, activation)

    def compute_output_shape(self, input_shape):
        if self.reduce_input:
            assert input_shape and len(input_shape) >= 2
            assert input_shape[-1]
            output_shape = list(input_shape)
            output_shape[-1] = self.units
            return tuple(output_shape)
        else:
            return input_shape


class SoftFS2(Layer):

    def __init__(self, units,
                 kernel_initializer='he_normal',
                 factor=0.,
                 norm='Inf',
                 row_norm=None,
                 row_factor=None,
                 column_norm=None,
                 column_factor=None,
                 kernel_constraint=None,
                 step_function=softstep(),
                 reduce_input=True,
                 dropout=0.,
                 **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(SoftFS2, self).__init__()
        self.units = units
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = FeatureSelectionRegularizer(
            factor=factor, norm=norm, row_norm=row_norm, row_factor=row_factor,
            column_norm=column_norm, column_factor=column_factor, step_function=step_function
        )
        self.kernel_constraint = kernel_constraint
        self.supports_masking = True
        self.step_function = step_function
        self.kernel = None
        self.reduce_input = reduce_input
        self.dropout = dropout

    def build(self, input_shape):
        assert len(input_shape) >= 2
        input_dim = numpy_prod(input_shape[1:])

        self.kernel = self.add_weight(shape=(input_dim, self.units),
                                      initializer=self.kernel_initializer,
                                      name='kernel',
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)
        self.reduce_input = self.reduce_input if len(input_shape) == 2 else False
        self.built = True

    def call(self, inputs, training=None, **kwargs):

        def dropped_kernel():
            if not self.dropout:
                return self.kernel
            else:
                random_uniform = K.random_uniform(K.int_shape(self.kernel))
                return K.switch(K.less(random_uniform, self.dropout), -1e6 * K.ones_like(self.kernel), self.kernel)

        kernel = K.in_train_phase(dropped_kernel, self.kernel, training=training)

        if not self.reduce_input:
            kernel = K.sum(kernel, axis=-1)
            kernel = K.reshape(kernel, shape=self.input_shape[1:])
        sigmoid_kernel = self.step_function(kernel)
        # r_sigmoid_kernel = K.round(sigmoid_kernel)
        # sigmoid_kernel = K.in_train_phase(sigmoid_kernel, r_sigmoid_kernel, training=training)
        output = K.dot(inputs, sigmoid_kernel) if self.reduce_input else inputs * sigmoid_kernel
        # output = K.switch(K.less(output, 1e-2), K.zeros_like(output), output)
        return output

    def add_to_model(self, model, input_shape, activation=None):
        return fs_add_to_model(self, model, input_shape, activation)

    def compute_output_shape(self, input_shape):
        if self.reduce_input:
            assert input_shape and len(input_shape) >= 2
            assert input_shape[-1]
            output_shape = list(input_shape)
            output_shape[-1] = self.units
            return tuple(output_shape)
        else:
            return input_shape


class HardFS(Layer):

    def __init__(self, units,
                 kernel_initializer='he_normal',
                 kernel_regularizer=None,
                 kernel_constraint=None,
                 dropout=0.,
                 step_function=softstep_for_fs(),
                 reduce_input=True,
                 kernel_reduce=None,
                 **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(HardFS, self).__init__()
        self.units = units
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = RegularizerList(
            regularizer_list=[
                l2(0.0001),
                MatrixNorm(norm='inf', factor=.0001, activation=step_function)
            ]
        ) if kernel_regularizer is None else kernel_regularizer
        self.kernel_constraint = kernel_constraint
        self.dropout = dropout
        self.step_function = step_function
        self.kernel = None
        self.reduce_input = reduce_input
        self.kernel_reduce = kernel_reduce

    def build(self, input_shape):
        assert len(input_shape) >= 2
        input_dim = numpy_prod(input_shape[1:])

        self.kernel = self.add_weight(shape=(input_dim, self.units),
                                      initializer=self.kernel_initializer,
                                      name='kernel',
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)
        self.reduce_input = self.reduce_input if len(input_shape) == 2 else False
        self.built = True

    def call(self, inputs, training=None, **kwargs):
        sigmoid_kernel = self.step_function(self.kernel)

        def dropped_kernel():
            if not self.dropout:
                return sigmoid_kernel
            else:
                random_uniform = K.random_uniform(K.int_shape(self.kernel))
                return K.switch(K.less(random_uniform, self.dropout), K.zeros_like(sigmoid_kernel), sigmoid_kernel)

        sigmoid_kernel = K.in_train_phase(dropped_kernel, sigmoid_kernel, training=training)
        if not self.reduce_input:
            sigmoid_kernel = K.sum(sigmoid_kernel, axis=-1)
            sigmoid_kernel = K.reshape(sigmoid_kernel, shape=self.input_shape[1:])

        if self.kernel_reduce is not None:
            sigmoid_kernel /= getattr(K, self.kernel_reduce)(sigmoid_kernel, axis=0, keepdims=True)
        output = K.dot(inputs, sigmoid_kernel) if self.reduce_input else inputs * sigmoid_kernel
        return output

    def add_to_model(self, model, input_shape, activation=None):
        return fs_add_to_model(self, model, input_shape, activation)

    def compute_output_shape(self, input_shape):
        if self.reduce_input:
            assert input_shape and len(input_shape) >= 2
            assert input_shape[-1]
            output_shape = list(input_shape)
            output_shape[-1] = self.units
            return tuple(output_shape)
        else:
            return input_shape


# class SoftFS(Dense):
# 
#     def __init__(self, units,
#                  kernel_initializer='he_normal',
#                  matrix_regularizer=0.,
#                  kernel_constraint=None,
#                  matrix_norm=None,
#                  step_function=softstep(),
#                  **kwargs):
#         if 'input_shape' not in kwargs and 'input_dim' in kwargs:
#             kwargs['input_shape'] = (kwargs.pop('input_dim'),)
#         super(SoftFS, self).__init__(
#             units=units,
#             use_bias=False,
#             kernel_initializer=kernel_initializer,
#             kernel_regularizer=FeatureSelectionRegularizer(
#                 matrix_factor=matrix_regularizer, matrix_norm=matrix_norm, step_function=step_function
#             ),
#             kernel_constraint=kernel_constraint,
#             **kwargs
#         )
#         self.matrix_regularizer = matrix_regularizer
#         self.supports_masking = True
#         self.matrix_norm = matrix_norm
#         self.step_function = step_function
# 
#     def call(self, inputs, training=None, **kwargs):
#         sigmoid_kernel = self.step_function(self.kernel)
#         output = K.dot(inputs, sigmoid_kernel)
#         return output
# 
#     def add_to_model(self, model, activation=None):
#         return fs_add_to_model(self, model, activation)
# 
# 
# class HardFS(Dense):
# 
#     def __init__(self, units,
#                  kernel_initializer='he_normal',
#                  kernel_regularizer=None,
#                  kernel_constraint=None,
#                  dropout=0.,
#                  step_function=softstep(),
#                  **kwargs):
#         if 'input_shape' not in kwargs and 'input_dim' in kwargs:
#             kwargs['input_shape'] = (kwargs.pop('input_dim'),)
#         super(HardFS, self).__init__(
#             units=units,
#             use_bias=False,
#             kernel_initializer=kernel_initializer,
#             kernel_regularizer=kernel_regularizer,
#             kernel_constraint=kernel_constraint,
#             **kwargs
#         )
#         self.supports_masking = True
#         self.dropout = dropout
#         self.step_function = step_function
# 
#     def call(self, inputs, training=None, **kwargs):
# 
#         def dropped_kernel():
#             if not self.dropout:
#                 return self.kernel
#             else:
#                 random_uniform = K.random_uniform(K.int_shape(self.kernel))
#                 return K.switch(K.less(random_uniform, self.dropout), -1e6 * K.ones_like(self.kernel), self.kernel)
# 
#         dropout_kernel = K.in_train_phase(dropped_kernel, self.kernel, training=training)
# 
#         argmax = K.argmax(dropout_kernel, axis=0)
#         one_hot = K.transpose(K.one_hot(argmax, K.int_shape(self.kernel)[0]))
#         kernel = one_hot * self.kernel
#         sigmoid_kernel = self.step_function(kernel)
#         output = K.dot(inputs, sigmoid_kernel)
#         return output
# 
#     def add_to_model(self, model, activation=None):
#         return fs_add_to_model(self, model, activation)
