from keras.regularizers import Regularizer, l2
from keras import backend as K
from keras_examples.shared.functions import softstep, sigmoid, matrix_norm
from keras import constraints


# class FeatureSelectionRegularizer(Regularizer):
#
#     def __init__(self, factor=0., norm='1', row_norm=None, row_factor=None, column_norm=None, column_factor=None, step_function=None):
#         super(FeatureSelectionRegularizer, self).__init__()
#         self.row_norm = norm if row_norm is None else row_norm
#         self.row_factor = factor if row_factor is None else row_factor
#         self.column_norm = norm if column_norm is None else column_norm
#         self.column_factor = factor if column_factor is None else column_factor
#         self.step_function = step_function
#
#     def __call__(self, x):
#         cost = 0.
#         step_x = self.step_function(x) if self.step_function is not None else x
#         if self.row_factor:
#             matrix = K.dot(step_x, K.transpose(step_x))
#             shape = K.int_shape(matrix)[0]
#             mask = K.ones_like(matrix) - K.eye(shape)
#             cost += self.row_factor * matrix_norm(mask * matrix, self.row_norm)
#         if self.column_factor:
#             matrix = K.dot(K.transpose(step_x), step_x)
#             shape = K.int_shape(matrix)[0]
#             factor = self.column_factor # / (shape * (shape - 1.))
#             mask = K.ones_like(matrix) - K.eye(shape)
#             cost += factor * matrix_norm(mask * matrix, self.column_norm)
#
#             cost += self.column_factor * K.sum(K.relu(1. - K.max(step_x, axis=0)))
#             # cost += self.column_factor * K.relu(1. - K.max(K.max(step_x, axis=1)))
#
#
#             # step_x = softstep()(x)
#             # sum_step_x = K.sum(step_x, axis=0)
#             # max_step_x = K.max(step_x, axis=0)
#             # cost += factor * K.max(sum_step_x - max_step_x)
#             # cost += .1 * K.sum(K.relu(1. - max_step_x))
#             # cost += self.column_factor * K.relu(1. - trace)
#             # cost += self.column_factor * K.sum(K.square(trace - 1.))
#             # trace = K.sum(step_x * step_x, axis=0)
#             # cost += factor * K.sum(K.square(trace - 1.))
#             # cost += self.column_factor * K.sum(K.square(trace - 1.))
#         # max_step_x = K.sum(step_x * step_x, axis=0)
#         # cost += 1. * K.sum(K.square(1. - max_step_x))
#         return cost



class FeatureSelectionRegularizer(Regularizer):

    def __init__(self, factor=0., norm='1', row_norm=None, row_factor=None, column_norm=None, column_factor=None, step_function=None):
        super(FeatureSelectionRegularizer, self).__init__()
        self.row_norm = norm if row_norm is None else row_norm
        self.row_factor = factor if row_factor is None else row_factor
        self.column_norm = norm if column_norm is None else column_norm
        self.column_factor = factor if column_factor is None else column_factor
        self.step_function = step_function

    def __call__(self, x):
        cost = 0.
        step_x = self.step_function(x) if self.step_function is not None else x
        if self.row_factor:
            cost += self.row_factor * K.relu(K.max(K.sum(K.abs(step_x), axis=1)) - 1.)
        if self.column_factor:
            cost += self.column_factor * K.max(K.relu(K.sum(K.abs(step_x), axis=0) - 1.))
        return cost


class FeatureSelectionRegularizer2(Regularizer):

    def __init__(self, matrix_factor=0., matrix_norm=None, step_function=None):
        super(FeatureSelectionRegularizer2, self).__init__()
        self.matrix_factor = K.cast_to_floatx(matrix_factor)
        self.matrix_norm = matrix_norm
        self.step_function = step_function

    def __call__(self, x):
        if not self.matrix_factor:
            return 0.0
        else:
            return self.matrix_factor * self.__get_matrix_regularization(x)

    def __get_matrix_regularization(self, x):
        step_x = self.step_function(x) if self.step_function is not None else x

        def sum_matrix_per_row(v):
            row = K.dot(step_x, v[:, None])
            return K.sum(row * row) if self.matrix_norm in ['frobenius', 'frob'] else K.sum(row)

        diagonal = K.sum(step_x * step_x, axis=-1)
        if self.matrix_norm in ['frobenius', 'frob']:
            diagonal *= diagonal

        sum_per_row = K.map_fn(sum_matrix_per_row, step_x) - diagonal

        if self.matrix_norm in ['frobenius', 'frob']:
            matrix_regularization = K.sum(sum_per_row)
            if '_2' not in self.matrix_norm:
                matrix_regularization = K.sqrt(matrix_regularization + 1e-8)
        elif self.matrix_norm in [None, 1, '1', 'Inf']:
            matrix_regularization = K.max(sum_per_row)
        else:
            raise Exception('matrix norm not supported')

        return matrix_regularization
