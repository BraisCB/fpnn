import keras.backend as K
from keras.layers import Dense, Input
from keras_examples.feature_selection.custom.kernels import RBF, Poly, Sigmoid, Linear
from keras_examples.feature_selection.custom.layers import Mask
from keras_examples.feature_selection.custom import saliencies
from keras_examples.shared import utils
from keras.regularizers import l2, l1
from keras.models import Model
from keras import optimizers
from keras.callbacks import Callback
import numpy as np


class EarlyStoppingByLossVal(Callback):
    def __init__(self, monitor='loss', value=0.01, verbose=0):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            print("Early stopping requires %s available!" % self.monitor)
            exit()

        if current < self.value:
            if self.verbose > 0:
                print("Epoch %05d: early stopping THR" % epoch)
            self.model.stop_training = True


class SVC:

    def __init__(self, nfeatures, C=1.0, kernel='rbf', degree=3, gamma='auto', loss_function='square_hinge',
                 coef0=0.0, lasso=0.0,
                 tol=1e-8, class_weight=None,
                 verbose=0, optimizer='adam', optimizer_args=None, train_reduced=True):
        self.nfeatures = nfeatures
        self.C = C
        self.kernel = kernel
        self.degree = degree
        self.gamma = gamma
        self.loss_function = loss_function
        self.coef0 = coef0
        self.lasso = lasso
        self.tol = tol
        self.class_weight = class_weight
        self.verbose = verbose
        self.optimizer = optimizer
        self.optimizer_args = optimizer_args
        self.model = None
        self.train_reduced = False if lasso > 0.0 else train_reduced
        self.reduced_model = None
        self.reduce_function = None
        self.loss = self.__loss_function()
        self.output_shape = None
        self.layers = None
        self.input = None
        self.output = None
        self.saliency = None

    def __loss_function(self):
        def loss(y_true, y_pred):
            out = K.relu(1.0 + (1.0 - 2.0*y_true) * y_pred)
            if 'square' in self.loss_function:
                out = K.square(out)
            if self.class_weight is not None:
                out = self.class_weight * out
            return self.C * K.sum(out, axis=-1)
        return loss

    def __transform_data(self, X, batch_size=50):
        data = []
        index = 0
        ldata = len(X)
        while index < ldata:
            new_index = min(ldata, index + batch_size)
            data.append(self.reduce_function([X[index:new_index], 0])[0])
            index = new_index
        return np.concatenate(data, axis=0)

    def __create_model(self, landmarks, nclasses):
        input = Input(shape=self.nfeatures)
        x = Mask(kernel_regularizer=l1(self.lasso))(input) if self.lasso > 0 else input
        if self.kernel == 'linear':
            kernel_svc = Linear(landmarks=landmarks)
        elif self.kernel == 'rbf':
            kernel_svc = RBF(landmarks=landmarks, gamma=self.gamma)
        elif self.kernel == 'poly':
            kernel_svc = Poly(landmarks=landmarks, gamma=self.gamma, degree=self.degree, coef0=self.coef0)
        elif self.kernel == 'sigmoid':
            kernel_svc = Sigmoid(landmarks=landmarks, gamma=self.gamma, coef0=self.coef0)
        else:
            raise Exception('kernel ' + str(self.kernel) + ' not supported')
        # x = Mask(kernel_regularizer=l1(0.01), kernel_constraint='NonNeg')(x)
        x = kernel_svc(x)
        kernel_svc.set_weights([landmarks if self.kernel == 'rbf' else landmarks.T])
        # x = Mask(kernel_regularizer=l1(0.01), kernel_constraint='NonNeg')(x)
        classifier = Dense(nclasses, use_bias=True, kernel_initializer='truncated_normal', kernel_regularizer=l2(0.001))
        output = classifier(x)
        self.model = Model(input, output)
        self.output_shape = self.model.output_shape
        self.output = self.model.output
        self.layers = self.model.layers
        self.input = self.model.input
        optimizer_args = {'lr': 1e-3} if self.optimizer_args is None else self.optimizer_args
        optimizer = getattr(optimizers, self.optimizer)(**optimizer_args)
        self.model.compile(loss=self.loss, optimizer=optimizer, metrics=['acc'])
        self.saliency = saliencies.get_saliency('hinge', self.model)
        if self.train_reduced:
            reduced_input = Input(shape=x.get_shape().as_list()[1:])
            reduced_output = classifier(reduced_input)
            self.reduced_model = Model(reduced_input, reduced_output)
            self.reduced_model.compile(loss=self.loss, optimizer=optimizer, metrics=['acc'])
            lp = K.learning_phase()
            self.reduce_function = K.function([self.model.input, lp], [x])

    def get_reduced_data(self, X, nclasses):
        if self.model is None:
            self.__create_model(X, nclasses)
        return self.__transform_data(X)

    def fit(self, X, y, batch_size=200, epochs=200, verbose=0, balance_data=True, reduced_data=None, callbacks=None):
        y = np.asarray(y)
        nclasses = len(np.unique(y)) if len(y.shape) == 1 else y.shape[-1]
        if self.model is None:
            self.__create_model(X, nclasses)
        if self.train_reduced:
            data = self.__transform_data(X) if reduced_data is None else reduced_data
            model = self.reduced_model
        else:
            data = X
            model = self.model
        data, label = utils.balance_data(data, y) if balance_data else (data, y)
        callbacks = [] if callbacks is None else callbacks
        callbacks += [
            EarlyStoppingByLossVal(monitor='loss', value=self.tol, verbose=1)
        ]
        batch_size = min(len(data), batch_size)
        model.fit(
            data, label, batch_size=batch_size, epochs=epochs, verbose=verbose, callbacks=callbacks
        )

    def predict(self, X, batch_size=50):
        return self.model.predict(X, batch_size=batch_size)

    def evaluate(self, X, Y, **kwargs):
        return self.model.evaluate(X, Y, **kwargs)
