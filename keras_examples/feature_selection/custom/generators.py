from keras.preprocessing.image import ImageDataGenerator
import numpy as np


class NoiseDataGenerator(ImageDataGenerator):
    # Class is a dataset wrapper for better training performance
    def __init__(self, negative=False,
                 gaussian_stddev=0.,
                 uniform_range=0.,
                 minval=None,
                 maxval=None,
                 **kwargs):
        self.gaussian_stddev = gaussian_stddev
        self.uniform_range = uniform_range
        self.minval = minval
        self.maxval = maxval
        self.negative = negative
        super(NoiseDataGenerator, self).__init__(**kwargs)

    def random_transform(self, x, seed=None):
        x = super(NoiseDataGenerator, self).random_transform(x, seed)

        if self.gaussian_stddev:
            x += np.random.randn(*x.shape) * self.gaussian_stddev

        if self.uniform_range:
            x += np.random.uniform(-self.uniform_range, self.uniform_range)

        if self.minval:
            x = np.maximum(self.minval, x)

        if self.maxval:
            x = np.minimum(self.maxval, x)

        if self.negative:
            if np.random.random() < 0.5:
                maxval = 1. if self.maxval is None else self.maxval
                x = maxval - x
        return x
