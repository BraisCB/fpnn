from keras_examples.datasets.nips_challenge import load_data
from keras_examples.custom.feature_selection import get_rank
from keras_examples.custom.svm import SVC
import numpy as np
from keras.utils import to_categorical
import json
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


dataset_names = [
    'arcene',
    'dexter',
    'madelon',
    'dorothea',
    'gisette',
]

reps = 2
b_size = 50
epochs = 250
gamma = 0.9

for dataset_name in dataset_names:

    fs_filename = './keras_examples/info/feature_selection/svm_fixed/' + dataset_name + '_svm_ranks.json'

    print('loading dataset', dataset_name)
    dataset = load_data('./datasets/' + dataset_name + '/' + dataset_name, normalize=True)
    print('data loaded. labels =', dataset['train']['data'].shape)
    input_shape = dataset['train']['data'].shape[-1:]
    batch_size = min(len(dataset['train']['data']), b_size)

    uclasses = np.unique(dataset['train']['label'])
    nclasses = len(uclasses)

    data = dataset['train']['data']
    data = (data - np.mean(data, axis=0)) / np.maximum(1e-6, np.std(data, axis=0))
    label = to_categorical(dataset['train']['label'], nclasses)

    for fs_mode in ['simonyan']:
        for lasso in [0.0]:
            if lasso == 0.0 and fs_mode == 'lasso':
                continue
            name = dataset_name + '_' + fs_mode + '_l_' + str(lasso) + '_g_' + str(gamma)
            print(name)
            kwargs = {
                'kernel': 'rbf',
                'lasso': lasso
            }
            rank = get_rank(fs_mode, data=data, label=label, create_model_func=SVC,
                            gamma=gamma, reps=reps,
                            batch_size=batch_size, epochs=epochs, **kwargs)

            try:
                with open(fs_filename) as outfile:
                    info_data = json.load(outfile)
            except:
                info_data = {}

            if fs_mode not in info_data:
                info_data[fs_mode] = []

            info_data[fs_mode].append(
                {
                    'lasso': lasso,
                    'gamma': gamma,
                    'rank': rank.tolist()
                }
            )

            with open(fs_filename, 'w') as outfile:
                json.dump(info_data, outfile)

            del rank


