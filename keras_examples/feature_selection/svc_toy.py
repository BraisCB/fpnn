from keras_examples.feature_selection.custom.sklearn_parser import SVC
import numpy as np
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

root_directory = './keras_examples/feature_selection/scripts/nips/info/svc/'
datasets_directory = './keras_examples/feature_selection/nips/datasets/'


if __name__ == '__main__':
    os.chdir('../../')

    train_data = np.array([[-1., -.1], [1., .3]])
    train_label = np.array([[1, 0],[0, 1]]).astype(int)

    model = SVC((2,), kernel='linear', saliency_reduce_func=None, use_pca=True)
    model.fit(train_data, train_label)

    print(model.predict(train_data))
    saliency = model.saliency([np.array([[1., -.1]]), np.array([[0, 1]]).astype(int), 0])[0]

    print(saliency)
    print(model.evaluate([np.array([[1., -.1]])], np.array([[0, 1]])))

