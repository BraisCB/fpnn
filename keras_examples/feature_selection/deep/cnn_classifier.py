from .cnn_ranker import load_dataset
from keras.utils import to_categorical
from keras import backend as K, callbacks
import numpy as np
import json
from . import network_models
from copy import deepcopy
import os
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

reps = 4
gamma = 0.75
regularization = 0.0
batch_size = 100
epochs = 40


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
set_session(tf.Session(config=config))


dataset_names = ['mnist', 'cifar10', 'cifar100']
directory = './keras_examples/feature_selection/deep/info/'
rank_names = ['cnnsimple', 'cnnsimple_normalized', 'cnnallconv', 'cnnallconv_normalized']
fs_methods = ['simonyan', 'lasso']
network_names = ['cnnsimple', 'cnnallconv']

ranks_directory = directory + 'ranks/'
results_directory = directory + 'results/'


def get_factor(n, max_n):
    if n < 0.1*max_n:
        return 1.5
    elif n < 0.5*max_n:
        return 2.0
    else:
        return 2.5


def scheduler(epoch):
    if epoch < 12:
        return .01
    elif epoch < 24:
        return .001
    elif epoch < 36:
        return .0001
    else:
        return .00001


for dataset_name in dataset_names:
    print('loading dataset', dataset_name)
    dataset = load_dataset(dataset_name)
    for fs_method in fs_methods:
        for rank_name in rank_names:
            extra_directories = dataset_name + '/' + fs_method + '/'
            rank_filename = ranks_directory + extra_directories + rank_name + '_' + str(gamma) + '_rank.json'
            result_directory = results_directory + extra_directories
            try:
                os.makedirs(result_directory)
            except:
                pass
            result_filename = result_directory + rank_name + '_' + str(gamma) + '_result.json'
            try:
                with open(rank_filename) as outfile:
                    rank_json = json.load(outfile)
            except:
                continue
            data = np.asarray(dataset['train']['data'])
            label = np.asarray(dataset['train']['label'])
            train_labels = to_categorical(label)
            print('data loaded. labels =', data.shape)

            valid_data = np.asarray(dataset['test']['data'])
            valid_label = np.asarray(dataset['test']['label'])

            max_features = int(np.prod(data.shape[1:]))

            nclasses = train_labels.shape[-1]

            for method in rank_json:
                rank_list = rank_json[method]
                for example in rank_list:
                    for network_name in network_names:

                        print('METHOD : ', method)
                        print('NETWORK : ', network_name)
                        print('RANK :', rank_name)
                        if 'regularization' in example:
                            print('regularization : ', example['regularization'])
                        if 'gamma' in example:
                            print('gamma : ', example['gamma'])
                        output = deepcopy(example)
                        nfeats = []
                        accuracies = []
                        rank = np.array(example['rank']).astype(int)
                        n_features = 10
                        best_acc = 0
                        best_feat = 0
                        # max_features = int(limit * data.shape[-1])
                        while n_features <= max_features:
                            r_accuracy = []
                            generator = None
                            if 'dense' in network_name:
                                data_min = np.reshape(data, [-1, np.prod(data.shape[1:])])[:, rank[:n_features]]
                                valid_data_min = np.reshape(valid_data, [-1, np.prod(valid_data.shape[1:])])[:, rank[:n_features]]
                            else:
                                mask = np.zeros(data.shape[1:])
                                mask.flat[rank[:n_features]] = 1.0
                                data_min = data * mask
                                valid_data_min = valid_data * mask
                                generator = dataset['generator']
                            for rep in range(reps):
                                create_model_func = getattr(network_models, network_name.split('_')[0])
                                input_shape = data_min.shape[1:]
                                model = create_model_func(
                                    input_shape=input_shape, nclasses=nclasses, regularization=regularization
                                )
                                if generator is None:
                                    model.fit(data_min, train_labels, batch_size=batch_size, epochs=epochs, verbose=0, callbacks=[
                                        callbacks.LearningRateScheduler(scheduler)
                                    ])
                                else:
                                    model.fit_generator(
                                        generator.flow(data_min, train_labels, batch_size=batch_size),
                                        steps_per_epoch=len(data_min) // batch_size, epochs=epochs,
                                        callbacks=[
                                            # callbacks.ModelCheckpoint(
                                            #     './keras_examples/weights/' + name + '_Weights.h5',
                                            #     monitor="val_acc",
                                            #     save_best_only=True,
                                            #     verbose=1
                                            # ),
                                            callbacks.LearningRateScheduler(scheduler)
                                        ],
                                        verbose=0
                                    )
                                yPred = np.argmax(model.predict(valid_data_min), axis=-1)[:, None]
                                r_accuracy.append((yPred == valid_label).sum() / len(yPred) * 100)
                                del model
                                K.clear_session()
                            accuracy = np.mean(r_accuracy)
                            print('n_features : ', n_features, ', acc :', accuracy)
                            if accuracy > best_acc:
                                # print("Accuracy : ", accuracy)
                                best_acc = accuracy
                                best_feat = n_features
                            nfeats.append(n_features)
                            accuracies.append(r_accuracy)
                            if n_features == max_features:
                                break
                            n_features = min(
                                max(
                                    int(n_features * get_factor(n_features, max_features)),
                                    n_features + 1),
                                max_features
                            )
                        output['classification'] = {
                            'network': network_name,
                            'n_features': nfeats,
                            'accuracy': accuracies
                        }
                        print('best : ', best_acc, ' , feats : ', best_feat)
                        accuracies = np.array(np.mean(accuracies, axis=-1))
                        nfeats = np.array(nfeats)
                        roc = 0.5 * (accuracies[1:] + accuracies[:-1]) * (nfeats[1:] - nfeats[:-1])
                        roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
                        print('ROC : ', roc)

                        try:
                            with open(result_filename) as outfile:
                                results = json.load(outfile)
                        except:
                            results = {}

                        if method not in results:
                            results[method] = {}

                        if network_name not in results[method]:
                            results[method][network_name] = []
                        results[method][network_name].append(output)

                        with open(result_filename, 'w') as outfile:
                            json.dump(results, outfile)
