import numpy as np
import json
from matplotlib import pyplot as plt
import os


gammas = [0.0, 0.75]
factor = 0.95

dataset_names = ['mnist', 'cifar10', 'cifar100']
directory = './info/'
rank_names = ['cnnsimple', 'wrn164_nonormalized', 'wrn164']
fs_methods = ['simonyan', 'lasso']
network_names = ['wrn164']

ranks_directory = directory + 'ranks/'
results_directory = directory + 'results/'


if __name__ == '__main__':
    # os.chdir('../../../')
    plt.ion()
    for dataset_name in dataset_names:
        plt.figure()
        plt.title('DATASET : ' + dataset_name)
        legends = []
        for rank_name in rank_names:

            for fs_method in fs_methods:
                for gamma in gammas:
                    if (fs_method == 'simonyan' and rank_name == 'wrn164'):
                        continue
                    extra_directories = dataset_name + '/' + fs_method + '/'
                    result_directory = results_directory + extra_directories
                    result_filename = result_directory + rank_name + '_' + str(gamma) + '_result.json'
                    try:
                        with open(result_filename) as outfile:
                            result_json = json.load(outfile)
                    except:
                        continue
                    if fs_method not in result_json:
                        continue
                    result_dict = result_json[fs_method]
                    for network_name in network_names:
                        if network_name not in result_dict:
                            continue
                        examples = result_dict[network_name]
                        for example in examples:
                            print('DATASET : ', dataset_name)
                            print('METHOD : ', fs_method)
                            print('NETWORK : ', network_name)
                            print('RANK :', rank_name)
                            print('GAMMA : ', gamma)
                            if 'regularization' in example:
                                print('regularization : ', example['regularization'])
                            if 'lasso' in example:
                                print('lasso : ', example['lasso'])
                            nfeats = example['classification']['n_features'][:]
                            accuracy = example['classification']['accuracy'][:]
                            accuracies = np.mean(accuracy, axis=-1)
                            best_index = np.argmax(accuracies)
                            print('best : ', accuracies[best_index], ' , feats : ', nfeats[best_index])
                            nfeats = np.array(nfeats)
                            roc = 0.5 * (accuracies[1:] + accuracies[:-1]) * (nfeats[1:] - nfeats[:-1])
                            roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
                            print('ROC : ', roc)
                            good_value = accuracies[best_index]*factor
                            index = np.min(np.argwhere(accuracies > good_value))
                            print('good : ', accuracies, ' , feats : ', nfeats)
                            # print('good2 : ', accuracies[8], ' , feats2 : ', nfeats[8])
                            plt.plot(nfeats, accuracies)
                            legends.append(
                                fs_method + ' l1 : ' + str(example['lasso']) + ' g : ' + str(gamma) + ' r : ' + rank_name
                            )
        plt.legend(tuple(legends))
        plt.show()
        plot_filename = results_directory + dataset_name + '/results2.png'
        # plt.savefig(plot_filename)