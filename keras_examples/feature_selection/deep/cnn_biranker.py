from keras.utils import to_categorical
from keras import callbacks
from keras.datasets import mnist, cifar10, cifar100
from . import network_models
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9
set_session(tf.Session(config=config))


gamma = 0.1
b_size = 128
epochs = 200
reps = 1

dataset_names = ['mnist', 'cifar10', 'cifar100']
directory = './keras_examples/feature_selection/deep/info/birank/'
network_names = ['wrn164']


def scheduler(epoch, wrn=True):
    if not wrn:
        if epoch < 12:
            return .01
        elif epoch < 24:
            return .001
        elif epoch < 36:
            return .0001
        else:
            return .00001
    else:
        if epoch < 60:
            return 0.1
        elif epoch < 120:
            return 0.02
        elif epoch < 160:
            return 0.004
        else:
            return 0.0008


rank_kwargs = {
    'reps': reps,
    'gamma': gamma
}


def load_dataset(dataset, normalize=True):
    if dataset == 'mnist':
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        x_train = np.expand_dims(x_train, axis=-1)
        x_test = np.expand_dims(x_test, axis=-1)
        generator = ImageDataGenerator(width_shift_range=4. / 28,
                                       height_shift_range=4. / 28,
                                       fill_mode='reflect')
    elif dataset == 'cifar10':
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        generator = ImageDataGenerator(width_shift_range=5. / 32,
                                       height_shift_range=5. / 32,
                                       fill_mode='reflect',
                                       horizontal_flip=True)
    elif dataset == 'cifar100':
        (x_train, y_train), (x_test, y_test) = cifar100.load_data()
        generator = ImageDataGenerator(width_shift_range=5. / 32,
                                       height_shift_range=5. / 32,
                                       fill_mode='reflect',
                                       horizontal_flip=True)
    else:
        raise Exception('dataset not found')
    y_train = np.reshape(y_train, [-1, 1])
    y_test = np.reshape(y_test, [-1, 1])
    if normalize:
        mean = x_train.mean(axis=0)
        std = np.maximum(1e-6, x_train.std(axis=0))
        x_train = (x_train - mean) / std
        x_test = (x_test - mean) / std
    output = {
        'train': {
            'data': x_train,
            'label': y_train
        },
        'test': {
            'data': x_test,
            'label': y_test
        },
        'generator': generator
    }
    return output


def main():

    sd_directory = directory + 'ranks/'

    for dataset_name in dataset_names:
        print('dataset =', dataset_name)
        dataset = load_dataset(dataset_name)

        for network_name in network_names:

            model_func = getattr(network_models, network_name.split('_')[0])

            train_data = np.asarray(dataset['train']['data'])
            train_labels = dataset['train']['label']
            num_classes = len(np.unique(train_labels))

            train_labels = to_categorical(train_labels, num_classes=num_classes)
            batch_size = min(len(train_data), b_size)

            test_data = np.asarray(dataset['test']['data'])
            test_labels = dataset['test']['label']
            test_labels = to_categorical(test_labels, num_classes=num_classes)

            fit_kwargs = {
                'epochs': epochs,
                'callbacks': [
                    callbacks.LearningRateScheduler(scheduler)
                ],
                'verbose': 0,
                'steps_per_epoch': len(train_data) // batch_size
            }

            generator = dataset['generator']
            generator_kwargs = {
                'batch_size': batch_size
            }
            if 'dense' in network_name:
                generator = None
                fit_kwargs['batch_size'] = batch_size
                if dataset_name != 'mnist':
                    train_data_fliplr = np.asarray([np.fliplr(d) for d in train_data])
                    train_data = np.concatenate((train_data, train_data_fliplr), axis=0)
                    train_labels = np.concatenate((train_labels, train_labels), axis=0)
                train_data = np.reshape(train_data, [-1, np.prod(train_data.shape[1:])])
            else:
                fit_kwargs['steps_per_epoch'] = len(train_data) // batch_size

            for lasso in [0.0, 5e-4]:
                for regularization in [5e-4]:
                    name = dataset_name + '_l_' + str(lasso) + '_g_' + str(gamma) + '_r_' + str(regularization)
                    print(name)
                    model_kwargs = {
                        'nclasses': num_classes,
                        'lasso': lasso,
                        'regularization': regularization
                    }
                    model = model_func(train_data.shape[1:], **model_kwargs)

                    history = model.fit_generator(
                        generator.flow(train_data, train_labels, batch_size=batch_size),
                        steps_per_epoch=len(train_data) // batch_size, epochs=epochs,
                        callbacks=[
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        validation_data=(test_data, test_labels),
                        validation_steps=test_data.shape[0] // batch_size,
                        verbose=2
                    )

                    total_saliency = np.zeros_like(train_data)
                    for label in range(num_classes):
                        output_label = np.zeros_like(train_labels)
                        output_label[:, label] = 1.0

                        index = 0
                        ndata = len(train_data)
                        saliency = []
                        while index < ndata:
                            new_index = min(ndata, index + batch_size)
                            saliency.append(model.saliency([train_data[index:new_index], output_label[index:new_index], 0])[0])
                            index = new_index
                        saliency = np.concatenate(tuple(saliency), axis=0)
                        total_saliency += saliency
                    saliency = total_saliency
                    nfeats = int(np.prod(saliency.shape[1:]))
                    saliency = np.reshape(saliency, [-1, nfeats])

                    saliency_sort = np.argsort(saliency, axis=-1)
                    new_feats = int(gamma * nfeats)
                    saliency_sort += nfeats * np.reshape(np.arange(ndata).astype(int), [-1, 1])
                    zero_values = saliency_sort[:, :-new_feats].flatten()

                    new_train_data = train_data.copy()
                    new_train_data.flat[zero_values] = 0.0

                    total_saliency = np.zeros_like(test_data)
                    for label in range(num_classes):
                        output_label = np.zeros_like(test_labels)
                        output_label[:, label] = 1.0

                        index = 0
                        ndata = len(test_data)
                        saliency = []
                        while index < ndata:
                            new_index = min(ndata, index + batch_size)
                            saliency.append(
                                model.saliency([test_data[index:new_index], output_label[index:new_index], 0])[0])
                            index = new_index
                        saliency = np.concatenate(tuple(saliency), axis=0)
                        total_saliency += saliency
                    saliency = total_saliency
                    nfeats = int(np.prod(saliency.shape[1:]))
                    print('NFEATS : ', nfeats)
                    saliency = np.reshape(saliency, [-1, nfeats])

                    saliency_sort = np.argsort(saliency, axis=-1)
                    new_feats = int(gamma * nfeats)
                    saliency_sort += nfeats * np.reshape(np.arange(ndata).astype(int), [-1, 1])
                    zero_values = saliency_sort[:, :-new_feats].flatten()

                    new_test_data = test_data.copy()
                    new_test_data.flat[zero_values] = 0.0

                    evaluation = model.evaluate(new_test_data, test_labels)
                    print(evaluation)

                    model = model_func(new_train_data.shape[1:], **model_kwargs)

                    history = model.fit_generator(
                        generator.flow(new_train_data, train_labels, batch_size=batch_size),
                        steps_per_epoch=len(new_train_data) // batch_size, epochs=epochs,
                        callbacks=[
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        validation_data=(new_test_data, test_labels),
                        validation_steps=new_test_data.shape[0] // batch_size,
                        verbose=2
                    )


if __name__ == '__main__':
    main()