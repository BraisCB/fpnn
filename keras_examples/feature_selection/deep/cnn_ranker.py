from ..custom.feature_selection import get_rank
from keras.utils import to_categorical
from keras import callbacks
from keras.datasets import mnist, cifar10, cifar100
from . import network_models
import json
import numpy as np
import os
from keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9
set_session(tf.Session(config=config))


gamma = 0.0
b_size = 128
epochs = 200
reps = 1

dataset_names = ['cifar10', 'cifar100', 'mnist']
directory = './keras_examples/feature_selection/deep/info/'
network_names = ['wrn164_nonormalized']
methods = ['simonyan']


def scheduler(epoch, wrn=True):
    if not wrn:
        if epoch < 12:
            return .01
        elif epoch < 24:
            return .001
        elif epoch < 36:
            return .0001
        else:
            return .00001
    else:
        if epoch < 60:
            return 0.1
        elif epoch < 120:
            return 0.02
        elif epoch < 160:
            return 0.004
        else:
            return 0.0008


rank_kwargs = {
    'reps': reps,
    'gamma': gamma
}


def load_dataset(dataset, normalize=True):
    if dataset == 'mnist':
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        x_train = np.expand_dims(x_train, axis=-1)
        x_test = np.expand_dims(x_test, axis=-1)
        generator = ImageDataGenerator(width_shift_range=4. / 28,
                                       height_shift_range=4. / 28,
                                       fill_mode='reflect')
    elif dataset == 'cifar10':
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        generator = ImageDataGenerator(width_shift_range=5. / 32,
                                       height_shift_range=5. / 32,
                                       fill_mode='reflect',
                                       horizontal_flip=True)
    elif dataset == 'cifar100':
        (x_train, y_train), (x_test, y_test) = cifar100.load_data()
        generator = ImageDataGenerator(width_shift_range=5. / 32,
                                       height_shift_range=5. / 32,
                                       fill_mode='reflect',
                                       horizontal_flip=True)
    else:
        raise Exception('dataset not found')
    y_train = np.reshape(y_train, [-1, 1])
    y_test = np.reshape(y_test, [-1, 1])
    if normalize:
        mean = x_train.mean(axis=0)
        std = np.maximum(1e-6, x_train.std(axis=0))
        x_train = (x_train - mean) / std
        x_test = (x_test - mean) / std
    output = {
        'train': {
            'data': x_train,
            'label': y_train
        },
        'test': {
            'data': x_test,
            'label': y_test
        },
        'generator': generator
    }
    return output


def main():

    sd_directory = directory + 'ranks/'
    try:
        os.makedirs(sd_directory)
    except:
        pass

    for dataset_name in dataset_names:
        print('dataset =', dataset_name)
        dataset = load_dataset(dataset_name)

        for network_name in network_names:

            model_func = getattr(network_models, network_name.split('_')[0])

            train_data = np.asarray(dataset['train']['data'])
            train_labels = dataset['train']['label']
            num_classes = len(np.unique(train_labels))

            train_labels = to_categorical(train_labels, num_classes=num_classes)
            batch_size = min(len(train_data), b_size)

            fit_kwargs = {
                'epochs': epochs,
                'callbacks': [
                    callbacks.LearningRateScheduler(scheduler)
                ],
                'verbose': 0
            }

            generator = dataset['generator']
            generator_kwargs = {
                'batch_size': batch_size
            }
            if 'dense' in network_name:
                generator = None
                fit_kwargs['batch_size'] = batch_size
                if dataset_name != 'mnist':
                    train_data_fliplr = np.asarray([np.fliplr(d) for d in train_data])
                    train_data = np.concatenate((train_data, train_data_fliplr), axis=0)
                    train_labels = np.concatenate((train_labels, train_labels), axis=0)
                train_data = np.reshape(train_data, [-1, np.prod(train_data.shape[1:])])
            else:
                fit_kwargs['steps_per_epoch'] = len(train_data) // batch_size


            for fs_mode in methods:
                for lasso in [0.0, 5e-4]:
                    if lasso == 0.0 and fs_mode == 'lasso':
                        continue
                    print('reps : ', reps)
                    print('method : ', fs_mode)
                    for regularization in [5e-4]:
                        name = dataset_name + '_' + fs_mode + '_l_' + str(lasso) + '_g_' + str(gamma) + \
                               '_r_' + str(regularization)
                        print(name)
                        model_kwargs = {
                            'nclasses': num_classes,
                            'lasso': lasso,
                            'regularization': regularization
                        }
                        rank = get_rank(fs_mode, data=train_data, label=train_labels, model_func=model_func,
                                        model_kwargs=model_kwargs, fit_kwargs=fit_kwargs, generator=generator,
                                        generator_kwargs=generator_kwargs, rank_kwargs=rank_kwargs)
                        rank_directory = sd_directory + dataset_name + '/' + fs_mode + '/'
                        try:
                            os.makedirs(rank_directory)
                        except:
                            pass
                        output_filename = rank_directory + network_name + '_' + str(gamma) + '_rank.json'

                        try:
                            with open(output_filename) as outfile:
                                info_data = json.load(outfile)
                        except:
                            info_data = {}

                        if fs_mode not in info_data:
                            info_data[fs_mode] = []

                        info_data[fs_mode].append(
                            {
                                'lasso': lasso,
                                'gamma': gamma,
                                'regularization': regularization,
                                'rank': rank.tolist(),
                                'reps': reps
                            }
                        )

                        with open(output_filename, 'w') as outfile:
                            json.dump(info_data, outfile)

                        del rank


if __name__ == '__main__':
    main()