import numpy as np
import json



dataset_names = [
    ('arcene', 7000),
    ('dexter', 9947),
    ('gisette', 2500),
    ('madelon', 20),
    ('dorothea', 50000)
]


for dataset_stats in dataset_names:
    dataset_name, max_features = dataset_stats
    print('DATASET : ', dataset_name)
    legends = []
    for weka in [True, False]:
        fs_filename = './keras_examples/info/feature_selection/'

        output_filename = fs_filename + 'nn_weka/' + dataset_name + '_weka_results.json' if weka else \
            fs_filename + 'nn_ranks_gamma_2/' + dataset_name + '_nn_results.json'
        try:
            with open(output_filename) as outfile:
                results = json.load(outfile)
        except:
            continue

        for method in results:
            rank_list = results[method]
            outfile = fs_filename + 'nn_weka/' + dataset_name + '_' + method + '_weka_results.csv' if weka else \
                fs_filename + 'nn_ranks_gamma_2/' + dataset_name + '_' + method + '_nn_results.csv'
            for example in rank_list:
                print('METHOD : ', method)
                if 'lasso' in example:
                    print('lasso : ', example['lasso'])
                if 'gamma' in example:
                    print('gamma : ', example['gamma'])
                if 'reps' in example:
                    print('reps : ', example['reps'])
                accuracy = np.array(example['classification']['accuracy'])

                np.savetxt(outfile, accuracy, delimiter=",")


