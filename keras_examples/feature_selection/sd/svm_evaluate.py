from keras_examples.datasets.nips_challenge import load_data
from keras_examples.custom.utils import balance_data
from sklearn.svm import SVC
import numpy as np
import json
from copy import deepcopy
import os


dataset_names = [
    'sd1',
    'sd2',
    'sd3',
]

kfold = 10
token = '_8.0_'

results_directory = './keras_examples/feature_selection/sd/info/results/svm/'

methods = [f for f in os.listdir(results_directory) if os.path.isdir(os.path.join(results_directory, f))]


for dataset_name in dataset_names:
    for method in methods:
        accs = None
        fs_directory = results_directory + method + '/' + dataset_name + '_' + str(kfold) + '/'
        try:
            filenames = [f for f in os.listdir(fs_directory) if os.path.isfile(os.path.join(fs_directory, f))]
        except:
            continue
        count_filenames = 0
        for filename in filenames:
            segments = filename.split('_')
            if len(segments) > 4 and token not in filename:
                continue
            complete_filename = fs_directory + filename
            with open(complete_filename) as outfile:
                results = json.load(outfile)[method]

            for i, result in enumerate(results):
                if accs is None:
                    accs = np.zeros((len(results), len(result['classification']['accuracy'])))
                    cs = np.zeros(len(results))
                    n_feats = np.zeros(len(result['classification']['accuracy'])).astype(int)
                accs[i] += np.asarray(result['classification']['accuracy']).mean(axis=-1)
                n_feats = np.asarray(result['classification']['n_features'])
                cs[i] = result['classification']['C']
            count_filenames += 1

        accs /= count_filenames
        best_accs_by_cs = accs.max(axis=-1)
        index_best_accs_by_cs = accs.argmax(axis=-1)

        index_best_acc = best_accs_by_cs.argmax()
        best_acc = best_accs_by_cs[index_best_acc]
        best_n_feats = n_feats[index_best_accs_by_cs[index_best_acc]]
        best_c = cs[index_best_acc]

        print('DATASET : ', dataset_name)
        print('METHOD : ', method)
        print('ACC : ', best_acc, ' , N_FEATS : ', best_n_feats, ' , C : ', best_c)

