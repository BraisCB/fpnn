from keras_examples.custom.svm import SVC
from keras_examples.custom.feature_selection import get_rank
from keras_examples.datasets.sd import load_data
from keras.utils import to_categorical
import json
import numpy as np
import os


gamma = 0.9
b_size = 100
epochs = 1000
reps = 5

directory = './keras_examples/feature_selection/sd/info/'

dataset_names = ['sd1', 'sd2', 'sd3']


def main():

    sd_directory = directory + 'ranks/'
    try:
        os.makedirs(sd_directory)
    except:
        pass

    svm_directory = sd_directory + 'arff/'

    fs_directory = './datasets/sd/'

    for dataset_name in dataset_names:
        print('dataset =', dataset_name)
        dataset = load_data('./datasets/sd/' + dataset_name)

        train_data = np.asarray(dataset['train']['data'])
        train_labels = dataset['train']['label']
        num_classes = len(np.unique(train_labels))

        train_labels = to_categorical(train_labels, num_classes=num_classes)
        batch_size = min(len(train_data), b_size)

        for fs_mode in ['simonyan']: #, 'lasso']:
            for lasso in [0.0, 1e-2]:
                if lasso == 0.0 and fs_mode == 'lasso' or lasso > 0.0 and fs_mode == 'simonyan':
                    continue
                print('reps : ', reps)
                print('method : ', fs_mode)
                for power in range(0, 10):
                    name = dataset_name + '_' + fs_mode + '_l_' + str(lasso) + '_g_' + str(gamma) + \
                           '_r_' + str(power)
                    print(name)
                    C = float(pow(2.0, power))
                    print('C : ', C)
                    kwargs = {
                        'lasso': lasso,
                        'C': C,
                    }
                    rank = get_rank(fs_mode, data=train_data, label=train_labels, create_model_func=SVC,
                                    gamma=gamma, reps=reps,
                                    batch_size=batch_size, epochs=epochs, **kwargs)
                    rank_directory = svm_directory + fs_mode + '/'
                    try:
                        os.makedirs(rank_directory)
                    except:
                        pass
                    output_filename = rank_directory + dataset_name + '_rank.json'

                    try:
                        with open(output_filename) as outfile:
                            info_data = json.load(outfile)
                    except:
                        info_data = {}

                    if fs_mode not in info_data:
                        info_data[fs_mode] = []

                    info_data[fs_mode].append(
                        {
                            'lasso': lasso,
                            'gamma': gamma,
                            'C': C,
                            'rank': rank.tolist(),
                            'reps': reps
                        }
                    )

                    with open(output_filename, 'w') as outfile:
                        json.dump(info_data, outfile)

                    del rank


if __name__ == '__main__':
    main()