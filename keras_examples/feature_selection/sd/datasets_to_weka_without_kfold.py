from keras_examples.datasets.sd import load_data
from keras_examples.datasets.arff_parser import to_arff
import os


dataset_names = ['sd1', 'sd2', 'sd3']
subsets = ['train']


def main():
    # load data
    sd_directory = './keras_examples/feature_selection/sd/info/datasets/arff/'
    try:
        os.makedirs(sd_directory)
    except:
        pass

    for dataset_name in dataset_names:
        print('dataset =', dataset_name)
        dataset = load_data('./datasets/sd/' + dataset_name)

        fs_directory = sd_directory + dataset_name + '/'
        try:
            os.makedirs(fs_directory)
        except:
            pass

        to_arff(dataset, fs_directory, dataset_name, subsets)


if __name__ == '__main__':
    main()
