from keras_examples.datasets.nips_challenge import load_data
from keras_examples.custom.utils import balance_data
from sklearn.svm import SVC
import numpy as np
import json
from copy import deepcopy
import os


dataset_names = [
    ('sd1', 50),
    ('sd2', 100),
    ('sd3', 150)
]

folders = [
    'simonyan', # 'simonyan2', 'simonyan3', 'MIM', 'MRMR', 'relieff'#
]

is_weka = [
    False, # False, False
]

datasets_directory = './keras_examples/feature_selection/sd/info/datasets/json/'
ranks_directory = './keras_examples/feature_selection/sd/info/ranks/svm/'
results_directory = './keras_examples/feature_selection/sd/info/results/svm/'

reps = 10
kfold = 10

for dataset_stats in dataset_names:
    dataset_name, max_features = dataset_stats
    fs_directory = datasets_directory + dataset_name + '_' + str(kfold) + '/'
    filenames = [f for f in os.listdir(fs_directory) if os.path.isfile(os.path.join(fs_directory, f))]
    for filename in filenames:
        print('loading dataset', filename)
        complete_filename = fs_directory + filename
        with open(complete_filename) as outfile:
            dataset = json.load(outfile)
        data = np.asarray(dataset['train']['data'])
        label = np.asarray(dataset['train']['label'])
        print('data loaded. labels =', data.shape)
        input_shape = data.shape[-1:]

        nclasses = len(np.unique(label))

        data_mean = np.mean(data, axis=0)
        data_std = np.maximum(1e-6, np.std(data, axis=0))
        data = (data - data_mean) / data_std

        data, label = balance_data(data, label)

        valid_data = np.asarray(dataset['test']['data'])
        valid_data = (valid_data - data_mean) / data_std
        valid_label = np.asarray(dataset['test']['label'])

        total_features = data.shape[-1]

        for folder, weka in zip(folders, is_weka):
            rank_filename = ranks_directory + folder + '/' + dataset_name + '_' + str(kfold) + '/' + filename[:-5] + '_rank.json'
            result_directory = results_directory + folder + '/' + dataset_name + '_' + str(kfold) + '/'
            try:
                os.makedirs(result_directory)
            except:
                pass
            try:
                with open(rank_filename) as outfile:
                    ranks = json.load(outfile)
            except:
                continue

            for method in ranks:
                print('METHOD : ', method)
                rank_list = ranks[method]
                for i, example in enumerate(rank_list):
                    result_filename = result_directory + filename[:-5]
                    if 'regularization' in example:
                        result_filename += '_' + str(example['regularization'])
                    result_filename += '_result.json'
                    for power in range(-5, 10):
                        print('POWER : ', power)
                        if 'regularization' in example:
                            print('regularization : ', example['regularization'])
                        if 'gamma' in example:
                            print('gamma : ', example['gamma'])
                        output = deepcopy(example)
                        nfeats = []
                        accuracies = []
                        rank = np.array(example['rank']).astype(int) - int(weka)
                        n_features = 1
                        best_acc = 0
                        best_feat = 0
                        # max_features = int(limit * data.shape[-1])
                        while n_features <= max_features:
                            # print('n_features : ', n_features)
                            r_accuracy = []
                            train_accuracy = 0.0
                            data_min = data[:, rank[:n_features]]
                            for rep in range(reps):
                                model = SVC(C=float(pow(2.0, power)), max_iter=1000)
                                model.fit(data_min, label)
                                train_accuracy += (model.predict(data_min) == label).sum() / len(label) * 100
                                yPred = model.predict(valid_data[:, rank[:n_features]])
                                r_accuracy.append((yPred == valid_label).sum() / len(yPred) * 100)
                                del model
                            accuracy = np.mean(r_accuracy)
                            train_accuracy /= reps
                            if accuracy > best_acc:
                                # print("Accuracy : ", accuracy)
                                best_acc = accuracy
                                best_feat = n_features
                            nfeats.append(n_features)
                            accuracies.append(r_accuracy)
                            if n_features == max_features:
                                break
                            n_features += 1
                        output['classification'] = {
                            'C': float(pow(2.0, power)),
                            'n_features': nfeats,
                            'accuracy': accuracies
                        }
                        print('best : ', best_acc, ' , feats : ', best_feat)
                        accuracies = np.array(np.mean(accuracies, axis=-1))
                        nfeats = np.array(nfeats)
                        roc = 0.5 * (accuracies[1:] + accuracies[:-1]) * (nfeats[1:] - nfeats[:-1])
                        roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
                        print('ROC : ', roc)
                        output['percentiles'] = []
                        for per in [0.1, 0.25, 0.5, 1.0]:
                            n_features = int(max_features * per)
                            r_accuracy = []
                            data_min = data[:, rank[:n_features]]
                            for rep in range(reps):
                                model = SVC(C=float(pow(2.0, power)), max_iter=1000)
                                model.fit(data_min, label)

                                yPred = model.predict(valid_data[:, rank[:n_features]])
                                r_accuracy.append((yPred == valid_label).sum() / len(yPred) * 100)

                                del model
                            accuracy = np.mean(r_accuracy)
                            print("Percentile : ", per)
                            print("Accuracy : ", accuracy)
                            print("Error : ", 100 - accuracy)
                            output['percentiles'].append(
                                {
                                    'percentile': per,
                                    'n_features': n_features,
                                    'accuracy': r_accuracy
                                }
                            )

                        try:
                            with open(result_filename) as outfile:
                                results = json.load(outfile)
                        except:
                            results = {}

                        if method not in results:
                            results[method] = []

                        results[method].append(output)

                        with open(result_filename, 'w') as outfile:
                            json.dump(results, outfile)
