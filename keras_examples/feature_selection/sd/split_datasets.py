from keras_examples.datasets.sd import load_data
import numpy as np
import os
from sklearn.model_selection import KFold
import json


reps = 2
n_splits = 10
kfold = KFold(n_splits=n_splits, shuffle=True)

dataset_names = ['sd1', 'sd2', 'sd3']


def main():
    # load data
    sd_directory = './keras_examples/feature_selection/sd/info/datasets/json/'
    try:
        os.makedirs(sd_directory)
    except:
        pass

    for dataset_name in dataset_names:
        print('dataset =', dataset_name)
        dataset = load_data('./datasets/sd/' + dataset_name)
        data = dataset['train']['data']  # data
        labels = dataset['train']['label']  # label

        fs_directory = sd_directory + dataset_name + '_' + str(n_splits) + '/'
        try:
            os.makedirs(fs_directory)
        except:
            pass

        for rep in range(reps):
            fold = 0
            for train_index, test_index in kfold.split(data):
                train_data, test_data = data[train_index].copy(), data[test_index].copy()
                # me = np.mean(train_data, axis=0)
                # st = np.std(train_data, axis=0)
                # train_data = (train_data - me) / st
                # test_data = (test_data - me) / st
                train_labels, test_labels = labels[train_index].copy(), labels[test_index].copy()

                new_dataset = {
                    'train': {
                        'data': train_data.tolist(),
                        'label': train_labels.tolist()
                    },
                    'test': {
                        'data': test_data.tolist(),
                        'label': test_labels.tolist()
                    }
                }

                output_filename = fs_directory + dataset_name + '_' + str(rep) + '_' + str(fold) + '.json'
                with open(output_filename, 'w') as outfile:
                    json.dump(new_dataset, outfile)

                fold += 1


if __name__ == '__main__':
    main()