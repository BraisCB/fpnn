from keras_examples.feature_selection.skfeature_parser import get_feature_indexes
import json
import numpy as np
import os


b_size = 100
epochs = 1000
reps = 5

directory = './keras_examples/feature_selection/sd/info/'

dataset_names = ['sd1', 'sd2', 'sd3']
methods = ['MIM', 'MRMR']
k_fold = 10


def main():
    sd_directory = directory + 'ranks/'
    try:
        os.makedirs(sd_directory)
    except:
        pass

    svm_directory = sd_directory + 'svm/'

    dataset_directory = directory + 'datasets/json/'

    for dataset_name in dataset_names:
        print('dataset =', dataset_name)

        fs_directory = dataset_directory + dataset_name + '_' + str(k_fold) + '/'

        filenames = [f for f in os.listdir(fs_directory) if os.path.isfile(os.path.join(fs_directory, f))]

        for filename in filenames:
            print(filename)
            complete_filename = fs_directory + filename
            with open(complete_filename) as outfile:
                dataset = json.load(outfile)

            train_data = np.asarray(dataset['train']['data'])
            train_labels = np.asarray(dataset['train']['label'])

            for fs_mode in methods: #, 'lasso']:
                print('METHOD : ', fs_mode)
                output = get_feature_indexes(train_data, train_labels, fs_mode)
                rank_directory = svm_directory + fs_mode + '/' + dataset_name + '_' + str(k_fold) + '/'
                try:
                    os.makedirs(rank_directory)
                except:
                    pass
                output_filename = rank_directory + filename[:-5] + '_rank.json'

                try:
                    with open(output_filename) as outfile:
                        info_data = json.load(outfile)
                except:
                    info_data = {}

                if fs_mode not in info_data:
                    info_data[fs_mode] = []

                info_data[fs_mode].append(output)

                with open(output_filename, 'w') as outfile:
                    json.dump(info_data, outfile)

                del output


if __name__ == '__main__':
    main()