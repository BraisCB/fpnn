from keras_examples.datasets.arff_parser import to_arff
import json
import os

gamma = 0.9
b_size = 100
epochs = 1000
reps = 2

directory = './keras_examples/feature_selection/sd/info/datasets/'

dataset_names = ['sd1', 'sd2', 'sd3']
subsets = ['train']

def main():

    weka_directory = directory + 'weka/'
    try:
        os.makedirs(weka_directory)
    except:
        pass

    dataset_directory = directory + 'json/'

    for dataset_name in dataset_names:
        print('dataset =', dataset_name)

        fs_directory = dataset_directory + dataset_name + '/'
        arff_directory = weka_directory + dataset_name + '/'
        try:
            os.makedirs(arff_directory)
        except:
            pass

        filenames = [f for f in os.listdir(fs_directory) if os.path.isfile(os.path.join(fs_directory, f))]

        for filename in filenames:
            print(filename)
            complete_filename = fs_directory + filename
            with open(complete_filename) as outfile:
                dataset = json.load(outfile)

            to_arff(dataset, arff_directory, filename[:-5], subsets)


if __name__ == '__main__':
    main()