from keras_examples.feature_selection.custom.svm import SVC
from keras_examples.feature_selection.custom.feature_selection import get_rank
from keras.utils import to_categorical
from keras import callbacks
import json
import numpy as np
import os


gamma = 0.9
b_size = 100
epochs = 1000
reps = 5
kfolds = 10

directory = './keras_examples/feature_selection/sd/info/'

dataset_names = ['sd1', 'sd2', 'sd3']

def scheduler(epoch):
    if epoch < 500:
        return .001
    elif epoch < 900:
        return .0002
    elif epoch < 1900:
        return .00004
    else:
        return .000008


rank_kwargs = {
    'reps': reps,
    'gamma': gamma
}

fit_kwargs = {
    'epochs': epochs,
    'callbacks': [
        callbacks.LearningRateScheduler(scheduler)
    ],
    'verbose': 0
}

def main():

    sd_directory = directory + 'ranks/'
    try:
        os.makedirs(sd_directory)
    except:
        pass

    svm_directory = sd_directory + 'svm/'

    dataset_directory = directory + 'datasets/json/'

    for dataset_name in dataset_names:
        print('dataset =', dataset_name)

        fs_directory = dataset_directory + dataset_name + '_' + str(kfolds) + '/'

        filenames = [f for f in os.listdir(fs_directory) if os.path.isfile(os.path.join(fs_directory, f))]

        for filename in filenames:
            print(filename)
            complete_filename = fs_directory + filename
            with open(complete_filename) as outfile:
                dataset = json.load(outfile)

            train_data = np.asarray(dataset['train']['data'])
            train_labels = dataset['train']['label']
            num_classes = len(np.unique(train_labels + dataset['test']['label']))

            train_labels = to_categorical(train_labels, num_classes=num_classes)
            batch_size = min(len(train_data), b_size)
            fit_kwargs['batch_size'] = batch_size

            for fs_mode in ['simonyan']:
                for lasso in [0.0, 1e-4]:
                    if lasso == 0.0 and fs_mode == 'lasso' or lasso > 0.0 and fs_mode == 'simonyan':
                        continue
                    print('reps : ', reps)
                    print('method : ', fs_mode)
                    for power in range(0, 5):
                        regularization = pow(2.0, power)
                        name = dataset_name + '_' + fs_mode + '_l_' + str(lasso) + '_g_' + str(gamma) + \
                               '_r_' + str(regularization)
                        print(name)
                        model_kwargs = {
                            'lasso': lasso,
                            'C': regularization,
                        }
                        rank = get_rank(fs_mode, data=train_data, label=train_labels, model_func=SVC,
                                        model_kwargs=model_kwargs, fit_kwargs=fit_kwargs, rank_kwargs=rank_kwargs)
                        rank_directory = svm_directory + fs_mode + '/' + dataset_name + '_' + str(kfolds) + '/'
                        try:
                            os.makedirs(rank_directory)
                        except:
                            pass
                        output_filename = rank_directory + filename[:-5] + '_rank.json'

                        try:
                            with open(output_filename) as outfile:
                                info_data = json.load(outfile)
                        except:
                            info_data = {}

                        if fs_mode not in info_data:
                            info_data[fs_mode] = []

                        info_data[fs_mode].append(
                            {
                                'lasso': lasso,
                                'gamma': gamma,
                                'regularization': regularization,
                                'rank': rank.tolist(),
                                'reps': reps
                            }
                        )

                        with open(output_filename, 'w') as outfile:
                            json.dump(info_data, outfile)

                        del rank


if __name__ == '__main__':
    main()