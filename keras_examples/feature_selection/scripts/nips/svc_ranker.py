from keras_examples.feature_selection.nips.datasets import load_dataset
from keras_examples.feature_selection.custom.feature_selection import get_rank
from keras_examples.feature_selection.custom.sklearn_parser import SVC
import keras.utils.np_utils as kutils
import numpy as np
import json
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

dataset_names = [
    ('arcene', 0.1),
    ('madelon', 1e-3),
    ('dexter', 0.1),
    ('dorothea', 0.1),
    ('gisette', 1e-1),
]

reps = 1
gamma = 0.975
lasso = 0.0
classifier_gamma = .975

root_directory = './keras_examples/feature_selection/scripts/nips/info/normalized/'
datasets_directory = './keras_examples/feature_selection/nips/datasets/'


def remove_redundancy(X, thresh=0.99):
    # remove zeros
    stdcX = X.std(axis=0, keepdims=True)
    _, zeros = np.where(stdcX == 0.)
    _, valid = np.where(stdcX > 0.)
    cX = X[:, valid] - X[:, valid].mean(axis=0)
    corr = 1. / X.shape[0] *  (cX.T @ cX) / np.maximum((stdcX[:, valid].T @ stdcX[:, valid]), 1e-6)
    corr = np.triu(corr, 1)
    _, rows = np.where(corr > thresh)
    rows = np.unique(rows)
    correlated = np.concatenate((zeros, valid[rows]), axis=0).tolist()
    valid = list(set(valid.tolist()).difference(rows.tolist()))
    return valid, correlated


if __name__ == '__main__':
    os.chdir('../../../../')

    for dataset_name, regularization in dataset_names:

        fs_filename = root_directory + dataset_name + '_g_' + str(classifier_gamma) + '_results.json'

        print('loading dataset', dataset_name)
        dataset = load_dataset(dataset_name, directory=datasets_directory, normalize=dataset_name not in ['dexter', 'dorothea'])
        print('data loaded. labels =', dataset['train']['data'].shape)
        input_shape = dataset['train']['data'].shape[-1:]

        uclasses = np.unique(dataset['train']['label'])
        nclasses = len(uclasses)

        data = dataset['train']['data']
        label = dataset['train']['label']
        label = kutils.to_categorical(label, nclasses)
        print('data size after balance : ', data.shape)
        valid_data = dataset['validation']['data']
        valid_label = kutils.to_categorical(dataset['validation']['label'], nclasses)

        # valid_features, _ = remove_redundancy(data)
        # data = data[:, valid_features]
        # valid_data = valid_data[:, valid_features]

        for kernel in ['rbf', 'linear', 'poly', 'sigmoid']:
            print('gamma : ', gamma)
            name = dataset_name + '_' + kernel
            print(name)
            model_kwargs = {
                'C': 10.,
                'degree': 3.,
                'coef0': 1. * (kernel == 'poly'),
                'kernel': kernel,
                'class_weight': 'balanced',
                'cache_size': 4096,
                'max_iter': 10000,
                'use_pca': False
            }
            fit_kwargs = {
            }
            evaluate_kwargs = {
            }
            rank_kwargs = {
                'gamma': gamma,
                'reps': reps
            }
            saliency_kwargs = {
                'batch_size': 16,
            }
            rank = get_rank('simonyan', data=data, label=label, model_func=SVC,
                              rank_kwargs=rank_kwargs, fit_kwargs=fit_kwargs, model_kwargs=model_kwargs,
                              return_info=False, valid_data=valid_data, valid_label=valid_label)

            try:
                with open(fs_filename) as outfile:
                    info_data = json.load(outfile)
            except:
                info_data = {}

            if name not in info_data:
                info_data[name] = []

            n_features = input_shape[0]
            while n_features > 0:

                n_features = int(n_features * classifier_gamma)

            info_data[name].append(
                {
                    'lasso': lasso,
                    'gamma': gamma,
                    'regularization': regularization,
                    'rank': result['rank'],
                    'classification': result['classification'],
                    'reps': reps,
                    'model_kwargs': model_kwargs
                }
            )

            if not os.path.isdir(root_directory):
                os.makedirs(root_directory)

            with open(fs_filename, 'w') as outfile:
                json.dump(info_data, outfile)

            del result


