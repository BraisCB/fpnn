from keras_examples.feature_selection.custom.feature_selection import get_rank
from keras_examples.feature_selection.dataset_corazon.dataset_corazon import load_dataset
from keras_examples.feature_selection.custom.sklearn_parser import SVC
import json
import os
from keras_examples.shared.utils import balance_data
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from sklearn.model_selection import KFold
from keras.utils.np_utils import to_categorical
import numpy as np


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9
set_session(tf.Session(config=config))


gamma = 0.99
b_size = 128
epochs = 60
reps = 1

sd_directory = './keras_examples/feature_selection/scripts/dataset_corazon/info/'
dataset_file = './keras_examples/feature_selection/dataset_corazon/datasetCab.csv'
fs_mode = 'simonyan'


rank_kwargs = {
    'reps': reps,
    'gamma': gamma,
    'epsilon': 0
}

kfold = KFold(n_splits=3, shuffle=True, random_state=42)
fs_filename = sd_directory + 'svc_gamma_' + str(gamma) + '_ranks.json'


def main():

    dataset = load_dataset(dataset_file, normalize=False)
    feature_names = np.array(dataset['features'])

    data = dataset['train']['data']
    label = dataset['train']['label']

    # index = np.where(feature_names == 'SEX')[0][0]
    # label = data[:, index]
    # data = np.concatenate((data[:, :index], data[:, index + 1:]), axis=-1)

    label = to_categorical(label, 2)



    for train_index, test_index in kfold.split(data):

        train_data, test_data = data[train_index], data[test_index]
        train_label, test_label = label[train_index], label[test_index]
        
        # normalize
        train_mean = train_data.mean(axis=0)
        train_std = np.maximum(1e-6, train_data.std(axis=0))
        train_data = (train_data - train_mean) / train_std
        test_data = (test_data - train_mean) / train_std
        test_data, test_label = balance_data(test_data, test_label)

        print('train_label : ', np.sum(train_label, axis=0))
        print('test_label : ', np.sum(test_label, axis=0))

        fit_kwargs = {
        }

        for kernel in ['linear', 'rbf', 'poly', 'sigmoid']:
            print('reps : ', reps)
            print('kernel : ', kernel)
            for regularization in [5e-4]:
                name = kernel
                print(name)
                model_kwargs = {
                    'C': 10.0,
                    'kernel': kernel,
                    'degree': 3,
                    'coef0': 1. * (kernel != 'sigmoid'),
                    'class_weight': 'balanced'
                }
                result = get_rank(fs_mode, data=train_data, label=train_label, model_func=SVC,
                                  rank_kwargs=rank_kwargs, fit_kwargs=fit_kwargs, model_kwargs=model_kwargs,
                                  return_info=True, valid_data=test_data, valid_label=test_label)

                try:
                    with open(fs_filename) as outfile:
                        info_data = json.load(outfile)
                except:
                    info_data = {}

                if name not in info_data:
                    info_data[name] = []

                info_data[name].append(
                    {
                        'lasso': 0.0,
                        'gamma': gamma,
                        'regularization': regularization,
                        'rank': result['rank'],
                        'classification': result['classification'],
                        'reps': reps,
                        'rank_features': feature_names[result['rank']].tolist()
                    }
                )

                for f, a in zip(feature_names[result['rank']], np.mean(result['classification']['accuracy'], axis=-1)[::-1]):
                    print(f, ' : ', a)

                if not os.path.isdir(sd_directory):
                    os.makedirs(sd_directory)

                with open(fs_filename, 'w') as outfile:
                    json.dump(info_data, outfile)

                del result


if __name__ == '__main__':
    os.chdir('../../../../')
    main()