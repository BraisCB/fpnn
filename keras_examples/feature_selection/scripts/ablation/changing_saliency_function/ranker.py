from keras_examples.feature_selection.nips.datasets import load_dataset
from keras_examples.feature_selection.custom.feature_selection import get_rank
from keras_examples.shared.utils import balance_data
from keras_examples.shared.cross_validation import get_cross_validation_params
from keras_examples.feature_selection.custom.sklearn_parser import SVC
import keras.utils.np_utils as kutils
import numpy as np
import json
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

dataset_names = [
    #( 'arcene', 1e-1),
    # ('dexter', 1e-1),
    # ('madelon', 1e-3),
    ('dorothea', 1e-1),
    # ('gisette', 1e-3),
]

gamma = 0.975
b_size = 100
epochs = 200
reps = 1
lasso = 0.0

np.random.seed(1)

root_directory = './keras_examples/feature_selection/scripts/ablation/changing_saliency_function/info/'
datasets_directory = './keras_examples/feature_selection/nips/datasets/'


if __name__ == '__main__':
    os.chdir('../../../../../')
    for dataset_name, regularization in dataset_names:

        fs_filename = root_directory + dataset_name + '_gamma_' + str(gamma) + '_ranks.json'

        print('loading dataset', dataset_name)
        dataset = load_dataset(
            dataset_name, directory=datasets_directory,  normalize=dataset_name not in ['dexter', 'dorothea'],
            sparse=True
        )
        print('data loaded. labels =', dataset['train']['data'].shape)
        input_shape = dataset['train']['data'].shape[-1:]
        batch_size = min(dataset['train']['data'].shape[0], b_size)

        uclasses = np.unique(dataset['train']['label'])
        nclasses = len(uclasses)

        data = dataset['train']['data']
        label = dataset['train']['label']
        label = kutils.to_categorical(label, nclasses)
        valid_data = dataset['validation']['data']
        valid_label = kutils.to_categorical(dataset['validation']['label'], nclasses)

        for fs_mode in ['simonyan']:
            for kernel in ['linear', 'poly', 'sigmoid', 'rbf']:
                for reduce_func in ['sum', 'max']:
                    for class_func in ['sum', 'max']:
                        print('kernel : ', kernel)
                        name = fs_mode + '_k_' + kernel + '_r_' + reduce_func + '_c_' + class_func
                        print(name)
                        model_kwargs = {
                            'C': 10.,
                            'degree': 3.,
                            'coef0': 1. * (kernel == 'poly'),
                            'kernel': kernel,
                            'class_weight': 'balanced',
                            'cache_size': 4096,
                            'saliency_reduce_func': reduce_func,
                            'max_iter': 5000
                        }
                        fit_kwargs = {
                        }
                        evaluate_kwargs = {
                            'verbose': 0,
                            'batch_size': batch_size
                        }
                        rank_kwargs = {
                            'gamma': gamma,
                            'reps': reps
                        }
                        saliency_kwargs = {
                            'batch_size': 16,
                            'class_func': class_func,
                            'reduce_func': reduce_func
                        }
                        result = get_rank(fs_mode, data=data, label=label, model_func=SVC,
                                        rank_kwargs=rank_kwargs, fit_kwargs=fit_kwargs, model_kwargs=model_kwargs, saliency_kwargs=saliency_kwargs,
                                        return_info=True, valid_data=valid_data, valid_label=valid_label)

                        try:
                            with open(fs_filename) as outfile:
                                info_data = json.load(outfile)
                        except:
                            info_data = {}

                        if name not in info_data:
                            info_data[name] = []

                        info_data[name].append(
                            {
                                'lasso': lasso,
                                'gamma': gamma,
                                'regularization': regularization,
                                'rank': result['rank'],
                                'classification': result['classification'],
                                'reps': reps,
                                'model_kwargs': model_kwargs
                            }
                        )

                        if not os.path.isdir(root_directory):
                            os.makedirs(root_directory)

                        with open(fs_filename, 'w') as outfile:
                            json.dump(info_data, outfile)

                        del result, info_data
        del data, label, valid_data, valid_label, dataset
