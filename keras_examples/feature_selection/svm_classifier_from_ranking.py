from keras_examples.datasets.nips_challenge import load_data
from keras_examples.custom.utils import balance_data
from sklearn.svm import SVC
import numpy as np
import json
from copy import deepcopy


dataset_names = [
    ('arcene', 7000),
    ('dexter', 9947),
    ('gisette', 2500),
    ('madelon', 20),
    ('dorothea', 50000)
]

reps = 10
limit = 1.0


def mu(n_features, max_features):
    factor = n_features / max_features
    if factor < 0.1:
        return 0.9
    elif factor < 0.25:
        return 0.75
    else:
        return 0.6


for dataset_stats in dataset_names:
    dataset_name, max_features = dataset_stats
    print('loading dataset', dataset_name)
    dataset = load_data('./datasets/' + dataset_name + '/' + dataset_name, normalize=True)
    print('data loaded. labels =', dataset['train']['data'].shape)
    input_shape = dataset['train']['data'].shape[-1:]

    nclasses = len(np.unique(dataset['train']['label']))

    data = dataset['train']['data']
    label = dataset['train']['label']

    data, label = balance_data(data, label)

    valid_data = dataset['validation']['data']
    valid_label = dataset['validation']['label']

    for weka in [False]:
        fs_filename = './keras_examples/info/feature_selection/'
        filename = fs_filename + dataset_name + '_weka.json' if weka else \
            fs_filename + 'svm_ranks_reps/' + dataset_name + '_svm_ranks.json'
        output_filename = fs_filename + 'svm_ranks_reps/' + dataset_name + '_weka_results.json' if weka\
            else fs_filename + 'svm_ranks_reps/' + dataset_name + '_svm_results.json'
        try:
            with open(filename) as outfile:
                ranks = json.load(outfile)
        except:
            continue

        for method in ranks:
            print('METHOD : ', method)
            rank_list = ranks[method]
            for example in rank_list:
                print(example)
                output = deepcopy(example)
                nfeats = []
                accuracies = []
                rank = np.array(example['rank']).astype(int) - int(weka)
                n_features = 1
                # max_features = int(limit * data.shape[-1])
                while n_features <= max_features:
                    print('n_features : ', n_features)
                    accuracy = 0.0
                    data_min = data[:, rank[:n_features]]
                    for rep in range(reps):
                        model = SVC()
                        model.fit(data_min, label)

                        yPred = model.predict(valid_data[:, rank[:n_features]])
                        accuracy += (yPred == valid_label).sum() / len(yPred) * 100

                        del model
                    accuracy /= reps
                    print("Accuracy : ", accuracy)
                    print("Error : ", 100 - accuracy)
                    nfeats.append(n_features)
                    accuracies.append(accuracy)
                    if n_features == max_features:
                        break
                    n_features = min(int(n_features / mu(n_features, max_features)) + 1, max_features)
                output['classification'] = {
                    'n_features': nfeats,
                    'accuracy': accuracies
                }

                try:
                    with open(output_filename) as outfile:
                        results = json.load(outfile)
                except:
                    results = {}

                if method not in results:
                    results[method] = []

                results[method].append(output)

                with open(output_filename, 'w') as outfile:
                    json.dump(results, outfile)
