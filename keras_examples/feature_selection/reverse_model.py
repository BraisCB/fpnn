from keras.layers import Input, Dense, Multiply, Activation
from keras.models import Model
from keras.layers import Layer
from keras import backend as K
import numpy as np


class ReverseModel:

    def __init__(self, model, normalize=True):
        self.model = model
        self.input = Input(shape=model.output_shape[1:])
        self.output = self.input
        self.normalize = normalize
        for layer in model.layers[::-1]:
            if 'dense' in layer.name:
                self._add_reverse_dense(layer)
            # elif 'batch_normalization' in layer.name:
            #     self._add_reverse_batch_normalization(layer)
            else:
                continue
            # layer_input = L2_Normalize(axis=0)(layer.input) if self.normalize else layer.input
            # self.output = Multiply()([self.output, layer_input])
            # self.output = Activation('relu')(self.output)
        lp = K.learning_phase()
        self.reverse_model = Model([self.model.input, self.input], self.output)
        reduce_sum = K.sum(K.abs(self.output), axis=0)
        self.saliency_function = K.function([self.model.input, self.input, lp], [reduce_sum])

    def _add_reverse_dense(self, layer):
        weights = layer.get_weights()[0].T
        if self.normalize:
            weights /= np.linalg.norm(weights, axis=-1, keepdims=True)
        reverse_layer = Dense(weights.shape[-1], input_dim=weights.shape[0], use_bias=False)
        self.output = reverse_layer(self.output)
        reverse_layer.set_weights([weights])
        reverse_layer.trainable = False

    def _add_reverse_batch_normalization(self, layer):
        self.output = ReverseBatchNormalization(layer=layer)(self.output)


class ReverseBatchNormalization(Layer):

    def __init__(self, layer,
                 **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(ReverseBatchNormalization, self).__init__(**kwargs)
        gamma = layer.gamma
        var = layer.moving_variance
        dim = tuple((len(layer.output_shape) - 1) * [1] + list(layer.output_shape[-1:]))
        self.factor = K.reshape(gamma / var, dim)

    def call(self, inputs, **kwargs):
        output = self.factor * inputs
        return output

    def compute_output_shape(self, input_shape):
        return input_shape

    def get_config(self):
        base_config = super(ReverseBatchNormalization, self).get_config()
        return dict(list(base_config.items()))
