import json
from matplotlib import pyplot as pl
import matplotlib

l = 16
k = 4

with open('./keras_examples/info/json_files/cifar100_wrn_scores.json') as outfile:
    score_data = json.load(outfile)

softmax_scores = []
softmax_labels = ['CE (fixed classifier)', 'CE']
our_scores = []
our_labels = []
for score in sorted(score_data):
    if 'softmax' in score:
        softmax_scores.append(score_data[score][0])
    else:
        mode = 'EOL' if 'exponential' in score else 'TOL'
        our_scores.append(score_data[score][0])
        our_labels.append(mode)


ax = pl.figure()
legends = softmax_labels + our_labels

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 14}

matplotlib.rc('font', **font)
step = 10
last = -2
pl.plot(
    softmax_scores[0]['prob'][:last:step] + [softmax_scores[0]['prob'][last]],
    softmax_scores[0]['precision'][:last:step] + [softmax_scores[0]['precision'][last]],
    'r*-'
)
pl.plot(
    softmax_scores[1]['prob'][:last:step] + [softmax_scores[1]['prob'][last]],
    softmax_scores[1]['precision'][:last:step] + [softmax_scores[1]['precision'][last]],
    'ks-'
)
pl.plot(
    our_scores[0]['prob'][:last:step] + [our_scores[0]['prob'][last]],
    our_scores[0]['precision'][:last:step] + [our_scores[0]['precision'][last]],
    'b^-'
)
pl.plot(
    our_scores[1]['prob'][:last:step] + [our_scores[1]['prob'][last]],
    our_scores[1]['precision'][:last:step] + [our_scores[1]['precision'][last]],
    'go-'
)
pl.plot(
    softmax_scores[0]['prob'][:last:step] + [softmax_scores[0]['prob'][last]],
    softmax_scores[0]['recall'][:last:step] + [softmax_scores[0]['recall'][last]],
    'r*--'
)
pl.plot(
    softmax_scores[1]['prob'][:last:step] + [softmax_scores[1]['prob'][last]],
    softmax_scores[1]['recall'][:last:step] + [softmax_scores[1]['recall'][last]],
    'ks--'
)
pl.plot(
    our_scores[0]['prob'][:last:step] + [our_scores[0]['prob'][last]],
    our_scores[0]['recall'][:last:step] + [our_scores[0]['recall'][last]],
    'b^--'
)
pl.plot(
    our_scores[1]['prob'][:last:step] + [our_scores[1]['prob'][last]],
    our_scores[1]['recall'][:last:step] + [our_scores[1]['recall'][last]],
    'go--'
)

pl.axis((0,0.99,0.4,1.01))

pl.xlabel('probability')
pl.ylabel('score')
pl.title('CIFAR-100')
pl.legend(legends, loc='best')

pl.savefig('./keras_examples/info/cifar100_score.eps', bbox_inches='tight')

