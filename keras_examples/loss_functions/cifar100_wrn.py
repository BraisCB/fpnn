import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

import os
import keras_examples.wrn.wide_residual_network as wrn
from keras.datasets import cifar100
import keras.callbacks as callbacks
import keras.utils.np_utils as kutils
import keras_examples.shared.losses as losses
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.layers import Activation, Dense, AveragePooling2D, Flatten, Input
from keras import backend as K
from keras import optimizers
from keras.regularizers import l2
import json


batch_size = 128
nb_epoch = 200
img_rows, img_cols = 32, 32
nb_classes = 100
l = 16
k = 4

output_losses = [
    #{'type': 'softplus_orthogonal_loss', 'data': {'R': 1.0, 'lamda': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'softmax'},
    # {'type': 'softplus_orthogonal_loss', 'data': {'R': 1.0, 'lamda': 1.0}},
    # {'type': 'softplus_loss', 'data': {'R': 1.0, 'lamda': 1.0}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 0.5, 'lamda': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 3.0, 'lamda': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 2.0, 'lamda': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 4.0, 'lamda': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 1.5, 'lamda': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 2.5, 'lamda': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 2.0, 'lamda': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 1.0, 'lamda': 1.0}},
    # {'type': 'categorical_crossentropy'},
    {'type': 'confusion_loss', 'data': {'norm': 'frob_2'}},
    {'type': 'confusion_loss', 'data': {'norm': '1_2'}},
    {'type': 'confusion_loss', 'data': {'norm': 'Inf_2'}},
    {'type': 'confusion_loss', 'data': {'norm': 'frob'}},
    {'type': 'confusion_loss', 'data': {'norm': '1'}},
    {'type': 'confusion_loss', 'data': {'norm': 'Inf'}},
]

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9
set_session(tf.Session(config=config))

reps = 1

trainable = True

(trainX, trainY), (testX, testY) = cifar100.load_data(label_mode='fine')

trainX = trainX.astype('float32')
trainX_mean = trainX.mean(axis=0)
trainX_std = trainX.std(axis=0)
trainX = (trainX - trainX_mean) / trainX_std
testX = testX.astype('float32')
testX = (testX - trainX_mean) / trainX_std

trainY = kutils.to_categorical(trainY)
testY = kutils.to_categorical(testY)

directory = './keras_examples/loss_functions/info/'

generator = ImageDataGenerator(width_shift_range=5./32,
                               height_shift_range=5./32,
                               fill_mode='reflect',
                               horizontal_flip=True)

init_shape = (3, 32, 32) if K.image_dim_ordering() == 'th' else (32, 32, 3)

# For WRN-16-8 put N = 2, k = 8
# For WRN-28-10 put N = 4, k = 10
# For WRN-40-4 put N = 6, k = 4

for i in range(reps):
    for drop in [0.3]:
        for output_loss in output_losses:
            if 'orthogonal' in output_loss['type'] and trainable:
                continue
            print('loss =', output_loss, ', trainable = ', trainable)
            if 'data' in output_loss:
                if 'lamda' in output_loss['data']:
                    print('lamda =', output_loss['data']['lamda'])
                if 'R' in output_loss['data']:
                    print('R =', output_loss['data']['R'])
                if 'norm' in output_loss['data']:
                    print('norm =', output_loss['data']['norm'])
            kernel_initializer = 'he_normal'
            regularization = 0.0005 # if output_loss['type'] == 'softmax' else 0.0
            input = Input(shape=init_shape)
            deep_features = wrn.create_cifar_wide_residual_network(
                input, l=l, k=k, dropout=drop,
                kernel_initializer=kernel_initializer, regularization=regularization
            )
            deep_features = AveragePooling2D((8, 8))(deep_features)
            deep_features = Flatten()(deep_features)

            kernel_initializer = 'orthogonal' if not trainable else 'he_normal'
            classifier = Dense(
                nb_classes, use_bias=trainable, kernel_initializer=kernel_initializer,
                kernel_regularizer=l2(regularization) if regularization > 0.0 and trainable else None
            )
            classifier.trainable = trainable

            output = classifier(deep_features)

            if 'orthogonal' not in output_loss['type']:
                output = Activation('softmax')(output)

            model = Model(input, output)

            model.summary()

            def scheduler(epoch):
                if epoch < 60:
                    return .1
                elif epoch < 120:
                    return .02
                elif epoch < 160:
                    return .004
                else:
                    return .0008

            change_lr = callbacks.LearningRateScheduler(scheduler)

            name = output_loss['type']
            if 'data' in output_loss:
                if 'lamda' in output_loss['data']:
                    name += '_lamda_' + str(output_loss['data']['lamda'])
                if 'R' in output_loss['data']:
                    name += '_R_' + str(output_loss['data']['R'])
                if 'norm' in output_loss['data']:
                    name += '_norm_' + str(output_loss['data']['norm'])

            optimizer = optimizers.sgd(lr=0.1, momentum=0.9, nesterov=True) # adam(.01)  #

            loss_data = {} if 'data' not in output_loss else output_loss['data']
            loss = "categorical_crossentropy" if output_loss['type'] == 'categorical_crossentropy' else \
                getattr(losses, output_loss['type'])(**loss_data)

            metric = 'acc' # if output_loss['type'] == 'softmax' else ortho.softmax_accuracy

            model.compile(loss=loss, optimizer=optimizer, metrics=["acc"])
            print("Finished compiling")

            history = model.fit_generator(
                generator.flow(trainX, trainY, batch_size=batch_size),
                steps_per_epoch=len(trainX) // batch_size, epochs=nb_epoch,
                callbacks=[
                    # callbacks.ModelCheckpoint(
                    #     './keras_examples/weights/' + name + '_Weights.h5',
                    #     monitor="val_acc",
                    #     save_best_only=True,
                    #     verbose=1
                    # ),
                    callbacks.LearningRateScheduler(scheduler)
                ],
                validation_data=(testX, testY),
                validation_steps=testX.shape[0] // batch_size,
                verbose=2
            )

            accuracy = model.evaluate(testX, testY, batch_size=batch_size)[-1]
            error = 100 - accuracy
            print("Accuracy : ", accuracy)
            print("Error : ", error)

            if not os.path.isdir(directory):
                os.makedirs(directory)

            acc_name = directory + 'cifar100_wrn_' + str(l) + '_' + str(k) + '_acc.json'

            try:
                with open(acc_name) as outfile:
                    info_data = json.load(outfile)
            except:
                info_data = {}

            if name not in info_data:
                info_data[name] = []

            info_data[name].append(accuracy)

            with open(acc_name, 'w') as outfile:
                json.dump(info_data, outfile)

            del model
            K.clear_session()
            #
            # score_name = './keras_examples/weights/cifar100_wrn_' + str(l) + '_' + str(k) + '_score_info.json'
            #
            # try:
            #     with open(score_name) as outfile:
            #         score_data = json.load(outfile)
            # except:
            #     score_data = {}
            #
            # rango = np.array([0]) if 'orthogonal' not in output_loss['type'] \
            #     else np.arange(0.1, 2.0 * output_loss['data']['R'], output_loss['data']['R'] / 10)
            # for thresh in rango:
            #
            #     x_probs = np.arange(0, 1.01, 0.01)
            #     prec_probs = np.zeros_like(x_probs)
            #     rec_probs = np.zeros_like(x_probs)
            #
            #     if 'orthogonal' in output_loss['type']:
            #         yProbs = np.maximum(0.0, yPreds)
            #         yProbs /= np.maximum(thresh, np.sum(yProbs, axis=1, keepdims=True))
            #     else:
            #         yProbs = yPreds
            #     yMax = np.max(yProbs, axis=1)
            #
            #     for i, p in enumerate(x_probs):
            #         index = np.where(yMax >= p)[0]
            #         if len(index) > 0:
            #             rec_probs[i] = len(index) / len(yMax)
            #             prec_probs[i] = (yTrue_label[index] == yPred_label[index]).sum() / len(index)
            #
            #     pname = name + '_' + str(thresh)
            #     if pname not in score_data:
            #         score_data[pname] = {}
            #
            #     score_data[pname].append({
            #         'prob': x_probs.tolist(),
            #         'precision': prec_probs.tolist(),
            #         'recall': rec_probs.tolist()
            #     })
            #
            # with open(score_name, 'w') as outfile:
            #     json.dump(score_data, outfile)
