import numpy as np
import sklearn.metrics as metrics
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

# backend = 'theano'
import os
# os.environ['KERAS_BACKEND'] = backend
# os.environ['THEANO_FLAGS'] = 'device=cuda0,floatX=float32'
# os.environ['MKL_THREADING_LAYER'] = 'GNU'
import keras_examples.vgg.vgg_network as vgg
from keras.datasets import mnist
import keras.callbacks as callbacks
import keras.utils.np_utils as kutils
import keras_examples.shared.losses as ortho
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import plot_model
from keras.models import Model
from keras.layers import Activation, Dense, AveragePooling2D, Flatten, Convolution2D, Input, GlobalAveragePooling2D
from keras import backend as K
from keras import optimizers
from scipy.stats import ortho_group
import pickle
import json


nb_epoch = 800
img_rows, img_cols = 28, 28
nb_classes = 10
images_per_label = 500

output_losses = [
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 0.1, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 0.1, 'lamda': 10.0}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 1.0, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 1.0, 'lamda': 10.0}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 4.0, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 4.0, 'lamda': 10.0}},
    {'type': 'softmax'},
    # {'type': 'exponential_orthogonal', 'data': {'R': 0.5, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal', 'data': {'R': 0.5, 'lamda': 1.0}},
    # {'type': 'exponential_orthogonal', 'data': {'R': 1.0, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal', 'data': {'R': 1.0, 'lamda': 1.0}},
    # {'type': 'exponential_orthogonal', 'data': {'R': 2.0, 'lamda': 0.1}},
    {'type': 'exponential_orthogonal', 'data': {'R': 2.0, 'lamda': 1.0}},
    # {'type': 'exponential_orthogonal', 'data': {'R': 4.0, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal', 'data': {'R': 4.0, 'lamda': 1.0}},
    # {'type': 'trench_orthogonal', 'data': {'R': 0.1, 'lamda': 0.1}},
    # {'type': 'trench_orthogonal', 'data': {'R': 2.0, 'lamda': 0.1}},
    # {'type': 'trench_orthogonal', 'data': {'R': 4.0, 'lamda': 0.1}},
    # {'type': 'trench_orthogonal', 'data': {'R': 0.1, 'lamda': 1.0}},
    # {'type': 'trench_orthogonal', 'data': {'R': 2.0, 'lamda': 1.0}},
    {'type': 'trench_orthogonal', 'data': {'R': 4.0, 'lamda': 1.0}},
    # {'type': 'trench_orthogonal', 'data': {'R': 0.1, 'lamda': 10.0}},
    # {'type': 'trench_orthogonal', 'data': {'R': 2.0, 'lamda': 10.0}},
    # {'type': 'trench_orthogonal', 'data': {'R': 4.0, 'lamda': 10.0}},
]

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
set_session(tf.Session(config=config))

reps = 10

trainable = True
np.random.seed(42)

if __name__ == '__main__':
    os.chdir('../../')
    for i in range(reps):
        (trainX, trainY), (testX, testY) = mnist.load_data()
        labels = np.unique(trainY)
        new_trainX = []
        new_trainY = []
        new_testX = []
        new_testY = []
        for label in labels:
            pos = np.where(trainY == label)[0]
            np.random.shuffle(pos)
            new_trainX.append(trainX[pos[:images_per_label]])
            new_testX.append(trainX[pos[images_per_label:]])
            new_trainY.append(trainY[pos[:images_per_label]])
            new_testY.append(trainY[pos[images_per_label:]])
        new_testX.append(testX)
        new_testY.append(testY)
        trainX = np.concatenate(new_trainX)
        trainY = np.concatenate(new_trainY)
        testX = np.concatenate(new_testX)
        testY = np.concatenate(new_testY)

        print('train :', trainX.shape)
        print('test :', testX.shape)
        batch_size = 100

        trainX = np.expand_dims(trainX, -1)
        trainX = trainX.astype('float32')
        testX = np.expand_dims(testX, -1)
        testX = testX.astype('float32')
        trainX = trainX / 127.5 - 1.
        testX = testX / 127.5 - 1.

        trainY = kutils.to_categorical(trainY)
        testY = kutils.to_categorical(testY)

        generator = ImageDataGenerator(
            # horizontal_flip=True,
            width_shift_range=4./28.,
            height_shift_range=4./28.
        )

        init_shape = (1, 28, 28) if K.image_dim_ordering() == 'th' else (28, 28, 1)

        # For WRN-16-8 put N = 2, k = 8
        # For WRN-28-10 put N = 4, k = 10
        # For WRN-40-4 put N = 6, k = 4
        for drop in [0.0]:
            for output_loss in output_losses:
                for trainable in [False, True]:
                    if 'orthogonal' in output_loss['type'] and trainable:
                        continue
                    print('loss =', output_loss['type'], ', trainable = ', trainable)
                    if 'data' in output_loss:
                        if 'lamda' in output_loss['data']:
                            print('lamda =', output_loss['data']['lamda'])
                        if 'R' in output_loss['data']:
                            print('R =', output_loss['data']['R'])
                    kernel_initializer = 'he_normal'
                    regularization = 5e-4
                    input = Input(shape=init_shape)
                    deep_features = vgg.create_cifar10_vgg_network(
                        input,
                        kernel_initializer=kernel_initializer, regularization=regularization
                    )

                    kernel_initializer = 'orthogonal' if not trainable else 'he_normal'
                    classifier = Convolution2D(nb_classes, (1, 1), use_bias=trainable, kernel_initializer=kernel_initializer)
                    classifier.trainable = trainable

                    output = classifier(deep_features)
                    # output = GlobalAveragePooling2D()(output)
                    output = Flatten()(output)

                    if output_loss['type'] == 'softmax':
                        output = Activation('softmax')(output)

                    model = Model(input, output)

                    # model.summary()

                    def scheduler(epoch):
                        if epoch < 680:
                            return .01
                        elif epoch < 760:
                            return .001
                        else:
                            return .0001

                    change_lr = callbacks.LearningRateScheduler(scheduler)

                    name = 'mnist_vgg_' + output_loss['type'] + '_' + str(drop) + '_' + str(trainable)
                    if 'data' in output_loss:
                        if 'lamda' in output_loss['data']:
                            name += '_C_' + str(output_loss['data']['lamda'])
                        if 'R' in output_loss['data']:
                            name += '_R_' + str(output_loss['data']['R'])

                    # plot_model(model, name + '.png', show_shapes=False)

                    optimizer = optimizers.adam(lr=0.01)

                    loss_data = {'R': 1.0, 'lamda': 1.0} if 'data' not in output_loss else output_loss['data']
                    loss = "categorical_crossentropy" if output_loss['type'] == 'softmax' else \
                        getattr(ortho, output_loss['type'])(**loss_data)

                    metric = 'acc' # if output_loss['type'] == 'softmax' else ortho.softmax_accuracy

                    model.compile(loss=loss, optimizer=optimizer, metrics=["acc"])
                    print("Finished compiling")

                    #model.load_weights("weights/WRN-16-8 Weights.h5")
                    print("Model loaded.")

                    history = model.fit_generator(
                        generator.flow(trainX, trainY, batch_size=batch_size),
                        steps_per_epoch=len(trainX) // batch_size, epochs=nb_epoch,
                        callbacks=[
                            # callbacks.ModelCheckpoint(
                            #     './keras_examples/weights/' + name + '_Weights.h5',
                            #     monitor="val_acc",
                            #     save_best_only=True,
                            #     verbose=1
                            # ),
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        # validation_data=(testX, testY),
                        # validation_steps=testX.shape[0] // batch_size,
                        verbose=0
                    )

                    # with open('./keras_examples/vgg/info/mnist_' + name + '_history_' + str(i) + '.h5', 'wb') as file_pi:
                    #    pickle.dump(history.history, file_pi)
                    print('train : ', model.evaluate(trainX, trainY, verbose=0))

                    yPreds = model.predict(testX)
                    yPred = np.argmax(yPreds, axis=1)
                    yPred = kutils.to_categorical(yPred, num_classes=nb_classes)
                    yTrue = testY

                    accuracy = metrics.accuracy_score(yTrue, yPred) * 100
                    error = 100 - accuracy
                    print("Accuracy : ", accuracy)
                    print("Error : ", error)

                    acc_name = './keras_examples/vgg/info/mnist_vgg_acc_info_' + str(images_per_label) + '.json'

                    del model
                    K.clear_session()

                    try:
                        with open(acc_name) as outfile:
                            info_data = json.load(outfile)
                    except:
                        info_data = {}

                    if name not in info_data:
                        info_data[name] = []

                    info_data[name].append(accuracy)

                    with open(acc_name, 'w') as outfile:
                        json.dump(info_data, outfile)