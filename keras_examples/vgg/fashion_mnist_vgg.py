import numpy as np
import sklearn.metrics as metrics
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

# backend = 'theano'
import os
# os.environ['KERAS_BACKEND'] = backend
# os.environ['THEANO_FLAGS'] = 'device=cuda0,floatX=float32'
# os.environ['MKL_THREADING_LAYER'] = 'GNU'
import keras_examples.vgg.vgg_network as vgg
from keras.datasets import fashion_mnist
import keras.callbacks as callbacks
import keras.utils.np_utils as kutils
import keras_examples.shared.losses as ortho
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import plot_model
from keras.models import Model
from keras.layers import Activation, Dense, AveragePooling2D, Flatten, Convolution2D, Input, GlobalAveragePooling2D
from keras import backend as K
from keras import optimizers
from scipy.stats import ortho_group
import pickle
import json


batch_size = 100
nb_epoch = 100
img_rows, img_cols = 28, 28
nb_classes = 10

output_losses = [
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 0.1, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 0.1, 'lamda': 10.0}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 1.0, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 1.0, 'lamda': 10.0}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 4.0, 'lamda': 0.1}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 4.0, 'lamda': 10.0}},
    {'type': 'softmax'},
    {'type': 'exponential_orthogonal', 'data': {'R': 0.5, 'lamda': 0.1}},
    {'type': 'exponential_orthogonal', 'data': {'R': 0.5, 'lamda': 1.0}},
    {'type': 'exponential_orthogonal', 'data': {'R': 1.0, 'lamda': 0.1}},
    {'type': 'exponential_orthogonal', 'data': {'R': 1.0, 'lamda': 1.0}},
    {'type': 'exponential_orthogonal', 'data': {'R': 2.0, 'lamda': 0.1}},
    {'type': 'exponential_orthogonal', 'data': {'R': 2.0, 'lamda': 1.0}},
    {'type': 'exponential_orthogonal', 'data': {'R': 4.0, 'lamda': 0.1}},
    {'type': 'exponential_orthogonal', 'data': {'R': 4.0, 'lamda': 1.0}},
    {'type': 'trench_orthogonal', 'data': {'R': 0.1, 'lamda': 0.1}},
    {'type': 'trench_orthogonal', 'data': {'R': 2.0, 'lamda': 0.1}},
    {'type': 'trench_orthogonal', 'data': {'R': 4.0, 'lamda': 0.1}},
    {'type': 'trench_orthogonal', 'data': {'R': 0.1, 'lamda': 1.0}},
    {'type': 'trench_orthogonal', 'data': {'R': 2.0, 'lamda': 1.0}},
    {'type': 'trench_orthogonal', 'data': {'R': 4.0, 'lamda': 1.0}},
    {'type': 'trench_orthogonal', 'data': {'R': 0.1, 'lamda': 10.0}},
    {'type': 'trench_orthogonal', 'data': {'R': 2.0, 'lamda': 10.0}},
    {'type': 'trench_orthogonal', 'data': {'R': 4.0, 'lamda': 10.0}},
]

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
set_session(tf.Session(config=config))

reps = 3

trainable = True

if __name__ == '__main__':
    os.chdir('../../')
    (trainX, trainY), (testX, testY) = fashion_mnist.load_data()

    trainX = np.expand_dims(trainX, -1)
    trainX = trainX.astype('float32')
    testX = np.expand_dims(testX, -1)
    testX = testX.astype('float32')
    trainX_mean = trainX.mean(axis=0)
    trainX_std = trainX.std(axis=0)
    trainX = (trainX - trainX_mean) / np.maximum(1e-8, trainX_std)
    testX = (testX - trainX_mean) / np.maximum(1e-8, trainX_std)

    trainY = kutils.to_categorical(trainY)
    testY = kutils.to_categorical(testY)

    generator = ImageDataGenerator(
        horizontal_flip=True,
        width_shift_range=4./28.,
        height_shift_range=4./28.
    )

    init_shape = (1, 28, 28) if K.image_dim_ordering() == 'th' else (28, 28, 1)

    # For WRN-16-8 put N = 2, k = 8
    # For WRN-28-10 put N = 4, k = 10
    # For WRN-40-4 put N = 6, k = 4
    for drop in [0.0]:
        for i in range(reps):
            for output_loss in output_losses:
                for trainable in [False, True]:
                    if 'orthogonal' in output_loss['type'] and trainable:
                        continue
                    print('loss =', output_loss['type'], ', trainable = ', trainable)
                    if 'data' in output_loss:
                        if 'lamda' in output_loss['data']:
                            print('lamda =', output_loss['data']['lamda'])
                        if 'R' in output_loss['data']:
                            print('R =', output_loss['data']['R'])
                    kernel_initializer = 'he_normal'
                    regularization = 0.0
                    input = Input(shape=init_shape)
                    deep_features = vgg.create_cifar10_vgg_network(
                        input,
                        kernel_initializer=kernel_initializer, regularization=regularization
                    )

                    kernel_initializer = 'orthogonal' if not trainable else 'he_normal'
                    classifier = Convolution2D(nb_classes, (1, 1), use_bias=trainable, kernel_initializer=kernel_initializer)
                    classifier.trainable = trainable

                    output = classifier(deep_features)
                    # output = GlobalAveragePooling2D()(output)
                    output = Flatten()(output)

                    if output_loss['type'] == 'softmax':
                        output = Activation('softmax')(output)

                    model = Model(input, output)

                    model.summary()

                    def scheduler(epoch):
                        if epoch < 40:
                            return .01
                        elif epoch < 80:
                            return .001
                        else:
                            return .0001

                    change_lr = callbacks.LearningRateScheduler(scheduler)

                    name = 'fashion_mnist_vgg_' + output_loss['type'] + '_' + str(drop) + '_' + str(trainable)
                    if 'data' in output_loss:
                        if 'lamda' in output_loss['data']:
                            name += '_C_' + str(output_loss['data']['lamda'])
                        if 'R' in output_loss['data']:
                            name += '_R_' + str(output_loss['data']['R'])

                    # plot_model(model, name + '.png', show_shapes=False)

                    optimizer = optimizers.adam(lr=0.01)

                    loss_data = {'R': 1.0, 'lamda': 1.0} if 'data' not in output_loss else output_loss['data']
                    loss = "categorical_crossentropy" if output_loss['type'] == 'softmax' else \
                        getattr(ortho, output_loss['type'])(**loss_data)

                    metric = 'acc' # if output_loss['type'] == 'softmax' else ortho.softmax_accuracy

                    model.compile(loss=loss, optimizer=optimizer, metrics=["acc"])
                    print("Finished compiling")

                    #model.load_weights("weights/WRN-16-8 Weights.h5")
                    print("Model loaded.")

                    history = model.fit_generator(
                        generator.flow(trainX, trainY, batch_size=batch_size),
                        steps_per_epoch=len(trainX) // batch_size, epochs=nb_epoch,
                        callbacks=[
                            # callbacks.ModelCheckpoint(
                            #     './keras_examples/weights/' + name + '_Weights.h5',
                            #     monitor="val_acc",
                            #     save_best_only=True,
                            #     verbose=1
                            # ),
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        validation_data=(testX, testY),
                        validation_steps=testX.shape[0] // batch_size,
                        verbose=2
                    )

                    with open('./keras_examples/vgg/info/fashion_mnist_' + name + '_history_' + str(i) + '.h5', 'wb') as file_pi:
                        pickle.dump(history.history, file_pi)

                    yPreds = model.predict(testX)
                    yPred = np.argmax(yPreds, axis=1)
                    yPred = kutils.to_categorical(yPred)
                    yTrue = testY

                    accuracy = metrics.accuracy_score(yTrue, yPred) * 100
                    error = 100 - accuracy
                    print("Accuracy : ", accuracy)
                    print("Error : ", error)

                    acc_name = './keras_examples/vgg/info/fashion_mnist_vgg_acc_info.json'

                    del model
                    K.clear_session()

                    try:
                        with open(acc_name) as outfile:
                            info_data = json.load(outfile)
                    except:
                        info_data = {}

                    if name not in info_data:
                        info_data[name] = []

                    info_data[name].append(accuracy)

                    with open(acc_name, 'w') as outfile:
                        json.dump(info_data, outfile)