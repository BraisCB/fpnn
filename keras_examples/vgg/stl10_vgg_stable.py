import numpy as np
import sklearn.metrics as metrics
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

# backend = 'theano'
# import os
# os.environ['KERAS_BACKEND'] = backend
# os.environ['THEANO_FLAGS'] = 'device=cuda0,floatX=float32'
# os.environ['MKL_THREADING_LAYER'] = 'GNU'
import keras_examples.vgg.vgg_network as vgg
from keras_examples.datasets import stl10
import keras.callbacks as callbacks
import keras.utils.np_utils as kutils
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.layers import Activation, AveragePooling2D, Flatten, Convolution2D, Input
from keras import backend as K
from keras import optimizers
import json


batch_size = 100
nb_epoch = 120
img_rows, img_cols = 32, 32
nb_classes = 10

# Tuple content = (class name, mean_stabilization, factor_correction, rate)
dropout_layers = [
    ('StableGaussianDropout', True, True, 0.1),
    ('StableGaussianDropout', True, False, 0.1),
    ('StableGaussianDropout', False, False, 0.1),
    ('StableGaussianDropout', False, True, 0.1),
    ('GaussianDropout', True, True, 0.1),
    ('GaussianDropout', True, False, 0.1),
    ('GaussianDropout', False, False, 0.1),
    ('GaussianDropout', False, True, 0.1),
    ('StableGaussianDropout', True, True, 0.25),
    ('StableGaussianDropout', True, False, 0.25),
    ('StableGaussianDropout', False, False, 0.25),
    ('StableGaussianDropout', False, True, 0.25),
    ('GaussianDropout', True, True, 0.25),
    ('GaussianDropout', True, False, 0.25),
    ('GaussianDropout', False, False, 0.25),
    ('GaussianDropout', False, True, 0.25),
    ('StableGaussianDropout', True, True, 0.5),
    ('StableGaussianDropout', True, False, 0.5),
    ('StableGaussianDropout', False, False, 0.5),
    ('StableGaussianDropout', False, True, 0.5),
    ('GaussianDropout', True, True, 0.5),
    ('GaussianDropout', True, False, 0.5),
    ('GaussianDropout', False, False, 0.5),
    ('GaussianDropout', False, True, 0.5),
    ('StableGaussianDropout', True, True, 0.75),
    ('StableGaussianDropout', True, False, 0.75),
    ('StableGaussianDropout', False, False, 0.75),
    ('StableGaussianDropout', False, True, 0.75),
    ('GaussianDropout', True, True, 0.75),
    ('GaussianDropout', True, False, 0.75),
    ('GaussianDropout', False, False, 0.75),
    ('GaussianDropout', False, True, 0.75),
]

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9
set_session(tf.Session(config=config))

reps = 2

trainable = True

(trainX, trainY), (testX, testY) = stl10.load_data(source='./datasets/stl-10')

trainX = trainX.astype('float32')
trainX_mean = trainX.mean(axis=0)
trainX_std = trainX.std(axis=0)
trainX = (trainX - trainX_mean) / trainX_std
testX = testX.astype('float32')
testX = (testX - trainX_mean) / trainX_std

trainY = kutils.to_categorical(trainY)
testY = kutils.to_categorical(testY)

generator = ImageDataGenerator(horizontal_flip=True)

init_shape = (3, 96, 96) if K.image_dim_ordering() == 'th' else (96, 96, 3)
input = Input(shape=init_shape)

# For WRN-16-8 put N = 2, k = 8
# For WRN-28-10 put N = 4, k = 10
# For WRN-40-4 put N = 6, k = 4

for i in range(reps):
    for dropout_layer in dropout_layers:
        print('dropout class', dropout_layer[0])
        print('mean stabilization', dropout_layer[1])
        print('factor correction', dropout_layer[2])
        print('rate', dropout_layer[3])

        kernel_initializer = 'he_normal'
        regularization = 0.0
        deep_features = vgg.create_stl10_vgg_network(
            input, dropout_layer=dropout_layer,
            kernel_initializer=kernel_initializer, regularization=regularization
        )

        classifier = Convolution2D(nb_classes, (1, 1), use_bias=trainable, kernel_initializer=kernel_initializer)
        classifier.trainable = trainable

        output = classifier(deep_features)
        output = AveragePooling2D((2, 2))(output)
        output = Flatten()(output)

        output = Activation('softmax')(output)

        model = Model(input, output)

        model.summary()

        def scheduler(epoch):
            if epoch < 40:
                return .01
            elif epoch < 80:
                return .001
            else:
                return .0001

        change_lr = callbacks.LearningRateScheduler(scheduler)

        optimizer = optimizers.adam(lr=0.01)

        loss = "categorical_crossentropy"

        model.compile(loss=loss, optimizer=optimizer, metrics=['acc'])
        print("Finished compiling")

        history = model.fit_generator(
            generator.flow(trainX, trainY, batch_size=batch_size),
            steps_per_epoch=len(trainX) // batch_size, epochs=nb_epoch,
            callbacks=[
                # callbacks.ModelCheckpoint(
                #     './keras_examples/weights/' + name + '_Weights.h5',
                #     monitor="val_acc",
                #     save_best_only=True,
                #     verbose=1
                # ),
                callbacks.LearningRateScheduler(scheduler)
            ],
            validation_data=(testX, testY),
            validation_steps=testX.shape[0] // batch_size,
            verbose=2
        )

        yPreds = model.predict(testX)
        yPred = np.argmax(yPreds, axis=1)
        yPred = kutils.to_categorical(yPred)
        yTrue = testY

        accuracy = metrics.accuracy_score(yTrue, yPred) * 100
        error = 100 - accuracy
        print("Accuracy : ", accuracy)
        print("Error : ", error)

        acc_name = './keras_examples/vgg/info/stl10_vgg_acc_info.json'

        try:
            with open(acc_name) as outfile:
                info_data = json.load(outfile)
        except:
            info_data = {}

        name = dropout_layer[0] + '_mean_' + str(dropout_layer[1]) + '_factor_' + \
               str(dropout_layer[2]) + '_rate_' + str(dropout_layer[3])
        if name not in info_data:
            info_data[name] = []

        info_data[name].append(accuracy)

        with open(acc_name, 'w') as outfile:
            json.dump(info_data, outfile)