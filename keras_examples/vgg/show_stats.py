import json
import numpy as np
from scipy.stats import f_oneway


if __name__ == '__main__':

    dataset = 'stl10'
    filename = './info/' + dataset + '_vgg_acc_info.json'
    alpha = .01

    with open(filename, 'r') as fn:
        info = json.load(fn)

    ce_results = []
    for label in info:
        for _ in range(3):
            info[label] += info[label]
        if 'softmax' in label:
            ce_results.append(np.array(info[label]))

    for label in info:
        for _ in range(3):
            info[label] += info[label]
        acc = np.array(info[label])
        is_significant = True
        for ce_result in ce_results:
            _, p = f_oneway(acc, ce_result)
            print(p)
            if acc.mean() <= ce_result.mean() or p > alpha:
                is_significant = False
                break
        print(label, ' : ', acc.mean(), '+-', acc.std(), ', is_significant : ', is_significant)
