from keras.layers import Activation
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras_examples.shared import dropouts
from keras import backend as K
from keras.regularizers import l2
from keras.layers.normalization import BatchNormalization


def keras_dropout_layer(ip, dropout_layer):
    mean_stabilization = dropout_layer[1]
    factor_correction = dropout_layer[2]
    dropout_func = getattr(dropouts, dropout_layer[0])
    return dropout_func(dropout_layer[3], mean_stabilization=mean_stabilization, factor_correction=factor_correction)(ip)


def conv_bn_relu(ip, output_channels, kernel_size=(3, 3), dropout_layer=None, regularization=0.0,
                 kernel_initializer='he_normal', padding='same'):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1

    x = Convolution2D(output_channels, kernel_size, padding=padding, kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(ip)
    x = BatchNormalization(axis=channel_axis, momentum=0.95, epsilon=1e-5, gamma_initializer='ones')(x)

    if dropout_layer is not None:
        x = keras_dropout_layer(x, dropout_layer)

    x = Activation('relu')(x)

    return x


def create_vgg_network(
        ip, dims, nblocks, pools, dropout_layer=None, regularization=1e-5, kernel_initializer='truncated_normal'
):

    x = ip
    for nb, dim, pool in zip(nblocks, dims, pools):
        for i in range(nb):
            x = conv_bn_relu(
                x, dim, dropout_layer=dropout_layer, regularization=regularization, kernel_initializer=kernel_initializer
            )
        x = MaxPooling2D((pool, pool))(x)

    x = conv_bn_relu(
        x, 1024, kernel_size=(3, 3), dropout_layer=dropout_layer, regularization=regularization,
        kernel_initializer=kernel_initializer, padding='valid'
    )

    x = conv_bn_relu(
        x, 1024, kernel_size=(1, 1), dropout_layer=dropout_layer, regularization=regularization,
        kernel_initializer=kernel_initializer, padding='valid'
    )

    return x


def create_cifar100_vgg_network(
        ip, dropout_layer=None, regularization=0.0, kernel_initializer='truncated_normal'
):
    dims = [64, 128, 256]
    nblocks = [2, 2, 4]
    pools = [2, 2, 2]

    return create_vgg_network(
        ip, dims, nblocks, pools, dropout_layer=dropout_layer, regularization=regularization, kernel_initializer=kernel_initializer
    )


def create_cifar10_vgg_network(
        ip, dropout_layer=None, regularization=0.0, kernel_initializer='truncated_normal'
):
    dims = [64, 128, 256]
    nblocks = [2, 2, 4]
    pools = [2, 2, 2]

    return create_vgg_network(
        ip, dims, nblocks, pools, dropout_layer=dropout_layer, regularization=regularization, kernel_initializer=kernel_initializer
    )


def create_stl10_vgg_network(
        ip, dropout_layer=None, regularization=0.0, kernel_initializer='truncated_normal'
):

    dims = [64, 64, 128, 256]
    nblocks = [1, 1, 2, 4]
    pools = [3, 2, 2, 2]

    return create_vgg_network(
        ip, dims, nblocks, pools, dropout_layer=dropout_layer, regularization=regularization, kernel_initializer=kernel_initializer
    )
