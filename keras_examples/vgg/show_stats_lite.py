import json
import numpy as np
from scipy.stats import f_oneway
from matplotlib import pyplot as plt


if __name__ == '__main__':

    dataset = 'cifar10'
    list_images_per_label = [10, 50, 100, 500]
    alpha = .01
    labels = [
        (dataset + '_vgg_softmax_0.0_False', '$CE_{fixed}$'),
        (dataset + '_vgg_softmax_0.0_True', '$CE$'),
        (dataset + '_vgg_exponential_orthogonal_0.0_False_C_1.0_R_2.0', '$EOL$'),
        (dataset + '_vgg_trench_orthogonal_0.0_False_C_1.0_R_4.0', '$TOL$'),
    ]
    plot_opts = ['ro-', 'g^-', 'bx-', 'ks-']
    results = {}
    for images_per_label in list_images_per_label:
        filename = './info/' + dataset + '_vgg_acc_info_' + str(images_per_label) + '.json'

        with open(filename, 'r') as fn:
            info = json.load(fn)

        ce_results = []
        for label, plot_label in labels:
            if 'softmax' in label:
                ce_results.append(np.array(info[label]))

        for label, plot_label in labels:
            acc = np.array(info[label])
            is_significant = True
            for ce_result in ce_results:
                _, p = f_oneway(acc, ce_result)
                print(p)
                if acc.mean() <= ce_result.mean() or p > alpha:
                    is_significant = False
                    break
            if plot_label not in results:
                results[plot_label] = []
            results[plot_label].append(acc.mean())
            print(label, ' : ', acc.mean(), '+-', acc.std(), ', is_significant : ', is_significant)

    keys = [p for _, p in labels]
    plt.figure()
    for i, result in enumerate(results):
        plt.plot(list_images_per_label, results[result], plot_opts[i], linewidth=2)
    plt.legend(keys, loc='best', fontsize=12)
    plt.title(dataset, fontsize=14)
    plt.ylabel('Accuracy', fontsize=10)
    plt.xlabel('# of training images per label', fontsize=10)
    # plt.show()
    plt.savefig('./info/' + dataset + '.png', bbox_inches='tight', pad_inches=0.1)
