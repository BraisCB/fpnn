from keras.layers import Add, Activation, GaussianDropout
from keras.layers.convolutional import Convolution2D
from keras_examples.custom.normalization import BatchNormalization
from keras import backend as K
from keras.regularizers import l2
import numpy as np


def residual_block_v1(
        ip, output_channels, bottleneck=True, strides=(1, 1),
        dropout=0.0, regularization=0.0, kernel_initializer='he_normal', residual=True):
    init = None
    ip_channels = K.int_shape(ip)[-1]
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1

    if output_channels == ip_channels and np.prod(strides) == 1.0:
        if residual:
            init = ip
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(ip)
        if dropout > 0.0:
            x = GaussianDropout(dropout)(x)
        x = Activation('relu')(x)
    else:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(ip)
        if dropout > 0.0:
            x = GaussianDropout(dropout)(x)
        x = Activation('relu')(x)
        if residual:
            init = Convolution2D(
                output_channels, (1, 1), padding='same', strides=strides, kernel_initializer=kernel_initializer,
                use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None
            )(x)

    x = Convolution2D(output_channels, (3, 3), padding='same', strides=strides, kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    if dropout > 0.0:
        x = GaussianDropout(dropout)(x)
    x = Activation('relu')(x)

    x = Convolution2D(output_channels, (3, 3), padding='same', kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    if residual:
        m = Add()([init, x])
    else:
        m = x

    return m


def residual_block_v2(
        ip, output_channels, bottleneck=True, strides=(1, 1),
        dropout=0.0, regularization=0.0, kernel_initializer='he_normal', residual=True):
    init = None
    ip_channels = K.int_shape(ip)[-1]
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1

    inner_channels = output_channels // 4 if bottleneck else output_channels

    if output_channels == ip_channels and np.prod(strides) == 1.0:
        if residual:
            init = ip
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(ip)
        x = Activation('relu')(x)
    else:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(ip)
        x = Activation('relu')(x)
        if residual:
            init = Convolution2D(
                output_channels, (1, 1), padding='same', strides=strides, kernel_initializer=kernel_initializer,
                use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None
            )(x)

    x = Convolution2D(inner_channels, (1, 1), padding='same', strides=strides, kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    x = Activation('relu')(x)

    x = Convolution2D(inner_channels, (3, 3), padding='same', kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    x = Activation('relu')(x)

    x = Convolution2D(output_channels, (1, 1), padding='same', kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    if residual:
        m = Add()([init, x])
    else:
        m = x

    return m


def rn_block(ip, output_channels, N, bottleneck=True, strides=(1, 1),
             dropout=0.0, regularization=0.0, kernel_initializer='he_normal', residual=True, version=1):
    residual_block = residual_block_v2 if version==2 else residual_block_v1
    m = residual_block(
        ip, output_channels, bottleneck, strides, dropout,
        regularization=regularization, kernel_initializer=kernel_initializer, residual=residual
    )
    for i in range(N-1):
        m = residual_block(
            m, output_channels, bottleneck, dropout=dropout,
            regularization=regularization, kernel_initializer=kernel_initializer, residual=residual
        )
    return m


def create_cifar_residual_network(
        ip, depth=110, bottleneck=True, dropout=0.3, regularization=1e-5, kernel_initializer='he_normal', residual=True,
        version=1
):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    # ip = Input(shape=input_dim)
    N = (depth - 2) // 9 if version==2 else (depth - 2) // 6

    x = Convolution2D(
        16, (3, 3), padding='same', kernel_initializer=kernel_initializer,
        use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None
    )(ip)

    output_channel_basis = [64, 128, 256] if version==2 else [16, 32, 64]
    strides = [1, 2, 2]

    for ocb, stride in zip(output_channel_basis, strides):
        x = rn_block(
            x, ocb, N, bottleneck, strides=(stride, stride), dropout=dropout,
            regularization=regularization, kernel_initializer=kernel_initializer, residual=residual, version=version
        )

    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    x = Activation('relu')(x)

    return x
