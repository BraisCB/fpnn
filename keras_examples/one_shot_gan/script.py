from keras.datasets import mnist
from keras_examples.one_shot_gan.an import AN
from keras.layers import Input, Convolution2D, MaxPooling2D, Dense, Dropout, BatchNormalization, Activation, Flatten
from keras.models import Model
from keras.regularizers import l2
from keras import backend as K, optimizers
from keras.utils import to_categorical
import numpy as np
import json


l = 16
k = 4
reps = 50
instances_per_label = 1
num_classes = 10
input_shape = (28, 28, 1)
epochs = 10000
batch_size = min(128, num_classes * instances_per_label)
batch_size_extra = 128
filename = './keras_examples/one_shot_gan/info/accuracies.json'


# Load the dataset
(X_train, Y_train), (X_test, Y_test) = mnist.load_data()
X_train = X_train.astype('float32')
X_train = np.expand_dims(X_train, -1)
X_train_mean = X_train.mean(axis=0)
X_train_std = np.maximum(1e-6, X_train.std(axis=0))
X_train = (X_train - X_train_mean) / X_train_std
X_test = X_test.astype('float32')
X_test = np.expand_dims(X_test, -1)
X_test = (X_test - X_train_mean) / X_train_std


# Rescale -1 to 1
# X_train = X_train / 127.5 - 1.
# X_train = np.expand_dims(X_train, axis=3)

# X_test = X_test / 127.5 - 1.
# X_test = np.expand_dims(X_test, axis=3)

labels = np.unique(Y_train)

Y_train = to_categorical(Y_train)
Y_test = to_categorical(Y_test)

normal_accuracy = []
gan_accuracy = []

for rep in range(reps):

    train_X = []
    train_Y = []
    extra_X = []
    for label in labels:
        idx = np.where(Y_train[:, label] == 1)[0]
        np.random.permutation(idx)
        nlabel = min(instances_per_label, len(idx))
        train_X.append(X_train[idx[:nlabel]])
        train_Y.append(Y_train[idx[:nlabel]])
        extra_X.append(X_train[idx[nlabel:]])

    train_X = np.concatenate(train_X, axis=0)
    train_Y = np.concatenate(train_Y, axis=0)
    extra_X = np.concatenate(extra_X, axis=0)

    print('NORMAL TRAINING')
    model = AN(input_shape, num_classes, l=l, k=k, dropout=0.0, regularization=0.0, kernel_initializer='truncated_normal')
    normal_accuracy = model.fit(train_X, train_Y, X_test, Y_test, extra_X, epochs=epochs,
                                batch_size=batch_size, batch_size_extra=batch_size_extra,
                                train_combined=False, train_discriminator=False)

    print('AN TRAINING')
    model2 = AN(input_shape, num_classes, l=l, k=k, dropout=0.0, regularization=0.0, kernel_initializer='truncated_normal')
    an_accuracy = model2.fit(train_X, train_Y, X_test, Y_test, extra_X, epochs=epochs,
                             batch_size=batch_size, batch_size_extra=batch_size_extra,
                             train_combined=True, train_discriminator=True)

    try:
        with open(filename) as outfile:
            accuracies = json.load(outfile)
    except:
        accuracies = {}

    if 'normal' not in accuracies:
        accuracies['normal'] = []

    if 'an' not in accuracies:
        accuracies['an'] = []

    accuracies['normal'].append(normal_accuracy)
    accuracies['an'].append(an_accuracy)

    with open(filename, 'w') as outfile:
        json.dump(accuracies, outfile)

