from __future__ import print_function, division

from keras.datasets import mnist
from keras.layers import Input, Dense, Reshape, Flatten, Dropout
from keras.layers import BatchNormalization, Activation, ZeroPadding2D, AveragePooling2D, Convolution2D, MaxPooling2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Sequential, Model
from keras.optimizers import Adam
from keras_examples.one_shot_gan import wide_residual_network as wrn
from keras.regularizers import l2
from keras import backend as K
import matplotlib.pyplot as plt

import sys

import numpy as np


def create_model(input, dropout=0.0, regularization=0.0, bn=True):

    channel_axis = 1 if K.image_data_format() == "channels_first" else -1

    kernel_initializer = 'truncated_normal'

    x = Convolution2D(32, (3, 3), padding='same', kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(input)

    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)

    x = Activation('relu')(x)

    x = MaxPooling2D((2, 2))(x)

    x = Convolution2D(32, (3, 3), padding='same', kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)

    x = Activation('relu')(x)

    x = MaxPooling2D((2, 2))(x)

    x = Flatten()(x)

    x = Dense(1024, use_bias=False, kernel_initializer=kernel_initializer,
              kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)

    deep_features = Activation('relu')(x)

    if dropout > 0.0:
        deep_features = Dropout(dropout)(deep_features)

    return deep_features



class AN:

    def __init__(self, input_shape, nbclasses, l=16, k=1, dropout=0.0, regularization=0.0, kernel_initializer='truncated_normal'):
        self.input_shape = input_shape
        self.nbclasses = nbclasses
        self.l = l
        self.k = k
        self.dropout = dropout
        self.regularization = regularization
        self.kernel_initializer = kernel_initializer
        self.input = Input(shape=self.input_shape)

        optimizer = Adam(0.002)
        optimizer_combine = Adam(0.0002)

        # Build and compile the discriminator
        self.deep_features = self.build_deep_features()
        self.classifier = self.build_classifier()
        self.classifier.compile(loss='categorical_crossentropy',
                                optimizer=optimizer,
                                metrics=['acc'])
        self.discriminator = self.build_discriminator()
        self.discriminator.compile(loss='binary_crossentropy',
            optimizer=optimizer,
            metrics=['accuracy'])


        # For the combined model we will only train the generator
        self.discriminator.trainable = False

        # The discriminator takes generated images as input and determines validity
        validity = self.discriminator(self.deep_features.output)

        # The combined model  (stacked generator and discriminator)
        # Trains the generator to fool the discriminator
        self.combined = Model(self.input, validity)
        self.combined.compile(loss='binary_crossentropy', optimizer=optimizer_combine)

    def build_deep_features(self):

        # deep_features = wrn.create_cifar_wide_residual_network(
        #     self.input, l=self.l, k=self.k, dropout=self.dropout,
        #     kernel_initializer=self.kernel_initializer, regularization=self.regularization
        # )
        # deep_features = AveragePooling2D((7, 7))(deep_features)
        # deep_features = Flatten()(deep_features)

        deep_features = create_model(self.input)

        return Model(self.input, deep_features)

    def build_classifier(self):
        classifier = Sequential()

        classifier.add(
            Dense(self.nbclasses, input_shape=self.deep_features.output_shape[1:], use_bias=True,
                  kernel_initializer=self.kernel_initializer,
                  kernel_regularizer=l2(self.regularization) if self.regularization > 0.0 else None)
        )

        classifier.add(Activation('softmax'))

        output = classifier(self.deep_features.output)

        return Model(self.input, output)

    def build_discriminator(self):

        model = Sequential()

        # model.add(Flatten(input_shape=self.deep_features.output_shape[1:]))
        # model.add(Dense(512, input_shape=self.deep_features.output_shape[1:]))
        # model.add(LeakyReLU(alpha=0.2))
        # model.add(Dense(256))
        # model.add(LeakyReLU(alpha=0.2))
        model.add(Dense(1, activation='sigmoid', input_shape=self.deep_features.output_shape[1:]))
        # model.summary()

        img = Input(shape=self.deep_features.output_shape[1:])
        validity = model(img)

        return Model(img, validity)

    def fit(self, X_train, Y_train, X_test, Y_test, X_extra, epochs, batch_size=128, batch_size_extra=None,
              train_combined=True, train_discriminator=True):

        ldata = len(X_train)
        lextradata = len(X_extra)

        if batch_size_extra == None:
            batch_size_extra = batch_size
            
        idx_extra = np.random.permutation(len(X_extra))
        index_extra = 0

        y_true = np.argmax(Y_test, axis=-1)

        for epoch in range(epochs):

            # ---------------------
            #  Train Discriminator
            # ---------------------
            idx = np.random.permutation(ldata)

            index = 0
            while index < ldata:
                new_index = min(ldata, index + batch_size)

                if index_extra + batch_size_extra > lextradata:
                    idx_extra = np.random.permutation(len(X_extra))
                    index_extra = 0

                new_index_extra = index_extra + batch_size_extra

                # Adversarial ground truths
                labeled = np.ones((new_index - index, 1))
                unlabeled = np.zeros((new_index - index, 1))

                batch_X_train = X_train[idx[index:new_index]]
                batch_Y_train = Y_train[idx[index:new_index]]

                batch_X_extra = X_extra[idx[index:new_index]]
                index = new_index
                index_extra = new_index_extra

                # Train the discriminator
                if train_discriminator:
                    batch_X_train_deep_features = self.deep_features.predict(batch_X_train)
                    batch_X_extra_deep_features = self.deep_features.predict(batch_X_extra)
                    batch_X = np.concatenate((batch_X_train_deep_features, batch_X_extra_deep_features))
                    batch_Y = np.concatenate((labeled, unlabeled))
                    d_loss = self.discriminator.train_on_batch(batch_X, batch_Y)
                # ---------------------
                #  Train Combined
                # ---------------------

                # Train the generator (to have the discriminator label samples as valid)
                if train_combined:
                    for i in range(1):

                        if index_extra + batch_size_extra > lextradata:
                            idx_extra = np.random.permutation(len(X_extra))
                            index_extra = 0

                        new_index_extra = index_extra + batch_size_extra
                        batch_X_extra = X_extra[idx_extra[index_extra:new_index_extra]]

                        valid = np.ones((new_index_extra - index_extra, 1))

                        g_loss = self.combined.train_on_batch(batch_X_extra, valid)

                        index_extra = new_index_extra

                # ---------------------
                #  Train Classifier
                # ---------------------

                # Train the generator (to have the discriminator label samples as valid)
                c_loss = self.classifier.train_on_batch(batch_X_train, batch_Y_train)
                if epoch % 1000 == 0:
                    print("%d [C loss: %f, acc.: %.2f%%]" % (epoch, c_loss[0], 100 * c_loss[1]))

                    if train_discriminator:
                        print("%d [D loss: %f, acc.: %.2f%%]" % (epoch, d_loss[0], 100 * d_loss[1]))

                    # Plot the progress
                    if train_combined:
                        print ("%d [G loss: %f]" % (epoch, g_loss))

                    y_pred = np.argmax(self.classifier.predict(X_test, batch_size=batch_size), axis=-1)
                    accuracy = (y_pred == y_true).mean()
                    print("%d [Test acc.: %.2f%%]" % (epoch, accuracy))

        y_pred = np.argmax(self.classifier.predict(X_test, batch_size=batch_size), axis=-1)
        accuracy = (y_pred == y_true).mean()
        print("%d [Test acc.: %.2f%%]" % (epoch, accuracy))

        return accuracy

    def predict(self, X):
        self.classifier.predict(X)
