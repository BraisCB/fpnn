from keras.datasets import mnist, cifar10, cifar100
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
from keras.utils import to_categorical


def load_dataset(dataset, normalize=True):
    if dataset == 'mnist':
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        x_train = np.expand_dims(x_train, axis=-1)
        x_test = np.expand_dims(x_test, axis=-1)
        generator = ImageDataGenerator(width_shift_range=4. / 28,
                                       height_shift_range=4. / 28,
                                       fill_mode='reflect')
    elif dataset == 'cifar10':
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        generator = ImageDataGenerator(width_shift_range=5. / 32,
                                       height_shift_range=5. / 32,
                                       fill_mode='reflect',
                                       horizontal_flip=True)
    elif dataset == 'cifar100':
        (x_train, y_train), (x_test, y_test) = cifar100.load_data()
        generator = ImageDataGenerator(width_shift_range=5. / 32,
                                       height_shift_range=5. / 32,
                                       fill_mode='reflect',
                                       horizontal_flip=True)
    else:
        raise Exception('dataset not found')
    nclasses = len(np.unique(y_train))
    y_train = to_categorical(y_train, nclasses)
    y_test = to_categorical(y_test, nclasses)
    if normalize:
        mean = x_train.mean(axis=0)
        std = np.maximum(1e-6, x_train.std(axis=0))
        x_train = (x_train - mean) / std
        x_test = (x_test - mean) / std
    output = {
        'train': {
            'data': x_train,
            'label': y_train
        },
        'test': {
            'data': x_test,
            'label': y_test
        },
        'generator': generator
    }
    return output