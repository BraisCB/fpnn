from keras import backend as K
from keras.layers import Layer
import numpy as np
from keras_examples.accordion_network.custom.initializers import frequency_init


class AdaptiveClassWeight(Layer):

    def __init__(self,
                 labels,
                 comparable=None,
                 axis=-1,
                 momentum=0.999,
                 **kwargs):
        super(AdaptiveClassWeight, self).__init__(**kwargs)
        self.labels = labels
        self.comparable = comparable
        self.axis = axis
        self.momentum = momentum

    def build(self, input_shape):
        dim = input_shape[self.axis]
        if dim is None:
            raise ValueError('Axis ' + str(self.axis) + ' of '
                             'input tensor should have a defined dimension '
                             'but the layer received an input with shape ' +
                             str(input_shape) + '.')
        self.nlabels = np.prod(input_shape[self.axis])
        self.nbranches = 1 if self.comparable is None else np.prod(K.int_shape(self.comparable)[self.axis])

        self.loss_weights = self.add_weight(shape=(self.nbranches, self.nlabels),
                                         name='loss_weights',
                                         initializer='ones',
                                         trainable=False)
        self.built = True

    def get_loss_weights(self, indices):
        weights = self.loss_weights if self.nbranches == 1 else K.gather(self.loss_weights, indices)
        return weights

    def call(self, inputs, training=None, **kwargs):

        indices = K.constant(0, dtype='int32')
        winners = K.cast(K.argmax(self.labels, axis=self.axis), 'int32')
        labels = winners

        if self.comparable is not None:
            indices = K.cast(K.argmax(self.comparable, axis=self.axis), 'int32')
            labels = indices * self.nlabels + winners

        weights = self.get_loss_weights(indices)
        self.active_weights = weights

        def if_train():
            histogram = K.cast(K.tf.bincount(
                labels, minlength=self.nlabels * self.nbranches, maxlength=self.nlabels * self.nbranches
            ), 'float32')
            histogram = K.reshape(histogram, (self.nbranches, self.nlabels))
            histogram /= K.sum(histogram, axis=-1, keepdims=True) + 1e-6

            new_weights = self.momentum * self.loss_weights + (1.0 - self.momentum) * (1.0 - histogram)
            self.add_update([(self.loss_weights, new_weights)])
            return weights

        def if_test():
            return weights

        return [inputs, K.in_train_phase(if_train, if_test, training=training)]
        # return [inputs, K.ones_like(inputs)]

    def compute_output_shape(self, input_shape):
        if self.comparable is None:
            return [input_shape, K.int_shape(self.loss_weights)]
        else:
            return [input_shape, input_shape]


class _Option(Layer):

    def __init__(self, **kwargs):
        super(_Option, self).__init__(**kwargs)

    def _option_function(self, inputs, **kwargs):
        raise NotImplementedError

    def build(self, input_shape):
        # Used purely for shape validation.
        if not isinstance(input_shape, list):
            raise ValueError('An option layer should be called '
                             'on a list of inputs.')
        if len(input_shape) <  1:
            raise ValueError('An option layer should be called '
                             'on a list of 1 inputs at list. '
                             'Got ' + str(len(input_shape)) + ' inputs.')
        batch_sizes = [s[0] for s in input_shape if s is not None]
        batch_sizes = set(batch_sizes)
        batch_sizes -= set([None])
        branches_shape = input_shape[1:]

        if branches_shape[0] is None:
            output_shape = None
        else:
            output_shape = branches_shape[0][1:]
        for i in range(1, len(branches_shape)):
            if branches_shape[i] is None:
                shape = None
            else:
                shape = branches_shape[i][1:]
            if output_shape != shape:
                raise ValueError('All branch shapes (2nd element of the list) must be equeal. Got : ' +
                                 str(output_shape) + '. Expected ' + str(shape))
        self.built = True

    def call(self, inputs, **kwargs):
        return self._option_function(inputs, **kwargs)

    def compute_output_shape(self, input_shape):
        return input_shape[1]


class Option2(_Option):

    def __init__(self, comparable, reduction_function='argmax', axis=-1, **kwargs):
        super(Option2, self).__init__(**kwargs)
        self.comparable = comparable
        self.reduction_function = reduction_function
        self.axis = axis

    def _option_function(self, inputs, **kwargs):
        branches = inputs
        batch_size = K.shape(self.comparable)[0]
        stack_branches = K.stack(branches, axis=1)
        dim_0_index = K.arange(batch_size)[:, None]
        winners = K.cast(getattr(K, self.reduction_function)(self.comparable, axis=self.axis)[:, None], 'int32')
        indices = K.concatenate((dim_0_index, winners), axis=-1)
        return K.tf.gather_nd(stack_branches, indices=indices)


class Option(_Option):

    def __init__(self, comparable, axis=-1, **kwargs):
        super(Option, self).__init__(**kwargs)
        self.comparable = comparable
        self.axis = axis

    def _option_function(self, inputs, **kwargs):
        comparable_shape = K.int_shape(self.comparable)
        stack_branches = K.stack(inputs, axis=1)
        winners = K.argmax(self.comparable, axis=self.axis)[:, None]
        winners_one_hot = K.one_hot(winners, comparable_shape[self.axis])
        dim = len(K.int_shape(stack_branches))
        expanded_comparable_shape = [1] * dim
        expanded_comparable_shape[0] = -1
        expanded_comparable_shape[1] = comparable_shape[self.axis]
        expanded_comparable = K.reshape(winners_one_hot, tuple(expanded_comparable_shape))
        return K.sum(expanded_comparable * stack_branches, axis=1)


# class OptionAdd(_Option):
#
#     def _option_function(self, inputs, **kwargs):
#         comparable = inputs[0]
#         comparable_shape = K.int_shape(comparable)
#         branches = inputs[1:]
#         stack_branches = K.stack(branches, axis=1)
#         dim = len(K.int_shape(stack_branches))
#         expanded_comparable_shape = [1] * dim
#         expanded_comparable_shape[0] = -1
#         expanded_comparable_shape[1] = comparable_shape[-1]
#         expanded_comparable = K.reshape(comparable, tuple(expanded_comparable_shape))
#         return K.sum(expanded_comparable * stack_branches, axis=1)


# class _Conditional(Layer):
#
#     def __init__(self, branches, **kwargs):
#         super(_Conditional, self).__init__(**kwargs)
#         self.branches = branches
#         if isinstance(self.branches, dict):
#             self.branches = self.branches.items()
#
#     def build(self, input_shape):
#         for key, layer in self.branches:
#             layer.build(input_shape[1])
#         self.built = True
#
#     def _conditional_function(self, inputs):
#         raise NotImplementedError
#
#     def call(self, inputs, **kwargs):
#         return self._conditional_function(inputs, **kwargs)
#
#     def compute_output_shape(self, input_shape):
#         return self.branches[0][1].compute_output_shape(input_shape[1])
#
#
# class Case(_Conditional):
#
#     def _conditional_function(self, inputs, **kwargs):
#         output, _ = K.map_fn(
#             self.__fn_function,
#             (K.cast(K.argmax(inputs[0], axis=-1), dtype=K.tf.int32), inputs[1])
#         )
#
#     def __fn_function(self, inputs):
#         return self.__conditional_function_loop(self.branches, inputs)
#
#     def __conditional_function_loop(self, branches, inputs):
#         if len(self.branches) == 1:
#             return self.branches[0][1].call(inputs[1]),
#         else:
#             K.switch(
#                 K.equal(branches[0][0], inputs[0]),
#                 lambda: branches[0][1].call(K.expand_dims(inputs[1], axis=0)),
#                 lambda: self.__conditional_function_loop(branches[1:], inputs) if len(branches) > 2 else branches[1][1].call(inputs[1]),
#             )
#
#     # def __get_pred_fn_pairs(self, inputs):
#     #     comparable, input = inputs
#     #     comparable = K.argmax(comparable, axis=-1)
#     #     default = None
#     #     pred_fn_pairs = []
#     #     for key, layer in self.branches:
#     #         if key == 'default':
#     #             default = lambda: layer.call(input)
#     #         else:
#     #             pred_fn_pairs.append((K.equal(comparable, key), lambda: layer.call(input)))
#     #     return pred_fn_pairs, default
#
#     # def _conditional_function(self, inputs, **kwargs):
#     #     pred_fn_pairs, default = self.__get_pred_fn_pairs(inputs)
#     #     return CK.case(pred_fn_pairs=pred_fn_pairs, default=default)
#
#     # def _conditional_function(self, inputs, **kwargs):
#     #     return self.__conditional_function_loop(self.branches, (K.cast(K.argmax(inputs[0], axis=-1), dtype=K.tf.int32), inputs[1]))
#     #
#     # def __conditional_function_loop(self, branches, inputs):
#     #     if len(self.branches) == 1:
#     #         return self.branches[0][1].call(inputs[1])
#     #     else:
#     #         return K.tf.where(
#     #             K.equal(branches[0][0], inputs[0]),
#     #             branches[0][1].call(inputs[1]),
#     #             self.__conditional_function_loop(branches[1:], inputs) if len(branches) > 2 else branches[1][1].call(inputs[1])
#     #         )

