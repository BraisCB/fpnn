import numpy as np
from keras import backend as K
from keras.utils import to_categorical


class AccordionModel:

    def __init__(self, models, classifiers, losses=None, frequencies=None):
        self.models = models
        self.classifiers = classifiers
        self.losses = losses
        if frequencies is None:
            self.frequencies = []
            for i, model in enumerate(self.models):
                self.frequencies.append(
                    1.0 / model.output_shape[-1] * np.ones(model.output_shape[-1]) if i == 0 else
                    1.0 / model.output_shape[-1] * np.ones((self.models[i - 1].output_shape[-1], model.output_shape[-1]))
                )
        else:
            self.frequencies = frequencies
        self.active_models = list(range(len(self.models)))

    def fit(self, X, y, X_test=None, y_test=None, type='all', data_generator=None, batch_size=128, epochs=100,
            callbacks=None, sample_weight_momentum=0.95):
        callbacks = {} if callbacks is None else callbacks
        if type == 'all':
            return self.__fit_all_models_at_a_time(
                X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, sample_weight_momentum
            )
        elif type == 'one_by_one':
            return self.__fit_one_model_at_a_time(
                X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, sample_weight_momentum
            )
        else:
            raise Exception('type not supported')

    def __fit_one_model_at_a_time(
            self, X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, sample_weight_momentum
    ):
        pass
        # lenX = len(X)
        #
        # if data_generator is None:
        #     perm = np.arange(0, lenX, dtype=int)
        # else:
        #     generator = data_generator.flow(X, y, batch_size=batch_size)
        #
        # train_info = []
        # for model in self.models:
        #     train_info.append({})
        #     for metric_name in model.metrics_names:
        #         train_info[-1][metric_name] = []
        #
        # test_info = None
        # if X_test is not None and y_test is not None:
        #     test_info = []
        #     for model in self.models:
        #         test_info.append({})
        #         for metric_name in model.metrics_names:
        #             test_info[-1][metric_name] = []
        #
        # learning_rate = {} if 'learning_rate' not in callbacks else callbacks['learning_rate']
        #
        # for i, model in enumerate(self.models):
        #     for epoch in range(epochs):
        #         if epoch in learning_rate:
        #             print('Changing learning rate to', learning_rate[epoch])
        #             if isinstance(learning_rate[epoch], list):
        #                 K.set_value(model.optimizer.lr, learning_rate[epoch][i])
        #             else:
        #                 K.set_value(model.optimizer.lr, learning_rate[epoch])
        #         index = 0
        #         if data_generator is None:
        #             np.random.shuffle(perm)
        #         train_info_per_epoch = {}
        #         for metric_name in model.metrics_names:
        #             train_info_per_epoch[metric_name] = []
        #         while index + batch_size <= lenX:
        #             new_index = index + batch_size
        #             if data_generator is None:
        #                 batch_X, batch_y = X[perm[index:new_index]], y[perm[index:new_index]]
        #             else:
        #                 batch_X, batch_y = next(generator)
        #             if i==0:
        #                 batch_train_info = model.train_on_batch(batch_X, batch_y, class_weight=class_weights[i])
        #                 batch_frequency = np.histogram(np.argmax(batch_y, axis=-1), bins=bins[i], density=True)[0]
        #                 frequencies[i] = sample_weight_momentum * frequencies[i] + (
        #                         1.0 - sample_weight_momentum) * batch_frequency
        #                 frequencies[i] = np.clip(frequencies[i], 1e-4, 1.0)
        #                 class_weights[i] = np.min(frequencies[i]) / frequencies[i]
        #             else:
        #                 predicted_output = np.argmax(self.models[i-1].predict(batch_X, batch_size=batch_size), axis=-1)
        #                 sample_weight = class_weights[i][predicted_output]
        #                 batch_train_info = model.train_on_batch(batch_X, batch_y, sample_weight=sample_weight)
        #                 for j in range(model.output_shape[-1]):
        #                     batch_frequency_j = np.histogram(
        #                         np.argmax(batch_y[predicted_output == j], axis=-1), bins=bins[i], density=True
        #                     )[0]
        #                     frequencies[i][j] = sample_weight_momentum * frequencies[i][j] + (
        #                                         1.0 - sample_weight_momentum) * batch_frequency_j
        #                     frequencies[i][j] = np.clip(frequencies[i][j], 1e-4, 1.0)
        #                     class_weights[i][j] = np.max(frequencies[i][j]) / frequencies[i][j]
        #             for metric_name, metric_value in zip(model.metrics_names, batch_train_info):
        #                 if metric_name in ['loss', 'acc']:
        #                     train_info_per_epoch[metric_name].append(metric_value)
        #             index = new_index
        #         print('EPOCH : ', epoch)
        #         for metric_name in model.metrics_names:
        #             if metric_name in ['loss', 'acc']:
        #                 print('Train Model', i, metric_name, ': ', np.mean(train_info_per_epoch[metric_name]))
        #                 train_info[i][metric_name].append(train_info_per_epoch[metric_name])
        #         if test_info is not None:
        #             test_info_per_epoch = model.evaluate(X_test, y_test, batch_size=batch_size, verbose=0)
        #             for metric_name, metric_value in zip(model.metrics_names, test_info_per_epoch):
        #                 if metric_name in ['loss', 'acc']:
        #                     print('Test Model', i, metric_name, ': ', metric_value)
        #                     test_info[i][metric_name].append(metric_value)
        #
        # output = {
        #     'train': train_info
        # }
        #
        # if test_info is not None:
        #     output['test'] = test_info
        #
        # return output

    def __initialize_info_structure(self):
        info = []
        for model in self.models:
            info.append({})
            for metric_name in model.metrics_names:
                info[-1][metric_name] = []
        return info

    def __run_callbacks(self, callbacks, epoch):
        for callback in callbacks:
            callback['function'](self, callback['data'], epoch)

    def __fit_all_models_at_a_time(
            self, X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, sample_weight_momentum
    ):
        lenX = len(X)

        perm = None if data_generator is not None else np.arange(0, lenX, dtype=int)
        generator = None if data_generator is None else data_generator.flow(X, y, batch_size=batch_size)

        train_info = self.__initialize_info_structure()
        test_info = None if X_test is None or y_test is None else self.__initialize_info_structure()

        for epoch in range(epochs):
            self.__run_callbacks(callbacks, epoch)
            index = 0
            if data_generator is None:
                np.random.shuffle(perm)

            train_info_per_epoch = self.__initialize_info_structure()
            while index + batch_size <= lenX:
                new_index = index + batch_size
                if data_generator is None:
                    batch_X, batch_y = X[perm[index:new_index]], y[perm[index:new_index]]
                else:
                    batch_X, batch_y = next(generator)
                for i, model in enumerate(self.models):
                    if i in self.active_models:
                        batch_train_info = model.train_on_batch(batch_X, batch_y)
                        if i in self.classifiers:
                            batch_train_info_2 = self.classifiers[i].train_on_batch(batch_X, batch_y)
                        for metric_name, metric_value in zip(model.metrics_names, batch_train_info):
                            if metric_name in ['loss', 'acc']:
                                train_info_per_epoch[i][metric_name].append(metric_value)
                index = new_index
            print('EPOCH : ', epoch)
            for i in self.active_models:
                model = self.models[i]
                for metric_name in model.metrics_names:
                    if metric_name in ['loss', 'acc']:
                        print('Train Model', i, metric_name, ': ', np.mean(train_info_per_epoch[i][metric_name]))
                        train_info[i][metric_name].append(train_info_per_epoch[i][metric_name])
                if test_info is not None:
                    test_info_per_epoch = model.evaluate(X_test, y_test, batch_size=batch_size, verbose=0)
                    for metric_name, metric_value in zip(model.metrics_names, test_info_per_epoch):
                        if metric_name in ['loss', 'acc']:
                            print('Test Model', i, metric_name, ': ', metric_value)
                            test_info[i][metric_name].append(metric_value)

        output = {
            'train': train_info
        }

        if test_info is not None:
            output['test'] = test_info

        return output

    def __initialize_class_weights_and_bins(self):
        class_weights = []
        bins = []
        for i, model in enumerate(self.models):
            class_weights.append(np.ones_like(self.frequencies[i]))
            bins.append(np.arange(model.output_shape[-1] + 1, dtype=int))
        return class_weights, bins

    def __fit_all_models_at_a_time_old(
            self, X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, sample_weight_momentum
    ):
        lenX = len(X)

        perm = None if data_generator is not None else np.arange(0, lenX, dtype=int)
        generator = None if data_generator is None else data_generator.flow(X, y, batch_size=batch_size)

        class_weights, bins = self.__initialize_class_weights_and_bins()
        train_info = self.__initialize_info_structure()
        test_info = None if X_test is None or y_test is None else self.__initialize_info_structure()

        for epoch in range(epochs):
            self.__run_callbacks(callbacks, epoch)
            index = 0
            if data_generator is None:
                np.random.shuffle(perm)

            train_info_per_epoch = self.__initialize_info_structure()
            while index + batch_size <= lenX:
                new_index = index + batch_size
                if data_generator is None:
                    batch_X, batch_y = X[perm[index:new_index]], y[perm[index:new_index]]
                else:
                    batch_X, batch_y = next(generator)
                for i, model in enumerate(self.models):
                    if i in self.active_models:
                        if i == 0:
                            batch_train_info = model.train_on_batch(batch_X, batch_y, class_weight=class_weights[i])
                            batch_frequency = np.histogram(np.argmax(batch_y, axis=-1), bins=bins[i], density=True)[0]
                            self.frequencies[i] = sample_weight_momentum * self.frequencies[i] + (
                                    1.0 - sample_weight_momentum) * batch_frequency
                            self.frequencies[i] = np.clip(self.frequencies[i], 1e-4, 1.0)
                            class_weights[i] = np.min(self.frequencies[i]) / self.frequencies[i]
                        else:
                            predicted_output = np.argmax(self.models[i-1].predict(batch_X), axis=-1)
                            class_weight = class_weights[i][predicted_output]
                            batch_train_info = model.train_on_batch(batch_X, batch_y, class_weight=class_weight)
                            for j in range(model.output_shape[-1]):
                                pos = np.where(predicted_output == j)[0]
                                if len(pos) > 0:
                                    batch_frequency_j = np.histogram(
                                        np.argmax(batch_y[pos], axis=-1), bins=bins[i], density=True
                                    )[0]
                                    self.frequencies[i][j] = sample_weight_momentum * self.frequencies[i][j] + (
                                                        1.0 - sample_weight_momentum) * batch_frequency_j
                                    self.frequencies[i][j] = np.clip(self.frequencies[i][j], 1e-4, 1.0)
                                    class_weights[i][j] = np.min(self.frequencies[i][j]) / self.frequencies[i][j]
                        for metric_name, metric_value in zip(model.metrics_names, batch_train_info):
                            if metric_name in ['loss', 'acc']:
                                train_info_per_epoch[i][metric_name].append(metric_value)
                index = new_index
            print('EPOCH : ', epoch)
            for i in self.active_models:
                model = self.models[i]
                for metric_name in model.metrics_names:
                    if metric_name in ['loss', 'acc']:
                        print('Train Model', i, metric_name, ': ', np.mean(train_info_per_epoch[i][metric_name]))
                        train_info[i][metric_name].append(train_info_per_epoch[i][metric_name])
                if test_info is not None:
                    test_info_per_epoch = model.evaluate(X_test, y_test, batch_size=batch_size, verbose=0)
                    for metric_name, metric_value in zip(model.metrics_names, test_info_per_epoch):
                        if metric_name in ['loss', 'acc']:
                            print('Test Model', i, metric_name, ': ', metric_value)
                            test_info[i][metric_name].append(metric_value)

        output = {
            'train': train_info
        }

        if test_info is not None:
            output['test'] = test_info

        return output
