from keras import backend as K
import numpy as np


def weighted_categorical_crossentropy(weights):
    def loss(y_true, y_pred):
        y_pred = K.clip(y_pred, K.epsilon(), 1. - K.epsilon())
        return - K.sum(weights * y_true * K.log(y_pred), axis=-1)
    return loss



class WeightedCategoricalCrossEntropy:

    def __init__(self, nlabels, branches=1, axis=-1, momentum=0.95):
        self.branches = branches
        self.nlabels = nlabels
        self.frequencies = K.variable(1.0 / nlabels * np.ones((branches, nlabels)))
        self.axis = axis
        self.momentum = momentum

    def _update_and_retrieve_weights(self, indices, winners):
        frequencies = self.frequencies if self.branches == 1 else K.gather(self.frequencies, indices)
        weights = K.min(frequencies, axis=-1, keepdims=True) / frequencies

        def if_train():
            assign_frequencies = self._update_frequencies(indices, winners)
            with K.tf.control_dependencies([assign_frequencies]):
                return weights

        def if_test():
            return weights
        
        return K.in_train_phase(if_train, if_test, training=K.learning_phase())

    def _update_frequencies(self, indices, winners):
        labels = indices * self.nlabels + winners
        histogram = K.cast(K.tf.bincount(
            labels, minlength=self.nlabels * self.branches, maxlength=self.nlabels * self.branches
        ), 'float32')
        histogram = K.reshape(histogram, (self.branches, self.nlabels))
        histogram /= K.sum(histogram, axis=-1, keepdims=True)
        new_frequencies = self.momentum * self.frequencies + (1.0 - self.momentum) * histogram
        new_frequencies = K.clip(new_frequencies, 1e-4, 1.0)
        assign_frequencies = K.tf.assign(self.frequencies, new_frequencies)
        return assign_frequencies

    def get_loss(self, comparable=None):
        indices = 0
        if comparable is not None:
            indices = K.cast(K.argmax(comparable, axis=self.axis), 'int32')

        def categorical_crossentropy(y_true, y_pred):
            winners = K.cast(K.argmax(y_true, axis=self.axis), 'int32')
            weights = self._update_and_retrieve_weights(indices, winners)
            y_pred /= K.sum(y_pred, axis=self.axis, keepdims=True)
            y_pred = K.clip(y_pred, 1e-6, 1. - 1e-6)
            output = - K.sum(weights * y_true * K.log(y_pred), axis=self.axis)
            return output

        return categorical_crossentropy
