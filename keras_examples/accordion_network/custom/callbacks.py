from keras import backend as K


def learning_rate_changer(object, learning_rate, epoch):
    if isinstance(learning_rate, list):
        for i, model in enumerate(object.models):
            if epoch in learning_rate[i]:
                print('Changing model ', i, ' learning rate to', learning_rate[i][epoch])
                K.set_value(model.optimizer.lr, learning_rate[i][epoch])
    else:
        if epoch in learning_rate:
            print('Changing learning rate to', learning_rate[epoch])
            for i, model in enumerate(object.models):
                K.set_value(model.optimizer.lr, learning_rate[epoch])


def model_stopper(object, model_stop, epoch):
    updated_active_models = []
    if isinstance(model_stop, list):
        for i in object.active_models:
            if epoch == model_stop[i]:
                print('Stopping model ', i)
            else:
                updated_active_models.append(i)
    else:
        if epoch == model_stop:
            print('Stopping all model ')
        else:
            return
    object.active_models = updated_active_models