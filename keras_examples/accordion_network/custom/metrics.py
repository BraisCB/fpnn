from keras import backend as K


def predicted_label(y_true, y_pred):
    return K.argmax(y_pred, axis=-1)
