from keras.models import Model, Sequential
from keras import backend as K, optimizers
from keras.layers import Dense, Activation, BatchNormalization, Dropout, Input, Flatten, Convolution2D, MaxPool2D, Lambda
from keras.regularizers import l1, l2
from keras_examples.custom.layers import Mask, Reduce
from keras_examples.feature_selection.custom import saliencies
from keras_examples.wrn.wide_residual_network import wrn_block
from keras_examples.accordion_network.custom.initializers import flatten_basis
from keras_examples.accordion_network.custom.models import AccordionModel
from keras_examples.accordion_network.custom.layers import Option, AdaptiveClassWeight
from keras_examples.accordion_network.custom.losses import weighted_categorical_crossentropy


def __classifier_layers(input_shape, nclasses, kernel_initializer, trainable, use_bias, bn=True, regularization=0.0):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    layers = []
    if len(input_shape) > 1:
        layers += [
            Flatten(input_shape=input_shape),
            Dense(
                128, use_bias=use_bias, kernel_initializer=kernel_initializer, trainable=True,
                kernel_regularizer=l2(regularization) if regularization > 0.0 else None
            )
        ]
    else:
        layers += [
            Dense(
                128, use_bias=use_bias, kernel_initializer=kernel_initializer, trainable=True,
                kernel_regularizer=l2(regularization) if regularization > 0.0 else None, input_shape=input_shape
            )
        ]
    if bn:
        layers.append(BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones'))
    layers += [
        Activation('relu'),
        Dense(
            nclasses, use_bias=use_bias, kernel_initializer=kernel_initializer, trainable=trainable,
            kernel_regularizer=l2(regularization) if trainable and regularization > 0.0 else None
        ),
        Activation('softmax'),
    ]

    return layers


def cnnsimple(input_shape, nclasses=2, bn=False, kernel_initializer='he_normal', classifier_initializer='orthogonal',
               dropout=0.0, lasso=0.0, regularization=0.0, keep_variables=False, classifier_trainable=False):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape)
    labels = Input(shape=(nclasses, ))
    layer_dims = [2, 4]
    optimizer = optimizers.adam(lr=1e-2)

    models = []
    losses = []
    classifiers = {}

    x = Mask(kernel_regularizer=l1(lasso))(input) if lasso > 0 else input

    classifier_layers = __classifier_layers(
        input_shape=K.int_shape(x)[1:], nclasses=nclasses, kernel_initializer=kernel_initializer,
        trainable=True, use_bias=True, regularization=regularization, bn=bn
    )
    c = Sequential(classifier_layers)(x)
    weighted_loss = AdaptiveClassWeight(labels)
    c, weights = weighted_loss(c)
    model = Model(input, c)
    model.compile(loss=weighted_categorical_crossentropy(weights), optimizer=optimizer, metrics=['acc'],
                  target_tensors=[labels])
    model.summary()
    models.append(model)
    losses.append(weighted_loss)

    for i, layer_dim in enumerate(layer_dims):
        branches = []
        for j in range(nclasses):
            branch = Convolution2D(
                layer_dim, (3, 3), padding='same', use_bias=not bn, kernel_initializer=kernel_initializer,
                kernel_regularizer=l2(regularization) if regularization > 0.0 else None
            )(x)
            branches.append(branch)
        x = Option(models[-1].output)(branches)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)
        x = MaxPool2D((2, 2))(x)
        if i > -20:
            classifier_layers = __classifier_layers(
                input_shape=K.int_shape(x)[1:], nclasses=nclasses, kernel_initializer=classifier_initializer,
                trainable=classifier_trainable, use_bias=classifier_trainable,
                regularization=regularization, bn=bn
            )
            if True:
                model = Model(input, x)
                model.trainable = False
                c = model(input)
                c = Sequential(classifier_layers)(c)
                weighted_loss = AdaptiveClassWeight(labels)
                c, weights = weighted_loss(c)
                model2 = Model(input, c)
                model2.compile(loss=weighted_categorical_crossentropy(weights), optimizer=optimizer, metrics=['acc'],
                              target_tensors=[labels])
                classifiers[len(models)] = model2
                model2.summary()
            classifier = Sequential(classifier_layers)
            classifier.trainable = False
            c = classifier(x)
            weighted_loss = AdaptiveClassWeight(labels, models[-1].output)
            c, weights = weighted_loss(c)
            model = Model(input, c)
            model.compile(loss=weighted_categorical_crossentropy(weights), optimizer=optimizer, metrics=['acc'],
                          target_tensors=[labels])
            losses.append(weighted_loss)
            models.append(model)
            model.summary()
            if not keep_variables:
                model = Model(input, x)
                model.trainable = False
                x = model(input)

    x = Flatten()(x)

    branches = []
    for j in range(nclasses):
        branch = Dense(
            128, use_bias=not bn, kernel_initializer=kernel_initializer,
            kernel_regularizer=l2(regularization) if regularization > 0.0 else None
        )(x)
        branches.append(branch)

    x = Option(models[-1].output)(branches)
    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    if dropout > 0.0:
        x = Dropout(dropout)(x)
    x = Activation('relu')(x)

    classifier_layers = __classifier_layers(
        input_shape=K.int_shape(x)[1:], nclasses=nclasses, kernel_initializer=classifier_initializer,
        trainable=classifier_trainable, use_bias=classifier_trainable,
        regularization=regularization, bn=bn
    )
    if True:
        model = Model(input, x)
        model.trainable = False
        c = model(input)
        c = Sequential(classifier_layers)(c)
        weighted_loss = AdaptiveClassWeight(labels)
        c, weights = weighted_loss(c)
        model2 = Model(input, c)
        model2.compile(loss=weighted_categorical_crossentropy(weights), optimizer=optimizer, metrics=['acc'],
                      target_tensors=[labels])
        classifiers[len(models)] = model2
        model2.summary()
    classifier = Sequential(classifier_layers)
    classifier.trainable = False
    c = classifier(x)
    weighted_loss = AdaptiveClassWeight(labels, models[-1].output)
    c, weights = weighted_loss(c)
    model = Model(input, c)
    model.compile(loss=weighted_categorical_crossentropy(weights), optimizer=optimizer, metrics=['acc'],
                  target_tensors=[labels])
    losses.append(weighted_loss)
    models.append(model)
    model.summary()

    return AccordionModel(models, classifiers, losses)


def cnnsimple2(input_shape, nclasses=2, bn=True, kernel_initializer='he_normal', classifier_initializer='he_normal',
               dropout=0.0, lasso=0.0, regularization=0.0, keep_variables=False, classifier_trainable=True):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape)
    labels = Input(shape=(nclasses, ))
    layer_dims = [2, 4]
    optimizer = optimizers.adam(lr=1e-2)

    models = []
    losses = []
    classifiers = {}

    x = Mask(kernel_regularizer=l1(lasso))(input) if lasso > 0 else input

    classifier = __create_classifier(
        input_shape=K.int_shape(x)[1:], nclasses=nclasses, kernel_initializer=kernel_initializer,
        trainable=True, use_bias=True, regularization=regularization if classifier_trainable else 0.0
    )
    c = classifier(x)
    weighted_loss = WeightedCategoricalCrossEntropy(nlabels=nclasses)
    model = Model(input, c)
    model.compile(loss=weighted_loss.get_loss(), optimizer=optimizer, metrics=['acc'])
    models.append(model)
    losses.append(weighted_loss)

    for i, layer_dim in enumerate(layer_dims):
        branches = []
        for j in range(nclasses):
            branch = Convolution2D(
                layer_dim, (3, 3), padding='same', use_bias=not bn, kernel_initializer=kernel_initializer,
                kernel_regularizer=l2(regularization) if regularization > 0.0 else None
            )(x)
            if bn:
                branch = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(branch)
            branches.append(branch)
        x = Option(models[-1].output)(branches)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)
        x = MaxPool2D((2, 2))(x)
        if i > -20:
            classifier = __create_classifier(
                input_shape=K.int_shape(x)[1:], nclasses=nclasses, kernel_initializer=classifier_initializer,
                trainable=classifier_trainable, use_bias=classifier_trainable,
                regularization=regularization if classifier_trainable else 0.0
            )
            classifier.trainable = False
            c = classifier(x)
            weighted_loss = WeightedCategoricalCrossEntropy(nlabels=nclasses, branches=nclasses)
            model = Model(input, c)
            model.compile(loss=weighted_loss.get_loss(models[-1].output), optimizer=optimizer, metrics=['acc'])
            losses.append(weighted_loss)
            models.append(model)
            if classifier_trainable:
                model = Model(input, x)
                model.trainable = False
                c = model(input)
                classifier.trainable = True
                c = classifier(c)
                weighted_loss = WeightedCategoricalCrossEntropy(nlabels=nclasses)
                model2 = Model(input, c)
                model2.compile(loss=weighted_loss.get_loss(), optimizer=optimizer, metrics=['acc'])
                classifiers[len(models) - 1] = model2
            if not keep_variables:
                model = Model(input, x)
                model.trainable = False
                x = model(input)

    x = Flatten()(x)

    branches = []
    for j in range(nclasses):
        branch = Dense(
            128, use_bias=not bn, kernel_initializer=kernel_initializer,
            kernel_regularizer=l2(regularization) if regularization > 0.0 else None
        )(x)
        if bn:
            branch = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(branch)
        branches.append(branch)

    x = Option(models[-1].output)(branches)
    if dropout > 0.0:
        x = Dropout(dropout)(x)
    x = Activation('relu')(x)

    classifier = __create_classifier(
        input_shape=K.int_shape(x)[1:], nclasses=nclasses, kernel_initializer=classifier_initializer,
        trainable=classifier_trainable, use_bias=classifier_trainable,
        regularization=regularization if classifier_trainable else 0.0
    )
    classifier.trainable = False
    c = classifier(x)
    weighted_loss = WeightedCategoricalCrossEntropy(nlabels=nclasses, branches=nclasses)
    model = Model(input, c)
    model.compile(loss=weighted_loss.get_loss(models[-1].output), optimizer=optimizer, metrics=['acc'])
    losses.append(weighted_loss)
    models.append(model)
    model.summary()
    if classifier_trainable:
        model = Model(input, x)
        model.trainable = False
        c = model(input)
        classifier.trainable = True
        c = classifier(c)
        weighted_loss = WeightedCategoricalCrossEntropy(nlabels=nclasses)
        model2 = Model(input, c)
        model2.compile(loss=weighted_loss.get_loss(), optimizer=optimizer, metrics=['acc'])
        classifiers[len(models) - 1] = model2

    return AccordionModel(models, classifiers, losses)


def cnnallconv(input_shape, nclasses=2, bn=True, kernel_initializer='he_normal',
                      dropout=0.0, lasso=0.0, regularization=0.0):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape)
    layer_dims = [16, 32, 64]

    x = Mask(kernel_regularizer=l1(lasso))(input) if lasso > 0 else input

    for layer_dim in layer_dims:
        x = Convolution2D(layer_dim, (3,3), padding='same', use_bias=not bn, kernel_initializer=kernel_initializer,
                          kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)
        x = Convolution2D(layer_dim, (3, 3), padding='same', strides=(2, 2), use_bias=not bn,
                          kernel_initializer=kernel_initializer,
                          kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)

    x = Convolution2D(128, (3, 3), padding='same', use_bias=not bn,
                      kernel_initializer=kernel_initializer,
                      kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    if dropout > 0.0:
        x = Dropout(dropout)(x)
    x = Activation('relu')(x)

    x = Convolution2D(nclasses, (3, 3), padding='same', use_bias=not bn,
                      kernel_initializer=kernel_initializer,
                      kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    x = Reduce(axis=(1,2), reduce_function='mean')(x)
    output = Activation('softmax')(x)

    model = Model(input, output)

    optimizer = optimizers.adam(lr=1e-2)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    model.saliency = saliencies.get_saliency('categorical_crossentropy', model)

    return model


def wrn164(
    input_shape, nclasses=2, bn=True, kernel_initializer='he_normal',
                      dropout=0.0, lasso=0.0, regularization=0.0, keep_variables=False
):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    ip = Input(shape=input_shape)

    x = Mask(kernel_regularizer=l1(lasso))(ip) if lasso > 0 else ip

    x = Convolution2D(
        16, (3, 3), padding='same', kernel_initializer=kernel_initializer,
        use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None
    )(x)

    l = 16
    k = 4

    output_channel_basis = [16, 32, 64]
    strides = [1, 2, 2]

    N = (l - 4) // 6

    for ocb, stride in zip(output_channel_basis, strides):
        x = wrn_block(
            x, ocb * k, N, strides=(stride, stride), dropout=dropout,
            regularization=regularization, kernel_initializer=kernel_initializer
        )

    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    x = Activation('relu')(x)

    deep_features = Reduce(axis=(1, 2), reduce_function='mean')(x)

    classifier = Dense(nclasses, kernel_initializer=kernel_initializer)

    output = classifier(deep_features)
    output = Activation('softmax')(output)

    model = Model(ip, output)

    optimizer = optimizers.SGD(lr=1e-1)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    model.saliency = saliencies.get_saliency('categorical_crossentropy', model)

    return model





# from keras.layers import Add, Activation, Dropout, Dense, Input, Flatten, MaxPooling2D
# from keras.layers.convolutional import Convolution2D
# from keras.models import Model
# from keras import optimizers
# from keras.layers.normalization import BatchNormalization
# from keras import backend as K
# from keras.regularizers import l2
# from keras_examples.accordion_network.custom.conditional import Case
#
#
# def conditional_dict(input, output_channels, nbranches, kernel_size=(3, 3), strides=(1, 1),
#                      regularization=0.0, kernel_initializer='he_normal'):
#
#     branches = dict()
#     for i in range(nbranches):
#         if kernel_size is not None:
#             branch_layer = Convolution2D(
#                 output_channels, kernel_size=kernel_size, padding='same', strides=strides,
#                 kernel_initializer=kernel_initializer,
#                 kernel_regularizer=l2(regularization) if regularization > 0.0 else None
#             )
#         else:
#             branch_layer = Dense(
#                 output_channels, kernel_initializer=kernel_initializer,
#                 kernel_regularizer=l2(regularization) if regularization > 0.0 else None
#             )
#         branches[i] = branch_layer(input)
#
#     return branches
#
#
# def conditional_block(input, output_channels, nbranches, kernel_size=(3, 3), strides=(1, 1),
#                       regularization=0.0, kernel_initializer='he_normal', dropout=0.0, bn=True):
#     channel_axis = 1 if K.image_data_format() == "channels_first" else -1
#
#     classifier = classifier_block(input, nbranches)
#
#     branches = conditional_dict(input, output_channels, nbranches, kernel_size, strides, regularization, kernel_initializer)
#
#     x = Case()([classifier, branches])
#
#     if bn:
#         x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
#
#     if dropout > 0.0:
#         x = Dropout(dropout)(x)
#
#     x = Activation('relu')(x)
#
#     return x, classifier
#
#
# def classifier_block(input, nbranches):
#     shape = K.int_shape(input)
#     data = Flatten()(input) if len(shape) > 2 else input
#     classifier = Dense(nbranches, kernel_initializer='orthogonal', use_bias=False, activation='softmax')
#     classifier.trainable = False
#     output = classifier(data)
#     return output
#
#
# def accordionsimple(input_size, nclasses, regularization=0.0, kernel_initializer='he_normal', dropout=0.0, bn=True):
#     input = Input(shape=input_size)
#
#     output_channels = [4, 4]
#     dense_output_channels = [200]
#     x = input
#     models = []
#
#     for output_channel in output_channels:
#         x, classifier = conditional_block(x, output_channel, nclasses, kernel_size=(3, 3), strides=(1, 1),
#                                           regularization=regularization, kernel_initializer=kernel_initializer,
#                                           dropout=dropout, bn=bn)
#         x = MaxPooling2D(pool_size=(2,2), strides=(1,1))(x)
#
#         model = Model(input, classifier)
#         optimizer = optimizers.adam(lr=1e-2)
#         model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])
#         models.append(model)
#         model.trainable = False
#
#     x = Flatten()(x)
#     for output_channel in dense_output_channels:
#         x, classifier = conditional_block(x, output_channel, nclasses, kernel_size=None,
#                                           regularization=regularization, kernel_initializer=kernel_initializer,
#                                           dropout=dropout, bn=bn)
#         model = Model(input, classifier)
#         optimizer = optimizers.adam(lr=1e-2)
#         model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])
#         models.append(model)
#         model.trainable = False
#
#     output = Dense(nclasses, kernel_initializer='orthogonal', use_bias=False, activation='softmax')(x)
#     model = Model(input, output)
#     optimizer = optimizers.adam(lr=1e-2)
#     model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])
#     models.append(model)
#
#     return models
