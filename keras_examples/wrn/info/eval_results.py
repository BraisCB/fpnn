import json
import numpy as np
import scipy.stats as stats


dataset = 'cifar10' # 'cifar10', 'cifar100' o 'stl10'
network = 'wrn_16_4'
anova_thresh = 1e-2

file = './keras_examples/info/json_files/' + dataset + '_' + network + '_acc_info.json'
plot = False

with open(file) as outfile:
    info_data = json.load(outfile)

accuracies = {}
keys = info_data.keys()
keys = sorted(keys)
for method in keys:
    if 'distance' in method or 'hinge' in method:
        continue
    print('method =', method)
    accuracy = info_data[method]
    accuracies[method] = accuracy
    print('accuracy =', 100 - np.mean(accuracy), '+-', np.std(accuracy), 'min', np.min(accuracy), 'max', np.max(accuracy))

sofmax_methods = []
for key in accuracies:
    if 'softmax' in key:
        sofmax_methods.append(key)

for key in sorted(accuracies):
    if 'softmax' in key:
        continue
    max_pval = 0
    acc_i = accuracies[key]
    for sofmax_method in sofmax_methods:
        acc_j = accuracies[sofmax_method]
        max_pval = max(max_pval, stats.f_oneway(acc_i, acc_j)[1])
    print(key, max_pval)
    if max_pval < anova_thresh:
        print(key, 'has significative results with p =', anova_thresh)
