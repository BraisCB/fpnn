import numpy as np
import sklearn.metrics as metrics
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import keras_examples.wrn.wide_residual_network as wrn
from keras.datasets import cifar10
import keras.callbacks as callbacks
import keras.utils.np_utils as kutils
from keras_examples.custom import losses, metrics as custom_metrics
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model, load_model
from keras.layers import Activation, Dense, AveragePooling2D, Flatten, Input, Reshape
from keras import backend as K
from keras import optimizers
import pickle
import json


batch_size = 128
nb_epoch = 200
img_rows, img_cols = 32, 32
nb_classes = 10
naugments = 10
l = 16
k = 4

output_losses = [
    #{'type': 'softplus_orthogonal_loss', 'data': {'R': 1.0, 'C': 1.0 / (nb_classes - 1.0)}},
    {'type': 'cross_entropy_loss'},
    # {'type': 'softplus_orthogonal_loss', 'data': {'R': 1.0, 'C': 1.0}},
    # {'type': 'exponential_orthogonal_loss', 'data': {'R': 0.4, 'C': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'softplus_loss', 'data': {'R': 1.0, 'C': 1.0}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 1.0, 'C': 1.0 / (nb_classes - 1.0)}},
    # {'type': 'trench_orthogonal_loss', 'data': {'R': 1.0, 'C': 1.0}},
]

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9
set_session(tf.Session(config=config))

reps = 3

trainable = True

(trainX, trainY), (testX, testY) = cifar10.load_data()

trainX = trainX.astype('float32')
trainX_mean = trainX.mean(axis=0)
trainX_std = trainX.std(axis=0)
trainX = (trainX - trainX_mean) / trainX_std
testX = testX.astype('float32')
testX = (testX - trainX_mean) / trainX_std

trainY = kutils.to_categorical(trainY)
testY = kutils.to_categorical(testY)

generator = ImageDataGenerator(width_shift_range=5./32,
                               height_shift_range=5./32,
                               fill_mode='reflect',
                               horizontal_flip=True)

init_shape = (3, 32, 32) if K.image_dim_ordering() == 'th' else (32, 32, 3)
input = Input(shape=init_shape)

# For WRN-16-8 put N = 2, k = 8
# For WRN-28-10 put N = 4, k = 10
# For WRN-40-4 put N = 6, k = 4


for i in range(reps):
    for drop in [0.3]:
        for output_loss in output_losses:
            for reduction_function in ['max', 'min']:
                for augmented in [False, True]:
                    for trainable in [False, True]:
                        name = 'cifar10_wrn_' + str(l) + '_' + str(k) +'_' + output_loss['type'] + \
                               '_d_' + str(drop) + '_t_' + str(trainable) + '_a_' + str(augmented) + \
                               '_f_' + reduction_function
                        if 'orthogonal' in output_loss['type'] and trainable:
                            continue
                        if not augmented and reduction_function != 'max':
                            continue
                        print('loss =', output_loss['type'], ', trainable = ', trainable, ', augmented = ', augmented)
                        if 'data' in output_loss:
                            if 'C' in output_loss['data']:
                                print('C =', output_loss['data']['C'])
                            if 'R' in output_loss['data']:
                                print('R =', output_loss['data']['R'])
                        kernel_initializer = 'he_normal'
                        regularization = 0.0005 # if output_loss['type'] == 'softmax' else 0.0
                        deep_features = wrn.create_cifar_wide_residual_network(
                            input, l=l, k=k, dropout=drop,
                            kernel_initializer=kernel_initializer, regularization=regularization
                        )
                        deep_features = AveragePooling2D((8, 8))(deep_features)
                        deep_features = Flatten()(deep_features)

                        kernel_initializer = 'orthogonal' if not trainable else 'he_normal'
                        extra_dims = naugments if augmented else 1
                        classifier = Dense(nb_classes*extra_dims, use_bias=trainable, kernel_initializer=kernel_initializer)
                        classifier.trainable = trainable

                        output = classifier(deep_features)
                        if augmented:
                            output = Reshape((nb_classes, extra_dims))(output)

                        model = Model(input, output)

                        model.summary()

                        def scheduler(epoch):
                            if epoch < 60:
                                return .1
                            elif epoch < 120:
                                return .02
                            elif epoch < 160:
                                return .004
                            else:
                                return .0008

                        change_lr = callbacks.LearningRateScheduler(scheduler)

                        if 'data' in output_loss:
                            if 'C' in output_loss['data']:
                                name += '_C_' + str(output_loss['data']['C'])
                            if 'R' in output_loss['data']:
                                name += '_R_' + str(output_loss['data']['R'])

                        optimizer = optimizers.sgd(lr=0.1, momentum=0.9, nesterov=True)

                        loss_data = {'R': 1.0, 'C': 1.0} if 'data' not in output_loss else output_loss['data']
                        loss = getattr(losses, output_loss['type'])(**loss_data)

                        acc_metric = custom_metrics.accuracy(augmented=augmented, reduction_function=reduction_function) # if output_loss['type'] == 'softmax' else losses.softmax_accuracy

                        model.compile(loss=loss, optimizer=optimizer, metrics=[acc_metric])
                        print("Finished compiling")

                        #model.load_weights("weights/WRN-16-8 Weights.h5")
                        print("Model loaded.")

                        history = model.fit_generator(
                            generator.flow(trainX, trainY, batch_size=batch_size),
                            steps_per_epoch=len(trainX) // batch_size, epochs=nb_epoch,
                            callbacks=[
                                # callbacks.ModelCheckpoint(
                                #     './keras_examples/weights/' + name + '_Weights.h5',
                                #     monitor="val_acc",
                                #     save_best_only=True,
                                #     verbose=1
                                # ),
                                callbacks.LearningRateScheduler(scheduler)
                            ],
                            validation_data=(testX, testY),
                            validation_steps=testX.shape[0] // batch_size,
                            verbose=2
                        )

                        # model.save('./keras_examples/weights/' + name + '_model.h5')
                        # with open('./keras_examples/weights/cifar10_' + name + '_history_' + str(i) + '.h5', 'wb') as file_pi:
                        #     pickle.dump(history.history, file_pi)

                        acc_name = './keras_examples/info/json_files/augmented_cifar10_wrn_' + str(l) + '_' + str(k) + '_accuracies.json'

                        try:
                            with open(acc_name) as outfile:
                                info_data = json.load(outfile)
                        except:
                            info_data = {}

                        if name not in info_data:
                            info_data[name] = {}

                        yPreds = model.predict(testX)
                        for measure_function in ['min', 'max', 'mean']:
                            if augmented and measure_function != 'max':
                                continue
                            if measure_function not in info_data[name]:
                                info_data[name][measure_function] = []
                            f = getattr(np, measure_function)
                            yPred = f(yPreds, axis=-1) if augmented else yPreds
                            yPred = np.argmax(yPred, axis=1)
                            yPred = kutils.to_categorical(yPred)
                            accuracy = metrics.accuracy_score(testY, yPred) * 100
                            error = 100 - accuracy
                            print('Function : ', measure_function)
                            print("Accuracy : ", accuracy)
                            print("Error : ", error)
                            info_data[name][measure_function].append(accuracy)

                        with open(acc_name, 'w') as outfile:
                            json.dump(info_data, outfile)
