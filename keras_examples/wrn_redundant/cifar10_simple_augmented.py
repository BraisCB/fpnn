import numpy as np
import sklearn.metrics as metrics
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import keras_examples.wrn.wide_residual_network as wrn
from keras.datasets import cifar10
import keras.callbacks as callbacks
import keras.utils.np_utils as kutils
from keras_examples.custom import losses, metrics as custom_metrics
from keras_examples.custom.layers import Reduce
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model, load_model
from keras.layers import Activation, Dense, AveragePooling2D, Flatten, Input, Reshape, Convolution2D, MaxPooling2D, Dropout
from keras import backend as K
from keras import optimizers
import pickle
from keras.regularizers import l2
from keras.layers.normalization import BatchNormalization
import json


batch_size = 128
nb_epoch = 40
img_rows, img_cols = 32, 32
nb_classes = 10
max_augments = 10
dropout = 0.0


def create_model(input_shape, nclasses=10, augments=1, dropout=0.0, regularization=0.0, bn=True,
                 loss='square_hinge', loss_kwargs=None, metrics=None):

    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape)

    kernel_initializer = 'truncated_normal'

    x = Convolution2D(32, (3, 3), padding='same', kernel_initializer=kernel_initializer,
                      use_bias=not bn, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(input)

    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)

    x = Activation('relu')(x)

    x = MaxPooling2D((2, 2))(x)

    x = Convolution2D(32, (3, 3), padding='same', kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)

    x = Activation('relu')(x)

    x = MaxPooling2D((2, 2))(x)

    x = Convolution2D(32, (3, 3), padding='same', kernel_initializer=kernel_initializer,
                      use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)

    x = Activation('relu')(x)

    x = MaxPooling2D((2, 2))(x)

    x = Flatten()(x)

    x = Dense(4*4*32, use_bias=False, kernel_initializer=kernel_initializer,
              kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)

    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)

    x = Activation('relu')(x)

    if dropout > 0.0:
        x = Dropout(dropout)(x)

    last_layer = Dense(nclasses*augments, use_bias=False, kernel_initializer='orthogonal',
              kernel_regularizer=None)

    last_layer.trainable = False

    x = last_layer(x)

    output = Reshape((augments, nclasses))(x)

    loss = getattr(losses, loss)() if loss_kwargs is None else getattr(losses, loss)(**loss_kwargs)

    metrics = [] if metrics is None else metrics

    model = Model(input, output)

    optimizer = optimizers.adam(lr=1e-2)

    labels = Input(shape=(nclasses, ))

    model.compile(loss=loss, optimizer=optimizer, metrics=metrics, target_tensors=[labels])

    return model


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9
set_session(tf.Session(config=config))

reps = 1

trainable = True

(trainX, trainY), (testX, testY) = cifar10.load_data()

trainX = trainX.astype('float32')
trainX_mean = trainX.mean(axis=0)
trainX_std = trainX.std(axis=0)
trainX = (trainX - trainX_mean) / trainX_std
testX = testX.astype('float32')
testX = (testX - trainX_mean) / trainX_std

trainY = kutils.to_categorical(trainY)
testY = kutils.to_categorical(testY)

testY_label = np.argmax(testY, axis=-1)

generator = ImageDataGenerator(width_shift_range=5./32,
                               height_shift_range=5./32,
                               fill_mode='reflect',
                               horizontal_flip=True)

init_shape = (3, 32, 32) if K.image_dim_ordering() == 'th' else (32, 32, 3)

metrics = [
    custom_metrics.max_accuracy(augmented_index=1, reduction_function='max'),
    custom_metrics.max_accuracy(augmented_index=1, reduction_function='min'),
    custom_metrics.max_accuracy(augmented_index=1, reduction_function='mean'),
]

np_metrics = [
    'max', 'min', 'mean', 'median'
]

loss_kwargs = {
    'R': 1.0,
    'lamda': 1.0,
    'reduction_function_pos': 'sum',
    'reduction_function_neg': 'sum',
    'augmented_function_neg': 'max',
    'augmented_index': 1
}


for i in range(reps):
    for augments in range(1, max_augments + 1):
        for reduction_function in ['max', 'min']:
            print('augments', augments)
            print('reduction function', reduction_function)
            loss_kwargs['augmented_function_pos'] = reduction_function
            regularization = 0.0 # if output_loss['type'] == 'softmax' else 0.0
            model = create_model(init_shape, nclasses=10, augments=augments, dropout=dropout,
                                 regularization=regularization, bn=True,
                                 loss='square_hinge', loss_kwargs=loss_kwargs, metrics=metrics)

            model.summary()

            def scheduler(epoch):
                if epoch < 10:
                    return .01
                elif epoch < 20:
                    return .02
                elif epoch < 30:
                    return .004
                else:
                    return .0008

            change_lr = callbacks.LearningRateScheduler(scheduler)


            history = model.fit_generator(
                generator.flow(trainX, trainY, batch_size=batch_size),
                steps_per_epoch=len(trainX) // batch_size, epochs=nb_epoch,
                callbacks=[
                    # callbacks.ModelCheckpoint(
                    #     './keras_examples/weights/' + name + '_Weights.h5',
                    #     monitor="val_acc",
                    #     save_best_only=True,
                    #     verbose=1
                    # ),
                    callbacks.LearningRateScheduler(scheduler)
                ],
                validation_data=(testX, testY),
                validation_steps=testX.shape[0] // batch_size,
                verbose=2
            )

            # model.save('./keras_examples/weights/' + name + '_model.h5')
            # with open('./keras_examples/weights/cifar10_' + name + '_history_' + str(i) + '.h5', 'wb') as file_pi:
            #     pickle.dump(history.history, file_pi)

            acc_name = './keras_examples/wrn_redundant/info/augmented_cifar10_basic_accuracies.json'

            try:
                with open(acc_name) as outfile:
                    info_data = json.load(outfile)
            except:
                info_data = {}

            if augments not in info_data:
                info_data[augments] = {}

            if reduction_function not in info_data[augments]:
                info_data[augments][reduction_function] = {}

            yPred = model.predict(testX)

            for measure_function in np_metrics:
                print('Function : ', measure_function)
                func = getattr(np, measure_function)
                reduced_yPred = func(yPred, axis=1)
                accuracy = np.mean(np.equal(np.argmax(reduced_yPred, axis=-1), testY_label))

                if measure_function not in info_data[augments][reduction_function]:
                    info_data[augments][reduction_function][measure_function] = []

                info_data[augments][reduction_function][measure_function].append(accuracy)
                print("Accuracy : ", accuracy)

            with open(acc_name, 'w') as outfile:
                json.dump(info_data, outfile)
