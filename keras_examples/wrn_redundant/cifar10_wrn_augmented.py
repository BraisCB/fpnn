import numpy as np
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import keras_examples.wrn.wide_residual_network as wrn
from keras.datasets import cifar100
import keras.callbacks as callbacks
import keras.utils.np_utils as kutils
from keras_examples.custom import losses, metrics as custom_metrics
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.layers import Dense, AveragePooling2D, Flatten, Input, Reshape, Convolution2D
from keras import backend as K, optimizers
from keras.regularizers import l2
import json


batch_size = 128
nb_epoch = 200
img_rows, img_cols = 32, 32
nb_classes = 100
max_augments = 2
dropout = 0.3
l = 16
k = 4
axis = (1, 2, 3)


def create_wrn_model(input_shape, l=16, k=4, nclasses=10, augments=1, dropout=0.0, regularization=0.0,
                     kernel_initializer='he_normal', loss='square_softplus_2', loss_kwargs=None, metrics=None):

    input = Input(shape=input_shape)
    deep_features = wrn.create_cifar_wide_residual_network(
        input, l=l, k=k, dropout=dropout,
        kernel_initializer=kernel_initializer, regularization=regularization
    )

    classifier = Convolution2D(nclasses * augments, (4, 4), use_bias=False, kernel_initializer='orthogonal',
                       kernel_regularizer=None)

    # deep_features = Flatten()(deep_features)
    # kernel_initializer = 'orthogonal'
    # classifier = Dense(nb_classes * augments, use_bias=False, kernel_initializer=kernel_initializer)

    classifier.trainable = False

    output = classifier(deep_features)

    output = Reshape((5, 5, augments, nclasses))(output)

    # output = AveragePooling2D((4, 4))(output)

    # output = Reshape((augments, nclasses))(output)

    loss = getattr(losses, loss)() if loss_kwargs is None else getattr(losses, loss)(**loss_kwargs)

    metrics = [] if metrics is None else metrics

    model = Model(input, output)

    optimizer = optimizers.SGD(lr=1e-1)

    labels = Input(shape=(nclasses, ))

    model.compile(loss=loss, optimizer=optimizer, metrics=metrics, target_tensors=[labels])

    return model


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.5
set_session(tf.Session(config=config))

reps = 1

trainable = True

(trainX, trainY), (testX, testY) = cifar100.load_data()

trainX = trainX.astype('float32')
trainX_mean = trainX.mean(axis=0)
trainX_std = trainX.std(axis=0)
trainX = (trainX - trainX_mean) / trainX_std
testX = testX.astype('float32')
testX = (testX - trainX_mean) / trainX_std

trainY = kutils.to_categorical(trainY)
testY = kutils.to_categorical(testY)

testY_label = np.argmax(testY, axis=-1)

generator = ImageDataGenerator(width_shift_range=5./32,
                               height_shift_range=5./32,
                               fill_mode='reflect',
                               horizontal_flip=True)

init_shape = (3, 32, 32) if K.image_dim_ordering() == 'th' else (32, 32, 3)

metrics = [
    custom_metrics.max_accuracy(augmented_index=axis, reduction_function='max'),
    custom_metrics.max_accuracy(augmented_index=axis, reduction_function='min'),
    custom_metrics.max_accuracy(augmented_index=axis, reduction_function='mean'),
]

np_metrics = [
    'max', 'min', 'mean', 'median'
]

loss_kwargs = {
    'R': 1.0,
    'lamda': 1.0,
    'reduction_function_pos': 'sum',
    'reduction_function_neg': 'sum',
    'augmented_index': axis
}


for i in range(reps):
    for augments in range(max_augments, 0, -4):
        for reduction_function_pos in ['mean', 'sum', 'min', 'max']:
            for reduction_function_neg in ['mean', 'sum', 'min', 'max']:
                if (reduction_function_neg != reduction_function_pos and not (reduction_function_pos == 'min' and reduction_function_neg == 'mean')) or \
                        (reduction_function_neg == reduction_function_pos and reduction_function_pos == 'min'):
                    continue
                print('augments', augments)
                print('reduction function pos', reduction_function_pos)
                loss_kwargs['augmented_function_pos'] = reduction_function_pos
                print('reduction function neg', reduction_function_neg)
                loss_kwargs['augmented_function_neg'] = reduction_function_neg
                regularization = 0.0002 # if output_loss['type'] == 'softmax' else 0.0
                model = create_wrn_model(init_shape, l=l, k=k, nclasses=nb_classes, augments=augments, dropout=dropout,
                                     regularization=regularization, kernel_initializer='he_normal',
                                     loss='square_softplus_2', loss_kwargs=loss_kwargs, metrics=metrics)

                model.summary()

                def scheduler(epoch):
                    if epoch < 60:
                        return .1
                    elif epoch < 120:
                        return .02
                    elif epoch < 160:
                        return .004
                    else:
                        return .0008

                change_lr = callbacks.LearningRateScheduler(scheduler)

                history = model.fit_generator(
                    generator.flow(trainX, trainY, batch_size=batch_size),
                    steps_per_epoch=len(trainX) // batch_size, epochs=nb_epoch,
                    callbacks=[
                        # callbacks.ModelCheckpoint(
                        #     './keras_examples/weights/' + name + '_Weights.h5',
                        #     monitor="val_acc",
                        #     save_best_only=True,
                        #     verbose=1
                        # ),
                        callbacks.LearningRateScheduler(scheduler)
                    ],
                    validation_data=(testX, testY),
                    validation_steps=testX.shape[0] // batch_size,
                    verbose=2
                )

                # model.save('./keras_examples/weights/' + name + '_model.h5')
                # with open('./keras_examples/weights/cifar10_' + name + '_history_' + str(i) + '.h5', 'wb') as file_pi:
                #     pickle.dump(history.history, file_pi)

                acc_name = './keras_examples/wrn_redundant/info/cifar100_wrn_' + str(l) + '_' + str(k) + '_accuracies.json'

                try:
                    with open(acc_name) as outfile:
                        info_data = json.load(outfile)
                except:
                    info_data = {}

                if str(augments) not in info_data:
                    info_data[str(augments)] = {}

                key = reduction_function_pos + '_' + reduction_function_neg

                if key not in info_data[str(augments)]:
                    info_data[str(augments)][key] = {}

                yPred = model.predict(testX)

                for measure_function in np_metrics:
                    print('Function : ', measure_function)
                    func = getattr(np, measure_function)
                    reduced_yPred = func(yPred, axis=axis)
                    accuracy = np.mean(np.equal(np.argmax(reduced_yPred, axis=-1), testY_label))

                    if measure_function not in info_data[str(augments)][key]:
                        info_data[str(augments)][key][measure_function] = []

                    info_data[str(augments)][key][measure_function].append(accuracy)
                    print("Accuracy : ", accuracy)

                with open(acc_name, 'w') as outfile:
                    json.dump(info_data, outfile)
