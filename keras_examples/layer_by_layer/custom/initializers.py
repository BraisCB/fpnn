import numpy as np
from keras import backend as K


def flatten_basis(function='orthogonal'):
    def close(shape, dtype=None):
        basis = np.random.randn(shape[1], shape[0])
        for i in range(1, len(basis)):
            basis[i] = (np.random.randn(1, shape[0])*0.05/0.95 + 1.0) * basis[0]
        norm = np.linalg.norm(basis, axis=1, keepdims=True)
        basis /= norm
        return K.constant(basis.T, dtype=dtype, shape=shape)
    if function == 'close':
        return close
    else:
        return function
