import numpy as np
from keras import backend as K


class LayerByLayerModel:

    def __init__(self, models):
        self.models = models

    def fit(self, X, y, X_test=None, y_test=None, type='all', data_generator=None, batch_size=128, epochs=100,
            callbacks=None, class_weight=None, class_weight_momentum=None):
        callbacks = {} if callbacks is None else callbacks
        if type == 'all':
            return self.__fit_all_models_at_a_time(
                X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, class_weight, class_weight_momentum
            )
        elif type == 'one_by_one':
            return self.__fit_one_model_at_a_time(
                X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, class_weight, class_weight_momentum
            )
        else:
            raise Exception('type not supported')

    def __fit_one_model_at_a_time(
            self, X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, class_weight, class_weight_momentum
    ):
        lenX = len(X)

        if data_generator is None:
            perm = np.arange(0, lenX, dtype=int)
        else:
            generator = data_generator.flow(X, y, batch_size=batch_size)

        if class_weight_momentum is not None:
            frequency = 1.0 / y.shape[-1] * np.ones(y.shape[-1])
            class_weight = np.min(frequency) / frequency
            bins = np.arange(y.shape[-1] + 1, dtype=int)

        train_info = []
        for model in self.models:
            train_info.append({})
            for metric_name in model.metrics_names:
                train_info[-1][metric_name] = []

        test_info = None
        if X_test is not None and y_test is not None:
            test_info = []
            for model in self.models:
                test_info.append({})
                for metric_name in model.metrics_names:
                    test_info[-1][metric_name] = []

        learning_rate = {} if 'learning_rate' not in callbacks else callbacks['learning_rate']

        for i, model in enumerate(self.models):
            for epoch in range(epochs):
                if epoch in learning_rate:
                    print('Changing learning rate to', learning_rate[epoch])
                    if isinstance(learning_rate[epoch], list):
                        K.set_value(model.optimizer.lr, learning_rate[epoch][i])
                    else:
                        K.set_value(model.optimizer.lr, learning_rate[epoch])
                index = 0
                if data_generator is None:
                    np.random.shuffle(perm)
                train_info_per_epoch = {}
                for metric_name in model.metrics_names:
                    train_info_per_epoch[metric_name] = []
                while index + batch_size <= lenX:
                    new_index = index + batch_size
                    if data_generator is None:
                        batch_X, batch_y = X[perm[index:new_index]], y[perm[index:new_index]]
                    else:
                        batch_X, batch_y = next(generator)
                    batch_train_info = model.train_on_batch(batch_X, batch_y, class_weight=class_weight)
                    if class_weight_momentum is not None:
                        batch_frequency = np.histogram(np.argmax(batch_y, axis=-1), bins=bins, density=True)[0]
                        frequency = class_weight_momentum * frequency + (1.0 - class_weight_momentum) * batch_frequency
                        frequency = np.clip(frequency, 1e-4, 1.0)
                        class_weight = np.min(frequency) / frequency
                    for metric_name, metric_value in zip(model.metrics_names, batch_train_info):
                        train_info_per_epoch[metric_name].append(metric_value)
                    index = new_index
                print('EPOCH : ', epoch)
                for metric_name in model.metrics_names:
                    print('Train Model', i, metric_name, ': ', np.mean(train_info_per_epoch[metric_name]))
                    train_info[i][metric_name].append(train_info_per_epoch[metric_name])
                if test_info is not None:
                    test_info_per_epoch = model.evaluate(X_test, y_test, batch_size=batch_size, verbose=0)
                    for metric_name, metric_value in zip(model.metrics_names, test_info_per_epoch):
                        print('Test Model', i, metric_name, ': ', metric_value)
                        test_info[i][metric_name].append(metric_value)

        output = {
            'train': train_info
        }

        if test_info is not None:
            output['test'] = test_info

        return output

    def __fit_all_models_at_a_time(
            self, X, y, X_test, y_test, data_generator, batch_size, epochs, callbacks, class_weight, class_weight_momentum
    ):
        lenX = len(X)

        if data_generator is None:
            perm = np.arange(0, lenX, dtype=int)
        else:
            generator = data_generator.flow(X, y, batch_size=batch_size)

        if class_weight_momentum is not None:
            frequency = 1.0 / y.shape[-1] * np.ones(y.shape[-1])
            class_weight = np.min(frequency) / frequency
            bins = np.arange(y.shape[-1] + 1, dtype=int)

        train_info = []
        for model in self.models:
            train_info.append({})
            for metric_name in model.metrics_names:
                train_info[-1][metric_name] = []

        test_info = None
        if X_test is not None and y_test is not None:
            test_info = []
            for model in self.models:
                test_info.append({})
                for metric_name in model.metrics_names:
                    test_info[-1][metric_name] = []

        learning_rate = {} if 'learning_rate' not in callbacks else callbacks['learning_rate']

        for epoch in range(epochs):
            if epoch in learning_rate:
                print('Changing learning rate to', learning_rate[epoch])
                for i, model in enumerate(self.models):
                    if isinstance(learning_rate[epoch], list):
                        K.set_value(model.optimizer.lr, learning_rate[epoch][i])
                    else:
                        K.set_value(model.optimizer.lr, learning_rate[epoch])
            index = 0
            if data_generator is None:
                np.random.shuffle(perm)

            train_info_per_epoch = []
            for model in self.models:
                train_info_per_epoch.append({})
                for metric_name in model.metrics_names:
                    train_info_per_epoch[-1][metric_name] = []
            while index + batch_size <= lenX:
                new_index = index + batch_size
                if data_generator is None:
                    batch_X, batch_y = X[perm[index:new_index]], y[perm[index:new_index]]
                else:
                    batch_X, batch_y = next(generator)
                for i, model in enumerate(self.models):
                    batch_train_info = model.train_on_batch(batch_X, batch_y, class_weight=class_weight)
                    if class_weight_momentum is not None:
                        batch_frequency = np.histogram(np.argmax(batch_y, axis=-1), bins=bins, density=True)[0]
                        frequency = class_weight_momentum * frequency + (1.0 - class_weight_momentum) * batch_frequency
                        frequency = np.clip(frequency, 1e-4, 1.0)
                        class_weight = np.min(frequency) / frequency
                    for metric_name, metric_value in zip(model.metrics_names, batch_train_info):
                        train_info_per_epoch[i][metric_name].append(metric_value)
                index = new_index
            print('EPOCH : ', epoch)
            for i, model in enumerate(self.models):
                for metric_name in model.metrics_names:
                    print('Train Model', i, metric_name, ': ', np.mean(train_info_per_epoch[i][metric_name]))
                    train_info[i][metric_name].append(train_info_per_epoch[i][metric_name])
                if test_info is not None:
                    test_info_per_epoch = model.evaluate(X_test, y_test, batch_size=batch_size, verbose=0)
                    for metric_name, metric_value in zip(model.metrics_names, test_info_per_epoch):
                        print('Test Model', i, metric_name, ': ', metric_value)
                        test_info[i][metric_name].append(metric_value)

        output = {
            'train': train_info
        }

        if test_info is not None:
            output['test'] = test_info

        return output
