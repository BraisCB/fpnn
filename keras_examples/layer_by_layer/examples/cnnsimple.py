from keras_examples.layer_by_layer.datasets import load_dataset
from keras_examples.layer_by_layer import network_models
import json
import numpy as np


if __name__ == '__main__':
    filename = './keras_examples/layer_by_layer/info/cnnsimple_info.json'
    datasets = ['mnist']
    regularization = 0.0
    lasso = 0.0
    kernel_initializer = 'he_normal'
    classifier_initializer = 'he_normal'
    bn = False
    dropout = 0.0
    batch_size = 128
    epochs = 40
    class_weight_momentum = 0.95
    callbacks = {
        'learning_rate': {
            12: 0.002,
            24: 0.0004,
            36: 0.00008
        }
    }

    for dataset_name in datasets:
        dataset = load_dataset(dataset_name)
        input_shape = dataset['train']['data'].shape[1:]
        nclasses = dataset['train']['label'].shape[-1]
        class_weight = np.ones(nclasses)

        for keep_variables in [False, True]:
            print('KEEP VARIABLES : ', keep_variables)
            for type in ['all', 'one_by_one']:

                model = network_models.cnnsimple(
                    input_shape=input_shape, nclasses=nclasses, bn=bn, kernel_initializer=kernel_initializer,
                    classifier_initializer=classifier_initializer,
                    dropout=dropout, lasso=lasso, regularization=regularization, keep_variables=keep_variables
                )

                info = model.fit(
                    dataset['train']['data'], dataset['train']['label'],
                    dataset['test']['data'], dataset['test']['label'],
                    type=type, data_generator=dataset['generator'],
                    batch_size=batch_size, epochs=epochs, callbacks=callbacks,
                    class_weight=class_weight, class_weight_momentum=class_weight_momentum
                )
                with open(filename, 'w') as outfile:
                    json.dump(info, outfile)
