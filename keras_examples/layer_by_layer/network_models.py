from keras.models import Model, Sequential
from keras import backend as K, optimizers
from keras.layers import Dense, Activation, BatchNormalization, Dropout, Input, Flatten, Convolution2D, MaxPool2D
from keras.regularizers import l1, l2
from keras_examples.custom.layers import Mask, Reduce
from keras_examples.feature_selection.custom import saliencies
from keras_examples.wrn.wide_residual_network import wrn_block
from keras_examples.layer_by_layer.custom.initializers import flatten_basis
from keras_examples.layer_by_layer.custom.models import LayerByLayerModel


def cnnsimple(input_shape, nclasses=2, bn=False, kernel_initializer='he_normal', classifier_initializer='orthogonal',
                 dropout=0.0, lasso=0.0, regularization=0.0, keep_variables=False):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape)
    layer_dims = [2, 4]
    optimizer = optimizers.adam(lr=1e-2)

    models = []

    x = Mask(kernel_regularizer=l1(lasso))(input) if lasso > 0 else input

    for i, layer_dim in enumerate(layer_dims):
        x = Convolution2D(
            layer_dim, (3,3), padding='same', use_bias=not bn, kernel_initializer=kernel_initializer,
            kernel_regularizer=l2(regularization) if regularization > 0.0 else None
        )(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)
        x = MaxPool2D((2, 2))(x)
        if i > -20:
            c = Flatten()(x)
            c = Dense(128, use_bias=not bn, kernel_initializer=kernel_initializer,
                      kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(c)
            if bn:
                c = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(c)
            c = Activation('relu')(c)
            c = Dense(nclasses, use_bias=False, kernel_initializer=flatten_basis(classifier_initializer), trainable=False)(c)
            c = Activation('softmax')(c)
            model = Model(input, c)
            model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])
            models.append(model)
            if not keep_variables:
                model = Model(input, x)
                model.trainable = False
                x = model(input)

    x = Flatten()(x)

    x = Dense(128, use_bias=not bn, kernel_initializer=kernel_initializer,
              kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    if dropout > 0.0:
        x = Dropout(dropout)(x)
    x = Activation('relu')(x)

    x = Dense(nclasses, use_bias=False, kernel_initializer=flatten_basis(classifier_initializer), trainable=False)(x)
    output = Activation('softmax')(x)

    model = Model(input, output)

    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])
    models.append(model)

    model.summary()

    return LayerByLayerModel(models)


def cnnallconv(input_shape, nclasses=2, bn=True, kernel_initializer='he_normal',
                      dropout=0.0, lasso=0.0, regularization=0.0):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape)
    layer_dims = [16, 32, 64]

    x = Mask(kernel_regularizer=l1(lasso))(input) if lasso > 0 else input

    for layer_dim in layer_dims:
        x = Convolution2D(layer_dim, (3,3), padding='same', use_bias=not bn, kernel_initializer=kernel_initializer,
                          kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)
        x = Convolution2D(layer_dim, (3, 3), padding='same', strides=(2, 2), use_bias=not bn,
                          kernel_initializer=kernel_initializer,
                          kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)

    x = Convolution2D(128, (3, 3), padding='same', use_bias=not bn,
                      kernel_initializer=kernel_initializer,
                      kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    if bn:
        x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    if dropout > 0.0:
        x = Dropout(dropout)(x)
    x = Activation('relu')(x)

    x = Convolution2D(nclasses, (3, 3), padding='same', use_bias=not bn,
                      kernel_initializer=kernel_initializer,
                      kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    x = Reduce(axis=(1,2), reduce_function='mean')(x)
    output = Activation('softmax')(x)

    model = Model(input, output)

    optimizer = optimizers.adam(lr=1e-2)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    model.saliency = saliencies.get_saliency('categorical_crossentropy', model)

    return model


def wrn164(
    input_shape, nclasses=2, bn=True, kernel_initializer='he_normal',
                      dropout=0.0, lasso=0.0, regularization=0.0, keep_variables=False
):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    ip = Input(shape=input_shape)

    x = Mask(kernel_regularizer=l1(lasso))(ip) if lasso > 0 else ip

    x = Convolution2D(
        16, (3, 3), padding='same', kernel_initializer=kernel_initializer,
        use_bias=False, kernel_regularizer=l2(regularization) if regularization > 0.0 else None
    )(x)

    l = 16
    k = 4

    output_channel_basis = [16, 32, 64]
    strides = [1, 2, 2]

    N = (l - 4) // 6

    for ocb, stride in zip(output_channel_basis, strides):
        x = wrn_block(
            x, ocb * k, N, strides=(stride, stride), dropout=dropout,
            regularization=regularization, kernel_initializer=kernel_initializer
        )

    x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
    x = Activation('relu')(x)

    deep_features = Reduce(axis=(1, 2), reduce_function='mean')(x)

    classifier = Dense(nclasses, kernel_initializer=kernel_initializer)

    output = classifier(deep_features)
    output = Activation('softmax')(output)

    model = Model(ip, output)

    optimizer = optimizers.SGD(lr=1e-1)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    model.saliency = saliencies.get_saliency('categorical_crossentropy', model)

    return model


