from sklearn.datasets import fetch_rcv1
from keras import backend as K, callbacks
import numpy as np
import json
from keras_examples.text_classification import network_models
from keras_examples.shared.generators import SparseGenerator
from copy import deepcopy
import os
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from keras.utils import to_categorical
from scipy.sparse import csr_matrix
from sklearn.cluster import k_means_, SpectralClustering, KMeans
from sklearn.metrics.pairwise import cosine_similarity
from keras_examples.text_classification.custom.wkmeans import KPlusPlus



os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

reps = 2
regularization = 0.0
epochs = 40
batch_size = 500

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
set_session(tf.Session(config=config))


def scheduler(epoch):
    if epoch < 13:
        return 0.01
    elif epoch < 26:
        return 0.002
    else:
        return 0.0004

directory = './keras_examples/text_classification/datasets/'
output_directory = './keras_examples/text_classification/info/'
embeddings_file = './keras_examples/text_classification/embeddings/embeddings.npy'
targets = ['GCAT']

network_names = ['dense']


def KMeans_cosine_fit(n_clusters=10, n_jobs=-1, random_state=None):
    # Manually override euclidean
    def cos_dist(X, Y = None, Y_norm_squared = None, squared = False):
        #return pairwise_distances(X, Y, metric = 'cosine', n_jobs = 10)
        return np.exp(-cosine_similarity(X, Y))
    k_means_.euclidean_distances = cos_dist
    kmeans = k_means_.KMeans(n_clusters=n_clusters, n_jobs=n_jobs, random_state=random_state)
    return kmeans


if __name__ == '__main__':
    os.chdir('../../')
    # IMPORT EMBEDDINGS
    embeddings_matrix = np.load(embeddings_file)
    embeddings_matrix /= np.maximum(1e-6, np.sqrt(np.sum(embeddings_matrix * embeddings_matrix, axis=-1, keepdims=True)))

    nonzero_values = np.where(np.abs(embeddings_matrix).sum(axis=-1) > 0.)[0]
    zero_values = np.where(np.abs(embeddings_matrix).sum(axis=-1) == 0.)[0]
    print('Words without embedding : ', embeddings_matrix.shape[0] - nonzero_values.shape[0])

    rcv1 = fetch_rcv1(data_home=directory)

    target_pos = []
    for target in targets:
        target_pos.append(np.where(rcv1.target_names == target)[0][0])

    pos = np.argsort(target_pos)
    targets = [targets[p] for p in pos]

    data = rcv1.data
    print('Freq : ', np.sum(data[:, zero_values], axis=0))
    data[data > 0.] = 1.
    freqs = np.sum(data, axis=-1)
    freqs = np.ones(data.shape[0])
    result = to_categorical(rcv1.target[:, target_pos].toarray())

    features = [50, 100, 200, 300, 500, 750, 1000] #, 1500, 2000, 5000, 10000, data.shape[-1]]
    # features = [data.shape[-1]]

    filename = output_directory + 'baseline_rank_chi2.json'
    result_filename = output_directory + 'baseline_results_kmeans.json'

    target_key = '_'.join(targets)

    with open(filename) as outfile:
        ranks = json.load(outfile)[target_key]


    for kfold, example in enumerate(ranks):

        print('KFOLD : ', kfold)
        train_data = data[example['train_index']]
        train_result = result[example['train_index']]

        nnz = np.count_nonzero(train_result[:, -1]) / train_result.shape[0]
        class_weight=[1./(1. - nnz), 1./nnz]
        class_weight /= np.max(class_weight)
        print('pos values : ', nnz)
        print('neg values : ', 1. - nnz)
        print('class_weight : ', class_weight)

        valid_data = data[example['test_index']]
        valid_result = result[example['test_index']]

        for network_name in network_names:

            print('NETWORK : ', network_name)
            if 'regularization' in example:
                print('regularization : ', example['regularization'])
            if 'gamma' in example:
                print('gamma : ', example['gamma'])
            output = deepcopy(example)
            results = []
            predictions = []
            rank = np.array(example['rank']).astype(int)
            for n_features in features:
                print('nfeatures : ', n_features)
                print('computing centers')
                kmeans = KPlusPlus(
                    K=n_features - zero_values.shape[0],
                    X=embeddings_matrix[nonzero_values],
                    c=freqs[nonzero_values],
                    max_diff=0.00001
                )
                kmeans.init_centers()
                print('computing KMeans')
                kmeans = KMeans_cosine_fit(n_clusters=n_features - zero_values.shape[0]).fit(embeddings_matrix[nonzero_values])
                # kmeans.find_centers(method='++')
                print('Kmeans computed')
                weights = np.zeros(embeddings_matrix.shape[0])
                weights[zero_values] = np.arange(zero_values.shape[0]).astype(int)
                weights[nonzero_values] = kmeans.cluster_indices + zero_values.shape[0]
                weights = csr_matrix(to_categorical(weights, num_classes=n_features))
                data_min = train_data.dot(weights).astype(float)
                data_min[data_min > 1.] = 1.
                valid_data_min = valid_data.dot(weights).astype(float)
                valid_data_min[valid_data_min > 1.] = 1.

                r_results = []
                generator = None
                r_predictions = []
                for rep in range(reps):
                    create_model_func = getattr(network_models, network_name.split('_')[0])
                    input_shape = data_min.shape[1:]
                    model = create_model_func(
                        input_shape=input_shape, regularization=regularization
                    )
                    # model.fit(data_min, train_result, batch_size=batch_size, epochs=epochs, verbose=0, callbacks=[
                    #     callbacks.LearningRateScheduler(scheduler)], class_weight=class_weight
                    # )
                    print('start training')
                    model.fit_generator(
                        SparseGenerator(data_min, train_result, batch_size=batch_size, sparse=True),
                        steps_per_epoch=data_min.shape[0] // batch_size, epochs=epochs,
                        callbacks=[
                            # callbacks.ModelCheckpoint(
                            #     './keras_examples/weights/' + name + '_Weights.h5',
                            #     monitor="val_acc",
                            #     save_best_only=True,
                            #     verbose=1
                            # ),
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        validation_data=(valid_data_min, valid_result),
                        validation_steps=valid_data_min.shape[0] // batch_size,
                        class_weight=class_weight,
                        verbose=2
                    )
                    r_result = model.evaluate(valid_data_min, valid_result, verbose=0)[-1]
                    r_results.append(r_result)
                    r_prediction = np.argmax(model.predict(valid_data_min), axis=-1).tolist()
                    r_predictions.append(r_prediction)
                    print('FEATURES : ', n_features, ', ACC : ', r_result)

                    del model
                    K.clear_session()
                print('n_features : ', n_features, ', ACC :', np.mean(r_results))
                results.append(r_results)
                predictions.append(r_predictions)
            output['classification'] = {
                'network': network_name,
                'n_features': features,
                'results': results,
                'predictions': predictions
            }
            results = np.array(np.mean(results, axis=-1))
            nfeats = np.array(features)
            roc = 0.5 * (results[1:] + results[:-1]) * (nfeats[1:] - nfeats[:-1])
            roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
            print('ROC : ', roc)

            try:
                with open(result_filename) as outfile:
                    results = json.load(outfile)
            except:
                results = {}

            if target_key not in results:
                results[target_key] = []

            results[target_key].append(output)

            with open(result_filename, 'w') as outfile:
                json.dump(results, outfile)
