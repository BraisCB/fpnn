import json
import numpy as np
from keras_examples.text_classification.reuters import output_filename, output_directory
import os
import matplotlib
import matplotlib.pyplot as pl


if __name__ == '__main__':
    os.chdir('../../../')

    with open(output_directory + output_filename) as outfile:
        results = json.load(outfile)

    ax = pl.figure()
    legends = ['dense_chi2_acq', 'dense_dbscan_chi2_0.75_acq', 'dense_chi2_cluster_0.5_acq']
    font = {'family': 'normal',
            'weight': 'bold',
            'size': 14}

    matplotlib.rc('font', **font)

    for method in legends:
        print(method)
        print('n_features : ', results[method]['n_features'])
        print('accuracy mean : ', np.mean(results[method]['accuracy'], axis=-1))
        print('accuracy median : ', np.median(results[method]['accuracy'], axis=-1))

        # legends.append(method)

        pl.plot(
            results[method]['n_features'], np.mean(results[method]['accuracy'], axis=-1)
        )


    pl.xlabel('nfeatures')
    pl.ylabel('accuracy')
    pl.title('REUTERS')
    pl.legend(legends, loc='best')

    pl.savefig(output_directory + 'reuters_score.eps', bbox_inches='tight')
