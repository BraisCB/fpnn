n_features_list = [50, 100, 200, 300, 500, 750, 1000]
reps = 20
regularization = 5e-3
batch_size = 300
epochs = 80

dataset_file = './keras_examples/text_classification/reuters/data/reuters_word2vec.npy'
output_directory = './keras_examples/text_classification/reuters/info/'
output_filename = 'results_word2vec.json'
targets = ['acq']  # 'earn'
network_names = ['dense']


def scheduler(epoch):
    if epoch < 30:
        return 0.01
    elif epoch < 60:
        return 0.002
    else:
        return 0.0004