from keras.models import Model
from keras import backend as K, optimizers
from keras.layers import Dense, Activation, BatchNormalization, Dropout, Input, Lambda
from keras.regularizers import l1, l2
from keras_examples.feature_selection.custom.layers import Mask
from .custom.layers import SoftCluster, HardCluster


def dense(input_shape, nclasses=2, layer_dims=None, bn=True, kernel_initializer='he_normal',
                 dropout=0.0, lasso=0.0, regularization=0.0):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape, sparse=False)
    if layer_dims is None:
        layer_dims = [150, 50]
    x = input
    if lasso > 0:
        x = Mask(kernel_regularizer=l1(lasso))(x)

    for layer_dim in layer_dims:
        x = Dense(layer_dim, use_bias=not bn, kernel_initializer=kernel_initializer,
                  kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)

    x = Dense(nclasses, use_bias=True, kernel_initializer=kernel_initializer,
              kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    output = Activation('softmax')(x)

    model = Model(input, output)

    optimizer = optimizers.adam(lr=1e-4)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    return model


def dense_v1(input_shape, nfeatures, nclasses=2, layer_dims=None, bn=True, kernel_initializer='he_normal',
                 dropout=0.0, lasso=0.0, regularization=0.0):
    channel_axis = 1 if K.image_data_format() == "channels_first" else -1
    input = Input(shape=input_shape, sparse=False)
    if layer_dims is None:
        layer_dims = [250, 150]
    x = input
    if lasso > 0:
        x = Mask(kernel_regularizer=l1(lasso))(x)

    cluster_layer = SoftCluster(
        nfeatures, matrix_regularizer=0.5, matrix_norm='1', diagonal_norm='Inf', diagonal_regularizer=0., input_shape=input_shape
    )

    x = cluster_layer(x)
    # x = Activation('tanh')(x)
    # x = Activation(softstep(0.5))(x)
    # x = Activation(step(0.5))(x)
    x = Activation(lambda v: K.clip(v, 0., 1.))(x)
    for i, layer_dim in enumerate(layer_dims):
        x = Dense(layer_dim, use_bias=not bn, kernel_initializer=kernel_initializer,
                  kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
        if bn:
            x = BatchNormalization(axis=channel_axis, momentum=0.9, epsilon=1e-5, gamma_initializer='ones')(x)
        if dropout > 0.0:
            x = Dropout(dropout)(x)
        x = Activation('relu')(x)

    x = Dense(nclasses, use_bias=True, kernel_initializer=kernel_initializer,
              kernel_regularizer=l2(regularization) if regularization > 0.0 else None)(x)
    output = Activation('softmax')(x)

    model = Model(input, output)

    optimizer = optimizers.adam(lr=1e-2)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])
    model.cluster_layer = cluster_layer

    return model


def dense_v2(input_shape, nfeatures, nclasses=2, layer_dims=None, bn=True, kernel_initializer='he_normal',
                 dropout=0.0, lasso=0.0, regularization=0.0):

    classifier = dense((nfeatures, ), nclasses, layer_dims, bn, kernel_initializer, dropout, lasso, regularization)

    input = Input(shape=input_shape, sparse=False)
    cluster_layer = HardCluster(
        nfeatures, dropout=0.0, input_shape=input_shape
    )
    x = cluster_layer(input)
    # x = Activation('tanh')(x)
    # x = Activation(softstep(0.5))(x)
    # x = Activation(step(0.5))(x)
    x = Activation(lambda v: K.clip(v, 0., 1.))(x)

    output = classifier(x)

    model = Model(input, output)

    optimizer = optimizers.adam(lr=1e-2)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])
    model.cluster_layer = cluster_layer
    model.summary()

    return model

