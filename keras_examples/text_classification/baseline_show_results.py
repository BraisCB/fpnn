from sklearn.datasets import fetch_rcv1
import numpy as np
import json
import os
from keras.utils import to_categorical


if __name__ == '__main__':

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

    directory = './datasets/'
    output_directory = './info/'
    targets = ['GCAT']

    network_names = ['dense']

    rcv1 = fetch_rcv1(data_home=directory)

    target_pos = []
    for target in targets:
        target_pos.append(np.where(rcv1.target_names == target)[0][0])

    pos = np.argsort(target_pos)
    targets = [targets[p] for p in pos]

    target_key = '_'.join(targets)

    data = rcv1.data.astype('float32')
    data[data > 0.] = 1.
    result = to_categorical(rcv1.target[:, target_pos].toarray())

    features = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 750, 1000] #, 1500, 2000, 5000, 10000, data.shape[-1]]
    features = [data.shape[-1]]

    result_filename = output_directory + 'baseline_results_chi2.json'

    with open(result_filename) as outfile:
        results = json.load(outfile)[target_key]

    for kfold, example in enumerate(results):

        print('KFOLD : ', kfold)

        results = example['classification']['results']
        features = example['classification']['n_features']

        results = np.array(np.mean(results, axis=-1))
        nfeats = np.array(features)
        roc = 0.5 * (results[1:] + results[:-1]) * (nfeats[1:] - nfeats[:-1])
        roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
        print('ROC : ', roc)
        print('FEATS : ', nfeats)
        print('RESULTS : ', results)


