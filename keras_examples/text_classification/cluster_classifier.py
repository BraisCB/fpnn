from sklearn.datasets import fetch_rcv1
from keras import backend as K, callbacks
import numpy as np
import json
from copy import deepcopy
import os
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from keras.utils import to_categorical
from scipy import sparse
from keras_examples.text_classification import network_models
from keras_examples.shared.generators import SparseGenerator
from keras_examples.shared.functions import softstep


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

reps = 1
regularization = 0.0
epochs = 40
batch_size = 500

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
set_session(tf.Session(config=config))


def scheduler(epoch):
    if epoch < 30:
        return 0.01
    else:
        return 0.001


directory = './keras_examples/text_classification/datasets/'
output_directory = './keras_examples/text_classification/info/'
targets = ['GCAT']

network_names = ['dense_v2']
network_fs = 'dense'

rcv1 = fetch_rcv1(data_home=directory)

target_pos = []
for target in targets:
    target_pos.append(np.where(rcv1.target_names == target)[0][0])

pos = np.argsort(target_pos)
targets = [targets[p] for p in pos]

data = rcv1.data.astype('float32')
data[data > 0.] = 1.
result = to_categorical(rcv1.target[:, target_pos].toarray())

features = [50, 100, 150] # , 200, 250, 300, 350, 400, 450, 500, 750, 1000] #, 1500, 2000, 5000, 10000, data.shape[-1]]
thresholds = [0.5]

filename = output_directory + 'baseline_rank_chi2.json'


target_key = '_'.join(targets)

with open(filename) as outfile:
    ranks = json.load(outfile)[target_key]

for kfold, example in enumerate(ranks):

    print('KFOLD : ', kfold)
    train_data = data[example['train_index']]
    train_result = result[example['train_index']]

    nnz = np.count_nonzero(train_result[:, -1]) / train_result.shape[0]
    class_weight=[1./(1. - nnz), 1./nnz]
    class_weight /= np.max(class_weight)
    print('pos values : ', nnz)
    print('neg values : ', 1. - nnz)
    print('class_weight : ', class_weight)

    valid_data = data[example['test_index']]
    valid_result = result[example['test_index']]

    for network_name in network_names:

        print('NETWORK : ', network_name)
        result_filename = output_directory + 'cluster_results_' + network_name + '.json'
        if 'regularization' in example:
            print('regularization : ', example['regularization'])
        if 'gamma' in example:
            print('gamma : ', example['gamma'])
        output = deepcopy(example)
        results = {}
        predictions = {}
        for n_features in features:
            print('nfeatures : ', n_features)
            generator = None
            r_predictions = []

            cluster_create_model_func = getattr(network_models, network_name)
            input_shape = train_data.shape[1:]
            cluster_model = cluster_create_model_func(
                input_shape=input_shape, regularization=regularization, nfeatures=n_features
            )
            # model.fit(data_min, train_result, batch_size=batch_size, epochs=epochs, verbose=0, callbacks=[
            #     callbacks.LearningRateScheduler(scheduler)], class_weight=class_weight
            # )
            cluster_model.fit_generator(
                SparseGenerator(train_data, train_result, batch_size=batch_size),
                steps_per_epoch=train_data.shape[0] // batch_size, epochs=epochs,
                callbacks=[
                    # callbacks.ModelCheckpoint(
                    #     './keras_examples/weights/' + name + '_Weights.h5',
                    #     monitor="val_acc",
                    #     save_best_only=True,
                    #     verbose=1
                    # ),
                    callbacks.LearningRateScheduler(scheduler)
                ],
                validation_data=(valid_data, valid_result),
                validation_steps=valid_data.shape[0] // batch_size,
                class_weight=class_weight,
                verbose=2
            )
            # cluster_matrix = K.eval(K.sigmoid(cluster_model.cluster_layer.kernel))
            cluster_matrix = K.eval(softstep()(cluster_model.cluster_layer.kernel))
            # cluster_matrix = K.eval(0.5 * (K.sign(cluster_model.cluster_layer.kernel) + 1.))
            print('result : ', cluster_model.evaluate(valid_data, valid_result, verbose=0)[-1])
            del cluster_model
            K.clear_session()
            for threshold in thresholds:
                print('threshold : ', threshold)
                r_predictions = []
                r_results = []
                matrix = np.zeros_like(cluster_matrix)
                matrix[cluster_matrix > threshold] = 1.0
                data_min = train_data.dot(matrix)
                data_min[data_min > 1.0] = 1.0
                data_min = sparse.csr_matrix(data_min, shape=data_min.shape)
                valid_data_min = valid_data.dot(matrix)
                valid_data_min[valid_data_min > 1.0] = 1.0
                valid_data_min = sparse.csr_matrix(valid_data_min, shape=valid_data_min.shape)
                for rep in range(reps):
                    create_model_func = getattr(network_models, network_fs)
                    input_shape = data_min.shape[1:]
                    model = create_model_func(
                        input_shape=input_shape, regularization=regularization
                    )
                    # model.fit(data_min, train_result, batch_size=batch_size, epochs=epochs, verbose=0, callbacks=[
                    #     callbacks.LearningRateScheduler(scheduler)], class_weight=class_weight
                    # )
                    model.fit_generator(
                        SparseGenerator(data_min, train_result, batch_size=batch_size),
                        steps_per_epoch=data_min.shape[0] // batch_size, epochs=epochs,
                        callbacks=[
                            # callbacks.ModelCheckpoint(
                            #     './keras_examples/weights/' + name + '_Weights.h5',
                            #     monitor="val_acc",
                            #     save_best_only=True,
                            #     verbose=1
                            # ),
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        # validation_data=(valid_data_min, valid_result),
                        # validation_steps=valid_data_min.shape[0] // batch_size,
                        class_weight=class_weight,
                        verbose=0
                    )

                    r_result = model.evaluate(valid_data_min, valid_result, verbose=0)[-1]
                    r_results.append(r_result)
                    r_prediction = np.argmax(model.predict(valid_data_min), axis=-1).tolist()
                    r_predictions.append(r_prediction)
                    print('FEATURES : ', n_features, ', ACC : ', r_result, ', THRESH : ', threshold)

                    del model
                    K.clear_session()
                print('n_features : ', n_features, ', ACC :', np.mean(r_results))
                if threshold not in results:
                    results[threshold] = []
                if threshold not in predictions:
                    predictions[threshold] = []
                results[threshold].append(r_results)
                predictions[threshold].append(r_predictions)
        output['classification'] = {
            'network': network_name,
            'n_features': features,
            'results': results,
            'predictions': predictions
        }
        # results = np.array(np.mean(results, axis=-1))
        # nfeats = np.array(features)
        # roc = 0.5 * (results[1:] + results[:-1]) * (nfeats[1:] - nfeats[:-1])
        # roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
        # print('ROC : ', roc)

        try:
            with open(result_filename) as outfile:
                results = json.load(outfile)
        except:
            results = {}

        if target_key not in results:
            results[target_key] = []

        results[target_key].append(output)

        with open(result_filename, 'w') as outfile:
            json.dump(results, outfile)
