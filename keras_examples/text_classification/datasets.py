from sklearn.datasets import fetch_rcv1 as fetch_rcv1_sklearn
import requests
import os
import pandas


def fetch_rcv1(data_home=None, subset='all', download_if_missing=True,
               random_state=None, shuffle=False):

    rcv1 = fetch_rcv1_sklearn(data_home, subset, download_if_missing, random_state, shuffle)

    filename = 'stem.termid.idf.map.txt'

    home = './' if data_home is None else data_home
    home += 'RCV1/'

    if not os.path.exists(home + filename):
        url = 'http://www.ai.mit.edu/projects/jmlr/papers/volume5/lewis04a/a14-term-dictionary/' + filename
        print('downloading : ', url)
        r = requests.get(url)
        with open(home + filename, 'wb') as f:
            print('saving to : ', home + filename)
            f.write(r.content)

    rcv1_map = pandas.read_csv(home + filename, sep=' ', header=None).values
    rcv1.words = [m[0] for m in rcv1_map]
    rcv1.word_to_id = {m[0]: (m[1] - 1) for m in rcv1_map}
    rcv1.id_to_word = {(m[1] - 1): m[0] for m in rcv1_map}

    return rcv1