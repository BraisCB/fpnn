from sklearn.feature_selection import chi2, SelectKBest
from keras_examples.text_classification.datasets import fetch_rcv1
import numpy as np
import os
import json
from sklearn.model_selection import KFold


embedding_directory = './keras_examples/text_classification/embeddings/'
dataset_directory = './keras_examples/text_classification/reuters/'
output_directory = './keras_examples/text_classification/info/'
targets = ['GCAT']

if __name__ == '__main__':
    os.chdir('../../')

    rcv1 = fetch_rcv1(data_home=dataset_directory)

    target_pos = []
    for target in targets:
        target_pos.append(np.where(rcv1.target_names == target)[0][0])

    pos = np.argsort(target_pos)
    targets = [targets[p] for p in pos]

    data = rcv1.data
    data[data > 0.] = 1.
    target = rcv1.target[:, target_pos].todense()

    kfold = KFold(n_splits=5, shuffle=True)

    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)

    filename = output_directory + 'baseline_rank_chi2.json'

    try:
        with open(filename) as outfile:
            ranks = json.load(outfile)
    except:
        ranks = {}

    target_key = '_'.join(targets)

    ranks[target_key] = []

    for train_index, test_index in kfold.split(data):
        train_data, train_label = data[train_index], target[train_index]

        print('computing Kbest')
        fs_method = SelectKBest(chi2, k=data.shape[-1]).fit(train_data, train_label)
        scores = fs_method.scores_
        scores[np.isnan(scores)] = 0.0
        rank = np.argsort(scores)[::-1].tolist()
        print('computing Kbest finished')

        ranks[target_key].append({
            'rank': rank,
            'train_index': train_index.tolist(),
            'test_index': test_index.tolist()
        })

    with open(filename, 'w') as outfile:
        json.dump(ranks, outfile)
