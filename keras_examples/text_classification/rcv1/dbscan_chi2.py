from sklearn.feature_selection import chi2, SelectKBest
from sklearn.cluster import DBSCAN
import numpy as np
import os
import json
from keras_examples.text_classification.rcv1 import n_features_list, epochs, regularization, reps, batch_size, \
    scheduler, dataset_directory, output_directory, targets, network_names, output_filename, k_fold, embeddings_file
from keras_examples.text_classification.rcv1 import network_models
from keras_examples.shared.generators import SparseGenerator
from keras import callbacks, backend as K
from keras_examples.text_classification.custom.merge import merge, create_matrix
from scipy.sparse import csr_matrix
from keras.utils import to_categorical
from sklearn.model_selection import KFold
from sklearn.datasets import fetch_rcv1


thresh_list = [0.3, 0.4, 0.5, 0.6, 0.7]

if __name__ == '__main__':
    os.chdir('../../../')

    rcv1_dataset = fetch_rcv1(data_home=dataset_directory)
    embeddings = np.load(embeddings_file)

    pos = np.where(np.abs(embeddings).sum(axis=-1) > 0.)[0]
    rcv1 = {
        'x': rcv1_dataset.data[:, pos], 'y': rcv1_dataset.target,
        'labels': rcv1_dataset.target_names.tolist(),
        'embeddings': embeddings[pos]
    }

    rcv1['embeddings'] /= np.sqrt(np.sum(rcv1['embeddings'] * rcv1['embeddings'], axis=0, keepdims=True))
    # print('defining clusters')
    # cluster = merge(rcv1['embeddings'], thresh)
    # print('clusters defined. Number of clusters : ', len(set(cluster.tolist())))


    target_pos = []
    for target in targets:
        target_pos.append(rcv1['labels'].index(target))

    pos = np.argsort(target_pos)
    targets = [targets[p] for p in pos]

    kfold = KFold(n_splits=k_fold, shuffle=True)

    for train_index, test_index in kfold.split(rcv1['x']):

        train_data = rcv1['x'][train_index]
        train_data[train_data > 0.] = 1.
        train_target = rcv1['y'][train_index[:, np.newaxis], target_pos].toarray()

        filename = output_directory + output_filename

        valid_data = rcv1['x'][test_index]
        valid_data[valid_data > 0.] = 1.
        valid_target = rcv1['y'][test_index[:, np.newaxis], target_pos].toarray()

        for thresh in thresh_list:
            print('using thresh ', thresh)

            result = {
                'n_features': n_features_list,
                'accuracy': []
            }

            nnz = np.count_nonzero(train_target[:, -1]) / train_target.shape[0]
            class_weight = [1. / (1. - nnz), 1. / nnz]
            class_weight /= np.max(class_weight)
            print('pos values : ', nnz)
            print('neg values : ', 1. - nnz)
            print('class_weight : ', class_weight)

            print('Computing DBSCAN')
            dbscan = DBSCAN(eps=1. - thresh, min_samples=2, metric='cosine', n_jobs=1).fit(rcv1['embeddings'])
            labels = dbscan.labels_
            index = np.max(labels) + 1
            for i, label in enumerate(labels):
                if label == -1:
                    labels[i] = index
                    index += 1
            matrix = csr_matrix(to_categorical(labels))
            train_data_cluster = train_data.dot(matrix)
            valid_data_cluster = valid_data.dot(matrix)
            train_data_cluster[train_data_cluster > 0.] = 1.
            valid_data_cluster[valid_data_cluster > 0.] = 1.
            print('DBSCAN computed. Number of clusters : ', index, matrix.shape[0])

            print('computing Kbest')
            fs_method = SelectKBest(chi2, k=train_data_cluster.shape[-1]).fit(train_data_cluster, train_target)
            scores = fs_method.scores_
            scores[np.isnan(scores)] = 0.0
            rank = np.argsort(scores)[::-1].tolist()
            print('computing Kbest finished')

            for network_name in network_names:
                target_key = network_name + '_dbscan_chi2_' + str(thresh) + '_' + '_'.join(targets)
                create_model_func = getattr(network_models, network_name.split('_')[0])
                last_execution = False
                for n_features in n_features_list:
                    if n_features > train_data_cluster.shape[-1]:
                        print('WARNING : reducing the number of features to ', train_data_cluster.shape[-1])
                        n_features = train_data_cluster.shape[-1]
                        last_execution = True
                    data_min = train_data_cluster[:, rank[:n_features]]
                    valid_data_min = valid_data_cluster[:, rank[:n_features]]
                    data_min[data_min > 1.] = 1.
                    valid_data_min[valid_data_min > 1.] = 1.
                    r_predictions = []
                    for rep in range(reps):
                        input_shape = data_min.shape[1:]
                        model = create_model_func(
                            input_shape=input_shape, regularization=regularization
                        )
                        model.fit_generator(
                            SparseGenerator(data_min, train_target, batch_size=batch_size, sparse=True),
                            steps_per_epoch=data_min.shape[0] // batch_size, epochs=epochs,
                            callbacks=[
                                # callbacks.ModelCheckpoint(
                                #     './keras_examples/weights/' + name + '_Weights.h5',
                                #     monitor="val_acc",
                                #     save_best_only=True,
                                #     verbose=1
                                # ),
                                callbacks.LearningRateScheduler(scheduler)
                            ],
                            validation_data=(valid_data_min, valid_target),
                            validation_steps=valid_data_min.shape[0] // batch_size,
                            class_weight=class_weight,
                            verbose=2
                        )
                        r_result = model.evaluate(valid_data_min, valid_target, verbose=0)[-1]
                        r_predictions.append(r_result)
                        # r_prediction = np.argmax(model.predict(valid_data_min), axis=-1).tolist()
                        # r_predictions.append(r_prediction)
                        print('FEATURES : ', n_features, ', ACC : ', r_result)

                        del model
                        K.clear_session()
                    result['accuracy'].append(r_predictions)
                    print(target_key)
                    print('n_features : ', n_features, ', ACC :', np.mean(r_predictions))
                    if last_execution:
                        break

                if os.path.isfile(filename):
                    with open(filename) as outfile:
                        results = json.load(outfile)
                else:
                    results = {}

                if target_key not in results:
                    results[target_key] = []

                results[target_key].append(result)

                if not os.path.isdir(output_directory):
                    os.makedirs(output_directory)

                with open(filename, 'w') as outfile:
                    json.dump(results, outfile)
