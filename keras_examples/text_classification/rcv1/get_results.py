import json
import numpy as np
from keras_examples.text_classification.rcv1 import output_filename, output_directory
import os
import matplotlib
import matplotlib.pyplot as pl


if __name__ == '__main__':
    os.chdir('../../../')

    with open(output_directory + output_filename) as outfile:
        results = json.load(outfile)

    ax = pl.figure()
    legends = ['dense_chi2_GCAT', 'dense_chi2_cluster_0.7_GCAT']
    font = {'family': 'normal',
            'weight': 'bold',
            'size': 14}

    matplotlib.rc('font', **font)

    for method in legends:
        print(method)
        n_features = results[method][0]['n_features']
        accuracy = np.mean(
            np.concatenate(
                [results[method][i]['accuracy'] for i in range(len(results[method]))], axis=-1
            ), axis=-1
        )
        print('n_features : ', n_features)
        print('accuracy mean : ', accuracy)
        # print('accuracy median : ', np.median(results[method]['accuracy'], axis=-1))

        # legends.append(method)

        pl.plot(
            n_features, accuracy
        )


    pl.xlabel('nfeatures')
    pl.ylabel('accuracy')
    pl.title('RCV1')
    pl.legend(legends, loc='best')

    pl.savefig(output_directory + 'rcv1_score.eps', bbox_inches='tight')
