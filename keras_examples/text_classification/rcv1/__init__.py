import numpy as np


n_features_list = [50, 100, 200, 300, 500, 750, 1000]
reps = 2
regularization = 5e-4
batch_size = 1000
epochs = 40

embeddings_file = './keras_examples/text_classification/rcv1/data/embeddings.npy'
dataset_directory = './keras_examples/text_classification/rcv1/data/'
output_directory = './keras_examples/text_classification/rcv1/info/'
output_filename = 'results_word2vec.json'
targets = ['GCAT']  # 'earn'
network_names = ['dense']
k_fold = 3
np.random.seed(42)


def scheduler(epoch):
    if epoch < 13:
        return 0.01
    elif epoch < 26:
        return 0.002
    else:
        return 0.0004