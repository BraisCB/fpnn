import numpy as np
import os, sys
import requests
import unicodedata
import zipfile
from nltk.stem import *
from keras.utils.data_utils import get_file


def remove_accents(input_str, decode):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    return only_ascii.decode(decode)


def fetch_glove(data_home=None, word_list=None, decoding='utf-8', stemmer=PorterStemmer()):

    data_home = './' if data_home is None else data_home

    filename = 'glove.840B.300d.zip'

    if not os.path.exists(data_home + filename):
        url = 'https://nlp.stanford.edu/data/' + filename
        print('downloading : ', url)

        get_file(filename, url, cache_dir=data_home, cache_subdir='.')
        # try:
        #     with open(data_home + filename, "wb") as f:
        #         response = requests.get(url, stream=True)
        #         total_length = response.headers.get('content-length')
        #
        #         if total_length is None:  # no content length header
        #             f.write(response.content)
        #         else:
        #             dl = 0
        #             total_length = int(total_length)
        #             for data in response.iter_content(chunk_size=4096):
        #                 dl += len(data)
        #                 f.write(data)
        #                 done = int(50 * dl / total_length)
        #                 sys.stdout.write("\r[%s%s] %.2f" % ('=' * done, ' ' * (50 - done), dl / total_length))
        #                 sys.stdout.flush()
        # except Exception as e:
        #     os.remove(data_home + filename)
        #     raise e

        # r = requests.get(url)
        # with open(data_home + filename, 'wb') as f:
        #     print('saving to : ', data_home + filename)
        #     f.write(r.content)

    archive = zipfile.ZipFile(data_home + filename, 'r')
    zip_filename = 'glove.840B.300d.txt'

    file = archive.open(zip_filename)

    model = {}
    cont = 0
    for line in file:
        cont += 1
        splitLine = line.split()
        word = remove_accents(str(splitLine[0])[2:-1], decoding)
        # if word_list is not None and (word not in word_list and stemmer.stem(word) not in word_list):
        #    continue
        embedding = np.array([float(val) for val in splitLine[1:]])
        model[word] = embedding
        # if word_list is not None:
        #     try:
        #         index = word_list.index(word)
        #     except:
        #         index = word_list.index(stemmer.stem(word))
        #     del word_list[index]
        # print(word, 'Words remaining', len(word_list), cont)
        # if len(word_list) == 0:
        #     break
    return model