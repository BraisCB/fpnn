from sklearn.datasets import fetch_rcv1
from keras import backend as K, callbacks
import numpy as np
import json
from keras_examples.text_classification import network_models
from keras_examples.shared.generators import SparseGenerator
from copy import deepcopy
import os
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
from keras.utils import to_categorical


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

reps = 2
regularization = 4e-5
epochs = 40
batch_size = 1000

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
set_session(tf.Session(config=config))


def scheduler(epoch):
    if epoch < 13:
        return 0.01
    elif epoch < 26:
        return 0.002
    else:
        return 0.0004


directory = './keras_examples/text_classification/datasets/'
output_directory = './keras_examples/text_classification/info/'
targets = ['GCAT']

network_names = ['dense']
if __name__ == '__main__':
    os.chdir('../../')
    rcv1 = fetch_rcv1(data_home=directory)

    target_pos = []
    for target in targets:
        target_pos.append(np.where(rcv1.target_names == target)[0][0])

    pos = np.argsort(target_pos)
    targets = [targets[p] for p in pos]

    data = rcv1.data.astype('float32')
    data[data > 0.] = 1.
    result = to_categorical(rcv1.target[:, target_pos].toarray())

    features = [50, 100, 200, 300, 500, 750, 1000] #, 1500, 2000, 5000, 10000, data.shape[-1]]
    # features = [data.shape[-1]]

    filename = output_directory + 'baseline_rank_chi2.json'
    result_filename = output_directory + 'baseline_results_chi2.json'

    target_key = '_'.join(targets)

    with open(filename) as outfile:
        ranks = json.load(outfile)[target_key]

    for kfold, example in enumerate(ranks):

        print('KFOLD : ', kfold)
        train_data = data[example['train_index']]
        train_result = result[example['train_index']]

        nnz = np.count_nonzero(train_result[:, -1]) / train_result.shape[0]
        class_weight=[1./(1. - nnz), 1./nnz]
        class_weight /= np.max(class_weight)
        print('pos values : ', nnz)
        print('neg values : ', 1. - nnz)
        print('class_weight : ', class_weight)

        valid_data = data[example['test_index']]
        valid_result = result[example['test_index']]

        for network_name in network_names:

            print('NETWORK : ', network_name)
            if 'regularization' in example:
                print('regularization : ', example['regularization'])
            if 'gamma' in example:
                print('gamma : ', example['gamma'])
            output = deepcopy(example)
            results = []
            predictions = []
            rank = np.array(example['rank']).astype(int)
            for n_features in features:
                print('nfeatures : ', n_features)
                r_results = []
                generator = None
                data_min = train_data[:, rank[:n_features]]
                valid_data_min = valid_data[:, rank[:n_features]]
                r_predictions = []
                for rep in range(reps):
                    create_model_func = getattr(network_models, network_name.split('_')[0])
                    input_shape = data_min.shape[1:]
                    model = create_model_func(
                        input_shape=input_shape, regularization=regularization
                    )
                    # model.fit(data_min, train_result, batch_size=batch_size, epochs=epochs, verbose=0, callbacks=[
                    #     callbacks.LearningRateScheduler(scheduler)], class_weight=class_weight
                    # )
                    model.fit_generator(
                        SparseGenerator(data_min, train_result, batch_size=batch_size, sparse=True),
                        steps_per_epoch=data_min.shape[0] // batch_size, epochs=epochs,
                        callbacks=[
                            # callbacks.ModelCheckpoint(
                            #     './keras_examples/weights/' + name + '_Weights.h5',
                            #     monitor="val_acc",
                            #     save_best_only=True,
                            #     verbose=1
                            # ),
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        validation_data=(valid_data_min, valid_result),
                        validation_steps=valid_data_min.shape[0] // batch_size,
                        class_weight=class_weight,
                        verbose=2
                    )
                    r_result = model.evaluate(valid_data_min, valid_result, verbose=0)[-1]
                    r_results.append(r_result)
                    # r_prediction = np.argmax(model.predict(valid_data_min), axis=-1).tolist()
                    # r_predictions.append(r_prediction)
                    print('FEATURES : ', n_features, ', ACC : ', r_result)

                    del model
                    K.clear_session()
                print('n_features : ', n_features, ', ACC :', np.mean(r_results))
                results.append(r_results)
                predictions.append(r_predictions)
            output['classification'] = {
                'network': network_name,
                'n_features': features,
                'results': results,
                # 'predictions': predictions
            }
            results = np.array(np.mean(results, axis=-1))
            nfeats = np.array(features)
            roc = 0.5 * (results[1:] + results[:-1]) * (nfeats[1:] - nfeats[:-1])
            roc = np.sum(roc) / (nfeats[-1] - nfeats[0])
            print('ROC : ', roc)

            try:
                with open(result_filename) as outfile:
                    results = json.load(outfile)
            except:
                results = {}

            if target_key not in results:
                results[target_key] = []

            results[target_key].append(output)

            with open(result_filename, 'w') as outfile:
                json.dump(results, outfile)
