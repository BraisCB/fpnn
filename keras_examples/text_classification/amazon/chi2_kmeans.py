from sklearn.feature_selection import chi2, SelectKBest
import numpy as np
import os
import json
from keras_examples.text_classification.amazon import n_features, epochs, regularization, reps, batch_size, \
    scheduler, dataset_file, output_directory, network_names, output_filename
from keras_examples.text_classification.custom.kmeans import MyKMeans
from keras_examples.text_classification.amazon import network_models
from keras_examples.shared.generators import SparseGenerator
from keras import callbacks, backend as K
from scipy import sparse as sp


thresh_list = [1e-3, .1, .2, .3, .4, .5, .6, .7, .8, .9, .999][::-1]

if __name__ == '__main__':
    os.chdir('../../../')

    amazon = np.load(dataset_file)
    amazon['embeddings'] /= np.linalg.norm(amazon['embeddings'], axis=1, keepdims=True)

    # print('defining clusters')
    # cluster = merge(amazon['embeddings'], thresh)
    # print('clusters defined. Number of clusters : ', len(set(cluster.tolist())))

    train_data = amazon['x_train']
    train_data[train_data > 0.] = 1.
    train_target = np.asarray(amazon['y_train']).reshape((-1, 1)) - 1

    filename = output_directory + output_filename

    print('computing Kbest')
    fs_method = SelectKBest(chi2, k=train_data.shape[-1]).fit(train_data, train_target)
    scores = fs_method.scores_
    scores[np.isnan(scores)] = 0.0
    rank = np.argsort(scores)[::-1].tolist()
    print('computing Kbest finished')

    valid_data = amazon['x_test']
    valid_data[valid_data > 0.] = 1.
    valid_target = np.asarray(amazon['y_test']).reshape((-1, 1)) - 1

    for thresh in thresh_list:
        print('using thresh ', thresh)

        result = {
            'merge_thresh': [],
            'n_features': [],
            'accuracy': []
        }

        nnz = np.count_nonzero(train_target[:, -1]) / train_target.shape[0]
        class_weight = [1. / (1. - nnz), 1. / nnz]
        class_weight /= np.max(class_weight)
        print('pos values : ', nnz)
        print('neg values : ', 1. - nnz)
        print('class_weight : ', class_weight)

        for network_name in network_names:
            target_key = network_name + '_chi2_kmeans_' + str(thresh)
            create_model_func = getattr(network_models, network_name.split('_')[0])
            print('creating matrix')
            kmeans = MyKMeans()
            kmeans.fit(amazon['embeddings'], ncluster=amazon['embeddings'][rank[:n_features]], max_distance=thresh)
            initial_matrix = sp.csc_matrix(kmeans.predict(amazon['embeddings'], to_categorical=True, max_distance=thresh))
            print('matrix created')
            nclusters = -1
            for merge_thresh in np.arange(0., 0.95, 0.05):
                print('Merge_thresh : ', merge_thresh)
                matrix = kmeans.merge_clusters(initial_matrix, merge_thresh)
                print('nelements in matrix : ', matrix.sum())
                if nclusters == matrix.shape[1]:
                    print('size was not changed. Skipping')
                    if nclusters > 1:
                        continue
                    else:
                        break
                nclusters = matrix.shape[1]
                data_min = train_data.dot(matrix)
                valid_data_min = valid_data.dot(matrix)
                data_min[data_min > 1.] = 1.
                valid_data_min[valid_data_min > 1.] = 1.
                r_predictions = []
                for rep in range(reps):
                    input_shape = data_min.shape[1:]
                    model = create_model_func(
                        input_shape=input_shape, regularization=regularization
                    )
                    model.fit_generator(
                        SparseGenerator(data_min, train_target, batch_size=batch_size, sparse=True),
                        epochs=epochs,
                        callbacks=[
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        validation_data=SparseGenerator(valid_data_min, valid_target, batch_size=batch_size, sparse=True, shuffle=False),
                        class_weight=class_weight,
                        verbose=2
                    )
                    r_result = model.evaluate(valid_data_min, valid_target, verbose=0)[-1]
                    r_predictions.append(r_result)
                    # r_prediction = np.argmax(model.predict(valid_data_min), axis=-1).tolist()
                    # r_predictions.append(r_prediction)
                    print('FEATURES : ', matrix.shape[1], ', ACC : ', r_result)

                    del model
                    K.clear_session()
                result['n_features'].append(matrix.shape[-1])
                result['accuracy'].append(r_predictions)
                result['merge_thresh'].append(merge_thresh)
                print(target_key)
                print('n_features : ', n_features, ', ACC :', np.mean(r_predictions))

            if os.path.isfile(filename):
                with open(filename) as outfile:
                    results = json.load(outfile)
            else:
                results = {}

            results[target_key] = result

            if not os.path.isdir(output_directory):
                os.makedirs(output_directory)

            with open(filename, 'w') as outfile:
                json.dump(results, outfile)
