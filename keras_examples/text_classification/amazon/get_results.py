import json
import numpy as np
from keras_examples.text_classification.amazon import output_filename, output_directory
import os
import matplotlib
import matplotlib.pyplot as pl


if __name__ == '__main__':
    os.chdir('../../../')

    with open(output_directory + output_filename) as outfile:
        results = json.load(outfile)

    ax = pl.figure()
    legends = ['dense_chi2', 'dense_chi2_cluster_0.6', 'dense_dbscan_chi2_0.8', 'dense_kmeans_chi2_15000']
    font = {'family': 'normal',
            'weight': 'bold',
            'size': 14}

    matplotlib.rc('font', **font)

    for method in legends:
        print(method)
        print('n_features : ', results[method]['n_features'])
        print('accuracy mean : ', np.mean(results[method]['accuracy'], axis=-1))
        print('accuracy median : ', np.median(results[method]['accuracy'], axis=-1))

        # legends.append(method)

        pl.plot(
            results[method]['n_features'], np.mean(results[method]['accuracy'], axis=-1)
        )


    pl.xlabel('nfeatures')
    pl.ylabel('accuracy')
    pl.title('AMAZON')
    pl.legend(legends, loc='best')

    pl.savefig(output_directory + 'amazon_score.eps', bbox_inches='tight')
