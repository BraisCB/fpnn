n_features = 500
reps = 1
regularization = 5e-4
batch_size = 5000
epochs = 40

dataset_file = './keras_examples/text_classification/amazon/data/amazon_fasttext.npy'
output_directory = './keras_examples/text_classification/amazon/info/'
output_filename = 'results_fasttext_antonio.json'
network_names = ['dense']


def scheduler(epoch):
    if epoch < 13:
        return 0.01
    elif epoch < 26:
        return 0.002
    else:
        return 0.0004