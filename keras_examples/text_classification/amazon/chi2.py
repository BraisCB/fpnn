from sklearn.feature_selection import chi2, SelectKBest
import numpy as np
import os
import json
from keras_examples.text_classification.amazon import epochs, regularization, reps, batch_size, \
    scheduler, dataset_file, output_directory, network_names, output_filename
from keras_examples.text_classification.amazon import network_models
from keras_examples.shared.generators import SparseGenerator
from keras import callbacks, backend as K
from keras.utils import to_categorical

n_features_list = [500, 497, 493, 477, 452, 414, 370, 314, 275, 227, 182, 124, 79, 51, 21, 10, 5, 1]

if __name__ == '__main__':
    os.chdir('../../../')

    amazon = np.load(dataset_file)

    train_data = amazon['x_train']
    train_data[train_data > 0.] = 1.
    train_target = np.asarray(amazon['y_train']).reshape((-1, 1)) - 1

    filename = output_directory + output_filename

    print('computing Kbest')
    fs_method = SelectKBest(chi2, k=train_data.shape[-1]).fit(train_data, train_target)
    scores = fs_method.scores_
    scores[np.isnan(scores)] = 0.0
    rank = np.argsort(scores)[::-1].tolist()
    print('computing Kbest finished')

    valid_data = amazon['x_test']
    valid_data[valid_data > 0.] = 1.
    valid_target = np.asarray(amazon['y_test']).reshape((-1, 1)) - 1

    result = {
        # 'rank': rank,
        'n_features': n_features_list,
        'accuracy': []
    }

    nnz = np.count_nonzero(train_target[:, -1]) / train_target.shape[0]
    class_weight = [1. / (1. - nnz), 1. / nnz]
    class_weight /= np.max(class_weight)
    print('pos values : ', nnz)
    print('neg values : ', 1. - nnz)
    print('class_weight : ', class_weight)

    for network_name in network_names:
        target_key = network_name + '_chi2'
        create_model_func = getattr(network_models, network_name.split('_')[0])
        for n_features in n_features_list:
            data_min = train_data[:, rank[:n_features]]
            valid_data_min = valid_data[:, rank[:n_features]]
            r_predictions = []
            for rep in range(reps):
                input_shape = data_min.shape[1:]
                model = create_model_func(
                    input_shape=input_shape, regularization=regularization
                )
                model.fit_generator(
                    SparseGenerator(data_min, train_target, batch_size=batch_size, sparse=True),
                    epochs=epochs,
                    callbacks=[
                        # callbacks.ModelCheckpoint(
                        #     './keras_examples/weights/' + name + '_Weights.h5',
                        #     monitor="val_acc",
                        #     save_best_only=True,
                        #     verbose=1
                        # ),
                        callbacks.LearningRateScheduler(scheduler)
                    ],
                    validation_data=SparseGenerator(valid_data_min, valid_target, batch_size=batch_size, sparse=True, shuffle=False),
                    class_weight=class_weight,
                    verbose=2
                )
                r_result = model.evaluate(valid_data_min, valid_target, verbose=0)[-1]
                r_predictions.append(r_result)
                # r_prediction = np.argmax(model.predict(valid_data_min), axis=-1).tolist()
                # r_predictions.append(r_prediction)
                print('FEATURES : ', n_features, ', ACC : ', r_result)

                del model
                K.clear_session()
            result['accuracy'].append(r_predictions)
            print(target_key)
            print('n_features : ', n_features, ', ACC :', np.mean(r_predictions))

        if os.path.isfile(filename):
            with open(filename) as outfile:
                results = json.load(outfile)
        else:
            results = {}

        results[target_key] = result

        if not os.path.isdir(output_directory):
            os.makedirs(output_directory)

        with open(filename, 'w') as outfile:
            json.dump(results, outfile)
