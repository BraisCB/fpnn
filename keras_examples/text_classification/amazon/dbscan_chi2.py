from sklearn.feature_selection import chi2, SelectKBest
from sklearn.cluster import DBSCAN, KMeans
import numpy as np
import os
import json
from keras_examples.text_classification.amazon import n_features_list, epochs, regularization, reps, batch_size, \
    scheduler, dataset_file, output_directory, network_names, output_filename
from keras_examples.text_classification.amazon import network_models
from keras_examples.shared.generators import SparseGenerator
from keras import callbacks, backend as K
from keras_examples.text_classification.custom.merge import merge, create_matrix
from keras_examples.text_classification.custom.dbscan import MyDBSCAN
from scipy.sparse import csr_matrix, csc_matrix
from keras.utils import to_categorical


thresh_list = [0.5, 0.6, 0.7, 0.8]
# thresh_list = [30000, 20000, 15000, 10000, 7500, 5000, 2500]

if __name__ == '__main__':
    os.chdir('../../../')

    amazon = np.load(dataset_file)

    # print('defining clusters')
    # cluster = merge(amazon['embeddings'], thresh)
    # print('clusters defined. Number of clusters : ', len(set(cluster.tolist())))

    train_data = amazon['x_train']

    train_data[train_data > 0.] = 1.
    freq = np.asarray(np.mean(train_data, axis=0))[0]
    pos = np.where(freq > 1e-5)[0]

    train_target = np.asarray(amazon['y_train']).reshape((-1, 1)) - 1

    filename = output_directory + output_filename

    valid_data = amazon['x_test']
    valid_data[valid_data > 0.] = 1.
    valid_target = np.asarray(amazon['y_test']).reshape((-1, 1)) - 1

    amazon['embeddings'] /= np.linalg.norm(amazon['embeddings'], axis=1, keepdims=True)

    # train_data = train_data.tocsc()[:, pos].tocsr()
    # valid_data = valid_data.tocsc()[:, pos].tocsr()
    # amazon['embeddings'] = amazon['embeddings'][pos]

    for thresh in thresh_list:
        print('using thresh ', thresh)

        result = {
            'n_features': n_features_list,
            'accuracy': []
        }

        nnz = np.count_nonzero(train_target[:, -1]) / train_target.shape[0]
        class_weight = [1. / (1. - nnz), 1. / nnz]
        class_weight /= np.max(class_weight)
        print('pos values : ', nnz)
        print('neg values : ', 1. - nnz)
        print('class_weight : ', class_weight)

        print('Computing DBSCAN')
        # dbscan = DBSCAN(eps=1. - thresh, min_samples=2, metric='cosine', n_jobs=1).fit(amazon['embeddings'])
        # dbscan = KMeans(n_clusters=thresh).fit(amazon['embeddings'])
        # labels = dbscan.labels_
        labels = MyDBSCAN(amazon['embeddings'], eps=1. - thresh, MinPts=5)
        index = np.max(labels) + 1
        for i, label in enumerate(labels):
            if label == -1:
                labels[i] = index
                index += 1
        indexes = tuple(np.array([np.arange(len(labels), dtype=int), labels]))
        ones = np.ones_like(labels)
        matrix = csr_matrix((ones, indexes), shape=(len(labels), index))
        train_data_cluster = train_data.dot(matrix)
        valid_data_cluster = valid_data.dot(matrix)
        train_data_cluster[train_data_cluster > 0.] = 1.
        valid_data_cluster[valid_data_cluster > 0.] = 1.
        print('DBSCAN computed. Number of clusters : ', index)

        print('computing Kbest')
        fs_method = SelectKBest(chi2, k=train_data_cluster.shape[-1]).fit(train_data_cluster, train_target)
        scores = fs_method.scores_
        scores[np.isnan(scores)] = 0.0
        rank = np.argsort(scores)[::-1].tolist()
        print('computing Kbest finished')

        for network_name in network_names:
            target_key = network_name + '_dbscan_chi2_' + str(thresh)
            create_model_func = getattr(network_models, network_name.split('_')[0])
            last_execution = False
            for n_features in n_features_list:
                if n_features > train_data_cluster.shape[-1]:
                    print('WARNING : reducing the number of features to ', train_data_cluster.shape[-1])
                    n_features = train_data_cluster.shape[-1]
                    last_execution = True
                data_min = train_data_cluster[:, rank[:n_features]]
                valid_data_min = valid_data_cluster[:, rank[:n_features]]
                data_min[data_min > 1.] = 1.
                valid_data_min[valid_data_min > 1.] = 1.
                r_predictions = []
                for rep in range(reps):
                    input_shape = data_min.shape[1:]
                    model = create_model_func(
                        input_shape=input_shape, regularization=regularization
                    )
                    model.fit_generator(
                        SparseGenerator(data_min, train_target, batch_size=batch_size, sparse=True),
                        steps_per_epoch=data_min.shape[0] // batch_size, epochs=epochs,
                        callbacks=[
                            # callbacks.ModelCheckpoint(
                            #     './keras_examples/weights/' + name + '_Weights.h5',
                            #     monitor="val_acc",
                            #     save_best_only=True,
                            #     verbose=1
                            # ),
                            callbacks.LearningRateScheduler(scheduler)
                        ],
                        validation_data=(valid_data_min, valid_target),
                        validation_steps=valid_data_min.shape[0] // batch_size,
                        class_weight=class_weight,
                        verbose=2
                    )
                    r_result = model.evaluate(valid_data_min, valid_target, verbose=0)[-1]
                    r_predictions.append(r_result)
                    # r_prediction = np.argmax(model.predict(valid_data_min), axis=-1).tolist()
                    # r_predictions.append(r_prediction)
                    print('FEATURES : ', n_features, ', ACC : ', r_result)

                    del model
                    K.clear_session()
                result['accuracy'].append(r_predictions)
                print(target_key)
                print('n_features : ', n_features, ', ACC :', np.mean(r_predictions))
                if last_execution:
                    break

            if os.path.isfile(filename):
                with open(filename) as outfile:
                    results = json.load(outfile)
            else:
                results = {}

            results[target_key] = result

            if not os.path.isdir(output_directory):
                os.makedirs(output_directory)

            with open(filename, 'w') as outfile:
                json.dump(results, outfile)
