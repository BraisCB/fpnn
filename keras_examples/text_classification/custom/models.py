import numpy as np
import warnings
from keras import backend as K
from keras.utils.data_utils import Sequence
from keras.utils.data_utils import GeneratorEnqueuer
from keras.utils.data_utils import OrderedEnqueuer
from keras import callbacks as cbks


class TwoStepModel:

    def __init__(self, cluster, classifier, combined):
        self.cluster = cluster
        self.cluster_layer = cluster.layers[-1]
        self.classifier = classifier
        self.combined = combined


class SiameseModel:

    def __init__(self, models, cluster):
        self.models = models
        self.cluster = cluster
        self.cluster_layer = cluster.layers[-1]

    def fit_generator(self,
                      generator,
                      steps_per_epoch=None,
                      epochs=1,
                      verbose=1,
                      callbacks=None,
                      validation_data=None,
                      validation_steps=None,
                      class_weight=None,
                      max_queue_size=10,
                      workers=1,
                      use_multiprocessing=False,
                      shuffle=True,
                      initial_epoch=0):
        """Trains the model on data generated batch-by-batch by a Python generator or an instance of `Sequence`.

        The generator is run in parallel to the model, for efficiency.
        For instance, this allows you to do real-time data augmentation
        on images on CPU in parallel to training your model on GPU.

        The use of `keras.utils.Sequence` guarantees the ordering
        and guarantees the single use of every input per epoch when
        using `use_multiprocessing=True`.

        # Arguments
            generator: A generator or an instance of `Sequence`
                (`keras.utils.Sequence`) object in order to avoid
                duplicate data when using multiprocessing.
                The output of the generator must be either
                - a tuple `(inputs, targets)`
                - a tuple `(inputs, targets, sample_weights)`.
                This tuple (a single output of the generator) makes a single
                batch. Therefore, all arrays in this tuple must have the same
                length (equal to the size of this batch). Different batches may
                have different sizes. For example, the last batch of the epoch
                is commonly smaller than the others, if the size of the dataset
                is not divisible by the batch size.
                The generator is expected to loop over its data
                indefinitely. An epoch finishes when `steps_per_epoch`
                batches have been seen by the model.
            steps_per_epoch: Integer.
                Total number of steps (batches of samples)
                to yield from `generator` before declaring one epoch
                finished and starting the next epoch. It should typically
                be equal to the number of samples of your dataset
                divided by the batch size.
                Optional for `Sequence`: if unspecified, will use
                the `len(generator)` as a number of steps.
            epochs: Integer. Number of epochs to train the model.
                An epoch is an iteration over the entire data provided,
                as defined by `steps_per_epoch`.
                Note that in conjunction with `initial_epoch`,
                `epochs` is to be understood as "final epoch".
                The model is not trained for a number of iterations
                given by `epochs`, but merely until the epoch
                of index `epochs` is reached.
            verbose: Integer. 0, 1, or 2. Verbosity mode.
                0 = silent, 1 = progress bar, 2 = one line per epoch.
            callbacks: List of `keras.callbacks.Callback` instances.
                List of callbacks to apply during training.
                See [callbacks](/callbacks).
            validation_data: This can be either
                - a generator for the validation data
                - tuple `(x_val, y_val)`
                - tuple `(x_val, y_val, val_sample_weights)`
                on which to evaluate
                the loss and any model metrics at the end of each epoch.
                The model will not be trained on this data.
            validation_steps: Only relevant if `validation_data`
                is a generator. Total number of steps (batches of samples)
                to yield from `validation_data` generator before stopping
                at the end of every epoch. It should typically
                be equal to the number of samples of your
                validation dataset divided by the batch size.
                Optional for `Sequence`: if unspecified, will use
                the `len(validation_data)` as a number of steps.
            class_weight: Optional dictionary mapping class indices (integers)
                to a weight (float) value, used for weighting the loss function
                (during training only). This can be useful to tell the model to
                "pay more attention" to samples from an under-represented class.
            max_queue_size: Integer. Maximum size for the generator queue.
                If unspecified, `max_queue_size` will default to 10.
            workers: Integer. Maximum number of processes to spin up
                when using process-based threading.
                If unspecified, `workers` will default to 1. If 0, will
                execute the generator on the main thread.
            use_multiprocessing: Boolean.
                If `True`, use process-based threading.
                If unspecified, `use_multiprocessing` will default to `False`.
                Note that because this implementation relies on multiprocessing,
                you should not pass non-picklable arguments to the generator
                as they can't be passed easily to children processes.
            shuffle: Boolean. Whether to shuffle the order of the batches at
                the beginning of each epoch. Only used with instances
                of `Sequence` (`keras.utils.Sequence`).
                Has no effect when `steps_per_epoch` is not `None`.
            initial_epoch: Integer.
                Epoch at which to start training
                (useful for resuming a previous training run).

        # Returns
            A `History` object. Its `History.history` attribute is
            a record of training loss values and metrics values
            at successive epochs, as well as validation loss values
            and validation metrics values (if applicable).

        # Raises
            ValueError: In case the generator yields data in an invalid format.

        # Example

        ```python
            def generate_arrays_from_file(path):
                while True:
                    with open(path) as f:
                        for line in f:
                            # create numpy arrays of input data
                            # and labels, from each line in the file
                            x1, x2, y = process_line(line)
                            yield ({'input_1': x1, 'input_2': x2}, {'output': y})

            model.fit_generator(generate_arrays_from_file('/my_file.txt'),
                                steps_per_epoch=10000, epochs=10)
        ```
        """
        wait_time = 0.01  # in seconds
        epoch = initial_epoch

        active_models = list(range(len(self.models)))

        do_validation = bool(validation_data)
        for i in active_models:
            self.models[i]._make_train_function()
            if do_validation:
                self.models[i]._make_test_function()

        is_sequence = isinstance(generator, Sequence)
        if not is_sequence and use_multiprocessing and workers > 1:
            warnings.warn(
                UserWarning('Using a generator with `use_multiprocessing=True`'
                            ' and multiple workers may duplicate your data.'
                            ' Please consider using the`keras.utils.Sequence'
                            ' class.'))
        if steps_per_epoch is None:
            if is_sequence:
                steps_per_epoch = len(generator)
            else:
                raise ValueError('`steps_per_epoch=None` is only valid for a'
                                 ' generator based on the `keras.utils.Sequence`'
                                 ' class. Please specify `steps_per_epoch` or use'
                                 ' the `keras.utils.Sequence` class.')

        # python 2 has 'next', 3 has '__next__'
        # avoid any explicit version checks
        val_gen = (hasattr(validation_data, 'next') or
                   hasattr(validation_data, '__next__') or
                   isinstance(validation_data, Sequence))
        if (val_gen and not isinstance(validation_data, Sequence) and
                not validation_steps):
            raise ValueError('`validation_steps=None` is only valid for a'
                             ' generator based on the `keras.utils.Sequence`'
                             ' class. Please specify `validation_steps` or use'
                             ' the `keras.utils.Sequence` class.')

        # Prepare display labels.
        out_labels = {}
        callback_metrics = {}
        for i in active_models:
            out_labels[i] = self.models[i].metrics_names
            callback_metrics[i] = out_labels[i] + ['val_' + n for n in out_labels[i]]

        # prepare callbacks
        _callbacks = {}
        callbacks_per_model = {}
        for i in active_models:
            self.models[i].history = cbks.History()
            _callbacks[i] = [cbks.BaseLogger(
                stateful_metrics=self.models[i].stateful_metric_names)]
            if verbose:
                _callbacks[i].append(
                    cbks.ProgbarLogger(
                        count_mode='steps',
                        stateful_metrics=self.models[i].stateful_metric_names
                    )
                )
            _callbacks[i] += (callbacks or []) + [self.models[i].history]
            callbacks_per_model[i] = cbks.CallbackList(_callbacks[i])

        callbacks = callbacks_per_model

        # it's possible to callback a different model than self:
        callback_models = {}
        for i in active_models:
            if hasattr(self, 'callback_model') and self.models[i].callback_model:
                callback_models[i] = self.models[i].callback_model
            else:
                callback_models[i] = self.models[i]
            callbacks[i].set_model(callback_models[i])
            callbacks[i].set_params({
                'epochs': epochs,
                'steps': steps_per_epoch,
                'verbose': verbose,
                'do_validation': do_validation,
                'metrics': callback_metrics[i],
            })
            callbacks[i].on_train_begin()

        enqueuer = None
        val_enqueuer = None

        val_xs = {}
        val_ys = {}
        val_samples_weights = {}
        try:
            if do_validation and not val_gen:
                # Prepare data for validation
                if len(validation_data) == 2:
                    val_x, val_y = validation_data
                    val_sample_weight = None
                elif len(validation_data) == 3:
                    val_x, val_y, val_sample_weight = validation_data
                else:
                    raise ValueError('`validation_data` should be a tuple '
                                     '`(val_x, val_y, val_sample_weight)` '
                                     'or `(val_x, val_y)`. Found: ' +
                                     str(validation_data))
                val_x_cluster = self.cluster.predict(val_x, batch_size=200)
                for i in active_models:
                    val_x_to_use = val_x_cluster if hasattr(self.models[i], 'use_cluster') and self.models[i].use_cluster else val_x
                    val_x_to_use, val_y_to_use, val_sample_weights = self.models[i]._standardize_user_data(
                        val_x_to_use, val_y, val_sample_weight)
                    val_data = val_x_to_use + val_y_to_use + val_sample_weights
                    if self.models[i].uses_learning_phase and not isinstance(K.learning_phase(), int):
                        val_data += [0.]
                    for cbk in callbacks[i]:
                        cbk.validation_data = val_data
                    val_xs[i] = val_x_to_use
                    val_ys[i] = val_y_to_use
                    val_samples_weights[i] = val_sample_weights

            if workers > 0:
                if is_sequence:
                    enqueuer = OrderedEnqueuer(generator,
                                               use_multiprocessing=use_multiprocessing,
                                               shuffle=shuffle)
                else:
                    enqueuer = GeneratorEnqueuer(generator,
                                                 use_multiprocessing=use_multiprocessing,
                                                 wait_time=wait_time)
                enqueuer.start(workers=workers, max_queue_size=max_queue_size)
                output_generator = enqueuer.get()
            else:
                if is_sequence:
                    output_generator = iter(generator)
                else:
                    output_generator = generator

            epoch_logs = {}
            for i in active_models:
                callback_models[i].stop_training = False
            # Construct epoch logs.

            while epoch < epochs:
                for i in active_models:
                    for m in self.models[i].stateful_metric_functions:
                        m.reset_states()
                    callbacks[i].on_epoch_begin(epoch)
                steps_done = 0
                batch_index = 0
                while steps_done < steps_per_epoch:
                    generator_output = next(output_generator)

                    if not hasattr(generator_output, '__len__'):
                        raise ValueError('Output of generator should be '
                                         'a tuple `(x, y, sample_weight)` '
                                         'or `(x, y)`. Found: ' +
                                         str(generator_output))

                    if len(generator_output) == 2:
                        x, y = generator_output
                        sample_weight = None
                    elif len(generator_output) == 3:
                        x, y, sample_weight = generator_output
                    else:
                        raise ValueError('Output of generator should be '
                                         'a tuple `(x, y, sample_weight)` '
                                         'or `(x, y)`. Found: ' +
                                         str(generator_output))
                    # build batch logs
                    batch_logs = {}
                    if x is None or len(x) == 0:
                        # Handle data tensors support when no input given
                        # step-size = 1 for data tensors
                        batch_size = 1
                    elif isinstance(x, list):
                        batch_size = x[0].shape[0]
                    elif isinstance(x, dict):
                        batch_size = list(x.values())[0].shape[0]
                    else:
                        batch_size = x.shape[0]
                    x_cluster = self.cluster.predict(x, batch_size=200)
                    for i in active_models:
                        batch_logs = {}
                        batch_logs['batch'] = batch_index
                        batch_logs['size'] = batch_size
                        callbacks[i].on_batch_begin(batch_index, batch_logs)

                        if hasattr(self.models[i], 'use_cluster'):
                            x_to_use = x_cluster if self.models[i].use_cluster else x
                        else:
                            x_to_use = x
                        outs = self.models[i].train_on_batch(
                            x_to_use, y, sample_weight=sample_weight, class_weight=class_weight
                        )

                        if not isinstance(outs, list):
                            outs = [outs]
                        for l, o in zip(out_labels[i], outs):
                            batch_logs[l] = o

                        callbacks[i].on_batch_end(batch_index, batch_logs)

                    batch_index += 1
                    steps_done += 1

                    # Epoch finished.
                    if steps_done >= steps_per_epoch and do_validation:
                        for i in active_models:
                            if val_gen:
                                val_outs = self.models[i].evaluate_generator(
                                    validation_data,
                                    validation_steps,
                                    workers=workers,
                                    use_multiprocessing=use_multiprocessing,
                                    max_queue_size=max_queue_size)
                            else:
                                # No need for try/except because
                                # data has already been validated.
                                val_outs = self.models[i].evaluate(
                                    val_xs[i], val_ys[i],
                                    batch_size=batch_size,
                                    sample_weight=val_samples_weights[i],
                                    verbose=0)
                            if not isinstance(val_outs, list):
                                val_outs = [val_outs]
                            # Same labels assumed.
                            for l, o in zip(out_labels[i], val_outs):
                                epoch_logs['val_' + l] = o

                    new_active_models = []
                    for i in active_models:
                        if not callback_models[i].stop_training:
                            new_active_models.append(i)

                    active_models = new_active_models
                    if not len(active_models):
                        break

                new_active_models = []
                for i in active_models:
                    callbacks[i].on_epoch_end(epoch, epoch_logs)
                    if not callback_models[i].stop_training:
                        new_active_models.append(i)
                epoch += 1
                active_models = new_active_models
                if not len(active_models):
                    break

        finally:
            try:
                if enqueuer is not None:
                    enqueuer.stop()
            finally:
                if val_enqueuer is not None:
                    val_enqueuer.stop()

        histories = []
        for i, model in enumerate(self.models):
            callbacks[i].on_train_end()
            histories.append(model.history)

        return histories

    # def __getattribute__(self, attr):
    #     if hasattr(self, attr):
    #         return  self.__dict__[attr]
    #     else:
    #         return self.models[-1].__dict__[attr]

    def evaluate(self, x=None, y=None, batch_size=None, verbose=1, sample_weight=None, steps=None):
        return self.models[-1].evaluate(
            x=x, y=y, batch_size=batch_size, verbose=verbose, sample_weight=sample_weight, steps=steps
        )

    def predict(self, x, batch_size=None, verbose=1, steps=None):
        return self.models[-1].predict(x=x, batch_size=batch_size, verbose=verbose, steps=steps)



