import numpy as np
from scipy import sparse as sp



class MyKMeans:
    
    def __init__(self):
        self.clusters = None

    def fit(self, D, ncluster, seed=None, batch_size=5000, max_iter=500, max_distance=np.Inf):
        D /= np.linalg.norm(D, axis=-1, keepdims=True)
    
        np.random.seed(seed)
    
        if isinstance(ncluster, np.ndarray):
            self.clusters = ncluster.T
        else:
            self.clusters = self.kmeansplusplus(D, ncluster)
    
        labels = self.predict(D, batch_size=batch_size, max_distance=max_distance)
    
        for iter in range(max_iter):
            print(iter, ' of ', max_iter)
    
            self.clusters = self.compute_clusters(D, labels)
            new_labels = self.predict(D, batch_size=batch_size, max_distance=max_distance)
    
            if (new_labels != labels).sum():
                print('nchanges : ', (new_labels != labels).sum())
                labels = new_labels
            else:
                break
    
        return
    
    def predict(self, D, to_categorical=False, batch_size=5000, max_distance=np.Inf):
        index = 0
        labels = np.zeros(len(D), dtype=int)
        while index < len(D):
            new_index = min(len(D), index + batch_size)
            scores = 1. - np.dot(D[index:new_index], self.clusters)
            min_scores = np.min(scores, axis=-1)
            labels[index:new_index] = -1 + (1 + np.argmin(scores, axis=-1)) * (min_scores < max_distance)
            index = new_index
        if to_categorical:
            labels = self.one_hot(labels, self.clusters.shape[1])
        return labels
    
    def compute_clusters(self, D, labels):
        new_clusters = self.clusters.copy()
        for idx in range(self.clusters.shape[-1]):
            pos = np.where(labels == idx)[0]
            if not len(pos):
                continue
            else:
                new_clusters[:, idx] = D[pos].mean(axis=0)
        new_clusters /= np.linalg.norm(new_clusters, axis=0, keepdims=True)
        return new_clusters
    
    def kmeansplusplus(self, D, nclusters):
        p = [np.random.randint(len(D))]
        scores = np.dot(D, D[p].T)
    
        while len(p) < nclusters:
            Dmin = np.square(scores)
            probs = Dmin / Dmin.sum()
            mn = np.random.multinomial(1, probs)
            p.append(np.where(mn == 1)[0][0])
            scores = np.minimum(scores, np.dot(D, D[p[-1]].T))
        return D[p].T

    def merge_clusters(self, matrix, thresh, seed=None, batch_size=5000):
        print('Init merging. Number of clusters : ', self.clusters.shape[1])
        labels = MyAgglomerativeClustering.fit(self.clusters.T, thresh)
        index = np.max(labels) + 1
        for i, label in enumerate(labels):
            if label == -1:
                labels[i] = index
                index += 1
        ulabels = np.unique(labels)
        new_clusters = []
        for label in ulabels:
            pos = np.where(labels == label)[0]
            new_clusters.append(
                sp.coo_matrix(matrix[:, pos].sum(axis=1))
            )
        new_clusters = sp.hstack(new_clusters, format='csc')
        new_clusters[new_clusters > 1.] = 1.
        print('Finish merging. Number of clusters : ', new_clusters.shape[1])
        return new_clusters

    @staticmethod
    def one_hot(y, num_classes=None):
        if not num_classes:
            num_classes = np.max(y) + 1
        n = y.shape[0]
        categorical = np.zeros((n, num_classes), dtype=np.float32)
        p = np.sort(np.where(y >= 0)[0])
        categorical[p, y[y >= 0]] = 1
        return categorical


class MyAgglomerativeClustering:

    @staticmethod
    def fit(D, eps):
        new_D = D / np.linalg.norm(D, ord=2, axis=-1, keepdims=True)
        D = D / np.linalg.norm(D, ord=2, axis=-1, keepdims=True)
                
        while True:
            if new_D.shape[0] == 1:
                break

            scores = 1. - np.dot(new_D, new_D.T)
            scores += 2. * np.eye(scores.shape[0])

            min_scores = scores.min(axis=1)
            argmin_scores = scores.argmin(axis=1)

            if min_scores.min() > eps:
                break

            p1 = min_scores.argmin()
            p2 = argmin_scores[p1]
            pos = list(set(range(new_D.shape[0])).difference({p1, p2}))
            new_D_aux = np.zeros((new_D.shape[0] - 1, new_D.shape[1]))
            new_D_aux[:-1] = new_D[pos]
            new_D_aux[-1] = np.sum(new_D[[p1, p2]], axis=0, keepdims=True)
            new_D = new_D_aux
            new_D /= np.linalg.norm(new_D, ord=2, axis=-1, keepdims=True)

        scores = 1. - np.dot(D, new_D.T)
        labels = np.argmin(scores, axis=-1)

        return labels
