import numpy as np


def merge(embeddings, thresh=.95):
    # normalize
    embeddings = np.asarray(embeddings)
    embeddings /= np.maximum(1e-6, np.sqrt(np.sum(embeddings * embeddings, axis=-1, keepdims=True)))
    cluster = np.arange(len(embeddings), dtype=int)
    for i, embedding in enumerate(embeddings):
        if np.sum(embedding * embedding) < 0.1:
            continue
        cos_similarity = embeddings.dot(embedding)
        similar = np.where(cos_similarity >= thresh)[0]
        similar_clusters = set(cluster[similar].tolist())
        min_cluster = np.min(cluster[similar])
        for similar_cluster in similar_clusters:
            cluster[cluster == similar_cluster] = min_cluster
    return cluster


def create_matrix_2(cluster, rank, nfeatures):

    matrix = np.zeros((len(cluster), nfeatures))
    index = 0
    used = np.zeros_like(rank).astype(bool)
    for pos in range(len(rank)):
        if used[pos]:
            if used.all():
                print('Warning : reducing the number of clusters to ', index)
                break
            continue
        cluster_value = cluster[rank[pos]]
        similar = np.where(cluster == cluster_value)[0]
        matrix[similar, index] = 1.
        used[similar] = True
        index = index + 1
        if index == nfeatures:
            break
    print('Number of clusters : ', index)
    return matrix


def create_matrix(embeddings, rank, nfeatures, thresh=0.9):
    embeddings = np.asarray(embeddings)
    embeddings /= np.maximum(1e-6, np.sqrt(np.sum(embeddings * embeddings, axis=-1, keepdims=True)))

    matrix = np.zeros((embeddings.shape[0], nfeatures))
    index = 0
    for pos in rank[:nfeatures]:
        embedding = embeddings[pos]
        cos_similarity = embeddings.dot(embedding)
        similar = np.where(cos_similarity >= thresh)[0]
        matrix[similar, index] = 1.
        index += 1
        if index == nfeatures:
            break
    print('Number of clusters : ', index, ', mean : ', np.mean(np.sum(matrix, axis=0)))
    return matrix
