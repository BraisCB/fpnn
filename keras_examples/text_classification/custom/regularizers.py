from keras.regularizers import Regularizer
from keras import backend as K
from keras_examples.shared.functions import softstep


class ClusterRegularizer(Regularizer):

    def __init__(self, matrix_factor=0., matrix_norm=None, diagonal_factor=0., diagonal_norm=None):
        self.matrix_factor = K.cast_to_floatx(matrix_factor)
        self.diagonal_factor = K.cast_to_floatx(diagonal_factor)
        self.matrix_norm = matrix_norm
        self.diagonal_norm = diagonal_norm

    def __call__(self, x):
        if not self.matrix_factor and not self.diagonal_factor:
            return 0.0
        # sig_x = K.sigmoid(x)
        sig_x = softstep()(x)
        # sig_x = 0.5 * (K.sign(x) + 1.)
        matrix = K.dot(K.transpose(sig_x), sig_x)
        diagonal = K.tf.matrix_diag_part(matrix)
        matrix -= K.tf.matrix_diag(diagonal)

        regularization = 0.
        if self.matrix_factor:
            regularization += self.matrix_factor * self.__get_matrix_regularization(matrix)
        if self.diagonal_factor:
            regularization += self.diagonal_factor * self.__get_diagonal_regularization(x)
        return regularization

    def __get_matrix_regularization(self, matrix):
        if self.matrix_norm in ['frobenius', 'frob']:
            nelem = K.int_shape(matrix)[0]
            matrix_regularization = K.sqrt(K.sum(matrix * matrix) + 1e-6) # / (nelem * (nelem - 1.))
        elif self.matrix_norm in [None, 1, '1', 'Inf']:
            matrix_regularization = K.max(K.sum(matrix, axis=0))
        else:
            raise Exception('matrix norm not supported')
        return matrix_regularization

    def __get_diagonal_regularization(self, diagonal):
        if self.diagonal_norm in [1, '1', None]:
            diagonal_regularization = K.relu(200 - K.sum(diagonal))
        elif self.diagonal_norm == 'Inf':
            diagonal_regularization = K.max(K.relu(1. - diagonal))
        elif self.diagonal_norm in [2, '2']:
            diagonal_relu = K.relu(1. - diagonal)
            diagonal_regularization = K.sqrt(K.sum(diagonal_relu * diagonal_relu))
        else:
            raise Exception('diagonal norm not supported')
        return diagonal_regularization

    def get_config(self):
        return {
            'matrix_factor': float(self.matrix_factor),
            'diagonal_factor': float(self.diagonal_factor),
            'matrix_norm': self.matrix_norm,
            'diagonal_norm': self.diagonal_norm
        }
