from keras.layers import Layer, Dense
from keras.legacy import interfaces
from .regularizers import ClusterRegularizer
from keras_examples.shared.functions import softstep, step
from keras import backend as K


class SoftCluster(Dense):

    @interfaces.legacy_dense_support
    def __init__(self, units,
                 kernel_initializer='he_normal',
                 diagonal_regularizer=0.,
                 matrix_regularizer=0.,
                 kernel_constraint=None,
                 matrix_norm=None,
                 diagonal_norm=None,
                 kernel_reduce=None,
                 hard_output=False,
                 **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(SoftCluster, self).__init__(
            units=units,
            use_bias=False,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=ClusterRegularizer(
                diagonal_factor=diagonal_regularizer, matrix_factor=matrix_regularizer,
                matrix_norm=matrix_norm, diagonal_norm=diagonal_norm
            ),
            kernel_constraint=kernel_constraint,
            **kwargs
        )
        self.matrix_regularizer = matrix_regularizer
        self.diagonal_regularizer = diagonal_regularizer
        self.supports_masking = True
        self.matrix_norm = matrix_norm
        self.diagonal_norm = diagonal_norm
        self.kernel_reduce = kernel_reduce
        self.hard_output = hard_output

    def call(self, inputs, training=None, **kwargs):
        if self.hard_output:
            sigmoid_kernel = step()(self.kernel)
        else:
            sigmoid_kernel = softstep()(self.kernel)
        if self.kernel_reduce is not None:
            sigmoid_kernel /= getattr(K, self.kernel_reduce)(sigmoid_kernel, axis=0, keepdims=True)
        output = K.dot(inputs, sigmoid_kernel)
        return output


class HardCluster(Dense):

    @interfaces.legacy_dense_support
    def __init__(self, units,
                 kernel_initializer='he_normal',
                 kernel_regularizer=None,
                 kernel_constraint=None,
                 dropout=0.,
                 **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(HardCluster, self).__init__(
            units=units,
            use_bias=False,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            kernel_constraint=kernel_constraint,
            **kwargs
        )
        self.dropout = dropout

    def call(self, inputs, training=None, **kwargs):

        def dropped_kernel():
            if not self.dropout:
                return self.kernel
            else:
                random_uniform = K.random_uniform(K.int_shape(self.kernel))
                return K.switch(K.less(random_uniform, self.dropout), -1e6 * K.ones_like(self.kernel), self.kernel)

        dropout_kernel = K.in_train_phase(dropped_kernel, self.kernel, training=training)

        argmax = K.argmax(dropout_kernel, axis=-1)
        one_hot = K.one_hot(argmax, K.int_shape(self.kernel)[-1])
        kernel = one_hot * self.kernel
        sigmoid_kernel = softstep()(kernel)
        output = K.dot(inputs, sigmoid_kernel)
        return output

